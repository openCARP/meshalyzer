include make.conf

define linkexe
 	ln -sf src/$(if $(filter $(shell uname),Darwin),meshalyzer.app/Contents/MacOS/,)$(1) .
endef

ifdef HDF5
all: obj hdf5 utils meshalyzer
else
all: obj meshalyzer
endif

meshalyzer: FORCE
	cd src && $(MAKE)
	$(call linkexe,meshalyzer) 

mesalyzer: FORCE
	cd src && $(MAKE) mesalyzer
	$(call linkexe,$@) 

ifdef HDF5
utils: hdf5 FORCE
	cd utils && make
	ln -sf utils/bin/* .

hdf5: FORCE
	cd HDF5API && make 
endif

clean :
	cd src && $(MAKE) clean
ifdef HDF5
	cd HDF5API && $(MAKE) clean
endif

obj: 
	mkdir -p obj

FORCE:

