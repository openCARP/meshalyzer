# Code of Conduct

Please see the code of conduct of the openCARP project:
<https://opencarp.org/dev/community/code-of-conduct>
