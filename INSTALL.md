<a name="meshalyzer"></a>
# Installation of meshalyzer

Meshalyzer is regularly used on Linux and Mac machines, and runs under Windows as well.

## Binary packages
We provide binary packages on the [release page](https://git.opencarp.org/openCARP/meshalyzer/-/releases).
Currently, it includes `AppImage`, which works on any recent enough Linux-based operating system and a `.zip` 
file with a Windows executable.

### AppImage (Linux)
1. Download the `.AppImage` file of the latest version
2. Make it executable, for example, `chmod +x Meshalyzer-3.2-x86_64.AppImage`
3. Run it `./Meshalyzer-3.2-x86_64.AppImage`

### Windows
1. Download the `.zip` file of the latest version
2. Extract it into a folder
3. Run `meshalyzer.exe`. To access the files on your drive `C:\`, use the path `/cygdrive/c` in meshalyzer's file browser.

If you want to build meshalyzer yourself, please check the following sections.

## Prerequisities

1. [libpng](http://libpng.org)

2. [libjpeg](http://libjpeg.sourceforge.net)

3. [OpenMP](http://openmp.org) is recommended to improve certain operations. For all compilers, except *clang* supplied by Apple, it is standard. A simple way to include it on Apple machines is with homebrew:
    `brew install libomp`
    
4. [FLTK 1.3.x](http://www.fltk.org/) or higher.

5. [freetype](https://freetype.org) to render fonts.

6. [glut](https://www.opengl.org/resources/libraries/glut/) or [glfw](https://www.glfw.org) for Linux and Windows only.    

7. [cmake](https://cmake.org) version 3.12 or higher, if you want to make compiling easier.

8. To make the "windowless" rendering version, [OSMesa](https://www.mesa3d.org/osmesa.html)

9. To read in VTK/VTU format meshes, install [VTK](https://www.vtk.org/) development files

10. [HDF5](https://www.hdfgroup.org) if you want to optionally use HDF5 data containers

Using your package manager is recommended for installing these packages if possible. On *macOS*, [homebrew](https://brew.sh/) works for us.

## Compiling

Clone the repository and enter the codebase folder `meshalyzer`.
```
git clone https://git.opencarp.org/openCARP/meshalyzer.git
cd meshalyzer
```
We recommend to always use the latest release version as the master branch is continuously under development and might not be as robust as the releases:
`git describe --tags --abbrev=0` returns the latest release version (e.g. '2.2'). Then you can check out this version:
`git checkout 2.2`.

We provide Makefiles and CMake files for compiling the code, choose the one that fits your workflow.

### Makefile

1. Edit **make.conf**, to set the following:

    1. If VTK is installed, set the `VTK_INC` and `VTK_DIR` variables to point to the directories containing the VTK header files and libraries, respectively. Comment them out otherwise.

    1. If you have an Apple machine, make sure that *HAVE_RETINA* is correct.

    1. If rendering is really slow and you have an AMD graphics card, uncomment *AMD_VIDEO*.

    1. To use *glfw* instead of *glut*, set GLUT_ON to 0.

    1. If you want to use the HDF5 data format, uncomment `HDF5=1`

5. Type `make -j`
6. For the windowless version, type `make mesalyzer`
7. The compiled executables will be found in the directory.
7. Copy the binaries to a suitable location

### CMake

1. We suggest to run CMake by out-of-source builds, which means all the compiled code goes into a `_build` directory separate from the source code.

2. If VTK is installed, type `cmake -S. -B_build`. Otherwise type `cmake -S. -B_build -DUSE_VTK=OFF`

3. Compile the code via `cmake --build _build`

4. In the main meshalyzer directory, you will find a link to the compiled executalbe.

### ccmake

For those who are familiar with [ccmake](https://cmake.org/cmake/help/v3.0/manual/ccmake.1.html):

1. `ccmake .`

2. configure, edit and generate

3. make -j

## Documentation

To compile the manual (LaTeX required)

1. `cd manual`
2. `pdflatex manual`
3. `pdflatex manual`

## macOS

First of all, the [command line tools](https://developer.apple.com/download/more/?=command%20line%20tools) or [Xcode](https://developer.apple.com/xcode/) must be installed.
Next, install the [prerequisities](#prerequisities) using your package manager. We use [homebrew](https://brew.sh/) as an example.

```
brew install libpng
brew install fltk
brew install freetype
brew install git
brew install cmake
```

Follow the steps in the [Compiling](#compiling) to build the code.

## Linux

For installing meshalyzer first you need to install the ["Prerequisites"](#prerequisities) using your package manager (e.g. `apt`, `yum`, or `dnf`).
The [Dockerfile](Dockerfile) provides an installation example based on CentOS7.

### Ubuntu

On Ubuntu (tested with Ubuntu 20.04), you can use:
```
apt-get update && apt-get install -y \
    build-essential \
    git \
    ca-certificates \
    fluid \
    freeglut3-dev \
    libfltk1.3-dev \
    libfreetype6 \
    libfreetype-dev \
    libglu1-mesa-dev \
    libpng-dev \
    libjpeg-dev \
    cmake
```

Follow the steps in [Compiling](#compiling) to build the code.

### CentOS

On CentOS (tested with CentOS 7), you can use:
```
yum update && yum install git \
gcc gcc-c++ \
libpng-devel \
libjpeg-devel \
fltk-devel \
fltk-fluid \
freetype \
freetype-devel \
freeglut-devel
```

If you want to compile `meshalyzer` with `cmake`, you will also need `cmake` version 3.12 or higher. 
You can know which version is provided by the package manager with the command:
```
yum info cmake
```

If the version is higher than 3.12, then use:
```
yum install cmake
```
Otherwise you can get cmake3 from [EPEL](https://docs.fedoraproject.org/en-US/epel/). On CentOS 7, you can use:
```
yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
yum install cmake3
```
Please note that you will then have to use the `cmake3` command instead of the `cmake` command to compile `meshalyzer`.

You can now follow the steps in [Compiling](#compiling) to build the code. 

## Windows

`meshalyzer` can be compiled to use the native windowing system under [Cygwin](https://www.cygwin.com).

#### Cygwin

First, install Cygwin and include the gcc compiler suite along with it. 
You will need to install at least the following packages:

- gcc-core
- gcc-g++
- make
- colorgcc
- libgcc1
- libfreetype
- libfreetype-devel
- libuuid1
- libjpeg8
- libjpeg-devel
- libpng-devel
- libglut3
- libglut-devel
- libGL-devel
- libGL1
- libGLU1
- libGLU-devel
- zlib
- zlib-devel

Make sure *libuuid-devel* is **NOT** installed. Remove it if need be and be warned, it can be persistent.

To pre-select the packages during installation just run
 `setup-x86_64.exe -P gcc-core gcc-g++ make libgcc1 colorgcc libuuid1 libjpeg8 libjpeg-devel libpng-devel libglut3 libglut-devel zlib zlib-devel libGL-devel libGL1 libGLU1 libGLU-devel`
 from the Windows commandline.

#### FLTK

Do **NOT** use the Cygwin FLTK package. Download the [source](https://www.fltk.org/software.php), and then open a Cygwin terminal. Unpack the source, `cd` to the top-level FLTK directory and execute
```
./configure --enable-cygwin
make -j
make install
```
If there is an error compiling *fluid*, change all ocurrences of`_snprintf` to `snprintf` in
*fluid/ExternalCodeEditor_WIN32.cxx*

To compile meshalyzer, clone it, `cd` to the top-level source directory 
```
make -j
```

## Post-compilation

You can execute meshalyzer by typing `./meshalyzer`

See [README.md](README.md) for an example.

To make meshalyzer available from anywhere on your machine, you need to copy it to your *bin* directory, or
add the meshalyzer directory to **PATH** by editing your *.bashrc* (or *.bash_profile*). 
Using the nano editor as an example, type the following command into the terminal:

```
nano ~/.bashrc
```

This will open an editor, scroll all the way down and add the following line, replacing '/home/somefolder/anotherfolder/meshalyzer' with the path of your meshalyzer folder:

```
export PATH=$PATH:/home/somefolder/anotherfolder/meshalyzer
```

press Control+X, then type Y and Enter to save the changes. Close all terminal instances!

open a terminal and type `meshalyzer`
