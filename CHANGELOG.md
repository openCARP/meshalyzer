# Changelog
All notable changes to this project will be documented in this file.

## Unreleased
- added cygbz2-1.dll cygbrotlidec-1.dll cygbrotlicommon-1.dll to Windows executable

## [5.4] - 2024-11-08
 - can drag-and-drop data/state files from external applications 
 - as-auxgrid can use IGB and time series .dat files
 - elemental data works on connections (in 2D)
 - added Durrer colour map (20 levels)
 - can now display nodal data as elemental data
 - added cygfreetype-6.dll to Windows executable
 - replace Docker-in-Docker service by kaniko and crane in CI jobs 
 - limit panning in time trace windows to data limits
 - middle mouse in time plot window also copies coordinates to selection buffer
 - shift+ctrl+arrow moves 100 frames in model window
 - Dockerfile now based on Ubuntu 20 instead of CentOS 7

## [5.3] - 2024-05-14
- package_data
    - store meshalyzer states
    - delete data files
    - text files can have comments
    - faster treatment of auxgrid time series
    - can select a column of an ASCII file for nodal data
- HDF5 browser no longer displays empty lists
- sync time ranges for time plots between instances
- sync time ranges for main and auxiliary grids
- horizontal and vertical clipping planes from the pull down menu
- improved ranging in time plots

## [5.2] - 2023-11-06
- added time annotations (.atm file)
- colour bar 
    - window replaced by control widget (Image submenu)
    - ticks can be centered under colour bar regions
- meshalyzer with GLFW will now render PNGs with text properly from the command line
- as-auxgrid can be specified on the command line by specifying a second *.pts* file
- PNGs can be trimmed

## [5.1] - 2023-8-20
- colour bar can be displayed in the model window
- scale and colour bar labels support Greek letters, and infinite super/sub-scripting
- **NOTE**: Colour bar and text will be rendered correctly using *meshalyzer* interactively with GLUT or GLFW. From the command line, only GLUT works. *mesalyzer* crashes if text is displayed (axes,colour bar, or scale bar) under Linux.

## [5.0] - 2023-06-23
- read in normal mesh as auxiliary grid
- package_data
    - handle binary dumps
    - as-auxgrid accepts ASCII and IGB datasets
- one menu button to read all data types
- data sets can be deleted
- improved HDF5 interface
- button to copy main colour scale to auxilliary grid
- facet shading only affects normal
- improved axes 
    - easier positioning
    - Freetype font rendering
    - fixed position under model translation
- display a scale bar 

## [4.2] - 2023-02-22
- point selection by mouse improved
    - selects closest nearby points
    - better selection on flat surfaces
    - proper masking by surfaces
    - can select auxiliary grid points
- faster multithreaded rendering of isosurfaces
- syncing enhancements
    - highlighted objects can be synced
    - dead data added to colour sync
- auxiliary grid colour control windows moved to control panel
- major improvements to usability of HDF5 data format
    - can specify datH5 file on command line
    - nicer text alignment
    - comments implemented
    - save repeated files (points or elements) as links
    - easier compilation
    - all data types work
- Bugs fixed
    - colouring aux grid surfaces

## [4.1] - 2023-01-03
- Colour and Property widgets no longer popup in separate windows but in the control panel
- provide Windows deployment including the required dynamic libraries to remove cygwin dependency
- verify PNG overwrite
- de/increasing colour range with popup no longer changes the mean
- dynamically change number of OMP threads
- new colourmap: B2G2R
- Bugs fixed
    - reading mshz file
    - surface not initially drawn
    - new auxilliary grid, with no timeplot, crashing
    - updating surface opacity

## [4.0] - 2022-11-07
- improved speed of colour-only updates with separate geometry and colour VBOs
- faster rendering of cut surfaces
- removed autoraise model window behaviour
- window size can be synced across linked instances
- colour maps *reverse* button
- new command line option to copy surfaces: *--cpSurf*
- transparent background for colour bar output
- computed surfaces now labelled by region label 
- large numbers output with commas in highlight information window
- included continuous integration build on Windows 11 via cygwin
- added metadata in codemeta.json
- established archival process for release versions
- fixed pasting of clipping planes

## [3.5] - 2022-09-01
- program information window updated
- model window size saved/restored
- ColourBarGUI window size saved/restored
- colour map popup to increase/decrease limit range
- 2 new diverging colour maps: PiYG, BrBG
- adjust vector arrow length to width ratio
- bug fixes for clearing curves, saving/restoring aux dead data, colour calibration with NaNs

## [3.4] - 2022-03-31
- times series fixed for auxiliary grids
- curve legend stability improved
- axes repositioning and resizing
- fully working *mesalyzer* (meshalyzer compiled with OSmesa)
- compiles with GLFW or GLUT
- no longer necessary to install GLEW
- RdBu colourmap added

## [3.3] - 2021-12-06
- annotated colour bar output
- running under Cygwin on Windows
- highlight info now independent of the number of threads
- fixed surface element picking for multiple surfaces
- ambient lighting for clipping planes
- fixed shading of isolines
- surfaces may ignore selected cutting planes
- copy surface function
- cut surfaces can ignore cutting planes

## [3.2] - 2021-08-31
- more control of Fresnel shading
- connections can have region numbers which define cables
- cylinders drawn as triangle strips
- removed CG colour scale
- more colour maps: turbo, CoolWarm, Plasma
- faster 3D connection rendering when colours change
- button to match isosurface colour to data colour
- Fresnel shading to accentuate edges of transparent surfaces
- only select hilight point in visible surface

## [3.1] - 2021-05-07
- antialiased PNG images
- default name for colour bar images is the name of the colour map

## [3.0] - 2021-05-07
- single window mode available combining control and model windows (--1)
- improved rendering of highlighted point
- can read CARP binary format
- new functions on curve legend widget
- improved color map implementation to make it easier to add new ones
- value inputs respond to up/down arrow
- can link on command line (--link PID)
- command line PNGs can now be bigger than the screen on Macs
- abort button when writing PNG sequences
- in highlight info window, the surface of the highlit point is now displayed
- new isoluminant colour map

## [2.2] - 2021-03-09
- vertices can be selected without being displayed
- per vertex normal data reduced for rendering
- highlight element input fields are clamped to maximum
- surface labels can have spaces
- greater control over vector length
- command line option to flip normals (--flipNorm)
- point vertices given false Z-component


## [2.1] - 2020-11-18
- vertices by default are now only drawn on displayed surfaces and connections
- auxilliary grids now have dead data
- new colour map Kindlmann
- threaded data reader replaced with on-demand reader
- display of element-based data
- can flip surface normals
- lighting effects for 2D points
- compiles with VTK9
- many bug fixes and things working as advertised

## [2.0] - 2020-06-28
- manipulation of meshes improved by completely rewriting rendering to use Vertex Buffer Objects
- Lighting
    - lighting direction controlled by trackball
    - better control of object coloring
    - eye and world lighting direction specification
    - reverse direction button
- Clipping 
    - plane selection radio buttons removed (just click)
    - copy/paste of orientations 
    - choose screen plane as clipping plane
- Adjacent `Ln` elements in `.elem` file which share endpoints are joined in 3D
- Isolines
    - When rendered in 3D, lines are formed with smoothly joined segments
    - filtering by coalescing close points
    - `match colors` button to match isolines with the number of colour used to display mesh data
- more PNG metadata added for auxgrids and vector data
- axes drawn as coloured 3D cylinders
- vertex selection only possible when vertices are displayed
- Colour scales added
    - divergent Purple-orange
    - cyclic Twilight

## [1.0] - 2020-23-06
- Initial and final release for immediate mode OpenGL rendering (pre 3.3)
