#include "Myslider.h"
#include <FL/Fl.H>
#include <FL/fl_ask.H>
#include <string>

const int w_valout = 40;

Myslider :: Myslider( int X, int Y, int W, int H, const char *L):
    Fl_Group(X,Y,W,H,L),
    valout(X,Y,w_valout,H),
    slider( X+w_valout, Y, W-w_valout, H )
{
  end();
  slider.type(FL_HOR_NICE_SLIDER);
  slider.minimum(0);
  slider.maximum(0);
  slider.step(1);
  valout.textsize(12);
}

void Myslider :: value( int t )
{
  slider.value( t );
  valout.value( t );
}

float Myslider :: value( void )
{
  return slider.value();
}

void Myslider:: maximum( int t )
{
  slider.maximum(t);
}

float Myslider:: maximum( void )
{
  return slider.maximum();
}

void Myslider:: minimum( int t )
{
  slider.minimum(t);
}

float Myslider:: minimum( void )
{
  return slider.minimum();
}

int Myslider :: handle( int event )
{
  switch ( event ) {
    case FL_KEYBOARD:
      switch (Fl::event_key()) {
        case FL_Left:
          if ( Fl::event_state(FL_SHIFT) ) {
            if ( value()>=minimum()+10 ) value(int(value())-10);
          } else
            if ( value()>minimum() ) value(int(value())-1);
          do_callback();
          return 1;
          break;
        case FL_Right:
          if ( Fl::event_state(FL_SHIFT) ) {
            if ( value()<=maximum()-10 ) value(int(value())+10);
          } else
            if ( value()< maximum() ) value(int(value())+1);
          do_callback();
          return 1;
          break;
        case 't':
          try {
            const char *tstr = fl_input( "Go to time:" );
            float tm = std::stof(tstr);
          } 
          catch(...){}
          return 1;
          break;
        case 'f':
          try {
            const char *fstr = fl_input( "Go to frame:" );
            float frame = std::stoi(fstr);
            if( frame>=0 && frame<=maximum() ) {
              value(frame);
              do_callback();
            }
          } 
          catch(...){}
          return 1;
          break;
      }
    case FL_FOCUS:
    case FL_UNFOCUS:
      return 1;
      break;
    case FL_PUSH:
    case FL_DRAG:
      if ( Fl::event_key()==FL_Button+2 || (Fl::event_key()==FL_Button+1 && Fl::event_state(FL_SHIFT)) ) {
        float newval = float(Fl::event_x()-x()-w_valout)/
                       float(w()-w_valout)*(maximum()-minimum())+minimum();
        if ( newval< minimum() ) newval = minimum();
        if ( newval>maximum() ) newval = maximum();
        value(int(newval));
        do_callback();
        return 1;
      } else if ( Fl::event_key() ==  FL_Button+1 ||
                  Fl::event_key() ==  FL_Button+3 ) {
        double inc = (Fl::event_key()==FL_Button+3) ? 10. : 1.;
        float currx = (value()-minimum())/(maximum()-minimum())*
                      float(w()-w_valout)+x()+w_valout;
        if ( Fl::event_x()>currx ) {
          if ( value()<=maximum()-inc ) value(int(value()+inc));
          do_callback();
        } else {
          if ( value()>=minimum()+inc ) value(int(value()-inc));
          do_callback();
        }
        return 1;
      }
      break;
  }
  return 0;
}

void Myslider :: step( double a )
{
  slider.step(a);
}
