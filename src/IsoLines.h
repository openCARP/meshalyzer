#ifndef ISOLINE_H
#define ISOLINE_H

#include "Model.h"
#include "DrawingObjects.h"
#include "CutSurfaces.h"
#include <vector>

#define BRANCH_TOL 0.2

class IsoLine {
    public:
        IsoLine( double v0, double v1, int n, int t ): _v0(v0),_v1(v1),_nl(n),
                                             _t(t) {_cnnx=new Connection(&_pts);}
        IsoLine( IsoLine &L ): _v0(L._v0),_v1(L._v1), _nl(L._nl), _t(L._t),
               _3D(L._3D), _branch(L._branch), _branch_tol(L._branch_tol),
               _pt_tol(L._pt_tol)
               { for(int i=0;i<2;i++)_branch_range[i]=L._branch_range[i];
                 memcpy( _color, L._color, 4*sizeof(GLfloat) ); 
                 _rd.lineWidth = L._rd.lineWidth; _rd.sides(L._rd.sides());
                                                 _cnnx=new Connection(&_pts);}
        ~IsoLine();
        int process( Surfaces *, DATA_TYPE *, bool, int );
        int process( CutSurfaces *, DATA_TYPE * );
        int nl(){ return _nl; }
        int v0(){ return _v0; }
        int v1(){ return _v1; }
        int tm(){ return _t;  }
        void draw( Colourscale *, GLfloat );
		void color( const GLfloat* c ){memcpy(_color,c,4*sizeof(GLfloat));}
        void branch( bool b, double min=0, double max=0, double tol=BRANCH_TOL )
          {_branch=b;if(b){_branch_range[0]=min;_branch_range[1]=max;_branch_tol=tol;} }
        bool branch(){return _branch;}
        double *branch_range(){ return _branch?_branch_range:NULL; }
        void auxMesh( const char * );
        void make_lines( std::vector<MultiPoint *>&, PPoint &, float, int s=0 );
        void buffer_lines( Colourscale *, GLfloat );
        GLfloat* buffer(){return _rd.buffer();} 
        void init_buff(void *c){_rd.setup_render_data(c);}
        void update_buff(){_rd.update_nodalbuff();}
        void render(){_rd.mesh_render(false);}
        void translucent( bool tl ){ if(tl){_rd.opaque(0);}else{_rd.opaque(-1);}}
        void material( GLfloat *matprop ) {_rd.set_material(0,matprop);}
        IsoLine &operator +=(IsoLine&);
        void threeD( bool b){_3D=b;_rd.threeD(b);}
        void width(GLfloat w){ _rd.lineWidth=w; }
        void clear();
        void closePt( float a ){ _pt_tol = a; }
        float closePt( ){ return _pt_tol; }
        bool  lined(){ return _lined; }
        void  set_clip( int s, GLint cs ){ _rd.clip_obey(s,cs); if(_rd.material_unset(s))_rd.def_light(s); } 
    private:
        Connection*           _cnnx;
        std::vector<float>         _val;
        PPoint                _pts;
        bool                  _branch=false; // do not interpolate over a value
        double                _branch_range[2];
        float                 _branch_tol=0.3;
        GLfloat               _color[4] = {0.,0.,0.,1.};
        double                _v0, _v1;
        int                   _nl;
        int                   _t;            //!< last time rendered
        bool                  _3D=false;     //!< render as 3D?
        bool                  _lined=false;  //!< connections assembled into lines?
        RenderLines           _rd;
        float                 _pt_tol = 0.;  //!< L1 norm for determining identical points
        std::multimap<int,std::pair<int,int>>_cnnx_surf;    //!< connections in each surface
};

#endif

