#ifndef CLIPSLIDER_H

#define CLIPSLIDER_H
#include <FL/Fl_Value_Slider.H>

/** intercept slider moving actions to identify when we are dragging
 */
class ClipSlider : public Fl_Value_Slider
{
  public:
    ClipSlider( int x, int y, int w, int h, const char *L=0 ):Fl_Value_Slider(x,y,w,h,L){}
       int handle( int e ){if(e==FL_DRAG){
                          do_callback(this,(long)1);
                        } else if(e==FL_RELEASE){
                          do_callback(this,(long)0);
                        } else if(e==FL_KEYDOWN && (Fl::event_key()==FL_Up||Fl::event_key()==FL_Down)){
                          do_callback(this,(long)0);
                        }
                        return Fl_Value_Slider::handle(e);
                       }

};

#endif
