#ifndef COLOURBAR_H
#define COLOURBAR_H

#ifdef OSMESA
#  include <GL/osmesa.h>
#elif defined(__APPLE__)
#  define GL_SILENCE_DEPRECATION
#  include <OpenGL/gl3.h>
#else
#  if defined(WIN32) || defined(__CYGWIN__)
#    define GLEW_STATIC 1
#  endif
#  include <GL/glew.h>
#endif


#include <string>
#include <FL/Fl.H>
#include <FL/Fl_Widget.H>
#include <FL/Fl_Image.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Gl_Window.H>
#include "Colourscale.h"
#include "ScreenTxt.h"

int generate_shader_program(const char* vertex_source, const char* fragment_source);

inline int get_cbar_shader()
{
  const char *vtxCbarShader = 
  "#version 410 core\n"
  "in vec4 coord;\n"
  "out vec2 texcoord;\n"
  "\n"
  "void main(void) {\n"
  "  gl_Position = vec4(coord.xy, 0, 1);\n"
  "  texcoord = coord.zw;\n"
  "}\n";

  const char *fragCbarShader =
  "#version 410 core\n"
  "in vec2 texcoord;\n"
  "layout(location=0) out vec4 fragColor;\n"
  "uniform sampler2D tex;\n"
  "uniform vec4 color;\n"
  "\n"
  "void main(void) {\n"
  "  fragColor = vec4(texture(tex, texcoord).rgb,1.);\n"
  "}\n";

  return generate_shader_program( vtxCbarShader, fragCbarShader );
}


inline int get_cbar_line_shader()
{
  const char *vtxCbarLineShader = 
  "#version 410 core\n"
  "in vec2 coord;\n"
  "\n"
  "void main(void) {\n"
  "gl_Position = vec4(coord.xy, 0, 1);\n"
  "}\n";

  const char *fragCbarLineShader =
  "#version 410 core\n"
  "layout(location=0) out vec4 fragColor;\n"
  "uniform vec3 color;\n"
  "\n"
  "void main(void) {\n"
  "  fragColor = vec4(color,1.);\n"
  "}\n";

  return generate_shader_program( vtxCbarLineShader, fragCbarLineShader );
}


typedef enum { CB_LAND=1, CB_PORTRAIT, CB_PORTRAIT_HORZ } CB_Orient;

class ColourBar {
    public:
  ColourBar(){}
  virtual void draw();
  void ntics( int n ){ _ntics=n; }
  void orient( CB_Orient o ){ _orient=o; }
  CB_Orient orient(void){ return _orient; }
  void side( bool rgt ){ _rgtdwn = rgt; }
  void font( int f ){ _font=f; }
  void fontSize( int s ){_fntsz = s;}
  void fmt( const char *f ){ _fmt=f; }
  void label( const char *l ){ _label=l; }
  void tick_len( int l ){ _ticlen=l; }
  void ctr_tick( bool b ){ _tic_ctr=b; }
  void length( float l ){ _len=l; }
  void rgb(uchar *a, int w, int h){delete _bar;_bar=new Fl_RGB_Image(a,w,h);}
  void cs( Colourscale *c ){ _cs = c; }
  void frame( bool b ){ _framed = b; }
  void width( int w ){ _width = w; }
  uchar *trim( Fl_Image *img, int &nx, int &ny, bool = false);
  void ogl_render(float,float,int,int);
  void user_data( void *v ){_ud = v; }
  void *user_data(){ return _ud; }

    private:
    std::string   _label;
    std::string   _fmt       = "%.2g";
    Fl_Font       _font      = FL_SCREEN;
    int           _fntsz     = 20;
    int           _ntics     = 1;
    int           _ticlen    = 10;        //!< in pixels
    bool          _tic_ctr   = false;     // center tics in colour ranges
    CB_Orient     _orient    = CB_LAND;
    bool          _rgtdwn    = false;     //!< label drawn on opposite side?
    uchar*        _rgb       = NULL;
    Fl_RGB_Image* _bar       = NULL;
    int           _width     = 30;        //!< in pixels
    GLfloat       _len       = 0.5;       //!< in screen portion
    int           _inset     = 100;       //!< in pixels
    int            ticgap    = 5;         //!< in pixels
    bool          _framed    = false;
    uchar         _br=255, _bg=255, _bb=255;   //!< background rgb
    Colourscale  *_cs;
    void          flip();
    void         *_ud;
    
    // for OpenGL rendering
    GLuint     _vbo[2];
    GLfloat    _col[4] = {0,0,0,1}; //black
    GLuint     _attribute_coord[2];
    GLuint     _uniform_tex;
    GLuint     _uniform_color[2];
    GLint      _program[2]={-1,-1};
    GLuint     _vao[2];

  void  get_labels(std::vector<int> &x, std::vector<int>&y, std::vector<std::string>& label);
  void  get_labels(std::vector<float> &x, std::vector<float>&y, std::vector<std::string>& label,
                   std::vector<TxtPos>& tpos, float w, float h, float sx, float sy );
  Fl_Image *get_cbar(int);
};

#endif
