/** This class implements an auxilliary grid which can be used to present 
 *  time varying (or fixed) data. The auxilliary grid itself can be totally dynamic,
 *  meaning that points and the elements derived from them can change. If both the
 *  grid and data are dynamic, there must be the same number of time instances. 
 *
 */
#ifndef AUXGRID_H
#define AUXGRID_H

#include <vector>
#include "render_utils.h"
#include "select_util.h"
#include "Model.h"
#include "DrawingObjects.h"
#include "plottingwin.h"
#include "DeadDataGUI.h"

class TBmeshWin;
class PlotWin;
class AuxGridFetcher;

class AuxGrid {
    private:
        AuxGridFetcher * _indexer = NULL;  //!< private implementation of indexer
        bool         _display   = true;
        bool         _datafied[maxobject];
        bool         _surf_fill = true;  //!< if false, draw outline
        bool         _vol_fill  = false;   //!< if false, draw wireframe
        bool         _autocol=false;
        bool         _show[maxobject]; //!< true to show object
        bool         _3D[maxobject+2];
        GLfloat      _color[maxobject+2][4];
        float        _size[maxobject+2];
        bool         _plottable = false;    //!< true if a time series can be plotted
        int          _hiVert    = 0;
        bool         _hilight   = false;
        static PlotWin* _timeplot;
        int          _sz_ts;               //!< size of time series
        float*       _time_series;
        bool         _clip      = false;   // is is clipped?
        float        _t0;
        float        _dt;
        void         fill_buff( void *, Model *m, int );
        RenderTris    _rd_vol;
        RenderTris    _rd_surf;
        RenderLines   _rd_lns;
        RenderSpheres _rd_pts;
        SelectVtx     _select;                      // !< select vertices
        GLfloat       _mat_prop[4] = { 0.7, 0.7, 80, 0.5 };
        std::string   _absfile;
        bool          _select_vtx = false;
    public:
        AuxGrid( const char *fn, TBmeshWin *, AuxGrid *ag=NULL, bool fake=false, float t0=0, float dt=1 );
        ~AuxGrid();

        Colourscale cs;
        void     draw( int, unsigned int, void* );
        void     color( Object_t o, GLfloat *r );
        GLfloat *color( Object_t o ){ return _color[o]; }
        bool     showobj( Object_t o, bool b ) { bool old=_show[o];_show[o]=b;return old;}
        bool     showobj( Object_t o ) { return _show[o];}
        void     threeD( Object_t o, bool b ) { _3D[o] = b; }
        bool     threeD( Object_t o ) const { return _3D[o]; }
        void     size( Object_t o, float s ) { _size[o] = s; }
        float    size( Object_t o ) const {return _size[o]; }
        int      num_tm();
        void     display( bool b ){ _display=b; }
        void     datify( Object_t o, bool b ){ _datafied[o]=b; }
        void     surfill( bool b ){ _surf_fill=b; }
        void     volfill( bool b ){ _vol_fill=b; }
        void     autocolor( bool b ){ _autocol=b; }
        void     optimize_cs(int);
        bool     highlight_vertex(int n, float &val, bool update_plot=true);
        void     highlight(bool b){_hilight=b;}
        int      highlight(){return _hilight ? _hiVert : -1;}
        void     plot(int);
        bool     plottable(){return _plottable;}
        int      time_series( int n, float *&d );
        void     time_X( float a, float b );
        bool     time_get_X( double &a, double &b );
        bool     data();
        int      num_vert();
        void     clip( bool b ){ _clip=b; }
        void     select(bool b){_select_vtx=b;}
        void     set_mat( GLfloat *mp, int s=0 ) { memcpy( _mat_prop, mp, 4*sizeof(GLfloat) ); 
                   _rd_vol.set_material(s,_mat_prop); _rd_surf.set_material(s,_mat_prop);
                   _rd_pts.set_material(s,_mat_prop );_rd_lns.set_material(s,_mat_prop); }
        void     lighting( GLfloat amb, GLfloat diff, GLfloat spec, GLfloat fres, int bf ) {  
                   _rd_vol.lighting(amb,diff,spec,fres,bf); _rd_surf.lighting(amb,diff,spec,fres,bf);
                   _rd_pts.lighting(amb,diff,spec,fres,bf);_rd_lns.lighting(amb,diff,spec,fres,bf); }
        GLfloat *get_mat() { return _mat_prop; }
        std::string   file(){ return _absfile; }
        DeadDataGUI*  _deadData  = NULL;
        TBmeshWin *mwtb = NULL;
        float    dt(){return _dt;}
        float    t0(){return _t0;}
        void     update_plotter(void);
        void     pick_vtx(void *,int);
};

#endif
