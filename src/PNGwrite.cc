#include "PNGwrite.h"
#include <stdlib.h>
#include "gitversion.h"

PNGwrite :: PNGwrite( FILE *out ) : fp(out)
{
  png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL );

  if (png_ptr == NULL) {
    fclose(fp);
    return;
  }
  info_ptr = png_create_info_struct(png_ptr);
  if (info_ptr == NULL) {
    fclose(fp);
    png_destroy_write_struct(&png_ptr,  (png_infopp)NULL);
    return;
  }
  setjmp(png_jmpbuf(png_ptr));
  png_init_io(png_ptr, fp);
}


/** write the frame buffer to a PNG file
 *
 * \param data    the buffer contents
 * \param align   data alignment for each row
 * \param topdown data in top-down order
 */
int PNGwrite :: write( const void *data, int align, bool topdown )
{
  setjmp(png_jmpbuf(png_ptr));

  png_set_IHDR( png_ptr, info_ptr, width, height, colour_depth, ctype,
                interlace_type, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

  if ( ctype == PNG_COLOR_TYPE_PALETTE )
    png_set_PLTE( png_ptr, info_ptr, cpalette, colour_depth );

  png_write_info(png_ptr, info_ptr);

  png_bytep row_pointers[height];
  int compsperpixel;
  if ( ctype == PNG_COLOR_TYPE_GRAY )
    compsperpixel = 1;
  else if ( ctype == PNG_COLOR_TYPE_GRAY_ALPHA )
    compsperpixel = 2;
  else if ( ctype == PNG_COLOR_TYPE_PALETTE )
    compsperpixel = 1;
  else if ( ctype == PNG_COLOR_TYPE_RGB )
    compsperpixel = 3;
  else if ( ctype == PNG_COLOR_TYPE_RGB_ALPHA )
    compsperpixel = 4;
  //flip vertically and make sure rows are aligned
  int pixel_data = compsperpixel*width*colour_depth/8;
  int pad = (align - (pixel_data%align))%align; 
  for ( png_uint_32 k = 0; k < height; k++) {
    png_uint_32 row   = topdown ? k : height-1-k;
    row_pointers[row] = (png_byte *)data + (pixel_data+pad)*k;
  }
  png_write_image(png_ptr, row_pointers);
  png_write_end(png_ptr, info_ptr);
  png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
  fclose( fp );
  return 1;
}

void
PNGwrite :: description( std::map<std::string,std::string> &meta )
{
  if( tptr != NULL ) {
    for( int i=1; i<ntext; i++ ) {
      free( tptr[i].key  );
      free( tptr[i].text );
    }
    delete tptr;
  }

  ntext = 2+meta.size();
  tptr = new png_text[ntext];

  tptr[0].key  = strdup("Software");
  tptr[0].text = strdup("meshalyzer");
  tptr[1].key  = strdup("Software version");
  tptr[1].text = strdup(gitversion);

  auto p = tptr+2;
  for( auto m : meta ) {
    p->key  = strdup( m.first.c_str() );
    p->text = strdup( m.second.c_str());
    p++;
  }
  for( int i=0; i<ntext; i++ ) {
    tptr[i].text_length = strlen( tptr[i].text );
    tptr[i].compression = PNG_TEXT_COMPRESSION_NONE;
  }

  png_set_text(png_ptr, info_ptr, tptr, ntext);
} 


