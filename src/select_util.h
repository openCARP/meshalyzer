#ifndef SELECT_UTILS_H
#define SELECT_UTILS_H

#include "legacyGL.h"

#include<vector>

inline int get_select_shader()
{
  const char *vertexShaderSource =
    "#version 400 core\n"
    "#extension GL_EXT_clip_cull_distance : enable\n" 
    "layout (location = 0) in vec3  aPos;\n"
    "layout (location = 1) in vec4  aColor;\n"
    "float gl_ClipDistance[6];\n"
    "\n"
    "out vec3  Pos;\n"
    "out vec3  FragPos;\n"
    "out vec4  Color;\n"
    "\n"
    "uniform mat4 model;\n"
    "uniform mat4 view;\n"
    "uniform mat4 projection;\n"
    "uniform vec4 clipEqn[6];\n"
    "\n"
    "void main()\n"
    "{\n"
    "  FragPos  = vec3(model * vec4(aPos, 1.0));\n"
    "  Color    = aColor;\n"
    "\n"
    "  gl_Position = projection * view * vec4(FragPos, 1.0);\n"
    "\n"
    "  for( int i=0; i<6; i++ )\n"
    "    gl_ClipDistance[i] = dot(vec4(aPos,1.0), clipEqn[i]);\n"
    "}\n";

  const char *fragmentShaderSource =
    "#version 400 core\n"
    "in vec4 Color;\n"
    "in vec3 FragPos;\n"
    "layout(location=0) out vec4 fragColor;\n"
    "void main()\n"
    "{\n"
    "\n"
    "    fragColor = Color;\n"
    "}\n";

  return generate_shader_program(vertexShaderSource, fragmentShaderSource);
}

/**
 * @brief VBO to draw only points and encode an integer in their colour
 */
class SelectVtx
{
    public: 
      SelectVtx(){}
      ~SelectVtx();

      void clear(){ nodalbuff.clear(); _n = 0; }
      void init_nodalbuff(bool);
      bool inited(){ return init; }
      void update_nodalbuff(){init_nodalbuff(true);}
      bool setup_render_data(void *c=NULL);
      void mesh_render();
      void context(void *c){ _context=c; }

      int      num(){return _n;}
      GLfloat *buffer(){ return nodalbuff.data(); }                                       
      GLfloat *buffer(int n){ _n=n; nodalbuff.resize(n*BUFF_SZ);return nodalbuff.data();}   // resize buffer for n elements
      GLfloat *buff_append(int n){_n+=n; nodalbuff.resize(_n*BUFF_SZ);return nodalbuff.data()+(_n-n)*BUFF_SZ;} //append n elements
      /** fill buffer position i with node ID */
      int      buff_vtx( unsigned i, const GLfloat *pos, unsigned int ID ){
                                         GLfloat *d=nodalbuff.data()+i*BUFF_SZ;
                                         memcpy(d,pos,3*sizeof(GLfloat));
                                         int2col(ID,d+COL_OFFSET);
                                         return BUFF_SZ;} 

      void     int2col( int i, GLfloat* col ){++i;for(int j=0;j<4;j++){
                                                      col[j]=(i&0xFF)/255.f; i>>=8; }
                                                      col[3]+=(UNUSED)/255.; }
      int      col2i( GLubyte *col ){ int d=col[3]-UNUSED; for(int i=2;i>=0;i--)d=d*256+col[i]; return d-1; }

      GLfloat  ptSize = 10.;

      static const int     BUFF_SZ    = 7;
      static const int     COL_OFFSET = 3;
      static const GLubyte UNUSED     = 0x90;   // bits to ignore when forming ID

    protected:
      bool   init = false;                     //!< is structure initialized?
      int    shader; 
      int    _n=0;                             //!< number of points
      float  tfm[4][4];                        //!< transformation matrix
      std::vector<GLfloat> nodalbuff;               //!< data buffer
      GLuint gl_nodalbuff, gl_vertexarray;     //!< for OpenGL binding
      bool   _dirty=true;                      //!< buffer needs repopulating
      void   set_clipping(int);
      void   finish_render();
      void  *_context=NULL;
};


#endif  // header guard
