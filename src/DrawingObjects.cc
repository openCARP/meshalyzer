#include "DrawingObjects.h"
#if HAVE_GL2PS
#include "gl2ps.h"
#endif
#include "Vector3D.h"
#include "VecData.h"

#ifdef _OPENMP
#include <omp.h>
static omp_lock_t writelock;
#endif 

bool MultiPoint::_init_lock = false;


/** convert 4-component float colour to ubytes
 *
 * \param[in]  cf float colour array
 * \param[out] cb ubyte colour array
 */
GLubyte *
float2ubyte( const GLfloat *cf, GLubyte *cb, float alpha )
{
  for( int i=0; i<4; i++) 
    cb[i] = (GLubyte)(cf[i]*255.);
  if( alpha>=0. ) cb[3]= (GLubyte)(alpha);
  return cb;
}


enum lpint_enum{ BOTH_ON_PLANE, NO_INTERSECTION };

/** find the intersection of a plane with a line
 *
 *  \param a  first point of line
 *  \param b  second point of line
 *  \param pc coefficients describing plane: ax+by+cz+d=0
 *  \param ip intersection point
 *
 *  \return fraction along a,b the intersection occurs
 */
float find_line_plane_intersect( const GLfloat* a, const GLfloat* b,
                             const GLfloat* pc, GLfloat* ip )
{
  float d1 = dot( a, pc )+pc[3];
  float d2 = dot( b, pc )+pc[3];
  float t  = d1/(d1-d2);

  GLfloat v[3];
  add( a, scale( sub(b,a,v), t ), ip );

  if ( t<0 || t>1 ) {
    if ( std::isinf(t) )
      throw BOTH_ON_PLANE;
    else
      throw NO_INTERSECTION;
  }
  return t;
}


/** turn translucency on/off
 *
 * \param b true for on
 */
void DrawingObj :: translucency( bool b )
{
  if ( b==true ) {
#if 0
    glPushAttrib(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
  } else {
    GLint sd;
    glGetIntegerv(GL_ATTRIB_STACK_DEPTH, &sd );
    if ( sd ) glPopAttrib();
#endif
  }
}



GLubyte* 
DrawingObj::colour( int i, const GLfloat *defcol, DATA_TYPE *data, Colourscale *cs, dataOpac *dopac, GLubyte *b )
{
  const GLfloat *c=colour( i, defcol, data, cs, dopac );
  for( int i=0; i<4; i++ )
    b[i] = (GLubyte)(c[i]*255);
  return b;
}


/** try a bunch of file names to open a file
 *
 * \param base basename of file
 * \param ext  extension for type of file without a .
 *
 * \note directories are ignored
 *
 * \return a pointer to the open file
 */
gzFile openFile( const char *base, const char* ext )
{
  gzFile      in;
  std::string      fn   = base;
  struct stat finfo;

  if ( stat(fn.c_str(),&finfo) || S_ISDIR(finfo.st_mode) ) {
    fn += ".gz";
    if ( (in=gzopen( fn.c_str(), "r")) == NULL ) {
      fn  = base;
      fn += ext;
      if ( (in=gzopen( fn.c_str(), "r")) == NULL ) {
        fn += ".gz";
        if ( (in=gzopen( fn.c_str(), "r")) == NULL ) {
          fn = base;
          fn += ".";
          fn += ext;
          if ( (in=gzopen( fn.c_str(), "r")) == NULL ) {
            fn += ".gz";
            if ( (in=gzopen( fn.c_str(), "r")) == NULL )
              throw 1;
          }
        }
      }
    }
  } else
    in = gzopen( fn.c_str(), "r");

  return in;
}


/**
 * @brief initialize OMP lock
 */
void
MultiPoint::init_lock()
{
#ifdef _OPENMP
  if( !_init_lock ) {
    omp_init_lock(&writelock);
    _init_lock=true;
  }
#endif 
}

/** define an object
 *
 * \param nl list of nodes defining object(s)
 * \param n  \#object to redefine
 *
 * \post obj \p n is redefined with new nodes
 */
void  MultiPoint::redefine( const int *nl, int n )
{
  memcpy( _node+n*_ptsPerObj, nl, _ptsPerObj*sizeof(int) );
}

/** define an object
 *
 * \param nl list of nodes defining object(s)
 * \param n  \#object to define
 *
 * \pre  \p nl must be at least _ptsPerObj*\p n long
 * \post \p _n is allocated if need be
 */
void  MultiPoint::define( const int *nl, int n )
{
  _node = (int*)realloc( _node, n*_ptsPerObj*sizeof(int) );
  memcpy( _node, nl, n*_ptsPerObj*sizeof(int) );
  _n = n;
}


/** add an object to the list
 *
 * \param n list of nodes defining object
 */
void MultiPoint::add( int *n )
{
  _n++;
  _node = (int*)realloc( _node, _n*_ptsPerObj*sizeof(int) );
  memcpy( _node+(_n-1)*_ptsPerObj*sizeof(int), n, _ptsPerObj*sizeof(int) );
}


/** add a volume element
 *
 * \param n list of nodes
 * \param r region
 */
void VolElement::add( int *n, int r )
{
  MultiPoint::add( n );
  _region = (int*)realloc( _region, _n*_ptsPerObj*sizeof(int) );
  _region[_n-1] = r;
}


/** determine if a clipping plane intersects an element.
 *
 *  \param pd      visible points
 *  \param cp      normalized clip plane, cp[0]x + cp[1]y +cp[2]z + cp[3] = 0
 *  \param e       the element in the list
 *  \param interp[out]  construct to interpolate data
 *  \param numedge number of edges for element
 *  \param edges   list of nodes defining edges
 *  \param e       element number
 *
 *  \return a surface element if intersection, NULL otherwise
 */
SurfaceElement*
VolElement::planecut( char *pd, GLfloat* cp,
                      Interpolator<DATA_TYPE>* &interp, int numedge,
                      const int edges[][2], int e=0 )
{
  // return if all vertices visible or all not visible
  int  p, *ndpt=_node+e*_ptsPerObj;
  bool visible= pd[*ndpt++];
  for ( p=1; p<_ptsPerObj; p++ )
    if ( pd[*ndpt++] != visible )
      break;
  if ( p==_ptsPerObj ) return NULL;

  // get intersections and centroid of intersections of plane with edges
  GLfloat intersect[6*numedge];
  float   d[2*_ptsPerObj];
  int     inode[4*_ptsPerObj];
  int     num_int=0;
  Vector3D<GLfloat> centroid;
  for ( int i=0; i<numedge; i++ ) {
    if ( (pd[_node[edges[i][0]]] + pd[_node[edges[i][1]]]) == 1 ) {
      inode[2*num_int]   = _node[edges[i][0]];
      inode[2*num_int+1] = _node[edges[i][1]];
      try {
        d[num_int] = find_line_plane_intersect(_pt->pt(_node[edges[i][0]]),
             _pt->pt(_node[edges[i][1]]), cp, intersect+3*num_int );
      } catch ( lpint_enum raison ) {
        if ( raison==BOTH_ON_PLANE ) {		// add both points
          d[num_int] = d[num_int+1] = 1;
          for ( int p=0; p<3; p++ ) {
            intersect[3*num_int+p]   = _pt->pt(_node[edges[i][0]] )[p];
            intersect[3*num_int+3+p] = _pt->pt(_node[edges[i][1]] )[p];
          }
          inode[2*num_int]   = inode[2*num_int+3] = _node[edges[i][0]];
          inode[2*num_int+1] = inode[2*num_int+2] = _node[edges[i][1]];
          centroid += intersect+3*num_int++;
        } else
          std::cerr << "Weirdness" << std::endl;
      }
      centroid += intersect+3*num_int++;
    }
  }
  centroid /= (GLfloat)num_int;

  // interpolation information
  if ( num_int ) interp = new Interpolator<DATA_TYPE>[num_int];
  for ( int i=0; i<num_int; i++ )
    interp[i].set( inode[2*i], inode[2*i+1], d[i] );

  if ( num_int>3 ) {
    // determine angular position on cutting plane
    float angle[num_int];
    for ( int i=0; i<num_int; i++ ) {
      Vector3D<GLfloat>rays(intersect+i*3);
      rays -= centroid;
      if ( fabs(cp[2]) > fabs(cp[1]) && fabs(cp[2])>fabs(cp[0]) )
        angle[i] = atan2( rays.Y(), rays.X() );
      else if ( fabs(cp[1]) > fabs(cp[0]) )
        angle[i] = atan2( rays.X(), rays.Z() );
      else
        angle[i] = atan2( rays.Z(), rays.Y() );
    }

    // simple sort the points by angle
    for ( int i=0; i<num_int-1; i++ ) {
      float min = angle[i];
      int   minind = i;
      for ( int j=i+1; j<num_int; j++ )
        if ( angle[j]<min )
          min = angle[minind=j];
      float  tmp = angle[i]; angle[i] = angle[minind]; angle[minind] = tmp;
      Interpolator<DATA_TYPE> itmp = interp[i]; interp[i] = interp[minind];
      interp[minind] = itmp;
      Vector3D<GLfloat> tp( intersect+3*i );
      memcpy( intersect+3*i, intersect+3*minind, 3*sizeof(GLfloat));
      memcpy( intersect+3*minind, tp.e, 3*sizeof(GLfloat));
    }
  }

  // make new element
  PPoint *pt = new PPoint;
  pt->add( intersect, num_int );
  pt->setVis( true );
  pt->offset( _pt->offset() );
  SurfaceElement *se;
  if ( num_int==3 )
    se = new Triangle( pt );
  else if( num_int==4 )
    se = new Quadrilateral( pt );
  else
    se = new PolyGon( pt, num_int );
  int nl[num_int];
  for ( int i=0; i<num_int; i++ ) nl[i]=i;
  se->add( nl );
  short_float *normal = new short_float[3];
  SHORT_VCP( normal, cp );
  se->nrml( normal );
  GLfloat a[3], b[3], orig[3], cr[3];
  assign( orig, pt->pt(0) );
  sub( assign(a, pt->pt(1)), orig, a ); 
  sub( assign(b, pt->pt(2)), orig, b ); 
  if( dot( cross( a, b, cr ), cp )<0 ) se->rev_order();
  return se;
}


/** for a surface with ordered node numbers, compute the normals
 *
 * \param e0 first element
 * \param e1 last element
 */
void 
SurfaceElement::compute_normals(int e0, int e1)
{
  if ( _nrml  ==NULL ) _nrml   = new GLfloat[e1*3+3];
  if ( _shnrml==NULL ) _shnrml = new short_float[e1*3+3];

  for ( int e=e0; e<=e1; e++ ) {
    const int* ele = _node+3*e;
    Vector3D<GLfloat> a(_pt->pt(ele[0]));
    Vector3D<GLfloat> b(_pt->pt(ele[1]));
    Vector3D<GLfloat> c(_pt->pt(ele[2]));

    b -= a;
    b = b.Cross(c-a);
    b.Normalize();
    assign( _nrml+3*e, b.data() );
    SHORT_VCP( _shnrml+3*e, _nrml+3*e );
  }
}


/**
 * @brief enter the isoedges into the edge list
 *
 * @param dat data at nodes
 * @param val isovalue
 * @param epm edge point map
 *
 * @ return if edges were added
 *
 * @post edges which contain isovalues are added to \epm but points need to be determined
 */
bool
MultiPoint::isoEdges( DATA_TYPE *dat, DATA_TYPE val, EdgePtMap &epm ) 
{
  // determine row index into table 
  unsigned int index=0;
  for( int i=_ptsPerObj-1; i>=0; i-- ) {
    index <<= 1;
    
    // Modified: data[_node[i]] > val to this
    if( dat[_node[i]]>=val )
      index += 1;
  }

  const int* poly        = iso_polys(index);
  int        npoly       = poly[0]; // number of polygons to create
  if (!npoly) return false;
  
  int        poly_start  = 1;       // first polygon defined after \#polygons

  for( int n=0; n<npoly; n++ ) {
    int npts = poly[poly_start];              // \#nodes defining polygon
    for( int i=0; i<npts; i++ ) {
      int pindex = poly_start+1+i*2;
      int n0     = _node[poly[pindex]];
      int n1     = _node[poly[pindex+1]];
      EdgePair edge( std::min(n0,n1), std::max(n0,n1) );
      epm[edge] = 0;
    }
    poly_start += npts*2+1;
  }
  return true;
}


/** linearly interpolate the position of a value along an element edge 
 *
 *  \param n0    vertex index 0
 *  \param n1    vertex index 1
 *  \param dat   data 
 *  \param val   data between dat0 and dat1
 *  \param pint[out]  location of val 
 *
 *  \return relative weight of node 0
 */
float
MultiPoint::edge_interp( int n0, int n1, DATA_TYPE* dat, DATA_TYPE val, GLfloat* pint )
{
  GLfloat edge[3];
  sub( (*_pt)[n1], (*_pt)[n0], edge );
  float d = (val-dat[n0])/(dat[n1]-dat[n0]);
  if( d<0 || d>1 )
    throw NO_INTERSECTION;
  ::add( (*_pt)[n0], scale( edge, d ), pint );
  return 1.-d;
}


/**
 * @brief compute isosurface
 *
 * @param dat        nodal data
 * @param val        isovalue
 * @param npoly      npolygons created
 * @param epts       isovalues edge points
 * @param epm        mapping of edge to point index
 * @param global_pt  use global point array?
 *
 * @return surface elements created
 *
 * \pre \p epts created through a call to isoEdges() and \p epm filled
 */
MultiPoint ** MultiPoint::isosurfaces( DATA_TYPE *dat, DATA_TYPE val, int &npoly,
        PPoint *epts, EdgePtMap &epm ) 
{
  // determine row index into table 
  unsigned int index=0;
  for( int i=_ptsPerObj-1; i>=0; i-- ) {
    index <<= 1;
    if( dat[_node[i]]>=val )
      index += 1;
  }

  const int* poly        = iso_polys(index);
  npoly                  = poly[0]; // number of polygons to create

  // do not continue if no polygons 
  if (!npoly) return NULL;
  
  int       poly_start   = 1;      // first polygon defined after \#polygons
  MultiPoint **isoele    = new MultiPoint *[npoly]; //element pointer list

  for( int n=0; n<npoly; n++ ) {
    int npts = poly[poly_start];              // \#nodes defining polygon
    int elePts[npts];                         // nodes defining the isoelement
    for( int i=0; i<npts; i++ ) {
      int pindex = poly_start+1+i*2;
      int n0     = _node[poly[pindex]];
      int n1     = _node[poly[pindex+1]];
      EdgePair edge( std::min(n0,n1), std::max(n0,n1) );
      elePts[i]  = epm[edge];
    }

    switch(poly[poly_start]) {
        case 1:
            assert(0);
            break;
        case 2:
            isoele[n] = new Connection( epts );
            break;
        case 3: 
            isoele[n] = new Triangle( epts );
            break;
        case  4:
            isoele[n] = new Quadrilateral( epts );
            break;
        default:
            isoele[n] = new PolyGon( epts, npts );
            break;
    }
    isoele[n]->define( elePts );

    if( poly[poly_start]>2 ) {                 // it is a surface element
      SurfaceElement *se = dynamic_cast<SurfaceElement*>(isoele[n]);
      se->compute_normals(0,0);
    }
    poly_start += npts*2+1;
  }
  return isoele;
}


/** determine the isosurface for a multipoint object
 *
 *  A row in a table is determined from the nodes above the threshold value
 *  Each row is of the form \n
 *      \#polygon \#sides_poly0 edge0_node0 edge0_node1 edge1_node0
 *      edge1_ node1 ... edgeN_node1 \#sides_poly1  edge0_node0 ...
 *      edgeN_node2
 *
 * \param        dat       data for all the nodes
 * \param[in]    val       value for the isosurface
 * \param[out]   npoly     number of polygons
 * \param[inout] epts      points defining isosurface
 * \param[inout] epm       map of edge nodes to isovalue intersection point (\p epts)
 * \param[in]    global_pt all element points are in a surface point list
 *
 *  \return a list of element pointers
 *  \post   \pnpoly is the number of elements in the list
 *  \post   \p epts and \p epm are updated with any new points created
 */
MultiPoint ** MultiPoint::isosurf( DATA_TYPE *dat, DATA_TYPE val, int &npoly,
        PPoint *epts, EdgePtMap &epm, bool global_pt ) 
{
  // determine row index into table 
  unsigned int index=0;
  for( int i=_ptsPerObj-1; i>=0; i-- ) {
    index <<= 1;
    
    // Modified: data[_node[i]] > val to this
    if( dat[_node[i]]>=val )
      index += 1;
  }

  const int* poly        = iso_polys(index);
  npoly                  = poly[0]; // number of polygons to create
  
  // do not continue if number of polygons are zero
  if (!npoly) return NULL;
  
  int       poly_start   = 1;      // first polygon defined after \#polygons
  MultiPoint **isoele    = new MultiPoint *[npoly]; //element pointer list

  for( int n=0; n<npoly; n++ ) {
    int npts = poly[poly_start];              // \#nodes defining polygon
    int elePts[npts];                         // nodes defining the isoelement
    for( int i=0; i<npts; i++ ) {
      int pindex = poly_start+1+i*2;
      int n0     = _node[poly[pindex]];
      int n1     = _node[poly[pindex+1]];
      EdgePair edge( std::min(n0,n1), std::max(n0,n1) );
      GLfloat pt[3]; 
      float d = edge_interp( n0, n1, dat, val, pt );
      int   newpt;
#ifdef _OPENMP 
      // avoid race conditions if updating or accessing epm
      omp_set_lock(&writelock);
#endif
      EdgePtMap::iterator eit;
      if( !global_pt || ((eit=epm.find(edge)) == epm.end())  ) {
        epm[edge] = newpt = epts->num();
        epts->add( pt, 1 );
      } else
        newpt = eit->second;
#ifdef _OPENMP
      omp_unset_lock(&writelock);
#endif
      elePts[i] = newpt;
    }

    switch(poly[poly_start]) {
        case 1:
            assert(0);
            break;
        case 2:
            isoele[n] = new Connection( epts );
            break;
        case 3: 
            isoele[n] = new Triangle( epts );
            break;
        case  4:
            isoele[n] = new Quadrilateral( epts );
            break;
        default:
            isoele[n] = new PolyGon( epts, npts );
            break;
    }
    isoele[n]->define( elePts );

    if( poly[poly_start]>2 ) {                 // it is a surface element
      SurfaceElement *se = dynamic_cast<SurfaceElement*>(isoele[n]);
      se->compute_normals(0,0);

      /*
      GLfloat *ptnrml = new GLfloat[3*npts];
      for( int i=0; i<npts; i++ ) {
        memcpy( ptnrml+3*i, se->nrml(), 3*sizeof(GLfloat) );
      }
      
      // make sure the normal points the correct way
      int n0 = _node[poly[poly_start+1]];
      int n1 = _node[poly[poly_start+2]];

      GLfloat norm[3];
      sub( _pt->pt(n0), _pt->pt(n1), norm );
      if( dat[n0]>val )
        scale( norm, -1 );

      if( dot( norm, se->nrml() ) < 0 )
        for( int ni=0; ni<npts; ni++ )
          scale( ptnrml+ni*3, -1 );

      se->vertnorm( ptnrml );
      */
    }
    poly_start += npts*2+1;
  }

#ifdef _OPENMP 
      omp_set_lock(&writelock);
#endif
  epts->setVis(true);
  epts->offset(_pt->offset());
#ifdef _OPENMP 
      omp_unset_lock(&writelock);
#endif

  return isoele;
}

/** 
 * return a list of lists of nodes defining the surface of a volume element
 *
 * for nontris, the node table is filled in by row with the number of nodes and then
 * the global node numbers
 * for tris,  the node table is filled in by row with the global node numbers
 *
 * \param ve   volume element
 * \param ft   table to fill in with the face info
 * \param ns   number of surfaces
 * \param nn   number of nodes per surface
 * \param nl   table of local nodes for each surface
 * \param tri  return all tris
 *
 * \return the number of faces
 */
int 
VolElement:: make_surf_nodelist( int ve, int ft[][MAX_NUM_SURF_NODES+1], const int ns, int nn, 
                                                        const int **nl, bool tri )
{
  int snum = tri ? 0 : ns;
  for( int s=0; s<ns; s++ ) {
    int *rptr = reinterpret_cast<int*>(nl)+s*nn;
    if( tri ) {
      for( int i=0; i<3; i++ ) 
        ft[snum][i] = _node[ve*_ptsPerObj+rptr[i]];
      snum++;
      if( nn==4 && rptr[3] != -1 ){
        for( int i=0; i<nn; i++ ) 
          ft[snum][i] = _node[ve*_ptsPerObj+rptr[(2+i)%4]];
        snum++;
      }
    } else {
      int i;
      for( i=0; i<nn; i++ ) {
        int lnode = *rptr++;
        if( lnode == -1 )
          break;
        ft[s][i+1] = _node[ve*_ptsPerObj+lnode];
      }
      ft[s][0] = i; 
    }
  }
  return snum;
}



