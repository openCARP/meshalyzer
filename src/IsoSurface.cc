// project
#include "IsoSurface.h"
#include "VecData.h"
#include "render_utils.h"
#define BRANCH_TOL 0.2

#ifdef _OPENMP
#include <omp.h>
#else
typedef int omp_int_t;
inline omp_int_t omp_get_thread_num()  {return 0;}
inline omp_int_t omp_get_max_threads() {return 1;}
#endif

/** calc isosurface
 *
 * \param m      model
 * \param dat    data displayed on model
 * \param v      isovalue
 * \param member true if part of model
 * \param t      time
 * \param branch branch cut information (NULL if none)
 */
void
IsoSurface::calc(Model* m, DATA_TYPE *dat, double v, 
		       std::vector<bool>&member, int t, double *branch )
{
  double brmin = branch ? branch[0]+BRANCH_TOL*(branch[1]-branch[0]) : 0;
  double brmax = branch ? branch[1]-BRANCH_TOL*(branch[1]-branch[0]) : 0;
  PPoint *pt = _p;
  _tm        = t;
  _val       = v;

  int numthread = omp_get_max_threads();

  EdgePtMap ep_map[numthread];
  std::vector<unsigned int>isovols;
  
#pragma omp parallel for shared(ep_map)
  for( int i=0; i<m->numVol(); i++ ) {
    int thrID = omp_get_thread_num();

    if( !member[i] ) continue;

    // ignore elements crossing the branch cut
    if( branch ) {
      const int* nodes= m->_vol[i]->obj();
      bool low=false, high=false;
      for( int n=0; n<m->_vol[i]->ptsPerObj(); n++ ) {
        if( dat[nodes[n]] <= brmin ) low=true;
        if( dat[nodes[n]] >= brmax ) high=true;
      }
      if( low && high ) 
        continue;
    }

    int np = m->_vol[i]->isoEdges(dat, v, ep_map[thrID]);
    if( np ) 
#pragma omp critical
      isovols.push_back(i);
  }
  // merge the maps into one
  for( int j=1; j<numthread; j++ ) 
    ep_map[0].merge(ep_map[j]);

  int npt = pt->num();
  pt->add( NULL, ep_map[0].size() );

  int nn=npt;
  for( auto mi = ep_map[0].begin(); mi != ep_map[0].end(); mi++ )
    mi->second = nn++;

  // count the edges for each thread
  std::vector<int> bucket(numthread+1, 0);
  const int work  = (ep_map[0].size()+numthread-1)/numthread;
  int task  = 1;
  int total = 0;
  for(size_t b=0;b<ep_map[0].bucket_count();b++) {
    total += ep_map[0].bucket_size(b);
    if( total >= work ) {
      bucket[task++] = b+1;
      total = 0;
    }
  }
  if( total )
    bucket[task] = ep_map[0].bucket_count();

  // now compute the isovalue points on the edges
#pragma omp parallel for schedule(static) 
  for( int thr=0; thr<numthread; thr++ ){
    for(size_t b=bucket[thr];b<bucket[thr+1];b++) {
      if( ep_map[0].bucket_size(b)==0 ) continue;
      for(auto bi=ep_map[0].begin(b);bi!=ep_map[0].end(b);bi++){
        GLfloat ip[3]; 
        unsigned int n0 = bi->first.first;
        unsigned int n1 = bi->first.second;
        float d = m->_vol[0]->edge_interp( n0, n1, dat, v, ip );
        pt->set(bi->second,ip);
      }
    }
  }

  // compute the isosurface elements
#pragma omp parallel for schedule(static) 
  for( int i=0; i<isovols.size(); i++ ) {

      int npoly;
      MultiPoint **lpoly = m->_vol[isovols[i]]->isosurfaces( dat, _val, npoly, pt, ep_map[0] );

      for( int j=0; j<npoly; j++ )
#pragma omp critical
      {
        _ele.push_back(static_cast<SurfaceElement*>(lpoly[j]));
      }
      delete[] lpoly;
  }

  determine_vert_norms(*_p);
  pt->setVis(true);
}


void 
IsoSurface::draw(bool recolor, bool flat) {
  glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
  zsort( NULL, 1, false );
  GLfloat *vb0 = _rd.buffer();
  if( fresh() ){
    GLfloat* vbo = _rd.buffer();
    buffer( vbo, flat );
  }
  if( recolor || fresh() ) {
   GLubyte *buff = _rd.colbuffer();
   for( int i=0; i<_rd.numVtx(); i++ )
      buffer_RGB( buff+VBO_RGBA_SZ*i, _fillcolor, true );
   _rd.update_col();
  } 
  if( fresh() ) _rd.update_nodalbuff();
  render();
}

IsoSurface::~IsoSurface()
{
  for( auto& ele : _ele ) {
    delete ele->pt();
    delete ele;
  }
  if(_vertnorm)free(_vertnorm);
  delete _p;
}


/** output the surface as an auxilliary grid
 *
 * \param basename base filename
 *
 * \pre the files have been opened and the number of elements and points 
 *      written to it
*/
void 
IsoSurface::saveAux( std::string basename, int pt_offset ) 
{
  std::string ptfile = basename+".pts_t";
  FILE*pout = fopen( ptfile.c_str(), "a" );
  const GLfloat *p = _p->pt();
  for( int i=0; i<_p->num(); i++, p+=3 )
    fprintf( pout, "%.1f %.1f %.1f\n", p[0], p[1], p[2] );
  fclose( pout );

  std::string elemfile = basename+".elem_t";
  FILE *eout = fopen( elemfile.c_str(), "a" );
  for( auto const& poly: _ele ) {
    const int* n = poly->obj();
    for( int j=0; j<poly->ptsPerObj(); j++ ){
      fprintf( pout, "%d ", n[j]+pt_offset );
    }
    fprintf( pout, "\n" );
  }
  fclose( eout );
}

void 
IsoSurface::clear( GLfloat *n )
{
  if ( !_ele.empty() ) 
    for ( int i=0; i<_ele.size(); i++ ) {
      delete _ele[i];
    }
  _ele.clear();
  _zlist.clear();
  free(_vertnorm);
  _vertnorm = NULL;
  delete _p;
  _p = new PPoint;
  _rd.clear(); 
  _fresh = true;
}


