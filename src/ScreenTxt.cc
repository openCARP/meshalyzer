/** \file */
#include "ScreenTxt.h"
#include <iostream>
#include "ttf_sans.h"
#include "ttf_greek.h"

const float delbl = 0.3; //!< factor by which to raise/lower baseline for sub/superscript
const float delfs = 0.7; //!< fontsize change for sub/superscript

enum Baseline {Base, Sup, Sub};

/**
 * @brief determine the height at which to start rendering
 *
 * @param nbl   baseline
 * @param delbl change in baseline
 * @param fs    font size
 * @param miny  lowest point of text to render
 * @param maxy  highest point of text to render
 * @param c_hgt height of last character rendered
 *
 * @return the offset to start rendering in pixels
 */
inline float bl_shift( Baseline nbl, float delbl, float fs, float miny, float maxy, float c_hgt ) {
  return nbl==Sup ? c_hgt-delbl*fs-miny : delbl*fs-maxy;
}


ScreenTxt::ScreenTxt( const char *fnt )
{
  if(FT_Init_FreeType(&_ft)) {
    std::cerr << "Could not initialize freetype library" << std::endl;
    throw 1;
  }
  if( !fnt ){
    FT_New_Memory_Face(_ft, FreeSans_ttf, FreeSans_ttf_len, 0, &_roman);
  } else {
    if( FT_New_Face(_ft, fnt, 0, &_roman)) {
      std::cerr << "Could not open font " << fnt << std::endl;
      throw 1;
    }
  }

  FT_New_Memory_Face(_ft, greek_ttf, greek_ttf_len, 0, &_greek);

  _program = get_screen_txt_shader();
 
  glGenBuffers(1, &_vbo);

  _attribute_coord = glGetAttribLocation(_program, "coord");
  _uniform_tex     = glGetUniformLocation(_program, "tex");
  _uniform_color   = glGetUniformLocation(_program, "color");

  glGenVertexArrays(1, &_vao);

  colour( _col );
}


/**
 * @brief find matching brace,bracket,etc
 *
 * @param p         where to start looking (null terminated)
 * @param brace     what to look for 
 * @param antibrace what opens a deeper level
 *
 * @return pointer to brace if found, else NULL
 */
const char *
find_matching( const char *p, char brace, char antibrace )
{
  int count=1;
  for( ; *p!='\0' && count; p++) {
    if( *p=='\\' ) {
      if( *(++p)=='\0' ) return NULL;
      continue;
    }
    if( *p==brace     ) count--;
    if( *p==antibrace ) count++;
    if( !count ) break;
  }
  return count? NULL : p;
}


/**
 * @brief render a string of text
 *
 * The following formatting is respected:
 * Sequence                    | meaning
 * ----------------------------|--------------------------
 * _{...}                      | subscript
 * _X                          | subscript X (one char)
 * ^{...}                      | superscript
 * ^X                          | superscript X (one char)
 * _{...}^{...} or ^{...}_{...}| sub- and superscript
 * _{...}^{...} or ^{...}_{...}| sub- and superscript
 *  _X^{...} or ^Y_{...}       | sub- and superscript
 * \$                          | toggle symbol font
 *
 * The following escape sequences are provided to print the literal:
 *  \\\\, \\\$, \\ _, \\^, \\{, \\}
 *
 * @param p0        start of string to render
 * @param p1        end of string to render
 * @param fntsz     font size
 * @param[inout] x0 string anchor x ordinate
 * @param[inout] y0 string anchor y ordinate
 *
 * @return height of last character rendered
 *
 * @post _face and baseline may be changed
 * @post anchor is advanced for rendering the next character
 *
 * \note This is a recursive function
 */
int
ScreenTxt::process(const char *p0, const char *p1, float fntsz, float &x0, float &y0)
{
  Baseline    bl     = Base;
  float       ox, oy;
  float       lastx, lasty;
  int         c_hgt  = fntsz;  //!< height of last character rendered

  for( ; p0<p1; p0++ ){ 
    if( *p0 == '\\' ) {                     // start of escaped literal
      if( *(++p0) == '\0' ) return c_hgt;
      if( !strchr( "\\$_^{}", *p0 ) )
        std::cerr << "Error in text: ignoring \\" << std::endl;
      c_hgt = char_render( *p0, x0, y0, fntsz );
      bl = Base;
    } else if( *p0 == '$' ) {              // toggle font
      _face = _face==_roman ? _greek : _roman;
    } else if( *p0=='_' || *p0=='^' ) {    // super or subscript
      
      Baseline nbl = *p0=='_' ? Sub : Sup;
      const char *end;
      bool        singleton = true;
      if( *(++p0) == '{' ) {
        if( !(end = find_matching( ++p0, '}', '{' )) ) throw 1;
        singleton = false;
      } else
        end = p0+1;

      bool sub_sup = (bl==Sup && nbl==Sub) || (bl==Sub && nbl==Sup);
      if( sub_sup ) {
        if( _vert ) {
          lasty = y0;
          y0 = oy;
        } else {
          lastx = x0; 
          x0 = ox;
        }
      } else {
        ox = x0;
        oy = y0;
      }
      
      int tx, bottom, top;
      process_size(p0, end, fntsz*delfs, tx, bottom, top );
      float dd = bl_shift( nbl, delbl, fntsz*delfs, bottom, top, c_hgt );
      if( !_vert ) {
        float y1 = y0 + _sy*dd;
        process( p0, end, fntsz*delfs, x0, y1 );
      } else {
        float x1 = x0 - _sx*dd*_vert;
        process( p0, end, fntsz*delfs, x1, y0 );
      }

      if( sub_sup ) {
        if( !_vert ) 
          x0 = std::max(x0, lastx );
        else if( _vert==1 )
          y0 = std::max(y0, lasty );
        else
          y0 = std::min(y0, lasty );
      }

      bl = sub_sup ? Base : nbl;
      p0 = singleton ? end-1 : end;
    } else {                               // a simple character to render
      c_hgt = char_render( *p0, x0, y0, fntsz );
      bl = Base;
    }
  }
  return c_hgt;
}

/**
 * @brief determine the size of a text string in pixels
 *
 * @param        p0    beginning of string
 * @param        p1    one past end of string
 * @param        fntsz font size in points
 * @param[inout] x     current x postion in points
 * @param[out]   ymin  minimum y position in points
 * @param[out]   ymax  maximum y position in points
 *
 * \post the width of the string is added to x
 *
 * \note assume that the string is written horizontally
 * \note This is a recursive function
 */
void
ScreenTxt::process_size(const char *p0, const char *p1, float fntsz, int &x, int &ymin, int &ymax )
{
  Baseline bl     = Base;
  float    ox, oy;
  int      cymin, cymax;

  ymin = ymax = 0;

  for( ; p0<p1; p0++ ){ 
    if( *p0 == '\\' ) {
      if( *(++p0) == '\0' ) return;
      if( !strchr( "\\$_^{}", *p0 ) )
        std::cerr << "Error in text: ignoring \\" << std::endl;
      x += char_size( *p0, fntsz, cymin, cymax );
      ymax = std::max( ymax, cymax );
      ymin = std::min( ymin, cymin );
      bl = Base;
    } else if( *p0 == '$' ) {
      _face = _face==_roman ? _greek : _roman;
    } else if( *p0=='_' || *p0=='^' ) {
      Baseline nbl = *p0=='_' ? Sub : Sup;
      const char *end;
      bool        singleton = false;
      if( *(++p0) != '{' ) {
        end = p0+1;
        singleton = true;
      } else {
        if( !(end=find_matching(++p0, '}', '{')) ) throw 1;
      }

      if( bl==Base ) ox = x; 

      bool sub_sup = (bl==Sup && nbl==Sub) || (bl==Sub && nbl==Sup);
      int  last_x;
      if( sub_sup ) {
        last_x = x;
        x = ox;
      }

      int tymax;
      process_size( p0, end, fntsz*delfs, x, cymin, tymax );
      int y1 = bl_shift(nbl, delbl, fntsz*delfs, cymin, tymax, cymax );
      ymax   = std::max( ymax, tymax+y1 );
      ymin   = std::min( ymin, cymin+y1 );
      
      if( sub_sup ) {
        bl = Base;
        x = std::max( x, last_x );
      } else 
        bl = nbl;
      p0 = end;
      if( singleton ) p0--;
    } else {
      x += char_size( *p0, fntsz, cymin, cymax );
      ymax = std::max( ymax, cymax );
      ymin = std::min( ymin, cymin );
      bl = Base;
    }
  }
}


/**
 * @brief render a single character
 *
 * @param c    the character
 * @param x    x ordinate
 * @param y    y ordinate
 * @param fs   font size
 *
 * \return the height of the character in pixels
 * 
 * \post \p x and \p y are advanced
 */
int
ScreenTxt::char_render(const char c, float &x, float &y, float fs) 
{
  static float ofs=0;

  if( fs != _fs ) {
    FT_Set_Pixel_Sizes(_roman, 0, fs);
    FT_Set_Pixel_Sizes(_greek, 0, fs);
    _fs = fs;
  }

  if(FT_Load_Char(_face, c, FT_LOAD_RENDER)) throw 1;

  FT_GlyphSlot g = _face->glyph;

  glTexImage2D(
          GL_TEXTURE_2D,
          0,
          GL_RED,
          g->bitmap.width,
          g->bitmap.rows,
          0,
          GL_RED,
          GL_UNSIGNED_BYTE,
          g->bitmap.buffer
          );

  if( !_vert ) {
    float x2 = x + g->bitmap_left * _sx;
    float y2 = y + g->bitmap_top  * _sy;
    float w  = g->bitmap.width*_sx;
    float h  = g->bitmap.rows*_sy;
    GLfloat box[4][4] = {
      {x2,     y2    , 0, 0},
      {x2 + w, y2    , 1, 0},
      {x2,     y2 - h, 0, 1},
      {x2 + w, y2 - h, 1, 1},
    };
    glBufferData(GL_ARRAY_BUFFER, sizeof(box), box, GL_DYNAMIC_DRAW);
    x += (g->advance.x/64) * _sx;
    y += (g->advance.y/64) * _sy;
  } else {
    float x2 = x - g->bitmap_top  * _sx * _vert;
    float y2 = y + g->bitmap_left * _sy * _vert;
    float w  = g->bitmap.rows*_sx;
    float h  = g->bitmap.width*_sy;
    GLfloat box[4][4] = {
      {x2,           y2          , 0, 0 },
      {x2 + w*_vert, y2          , 0, 1 },
      {x2,           y2 + h*_vert, 1, 0 },
      {x2 + w*_vert, y2 + h*_vert, 1, 1 }
    };
    glBufferData(GL_ARRAY_BUFFER, sizeof(box), box, GL_DYNAMIC_DRAW);
    y += (g->advance.x/64) * _sy * _vert ;
  }
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
  return g->bitmap_top;
}


/**
 * @brief return character dimensions in pixels
 *
 * @param c         the character
 * @param fs        font size
 * @param[out] ymin minimum Y 
 * @param[out] ymax maximum Y
 *
 * @return the width in pixels
 */
int ScreenTxt::char_size(const char c, float fs, int &ymin, int &ymax) 
{
  static float ofs=0;

  if( fs != _fs ) {
    FT_Set_Pixel_Sizes(_roman, 0, fs);
    FT_Set_Pixel_Sizes(_greek, 0, fs);
    _fs = fs;
  }

  if(FT_Load_Char(_face, c, FT_LOAD_RENDER)) {
    ymin = ymax = 0;
    return 0;
  }

  FT_GlyphSlot g = _face->glyph;

  ymax = g->bitmap_top;
  ymin = ymax - g->bitmap.rows;
  return g->advance.x/64;
}


/**
 * @brief render text
 * 
 *  \f[ \in [-1,1] \f]
 *
 * @param text to be rendered
 * @param x    x position \f$ \in [-1,1]\f$
 * @param y    y position \f$ \in [-1,1]\f$
 * @param w    window width in pixels
 * @param h    window height in pixels
 * @param fs   font size
 * @param vert draw vertical (1=up,-1=down)
 */
void 
ScreenTxt::render(const char *text, float x, float y, int w, int h, float fs, int vert) 
{
  _face = _roman;

  glUseProgram(_program);
  glEnable(GL_BLEND);
  glDisable(GL_DEPTH_TEST);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);    
  glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
  
  glUniform4fv(_uniform_color, 1, _col ); 
  
//#ifdef __APPLE__
  glBindVertexArray(_vao);
//#endif

  GLuint tex;
  glActiveTexture(GL_TEXTURE0);
  glGenTextures(1, &tex);
  glBindTexture(GL_TEXTURE_2D, tex);
  glUniform1i(_uniform_tex, 0);

  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  glEnableVertexAttribArray(_attribute_coord);
  glBindBuffer(GL_ARRAY_BUFFER, _vbo);
  glVertexAttribPointer(_attribute_coord, 4, GL_FLOAT, GL_FALSE, 0, 0);

  _sx   = 2./w;
  _sy   = 2./h;
  _vert = vert;
  try {
    process( text, text+strlen(text), fs, x, y ); 
  }
  catch(...) {
    std::cerr << "Badly formatted text: " << text << std::endl;
  }

  glEnable(GL_DEPTH_TEST);
  glDisableVertexAttribArray(_attribute_coord);
  glDeleteTextures(1, &tex);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
#ifdef __APPLE__
  glBindVertexArray(0);
#endif
}


ScreenTxt::~ScreenTxt()
{
  FT_Done_FreeType( _ft );  
  glDeleteBuffers( 1, &_vbo );
  glDeleteProgram( _program );
#ifdef __APPLE__
  glDeleteVertexArrays( 1, &_vao );
#endif
}


/**
 * @brief Determine bounding box for a string
 *
 * @param txt        text to measure
 * @param w          widow width in pixels
 * @param h          window height in pixels
 * @param fs         font size
 * @param txtW [out] text width in normalized screen units
 * @param txtH [out] text height in normalized screen units
 * @param vert       horizontal (0), up (1) or down (-1) text
 *
 * @return the lowest or leftest point of the text in normalized screen units
 */
float
ScreenTxt::size( const char *txt, int w, int h, float fs, float &txtW, float &txtH, int vert )
{
  txtW = 0;

  FT_Set_Pixel_Sizes(_roman, 0, fs);
  FT_Set_Pixel_Sizes(_greek, 0, fs);

  _face = _roman;
  _sx   = 2./w;
  _sy   = 2./h;
  _vert = vert;

  int minY  = 0;
  int maxY  = 0;
  int width = 0;

  try {
    process_size( txt, txt+strlen(txt), fs, width, minY, maxY );
  }
  catch(...) {
    std::cerr << "Badly formatted text: " << txt << std::endl;
  }

  if( vert == 0 ) {
    txtW = width*_sx;
    txtH = (maxY-minY)*_sy;
    return minY*_sy;
  } else {
    txtH = width*_sy*vert;
    txtW = (maxY-minY)*_sx;
    return vert==1 ? -maxY*_sx : minY*_sx;
  }
}


/**
 * @brief determine the starting coordinates to render text
 *
 * @param        txt  text to render
 * @param        pos  how to position text relative to anchor point
 * @param        w    window width in pixels
 * @param        h    window height in pixels
 * @param        fs   font size
 * @param[inout] x    anchor point x ordinate  
 * @param[inout] y    anchor point y ordinate
 * @param        vert +/-1 for vertical text, 0 for horizontal
 *
 * \note on return, \p x andd \p y are updated to the values to pass to render()
 *
 * \note the coordinates of the bottom left corner of the window are (-1,-1)
 */
void 
ScreenTxt::position( const char *txt, TxtPos pos, int w, int h, float fs, float &x, float &y, int vert )
{
  if( pos==N ) return;

  float txtW, txtH;
  float dip = size( txt, w, h, fs, txtW, txtH, vert );
  if( !vert )
    y -= dip;
  else
    x -= dip;

  if( pos==LL ){
  } else if( pos==LC) {
    x -= txtW/2.;
  } else if( pos==LR) {
    x -= txtW;
  } else if( pos==L) {
    y -= txtH/2.;
  } else if( pos==C) {
    x -= txtW/2.;
    y -= txtH/2.;
  } else if( pos==R) {
    x -= txtW;
    y -= txtH/2.;
  } else if( pos==UL) {
    y -= txtH;
  } else if( pos==UC) {
    x -= txtW/2;
    y -= txtH;
  } else if( pos==UR) {
    x -= txtW;
    y -= txtH;
  }
}               
