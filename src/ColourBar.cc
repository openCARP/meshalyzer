#include "ColourBar.h"
#include <FL/Enumerations.H>

#include <iostream>
#include <vector>
#include <algorithm>
#include "ScreenTxt.h"
#include "TBmeshWin.h"
#include "render_utils.h"

typedef uchar ucol3[3];
typedef uchar ucol4[4];

void make_framebuffer( GLsizei w, GLsizei h, GLuint &fb, GLuint &cb, GLuint &db, bool multisampled=false );

/** reverse the order of an array of RGB colours */
void
ColourBar::flip()
{
  ucol3 *c = (ucol3 *)_rgb;
  std::reverse( c, c+_cs->size() );
}

void
ColourBar::draw()
{
  _inset = 2*ticgap;

  ((TBmeshWin*)(user_data()))->redraw(VBO_ColBar);
}


/**
 * @brief get labels and their positions
 *
 * @param[out] x          x ordinates of labels
 * @param[out] y          y ordinates of labels
 * @param[ou]} label      tic labels
 * @param[out] tpos       label positioning
 * @param w      window width
 * @param h      window height
 * @param sx      scale factor
 * @param sy      scale factor
 *
 * \note positions are in normalized screen coordinates relative to origin
 */
void
ColourBar::get_labels(std::vector<float> &x, std::vector<float>&y,
                    std::vector<std::string>& label, std::vector<TxtPos>&tpos, 
                    float w, float h, float sx, float sy )
{
  if(!_ntics ) {
    x.clear();
    y.clear();
    tpos.clear();
    label.clear();
    return;
  }

  // make labels
  label.resize(_ntics==1?2:_ntics);
  float dv = (_cs->max()-_cs->min())/(label.size()-1);
  char tstr[1024];
  for( int i=0; i<label.size(); i++){
    snprintf( tstr, 1024, _fmt.c_str(), _cs->min()+i*dv );
    label[i] = tstr;
  }
  // determine tick loations and text orientation
  x.resize(label.size());
  y.resize(label.size());
  tpos.resize(label.size());
  if(_ntics==1){
    if( _orient==CB_LAND) {
      x[0] = -ticgap*sx;
      x[1] = w+ticgap*sx;
      y[0] = y[1] = -h/2;
      tpos[0] = R;
      tpos[1] = L;
    } else {
      x[0] = x[1] = w/2;
      y[0] = -h-ticgap*sx;
      y[1] = ticgap*sx;
      tpos[0] = UC;
      tpos[1] = LC;
    }
  } else {
    float t0, dtic;
    if( _tic_ctr ) {
      dtic = (_ntics-1.)/(1.-1./_cs->size());
      t0   = 1/float(_cs->size())/2.;
    } else {
      dtic = label.size()-1.;
      t0   = 0.;
    }
    for( float i=0; i<label.size(); i++ ) {
      if( _orient==CB_LAND) {
        x[i]    = w*(i/dtic+t0);
        tpos[i] = _rgtdwn ? LC : UC;
        if( _rgtdwn ) {
          y[i] =  (_ticlen+2*ticgap)*sy;
        } else{
          y[i] = -(_ticlen+2*ticgap)*sy-h;
        }
      }else{
        y[i]    = -h*(1.-i/dtic-t0);
        tpos[i] = _rgtdwn ? R : L;
        x[i]    = _rgtdwn ? -(_ticlen+2*ticgap)*sx : (_ticlen+2*ticgap)*sx+w;
      }
    }
  }
}


/**
 * @brief  trim as much background as possible and make background transparent
 *
 * @param img[in] image to trim
 * @param nx[out] new width
 * @param ny[out] new height
 * @param trans   transparent background?
 *
 * \return the data of the trimmed image, delete when finished with it
 */
uchar *
ColourBar::trim( Fl_Image *img, int &nx, int &ny, bool trans )
{
  int    w     = img->w();
  int    h     = img->h();
  auto   d     = img->data();
  ucol3 *pixel = (ucol3*)(d[0]);
  ucol3  bgd   = { _br, _bg, _bb };
  int    llx, lly, urx, ury;

  for( int j=0; j<h; j++ )
    for( int i=0; i<w; i++ )
      if( memcmp(pixel+j*w+i, bgd, sizeof(ucol3)) ){
        lly = j;
        j   = h;
        break;
      }

  for( int i=0; i<w; i++ )
    for( int j=0; j<h; j++ )
      if( memcmp(pixel+j*w+i, bgd, sizeof(ucol3)) ){
        llx = i;
        i   = w;
        break;
      }

  for( int j=h-1; j>=0; j-- )
    for( int i=0; i<w; i++ )
      if( memcmp(pixel+j*w+i, bgd, sizeof(ucol3)) ){
        ury = j;
        j   = -1;
        break;
      }

  for( int i=w-1; i>=0; i-- )
    for( int j=0; j<h; j++ )
      if( memcmp(pixel+j*w+i, bgd, sizeof(ucol3)) ){
        urx = i;
        i   = -1;
        break;
      }

  nx = urx - llx + 1;
  ny = ury - lly + 1;

  if( trans ) {
    ucol4 *newimg = new ucol4[nx*ny];
    for( int j=lly; j<=ury; j++ )
      for( int k=llx; k<=urx; k++ ){
        memcpy( newimg+(j-lly)*nx+k-llx, pixel+j*w+k, sizeof(ucol3) );
        if( !memcmp(  newimg+(j-lly)*nx+k-llx, bgd, 3 ) )
            newimg[(j-lly)*nx+k-llx][3]=0;
        else
            newimg[(j-lly)*nx+k-llx][3]=255;
      }
    return (uchar*)newimg;
  } else {
    ucol3 *newimg = new ucol3[nx*ny];
    for( int j=lly; j<=ury; j++ )
      memcpy( newimg+nx*(j-lly), pixel+llx+j*w, nx*sizeof(ucol3) );
    return (uchar*)newimg;
  }
}


Fl_Image *
ColourBar::get_cbar(int width)
{
  delete _rgb;
  _rgb = new uchar[_cs->size()*3];
  _cs->RGBmap(_rgb);
  Fl_Image* img_cpy;
  int w,h;
  ((TBmeshWin *)user_data())->get_win_sz( w, h );
  if( _orient==CB_LAND ) {
    rgb( _rgb, _cs->size(), 1 );
    img_cpy = _bar->copy( w*_len, width );
  } else {
    rgb( _rgb, 1, _cs->size() );
    img_cpy = _bar->copy( width, h*_len );
  }
  return img_cpy;
}

/**
 * @brief 
 *
 * @param x0 x origin of colour bar in nsc
 * @param y0 y origin of colour bar in nsc
 * @param sw window width in pixels
 * @param sh window height in pixels
 */
void 
ColourBar::ogl_render(float x0, float y0, int sw, int sh)
{
  if( _program[0]==-1){
    _program[0]         = get_cbar_shader();
    _attribute_coord[0] = glGetAttribLocation(_program[0],  "coord");
    _uniform_tex        = glGetUniformLocation(_program[0], "tex");
    _uniform_color[0]   = glGetUniformLocation(_program[0], "color");

    _program[1]         = get_cbar_line_shader();
    _attribute_coord[1] = glGetAttribLocation(_program[1],  "coord");
    _uniform_color[1]   = glGetUniformLocation(_program[1], "color");

    glGenBuffers(2, _vbo);
#ifdef __APPLE__
    glGenVertexArrays(2, _vao);
#endif
  }

  Fl_Image* cb_img = get_cbar(2);

  glUseProgram(_program[0]);
  glEnable(GL_BLEND);
  glDisable(GL_DEPTH_TEST);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);    
  glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );

  GLuint tex;
  glActiveTexture(GL_TEXTURE0);
  glGenTextures(1, &tex);
  glBindTexture(GL_TEXTURE_2D, tex);
  glUniform1i(_uniform_tex, 0);

#ifdef __APPLE__
  glBindVertexArray(_vao[0]);
#endif

  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  glEnableVertexAttribArray(_attribute_coord[0]);
  glBindBuffer(GL_ARRAY_BUFFER, _vbo[0]);
  glVertexAttribPointer(_attribute_coord[0], 4, GL_FLOAT, GL_FALSE, 0, 0);
  
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,
          cb_img->w(), cb_img->h(), 0,
          GL_RGB,
          GL_UNSIGNED_BYTE, 
          cb_img->data()[0] );

  float sx = 2./sw;
  float sy = 2./sh;

  float x2 = x0;
  float y2 = y0;
  float w  = (_orient!=CB_LAND ? _width : cb_img->w() )*sx;
  float h  = (_orient==CB_LAND ? _width : cb_img->h() )*sy;

  GLfloat box[4][4] = {
    {x2,   y2,   0, 1},
    {x2+w, y2,   1, 1},
    {x2,   y2-h, 0, 0},
    {x2+w, y2-h, 1, 0},
  };

  glBufferData(GL_ARRAY_BUFFER, sizeof(box), box, GL_DYNAMIC_DRAW);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
  glDisableVertexAttribArray(_attribute_coord[0]);
  glDeleteTextures(1, &tex);

  // draw tic labels
  float xp, yp;
  static ScreenTxt cbtxt;
  std::vector<float> xt, yt;
  std::vector<std::string> ticlab;
  std::vector<TxtPos> tp;
  get_labels( xt, yt, ticlab, tp, w, h, sx, sy );
  for( int i=0; i<xt.size(); i++ ) {
    xp=xt[i]+x2;
    yp=yt[i]+y2;
    cbtxt.position(ticlab[i].c_str(), tp[i], sw, sh, _fntsz, xp, yp);
    cbtxt.render(ticlab[i].c_str(), xp, yp, sw, sh, _fntsz);
  }

  // draw label
  TxtPos labpos;
  int vert = 0;
  if( _orient&1 ){      // horizontal label
    xp=x2+w/2;
    yp=y2;
    if( _orient == CB_LAND ) 
      labpos = _rgtdwn ? UC : LC;
    else
      labpos = LC;
    cbtxt.position(_label.c_str(), labpos, sw, sh, _fntsz, xp, yp);
    if( labpos == LC ) 
      yp += ticgap*sy;
    else
      yp -= ticgap*sy+h;
    if( _orient == CB_PORTRAIT_HORZ ){
      float dx, dy;
      float dip = ticlab.size() ? cbtxt.size(ticlab.back().c_str(),sw,sh,_fntsz,dx,dy): 0; 
      yp += _ntics==1?dy-dip+ticgap*sy:dy/2-dip;
    }
  }else if( _orient==CB_PORTRAIT ) {   // vertical label
    xp=x2;
    yp=y2-h/2;
    vert   = _rgtdwn ? -1 : 1;
    labpos = _rgtdwn ? L : R;
    cbtxt.position(_label.c_str(), labpos, sw, sh, _fntsz, xp, yp, vert);
    if( labpos == R ) 
      xp -= ticgap*sx;
    else
      xp += ticgap*sx+w;
  }
  cbtxt.render(_label.c_str(), xp, yp, sw, sh, _fntsz, vert);

  // draw lines
  glUseProgram(_program[1]);
#ifdef __APPLE__
  glBindVertexArray(_vao[1]);
#endif
  glEnableVertexAttribArray(_attribute_coord[1]);
  glBindBuffer(GL_ARRAY_BUFFER, _vbo[1]);
  glVertexAttribPointer(_attribute_coord[1], 2, GL_FLOAT, GL_FALSE, 0, 0);
  glLineWidth(2);
  glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_LINE_SMOOTH);
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);


  int      nlines = _framed*4 + (_ntics>1?xt.size():0);
  GLfloat *lnvtx  = new GLfloat[4*nlines];
  int      idx    = 0;
  if( _framed ) {
    lnvtx[idx++] = x2;   //UL
    lnvtx[idx++] = y2;
    lnvtx[idx++] = x2+w;
    lnvtx[idx++] = y2;
    lnvtx[idx++] = x2+w; //UR
    lnvtx[idx++] = y2;
    lnvtx[idx++] = x2+w;
    lnvtx[idx++] = y2-h;
    lnvtx[idx++] = x2+w; //LR
    lnvtx[idx++] = y2-h;
    lnvtx[idx++] = x2;
    lnvtx[idx++] = y2-h; 
    lnvtx[idx++] = x2;   //LL
    lnvtx[idx++] = y2-h;
    lnvtx[idx++] = x2;
    lnvtx[idx++] = y2;
  }
  if( _ntics>1 ) {
    for( int i=0; i<xt.size(); i++ ) {
      if( tp[i]==UC ){
        lnvtx[idx++] = x2+xt[i];
        lnvtx[idx++] = y2+yt[i]+ticgap*sy;
        lnvtx[idx++] = x2+xt[i];
        lnvtx[idx++] = y2+yt[i]+(_ticlen+ticgap)*sy;
      } else if( tp[i]==LC ) {
        lnvtx[idx++] = x2+xt[i];
        lnvtx[idx++] = y2+yt[i]-ticgap*sy;
        lnvtx[idx++] = x2+xt[i];
        lnvtx[idx++] = y2+yt[i]-(_ticlen+ticgap)*sy;
      } else if( tp[i]==R ) {
        lnvtx[idx++] = x2+xt[i]+ticgap*sx;
        lnvtx[idx++] = y2+yt[i];
        lnvtx[idx++] = x2+xt[i]+(_ticlen+ticgap)*sx;
        lnvtx[idx++] = y2+yt[i];
      } else if( tp[i]==L ) {
        lnvtx[idx++] = x2+xt[i]-ticgap*sx;
        lnvtx[idx++] = y2+yt[i];
        lnvtx[idx++] = x2+xt[i]-(_ticlen+ticgap)*sx;
        lnvtx[idx++] = y2+yt[i];
      }
    }
  }

  glBufferData(GL_ARRAY_BUFFER, nlines*4*sizeof(GLfloat), lnvtx, GL_DYNAMIC_DRAW);
  glDrawArrays(GL_LINES, 0, nlines*2);

  glDisableVertexAttribArray(_attribute_coord[1]);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  delete[] lnvtx;
  delete cb_img;
}
