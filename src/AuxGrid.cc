#include "AuxGrid.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include "VecData.h"
#include "legacyGL.h"
#include <math.h>

static GLfloat defPtColor[]   = {    0,    0,     0, 1};
static GLfloat defLineColor[] = {    0, 0.05, 0.883, 1}; 
static GLfloat defSurfColor[] = {   1.,  0.5,     0, 1};
static GLfloat defVolColor[]  = {0.141, 0.83, 0.618, 1};


char *get_line(gzFile);

PlotWin *AuxGrid::_timeplot = NULL;

bool has_ext( std::string, const char * );

/** reads in an integer from a file throws exception on error
 *
 * \param in file pointer
 * \return integer read from file
 *
 * \throw exception if int is not read from file
 */
int get_num( gzFile in )
{
  int number;

  char *p = get_line(in);
  if( !p || !sscanf( p, "%d", &number ) )
    throw;

  return number;
}

template<class T>
class AuxDataBase
{
    public:
        AuxDataBase(const char *f){_infile=gzopen(f,"r");}
        virtual int num()=0;          //!< number of frames
        virtual size_t numpt()=0;     //!< number of data at current time
        virtual void time(int) = 0;   //!< read a time slice
        virtual ~AuxDataBase(){gzclose(_infile);}
        virtual T val(size_t,size_t) = 0; //!< return value of a vertex at a time
        T* buf(){ return _frame.data(); }
    protected:
        gzFile _infile;
        std::vector<T>_frame;         //!< buffer for a time slice
        virtual T next()=0;
};


template<class T>
class AuxDat_t : public AuxDataBase<T>
{
    public:
        AuxDat_t(const char *f, int match_frames=-1);
        int  num(){return tm.size();}
        size_t numpt(){return _frame.size();} 
        void time( int t ) {
          gzseek(_infile,tm[t],SEEK_SET);
          _frame.resize( get_num(_infile) );
          for( int i=0; i<_frame.size(); i++ )
            _frame[i] = next();
        }
        T val(size_t t, size_t n) {
          z_off_t cpos = gztell( _infile );
          gzseek( _infile, tm[t], SEEK_SET );
          long numvtx=atoi(get_line(_infile));
          if( n>=numvtx ) { 
            std::cerr << "index out of range for time "<<t<<std::endl;
            throw 1;
          }
          for( int i=0; i<(int)n-1; i++ ) get_line(_infile);
          T res = next();
          gzseek( _infile, cpos, SEEK_SET );
          return res;
        }
    private:
        T next(){ return (T)strtod(get_line(_infile),NULL);}
        std::vector<long> tm;
        using AuxDataBase<T>::_infile;
        using AuxDataBase<T>::_frame;
};

template<class T>
AuxDat_t<T> :: AuxDat_t( const char *f, int match_frames ) : AuxDataBase<T>(f)
{
    try {
      tm.clear();
      int frames = get_num(_infile);

      // validate the number of frames
      if((match_frames!=-1) && (match_frames!=frames && frames!=1 && match_frames!=1)) {
        std::cerr << "incorrect number of timepoints in file("<<frames<<")"<<std::endl;
        throw 1;
      }
      // iterate all frames in file
      for (int frame = 0; frame < frames; frame++) {
        tm.push_back(gztell(_infile));
        int lines = get_num(_infile);
        for (int line = 0; line < lines; line++) {
          get_line(_infile);
        }
      }
    }
    catch(...) {
      return;
    }
}


template<class T>
class AuxDat : public AuxDataBase<T>
{
    public:
        AuxDat(const char *f, int match_frames=-1, int nvtx=0, int neles=0);
        int  num(){ return tm.size(); }  
        size_t  numpt(){ return _numDat; }  
        void time( int t ) { 
          gzseek( _infile, tm[t], SEEK_SET );
          for( int i=0; i<_numDat; i++ )
            _frame[i] = next();
        }
        T val(size_t t, size_t n) {
          if( n>=_numDat ) { 
            std::cerr << "data index out of range for time "<<t<<std::endl;
            throw 1;
          }
          z_off_t cpos = gztell( _infile );
          gzseek( _infile, tm[t], SEEK_SET );
          for( int i=0; i<(int)n-1; i++ ) get_line(_infile);
          T res = next();
          gzseek( _infile, cpos, SEEK_SET );
          return res;
        }

    private:
        T next(){ return (T)strtod( get_line(_infile),NULL);}
        std::vector<long> tm;
        int _numDat;
        bool vtx_dat=true;
        using AuxDataBase<T>::_infile;
        using AuxDataBase<T>::_frame;
};

template<class T>
AuxDat<T> :: AuxDat( const char *f, int match_frames, int nele, int nvtx ):AuxDataBase<T>(f){
    try {
      tm.clear();
      int numlines=0, frames;
      while( get_line(_infile) )
        numlines++;

      if( !(numlines%nvtx) ) {
        frames  = numlines/nvtx;
        _numDat  = nvtx;
      } else if( numlines%nele ) {
        std::cerr << "incorrect number of data points in file" << std::endl;
        throw 1;
      } else {
        frames  = numlines/nele;
        _numDat  = nele;
        vtx_dat = false;
      }

      // validate the number of frames
      if((match_frames!=-1) && (match_frames!=frames && frames!=1 && match_frames!=1)) {
        std::cerr << "incorrect number of timepoints in "<<f<<" ("<<frames<<")"<<std::endl; 
        throw 1;
      }
      // iterate all frames in file
      gzrewind(_infile);
      for (int frame = 0; frame < frames; frame++) {
        tm.push_back(gztell(_infile));
        for (int line = 0; line < _numDat; line++) {
          get_line(_infile);
        }
      }
    }
    catch(...) {
      return;
    }
    _frame.resize(_numDat);
}


template<class T>
class AuxDatIGB : public AuxDataBase<T>
{
    public:
        AuxDatIGB(const char *f, int match_frames=-1);
        int  num(){return h.t();}                //! number of frames
        size_t  numpt(){return h.slice_sz();}       //! number of points
        void time(int t){
          h.data_frame(t);
          h.read_data(_frame.data(),1);
        }
        T val(size_t t, size_t n) {
          z_off_t cpos = gztell( _infile );
          h.data_frame(t);
          gzseek( _infile, n*h.data_size(), SEEK_CUR );
          char buff[16];
          gzread( _infile, buff, h.data_size() );
          T datum = h.convert_buffer_datum<T>(buff, 0 );
          gzseek( _infile, cpos, SEEK_SET );
          return datum;
        }
    private:
        T    next(){ }
        IGBheader h;
        using AuxDataBase<T>::_infile;
        using AuxDataBase<T>::_frame;
};


template<class T>
AuxDatIGB<T> :: AuxDatIGB(const char *f, int match_frames):AuxDataBase<T>(f){
  h.fileptr(_infile);
  if( h.read() & 0xff )
    throw 1;
  if(match_frames!=-1 && (match_frames!=h.t() && h.t()!=1 && match_frames!=1)) 
    throw 1;
  _frame.resize(h.slice_sz());
}


class AuxGridFetcher
{
public:
  AuxGridFetcher():_model(NULL),_frame(-1){}
  virtual ~AuxGridFetcher(){};
  virtual Model *GetModel(int) =0;
  virtual int    num_tm() =0;
  virtual bool   plottable() =0;
  virtual int    time_series( int v, float *&d ) =0;
  virtual DATA_TYPE *data() = 0;
  virtual DATA_TYPE  data(size_t) = 0;
  Model *_model;
protected:
  int  _frame;
};
  

#ifdef USE_HDF5
class AuxGridHDF5 : public AuxGridFetcher {

public:
  AuxGridHDF5(const char * fn)
  {
    std::string gtype;
    parse_HDF5_grid( fn, gtype, _indx );
    std::string af = fn;
    _hin = H5Fopen(af.substr(0,af.find_last_of(":")).c_str(),H5F_ACC_RDONLY, H5P_DEFAULT);
    if( _hin ==H5I_INVALID_HID )
      throw 1;

    ch5s_aux_grid info;
    ch5s_aux_grid_info( _hin, _indx, &info );
    _max_frame = info.time_steps;
  }
  ~AuxGridHDF5()
  {
    ch5_close( _hin );
  }
  /** load model from frame in timepoints
   *
   * \param frame to load
   * \return pointer to model
   */
  Model *GetModel(int frame)
  { 
    // check if we have the frame loaded, if so just return the current model
    if (frame == _frame) {
      return _model;
    }
 
    // ensure the frame requested is valid
    if ( frame >= _max_frame ) {
      std::cerr << "GetModel() request to invalid frame number" << std::endl;
      throw 3;
    }

    // delete previous model
    if (_model) {
      delete _model;
      _model = NULL;
    }

    // delete previous data
    if (_data) {
      delete _data;
      _data = NULL;
    }
    
    _model = new Model();
    _model->read_instance(_hin, _indx, frame, _data);
    
    // update internal frame number
    _frame = frame;
  
    return _model;
  }
  int  num_tm(){return _max_frame; }
  bool plottable(){ 
    return ch5s_aux_time_const_npt(_hin, _indx, num_tm(), 1)==1 ;
  }
  int  time_series( int v, float *&d ){
    d = new float[num_tm()];
    ch5s_aux_time_series( _hin, _indx, v, d );
    return num_tm();
  }
  DATA_TYPE *data(){return _data;}
  DATA_TYPE  data(size_t i){return _data[i];}
private:
  hid_t        _hin;
  unsigned int _indx;     //!< grid index
  int          _max_frame;
  float       *_data=NULL;
};

#endif

/** private internal helper function that indexes frames to file positions
    if a normal points file is given, treat as an aux grid of t-dimension 1 **/
class AuxGridIndexer : public AuxGridFetcher
{
public:

  /** constructor
   *
   * Indexes the associated file at construction
   *
   * \param filename to index (without extention)
   */
  AuxGridIndexer(const char * filename)
  {
    std::string base = filename;

    if (has_ext(filename, ".pts_t"))
      base.erase(base.size()-6,6); // remove extension
    else if (has_ext(filename, ".pts")) {
      _fake = true;
      base.erase(base.size()-4,4); // remove extension
      _surff = base+".surf";
    }

    std::string fin = _fake ? "" : "_t";
    _pts_in  = index(_vec_pts_pos,  base.c_str(), std::string(".pts")+fin);
    _elem_in = index(_vec_elem_pos, base.c_str(), std::string(".elem")+fin,
                                       _vec_pts_pos.size() );
    _max_tm  = std::max( _vec_pts_pos.size(), _vec_elem_pos.size() );

    if( gzopen( (base+".dat"+fin).c_str(), "r" ) != Z_NULL ){
      if( _fake )  {
        gzrewind(_pts_in);
        gzrewind(_elem_in);
        _data = new AuxDat<float>( (base+".dat").c_str(), -1, get_num(_pts_in), get_num(_elem_in) );
      }else
        _data = new AuxDat_t<float>( (base+".dat"+fin).c_str() );
    } else if( gzopen( (base+".igb").c_str(), "r" ) != Z_NULL ){
        _data = new AuxDatIGB<float>( (base+".igb").c_str() );
    }

    if( _data ) _max_tm  = std::max( (size_t)_data->num(), _max_tm )-1;

    if (!_pts_in) {
      std::cerr << "Failed to open .pts_t file" << std::endl;
      throw 2;
    }
    int mm = mismatch();
    if( mm ) {
      std::cerr << "Number of data and points do not match for time "<<mm-1<<" in auxiliary grid" << std::endl;
      throw 3;
    }
    seek(0);
  }

  /** destructor
   *
   * Free's memory allocated internally by the object and closes all open file
   * pointers.
   */
  ~AuxGridIndexer()
  {
    if (_model) {
      delete _model;
      _model = 0;
    }

    if (_data) {
      delete[] _data;
      _data = NULL;
    }

    gzclose(_pts_in);
    gzclose(_elem_in);
  }
  
  /** load model from frame in timepoints
   *
   * \param frame to load
   * \return pointer to model
   */
  Model *GetModel(int frame)
  { 
    // check if we have the frame loaded, if so just return the current model
    if (frame == _frame) {
      return _model;
    }
 
    // ensure the frame requested is valid
    if (frame > _max_tm) {
      std::cerr << "GetModel() request to invalid frame number" << std::endl;
      throw 3;
    }

    // delete previous model
    if (_model) {
      delete _model;
      _model = 0;
    }
      
    // set position in all files to frame
    seek(frame);

    // load model from file
    _model = new Model();
    _model->read_instance(_pts_in, _elem_in);
    if( _fake ) 
      _model->add_surface_from_surf( _surff, true );
  
    // update internal frame number
    _frame = frame;
  
    return _model;
  }

  /** get the number of time points indexed
   *
   * \return the number of available timepoints
   */
  int num_tm()
  {
    return _max_tm+1;
  }

  /** return true if all time instances have the same number of points
   */
  bool plottable()
  {
    if( !_data ) return false;

    seek(0);
    int numpts = get_num(_pts_in);
    for( int i=1; i<_vec_pts_pos.size(); i++ ) {
      seek(i);
      if( get_num(_pts_in) != numpts ) {
        seek(0);
        return false;
      }
    }
    seek(0);
    return true;
  }

  /** return a time series for a point
   *
   * \param v the vertex number
   * \param d the series (will be allocated)
   * \return the size of the vector
   */
  int time_series( int v, float *&d )
  {
    if(!_data) {
      d = NULL;
      return 0;
    }
    d = new float[_data->num()];
    for( int i=0; i<_data->num(); i++ ) 
      d[i] = _data->val(i,v);
    return _data->num();
  }
  
  DATA_TYPE* data(){ if(_data)return _data->buf(); else return NULL;}

  DATA_TYPE  data(size_t i){ if(_data)return _data->buf()[i]; else throw 1;}

private:  

  /** index a files frame positions and store in vector
   *
   * This function will clear the vector, get the number of frames stored in the file
   * and store the position in the file associated with the start of each frame for
   * later use. If the expected frame count is required and provided, then we will
   * throw an exception if a different number of frames is encountered. 
   * 
   * \param vecpos       vector of file positions to store when indexing
   * \param filename     without extention
   * \param extention    filename to attempt to index
   * \param match_frames number of frames we expect the file to have 
   *                     (-1=if not required to match)
   *
   * \note 1 frame will always match any number of frames supplied
   *
   * \return open file handle or 0 if file could not be opened
   */
  gzFile index(std::vector<z_off_t> & vecpos, std::string filename, 
                       std::string extention, int match_frames=-1)
  {
    try {
      // clear vector of positions (indexes)
      vecpos.clear();

      // open input file to index
      gzFile infile = openFile(filename.c_str(), extention.c_str());

      // read number of frames in file
      int frames = 1;
      if( has_ext(extention, "_t" ) )
        frames = get_num(infile);

      // validate the number of frames
      if((match_frames!=-1) && (match_frames!=frames && frames!=1 && match_frames!=1)) {
        std::cerr << "incorrect number of timepoints in " << extention << 
            " file" << std::endl;
        throw 1;
      }

      // iterate all frames in file
      for (int frame = 0; frame < frames; frame++) {
        vecpos.push_back(gztell(infile));

        int lines = get_num(infile);

        for (int line = 0; line < lines; line++) {
          get_line(infile);
        }
      }

      // return open file handle
      return infile;
    }
    catch(...) {
      return 0;
    }
  }

  /** verify that the number of points matches the number of data points for all times
   *
   * \returns 0 if things jive, o.w, 1 more than the offending time
   */
  int mismatch()
  {

    if( !_data )
      return 0;

    if( _fake ) return 0;

    for( int t=0; t<=_max_tm; t++ ){
      seek( t );
      if( _data->numpt() != get_num(_pts_in) )
        return t+1;
    }
    return 0;
  }

  /** set all files to position in file relating to frame
   *
   * \param frame to set all file positions at
   */
  void seek(int frame)
  {
    if( frame>_max_tm ) {
      std::cerr << "illegal frame specified!" << std::endl;
      throw 3;
    }

    if( _vec_pts_pos.size() > 1 )
      gzseek(_pts_in, _vec_pts_pos[frame], SEEK_SET);
    else
      gzseek(_pts_in, _vec_pts_pos[0], SEEK_SET);

    if (_elem_in) {
      if( _vec_elem_pos.size() > 1 )
        gzseek(_elem_in, _vec_elem_pos[frame], SEEK_SET);
      else
        gzseek(_elem_in, _vec_elem_pos[0], SEEK_SET);
    }

    if (_data) {
        _data->time(frame);
    }
  }

  long unsigned int _max_tm;            //!< maximum time
  gzFile _pts_in;                       //!< points backing file
  gzFile _elem_in;                      //!< element backing file
  std::vector<z_off_t> _vec_pts_pos;    //!< frame indexed vector of positions into points file
  std::vector<z_off_t> _vec_elem_pos;   //!< frame indexed vector of positions into element file
  bool _fake = false;                   //!< really an ordinary model
  std::string _surff;                   //!< name of surface file to use
  AuxDataBase<DATA_TYPE> *_data=NULL;   //!< data
};


/**************************************************************************/
/********************** AuxGrid functions below here **********************/
/**************************************************************************/

/** constructor 
 *
 * \param fn        file name
 * \param ag        auxilliary grid from which settings are copied
 * \param fake      a regular model, not a true aux grid?
 * \param t0        start time for data
 * \param dt        time increment for data
 *
 * \throw 1 if any error in input
 */
AuxGrid::AuxGrid( const char *fn, TBmeshWin *tbmw, AuxGrid *ag, bool fake, float t0, float dt ):_dt(dt),_t0(t0),mwtb(tbmw)
{
#ifdef USE_HDF5
  if( strstr( fn, ".datH5:" ) ) {
    _indexer = new AuxGridHDF5(fn);
  } else
#endif
  if( has_ext( fn, ".pts_t" ) || fake&&has_ext(fn,".pts")  )
    _indexer = new AuxGridIndexer(fn);
  else
    throw 1;
  
  char rp[4096];
  std::string fname = fn;
  auto colon=fname.find(":");
  if( colon == std::string::npos ) colon=fname.size();
  _absfile = realpath(fname.substr(0,colon).c_str(),rp);
  if( colon != std::string::npos ) _absfile += fname.substr(colon);

  if( ag ){
    _hilight = ag->_hilight;
    _hiVert  = ag->_hiVert;
    _clip    = ag->_clip;
    _display = ag->_display;
  }

  for( int i=0; i<maxobject; i++ ) {
    if( ag ) {
      _show[i]     = ag->_show[i]; 
      _datafied[i] = ag->_datafied[i]; 
      _size[i]     = ag->_size[i];
      _3D[i]       = ag->_3D[i];
      for( int j=0; j<4; j++ )
        _color[i][j] = ag->_color[i][j];
    } else {
      _show[i]     = true;
      _datafied[i] = false;
      _color[i][3] = 1;
      _3D[i]       = false;
    }
  }
  if( !ag ) {
      size(Vertex,  10);
      size(Cnnx,    10);
      size(SurfEle, 10);
      size(VolEle,  10);
      color( Vertex,  defPtColor );
      color( Cnnx,    defLineColor );
      color( SurfEle, defSurfColor );
      color( VolEle,  defVolColor );
  }

  _plottable = _indexer->plottable();

  if( _plottable ){
    if( _timeplot == NULL ) {
      _timeplot = new PlotWin("Aux Time Series", this, false);
    }
    _sz_ts = _indexer->time_series(0,_time_series);
    _timeplot->set_data( 0, _sz_ts, _time_series, 0, _dt, _t0 );
    std::string datfile = fn;
    auto pos=datfile.find_last_of( '.' );
    _timeplot->datafile( datfile.substr(0, pos) );
  }

  if( _indexer )
    _indexer->GetModel(0);

  _rd_surf.set_material(0, _mat_prop);
  _rd_vol.set_material(0, _mat_prop);

  _deadData = new DeadDataGUI( tbmw, &cs, VBO_Aux|VBO_Aux_Color ); 
  _deadData->window->label("Aux Dead Data");
  
  optimize_cs(tbmw->time());
}


/** draw the grid at a particular time 
 *
 * \param t        the time
 * \param redraw   reason for redraw
 * \param context  OpenGL context
 *
 */
void 
AuxGrid :: draw( int t, unsigned int redraw, void *context )
{
  static int old_t = -1;

  if( !_display || t<0 ) 
    return;

  if( t >= num_tm() ) t = num_tm()-1;

  if( _select_vtx ) {
    pick_vtx(context,t);
    old_t = -1;
    return;
  }

  _rd_surf.lineWidth = size(SurfEle);
  _rd_vol.lineWidth  = size(VolEle);

  bool cpv[6];
  if( !_clip ) clip_off( cpv );

  bool rebuff = old_t != t;
  rebuff = rebuff || redraw&(VBO_Aux|VBO_Aux_Color);

  if( rebuff ) {

    Model *m = _indexer->GetModel(t);
    m->pt.size(size(Vertex));
    m->pt.threeD(threeD(Vertex));
    m->_cnnx->threeD(threeD(Cnnx));
    m->_cnnx->size(size(Cnnx));

    _rd_pts.setup_render_data(context);
    _rd_lns.setup_render_data(context);
    _rd_surf.setup_render_data(context);
    _rd_vol.setup_render_data(context);
    _rd_lns.lineWidth  = size(Cnnx);
    _rd_pts.radius(size(Vertex));
    _rd_pts.threeD(threeD(Vertex));
    _rd_lns.threeD(threeD(Cnnx));
    _rd_pts.clear();
    _rd_lns.clear();
    _rd_surf.clear();
    _rd_vol.clear();

    fill_buff( context, m, t ); 
  }

  _rd_lns.mesh_render();
  _rd_surf.mesh_render(!_surf_fill);
  _rd_pts.mesh_render();
  _rd_vol.mesh_render(!_vol_fill);

  if( !_clip ) clip_on( cpv );

  old_t = t;
}


/**
 * @brief fill VBO buffer 
 *
 * @param context openGL context
 * @param m       model
 * @param t       time
 */
void
AuxGrid :: fill_buff( void *context, Model *m, int t )
{
  if( _autocol ) {
    optimize_cs(t);
  }

  if( _show[Vertex] ) {
    m->pt.buffer( _rd_pts, 0, m->pt.num()-1, color(Vertex), &cs, 
     (_indexer->data()!=NULL && _datafied[Vertex]) ? _indexer->data() : NULL, 1, NULL );
  }

  if( _hilight ) {
    GLfloat hicol[] = { 1, 0, 0, 1 };
    _rd_pts.radius( size(Vertex)+std::min(2.,0.2*size(Vertex)) );
    m->pt.buffer( _rd_pts, _hiVert, _hiVert, hicol, NULL, NULL, 1, NULL );
    _rd_pts.radius(size(Vertex));
    if( _timeplot && _timeplot->window->shown() )
      _timeplot->highlight(t);
  }

  if( _show[Cnnx] ) {
    m->_cnnx->buffer( _rd_lns, color(Cnnx), &cs,
          (_indexer->data() && _datafied[Cnnx]) ? _indexer->data() : NULL );
  }

  if( _show[SurfEle]  ) {
    bool trans =  (color(SurfEle)[3]<OPAQ_LIM) || cs.deadTrans();
    Surfaces* sf    = m->surface(0);
    sf->zsort(context, 1, trans);
    GLfloat *buff   = _rd_surf.buff_append( sf->numtri() );
    auto     colbuf = _rd_surf.colbuffer();
    DATA_TYPE *dp   = (_indexer->data() && _datafied[SurfEle]) ? _indexer->data() : NULL;
    char cmode = CMODE(false,false,false);
    _rd_surf.opaque(-1);
    if( !trans ) {
      sf->colour( color(SurfEle), &cs, dp, NULL, colbuf );
      sf->buffer( buff, cmode );
    } else {
      sf->buffer_opaque( OPAQ_LIM, color(SurfEle), &cs, dp, NULL, colbuf, buff, cmode );
      _rd_surf.opaque(_rd_surf.opaque()-sf->zl_sz());
      buff   = _rd_surf.transBuffer();
      colbuf = _rd_surf.transColBuffer();
      for( auto e = sf->zl_begin(); e != sf->zl_end(); e++ ) {
        sf->buffer_elem( e->i, buff, cmode );
        sf->colour_elem( e->i, color(SurfEle), &cs, dp, NULL, colbuf, cmode );
      }
    }
  }

  if( _show[VolEle] && m->_vol.size() ) {

    DATA_TYPE*  data = (_indexer->data() && _datafied[VolEle]) ? _indexer->data() : NULL;
    int nl[20][MAX_NUM_SURF_NODES+1];

    for( int i=0; i<m->number(VolEle); i++ ) {
      int      ntri = m->_vol[i]->surfaces( nl, true );
      GLfloat *buff = _rd_vol.buff_append( ntri );
      for( int tri=0; tri<ntri; tri++ ) {
        GLfloat norm[3], ab[3], ac[3];
        sub( m->pt[nl[tri][1]], m->pt[nl[tri][0]], ab );
        sub( m->pt[nl[tri][2]], m->pt[nl[tri][0]], ac );
        normalize( cross( ab, ac, norm ) );
        SHORTVEC(shnorm, norm[0], norm[1], norm[2]);
        for( int n=0; n<3; n++ )  {
          const GLfloat *c  = m->_vol[i]->colour( nl[tri][n], color(VolEle), data, &cs, NULL );
          buff             += _rd_vol.buff_vtx( buff, m->pt[nl[tri][n]], shnorm, c, 0 );
        }
      }
    }
  } 

  _rd_lns.update_nodalbuff();
  _rd_pts.update_nodalbuff();
  _rd_surf.update_nodalbuff();
  _rd_vol.opaque(color(VolEle)[3]<OPAQ_LIM?0:-1);
  _rd_vol.update_nodalbuff();
}


/** destructor */
AuxGrid::~AuxGrid()
{
  delete _indexer;
  _indexer = NULL;
  if( _timeplot && _timeplot->ag == this ){ _timeplot->ag = NULL; }
  if( _deadData ){ delete _deadData; }
}

void AuxGrid :: optimize_cs( int tm )
{
  if(_indexer->data()==NULL) return;

  int i;
  for( i=0; i<_indexer->_model->pt.num(); i++ ) 
    if( isfinite(_indexer->data(i) ) )
      break;
  if( i==_indexer->_model->pt.num() ) return;
  
  DATA_TYPE min=_indexer->data(i), max=_indexer->data(i);

  for( ; i<_indexer->_model->pt.num(); i++ ) {

    if( !isfinite(_indexer->data(i)) ) continue;

    if( _indexer->data(i)<min ) {
      min=_indexer->data(i);
    }

    if( _indexer->data(i)>max ) {
      max=_indexer->data(i);
    }

  }
  cs.calibrate( min, max );
}


void AuxGrid::color(Object_t o, GLfloat * r)
{
  for (int i=0; i<4; i++) {
    _color[o][i] = r[i];
  }
}


int AuxGrid::num_tm()
{
  return _indexer->num_tm();
}


/** show the plot of the time series
 *
 *  \param tm time currently displayed
 */
void AuxGrid::plot(int tm)
{
  if( !_plottable )
    return;
  _sz_ts = _indexer->time_series( _hiVert, _time_series );
  _timeplot->window->show();
  _timeplot->set_data( _hiVert, _sz_ts, _time_series, tm, _dt, _t0 );
  _timeplot->window->redraw();
} 


/** is data present on the grid
 */
bool AuxGrid::data()
{
  return _indexer->data() != NULL;
}


/** how many vertices at present
 */
int AuxGrid::num_vert()
{
  return _indexer->_model->pt.num();
}

/** specify a vertex to highlight
 *
 *  \param      n           vertex
 *  \param[out] val         data value at vertex
 *  \param      update_plot whether to update time series
 *
 * \return true if there is data
 * \post \p val contains the node data value
 */
bool AuxGrid :: highlight_vertex( int n, float &val, bool update_plot )
{
  if( n >= _indexer->_model->pt.num() ) return false;

  _hiVert = n;
  if( _indexer->data() ) {
    if( update_plot && _plottable && _timeplot->window->shown() ) {
      _sz_ts = _indexer->time_series( _hiVert, _time_series );
      _timeplot->set_data( _hiVert, _sz_ts, _time_series, 0, _dt, _t0 );
      if( _timeplot->graph->crvi_vis() )static_curve_info_cb(_timeplot->graph);
    }
    val = _indexer->data(n);
    return true;
  }
  return false;
}

/**
* @brief  extrac a time series
*
* @param n node index
* @param d pointer to series
*
* \post \par d will be allocated
*/
int
AuxGrid::time_series( int n, float *&d )
{
  return _indexer->time_series( n, d );
}


/** make sure plotter knows who is boss */
void
AuxGrid::update_plotter( void ) 
{
  if(_timeplot) _timeplot->ag = this;
}


/**
 * @brief draw the points to be selected
 */
    void 
AuxGrid::pick_vtx(void *context, int t)
{
  _select.setup_render_data(context);
  _select.clear();

  auto& pt = _indexer->_model->pt;

  _select.buff_append( pt.num() );
  for ( int i=0; i<pt.num(); i++ )
    _select.buff_vtx( i, pt[i], i );

  _select.update_nodalbuff();
  _select.mesh_render();

  if( _show[SurfEle] && _surf_fill && color(SurfEle)[3]>=OPAQ_LIM ) {
    Model *m = _indexer->GetModel(t);
    _rd_surf.setup_render_data(context);
    auto    colbuf      = _rd_surf.colbuffer();
    GLfloat blackOpq[4] = {0,0,0,1.};
    m->surface(0)->colour( blackOpq, NULL, NULL, NULL, colbuf );
    _rd_surf.opaque(-1);
    _rd_surf.update_col();
    _rd_surf.mesh_render(false);
  }
}

void 
AuxGrid::time_X( float a, float b ){
  if(_timeplot)_timeplot->graph->set_X(a,b);
}


/** return the domain of the time plot displayed 
 *
 * \param[output] a x left
 * \param[output] b x right
 *
 * \return true iff there is something plotted
 * */
bool
AuxGrid::time_get_X( double &a, double &b ){
  double c, d;
  if(_timeplot){
    _timeplot->graph->range(a,b,c,d);
    return true;
  } else
    return false;
}

