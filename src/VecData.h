/*
 * VecData class - stores and draws vector data
 *
 * It stores the geometry because it is usually desirable to display the
 * vector data at a lower density than scalar data
 */
#ifndef VECDATA_H
#define VECDATA_H

#include "Colourscale.h"

// a=b
template<class T>
inline T* assign( T *a, const T *b )
{
  return static_cast<T*>(memcpy( a, b, 3*sizeof(T) ));
}

// a=(b,c,d)
template<class T>
inline T* assign( T *a, double b, double c, double d )
{
  a[0] = b; a[1] = c; a[2] = d;
  return a;
}

// dot 2 vectors
template<class T>
inline T dot( const T *a, const T *b )
{
  return a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
}

/**
 * @brief  cross product : c = aXb
 *
 * @tparam T
 * @param a
 * @param b
 * @param c
 *
 * @return the product
 * \note \p c cannot be \p a or \p b
 */
template<class T>
inline T* cross( const T* a, const T* b, T* c )
{
  c[0] =  a[1]*b[2] - a[2]*b[1];
  c[1] = -a[0]*b[2] + a[2]*b[0];
  c[2] =  a[0]*b[1] - a[1]*b[0];
  return c;
}

template<class T>
inline T magnitude( const T* a )
{
  return sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
}


template<class T>
inline T mag2( const T* a )
{
  return a[0]*a[0] + a[1]*a[1] + a[2]*a[2];
}


template<class T>
inline T* normalize( T* a )
{
  T mag = magnitude(a);
  for ( int i=0; i<3; i++ )
    a[i] /= mag;
  return a;
}


template<class T>
inline T* add( const T* a, const T* b, T* c )
{
  for ( int i=0; i<3; i++ )
    c[i] = a[i]+b[i] ;
  return c;
}
template<class T>
inline T* vec_add( const T* a, const T* b, T* c )
{
  return add( a, b, c );
}

template<class T>
inline T* vec_addTo(  T* a, const T* b )
{
  return add( a, b, a );
}

template<class T>
inline T* sub( const T* a, const T* b, T* c )
{
  for ( int i=0; i<3; i++ )
    c[i] = a[i]-b[i] ;
  return c;
}


template<class T>
inline T* scale( T* a, float f )
{
  for ( int i=0; i<3; i++ )
    a[i] *= f;
  return a;
}

template<class T>
inline bool equal_pt( const T* a, const T* b,  T tol )
{
  return abs(a[0]-b[0])<tol && abs(a[1]-b[1])<tol && abs(a[2]-b[2])<tol;
}

#include "render_utils.h"
#ifdef USE_HDF5
#include "ch5/ch5.h"
#endif

enum DataType {Vector,Scalar,FixedVCdata};

class VecData
{
  public:
    VecData(const char*);
    ~VecData();
    int          maxtime(){ return numtm-1; }
    void         draw(int,float,unsigned int,void *);
    void         length( float length ){ _length=length; }
    float        length() const { return _length; }
    void         colourize();
    GLfloat*     colour(){ return _colour; }
    void         colour(float r,float g, float b,float a ){_colour[0]=r;_colour[1]=g;_colour[2]=b;_colour[3]=a;}
    void         display(bool a) {_disp = a; }
    void         length_det( DataType dt ){if(dt!=Scalar||sdata){_length_det=dt;
                                                               optimize_len();}}
    DataType     length_det(void ){return _length_det;}
    void         colour_det( DataType dt ){if(dt!=Scalar||sdata)
	                                          {_colour_det=dt; optimize_cs();} }
    DataType     colour_det(void ){return _colour_det;}
    void         ambient( float v ){ _rd.ambient = v; }
    void         diffuse( float v ){ _rd.diffuse = v; }
    void         specular( float v ){ _rd.specular = v; }
    void         auto_cs(bool a){ autocal=a; }
    void         optimize_cs();
    void         optimize_len(int ctm=-1);
    void         stride( int a ){ _stride=a; }
    void         start( int a ){ _start=a; }
    void         stoch_stride( bool a ){ _stoch = a; }
    void         heads( bool b ){ _draw_heads=b; _rd.as_strip(!b); }
    Colourscale *cs;			// colour scale for display
    VecData&     operator=(const VecData*);
    bool         have_scalar(){ return sdata; }
    int          size(void){ return numpt; }
    std::string       file(){ return _absfile; }
    void         max_length( float d ) { maxmag = d; }
    float        max_length() { return maxmag; }
    void         min_rel_sz(float a){_min_rel_sz=a;} 
    void         ratio(float a){_ratio = a;}
  private:
    int      numpt       = 0;		// number of spatial points
    int      numtm       = 0;		// number of time instances
    float*   pts         = NULL;	// locations
    float*   vdata       = NULL;    // vector data
    float*   sdata       = NULL;    // scalar data
    float    _length     = 1;		// length of vectors
    GLfloat  _colour[4]= {1., 0., 0., 1.};	// colour of vectors
    int      _numcol;       // number of colours
    bool     _disp       = true;	// whether or not to draw
    float    maxmag      = 1;		// maximum mag used for scaling vector length 
    bool     autocal     = false;	// autocalibrate each time
    DataType _length_det = Vector;	// what determines length
    DataType _colour_det = Vector;  // what determines colour
    float scalar_min,		        // scalar data extrema
    scalar_max;
    int      _last_tm    = 0;             // last time drawn
    int      _stride     = 1;
    int      _start      = 0;
    bool     _stoch      = false;  // true for stochastic stride
    bool     _3D         = true;
    bool     _draw_heads = true;   // draw arrow heads
    float    _ratio      = 10.;    // ratio of arrow length to stick width
    float    _min_rel_sz = 0.05;   // minimum relative arrow length to draw
    RenderLines _rd;
    std::string   _absfile;
#ifdef USE_HDF5
    void     read_vec_HDF5( const char *);
#endif
    void     read_vec_nonHDF5( const char *);
};


#endif
