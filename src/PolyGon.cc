#include "DrawingObjects.h"

const int PolyGon::_zero=0;

/** colour buffer with one Polygon 
 *
 *  \param p0       index of first point to draw
 *  \param c        colour to use if no data
 *  \param cs       colour scale
 *  \param data     data associated with elements if flat, else nodes (NULL for no data display)
 *  \param dataopac data opacity
 *  \param buffer   buffer entry in which to place data
 *  \param cmode    colouring mode
 *
 *  \return number of triangles put in draw buffer
 */
int PolyGon::bufcol( int p0, GLfloat *c, Colourscale* cs,
                          DATA_TYPE* data, dataOpac* dataopac,
                          GLubyte *buffer, char cmode, bool safe )
{
  int i=p0*4;
  if( safe ) {
    for ( int j=0; j<_ptsPerObj; j++ )
      if ( !_pt->vis(_node[i+j]) )
        return 0;
  }

  for( int n=0; n<_ptsPerObj-2; n++ ) 
    for( int j=0; j<3; j++ ) {
      GLubyte bcol[4];

      auto buff_off = buffer + (j+n*3)*4; 

      int k = j==2 ? 0 : n+1+j;

      int cnode = CM_COL( cmode, p0, _node[i], _node[i+k] );
      colour( cnode, c, data, cs, dataopac, bcol );  
      memcpy( buff_off, bcol, 4 );
    }
  return _ptsPerObj-2;
}


/** fill buffer with one Polygon 
 *
 *  \param p0       index of first point to draw
 *  \param nrml     if flat element normal, else vertex normals (NULL for none)
 *  \param buffer   buffer entry in which to place data
 *  \param surf     surface we are in 
 *  \param cmode    colouring mode
 *
 *  \return number of triangles put in draw buffer
 */
int PolyGon::buffer( int p0, const short_float* nrml, GLfloat *buffer, 
                          int surf, char cmode )
{
  GLubyte *nulptr= NULL;

  int i=p0*4;
  for ( int j=0; j<_ptsPerObj; j++ )
    if ( !_pt->vis(_node[i+j]) )
      return 0;

  for( int n=0; n<_ptsPerObj-2; n++ ) 
    for( int j=0; j<3; j++ ) {
      GLfloat *buff_off = buffer + (j+n*3)*VBO_ELEM_SZ; 

      int k = j==2 ? 0 : n+1+j;

      const short_float *n = nrml+((cmode&CM_FLAT)?0:_node[i+k]*3);
      buff_vtx( buff_off, nulptr, _pt->pt(_node[i+k]), n, nulptr, surf );
    }
  return _ptsPerObj-2;
}


void  
PolyGon::rev_order(int a)
{
  int *n = _node+3*a;

  for( int i=0; i<_ptsPerObj/2; i++ ) {
    int j=_ptsPerObj-i-1;
    int tmp=n[j];
    n[j] = n[i];
    n[i] = tmp;
  }
}
