#ifndef RENDER_FUNCS_H
#define RENDER_FUNCS_H

#define GLSL_VERSION "#version 410 core\n"
#define GLSL_PREC "\n"

#define FAUX_Z 0.001   // fake radius of rendered 2D point

// needed to encode #defines as std::strings for GLSL
#define XSTR(A) STR(A)
#define STR(A) #A 

//"#extension GL_EXT_clip_cull_distance : enable\n" 
#define SHADER_LAYOUT GLSL_VERSION                    \
    "layout (location = 0) in vec3  aPos;\n"          \
    "layout (location = 1) in vec3  aNormal;\n"       \
    "layout (location = 2) in int   matNo;\n"         \
    "layout (location = 3) in vec4  aColor;\n"        \
    "\n"

#define COMMON_VARS "uniform vec3 lightColor;\n" \
    "uniform vec3  lightDir;\n"                  \
    "uniform float ambientStrength;\n"           \
    "uniform float specularStrength;\n"          \
    "uniform float diffuseStrength;\n"           \
    "uniform int   backLit;\n"                   \
    "uniform float fresnelExp;\n"                \
    "\n"


#include "render_utils.h"
#include "legacyGL.h"

int generate_shader_program(const char* vertex_source, const char* fragment_source);

inline int get_default_lighting_shader()
{
  const char *vertexShaderSource =
    SHADER_LAYOUT
    "out float gl_ClipDistance[" XSTR(NUM_CP) "];\n"
    "\n"
    "out vec3  Pos;\n"
    "out vec3  FragPos;\n"
    "out vec3  Normal;\n"
    "out vec3  Color;\n"
    "out float Opaque;\n"
    "out vec4  matProps;\n"
    "\n"
    "uniform mat4 model;\n"
    "uniform mat4 view;\n"
    "uniform mat4 projection;\n"
    "uniform vec4 clipEqn[" XSTR(NUM_CP) "];\n"
    "uniform vec4 materialProps[" XSTR(MAX_NUM_SURFS) "];\n"
    "uniform int  use_clip[" XSTR(MAX_NUM_SURFS) "];\n"
    "\n"
    "void main()\n"
    "{\n"
    "  //FragPos  = vec3(model * vec4(aPos, 1.0));\n"
    "  FragPos  = aPos;\n"
    "  Normal   = aNormal;\n"
    "  Color    = aColor.xyz;\n"
    "  Opaque   = aColor.w;\n"
    "  matProps = materialProps[matNo];\n"
    "\n"
    "  //gl_Position = projection * view * vec4(FragPos, 1.0);\n"
    "  gl_Position = view * vec4(FragPos, 1.0);\n"
    "\n"
    "  for( int i=0; i<" XSTR(NUM_CP) "; i++ ){\n"
    "    if( (use_clip[matNo]&(1<<i)) > 0 )\n"
    "      gl_ClipDistance[i] = dot(vec4(aPos,1), clipEqn[i]);\n"
    "    else\n"
    "      gl_ClipDistance[i] = 1;\n"
    "  }\n"
    "}\n";

  const char *fragmentShaderSource =
    GLSL_VERSION
    GLSL_PREC
    "in vec3 Normal;\n"
    "in vec3 Color;\n"
    "in vec3 FragPos;\n"
    "in float Opaque;\n"
    "in vec4 matProps;\n"
    "layout(location=0) out vec4 fragColor;\n"
    COMMON_VARS
    "void main()\n"
    "{\n"
    "\n"
    "    // diffuse\n"
    "    vec3 norm      = normalize(Normal);\n"
    "    vec3 nlightDir = normalize(lightDir);\n"
    "    float ldot     = dot(norm, nlightDir);\n"
    "    float fresnel  = 1.;\n"
    "    if( Opaque<0.95 && fresnelExp!=0. && Opaque>0.02 ) {\n"
    "      fresnel = 1.-(pow(1.-abs(ldot), fresnelExp));\n"
    "    }\n"
    "    if( backLit == 0 ) \n"
    "      ldot = max(ldot, 0.);\n"
    "    else if( ldot<0.0 ) \n"
    "      ldot = -ldot*matProps[3];\n"
    "    float spec     = matProps[0] * specularStrength * pow(ldot, matProps[2]);\n"
    "    float diffuse  = matProps[1] * diffuseStrength * ldot;\n"
    "    float ambient  = ambientStrength;\n"
    "\n"
    "    vec3 result  = ( (ambient+diffuse)*Color*fresnel + spec ) * lightColor;\n"
    "    fragColor    = vec4(result, max(1.0-fresnel,Opaque));\n"
    "}\n";

  return generate_shader_program(vertexShaderSource, fragmentShaderSource);
}

inline int get_wireframe_shader()
{
  const char *vertexShaderSource =
    SHADER_LAYOUT
    "out float gl_ClipDistance[" XSTR(NUM_CP) "];\n"
    "out vec3 FragPos;\n"
    "out vec3 Normal;\n"
    "out vec3 lCoord;\n"
    "out vec3 Color;\n"
    "out float Opaque;\n"
    "\n"
    "uniform mat4 model;\n"
    "uniform mat4 view;\n"
    "uniform mat4 projection;\n"
    "uniform vec4 clipEqn[" XSTR(NUM_CP) "];\n"
    "uniform int  use_clip[" XSTR(MAX_NUM_SURFS) "];\n"
    "\n"
    "void main()\n"
    "{\n"
    "  Normal = aNormal;\n"
    "  Opaque   = aColor.w;\n"
    "\n"
    "  int m = (gl_VertexID % 3);\n"
    "\n"
    "  if(m == 0)\n"
    "    lCoord = vec3(1,0,0);\n"
    "  else if(m == 1) \n"
    "    lCoord = vec3(0,1,0);\n"
    "  else\n"
    "    lCoord = vec3(0,0,1);\n"
    "\n"
    "  Color = aColor.xyz;\n"
    "  //FragPos = vec3(model * vec4(aPos, 1.0));\n"
    "  FragPos = aPos;\n"
    "  gl_Position = view * vec4(FragPos, 1.0);\n"
    "  //gl_Position = projection * view * vec4(FragPos, 1.0);\n"
    "\n"
    "  for( int i=0; i<" XSTR(NUM_CP) "; i++ )\n"
    "    if( (use_clip[matNo]&(1<<i)) > 0 )\n"
    "      gl_ClipDistance[i] = dot(vec4(aPos,1), clipEqn[i]);\n"
    "    else\n"
    "      gl_ClipDistance[i] = 1.0;\n"
    "}\n";

  const char *fragmentShaderSource =
    GLSL_VERSION
    GLSL_PREC
    "out vec4 FragColor;\n"
    "in vec3 Normal;\n"
    "in vec3 Color;\n"
    "in vec3 FragPos;\n"
    "in vec3 lCoord;\n"
    "in float Opaque;\n"
    "layout(location=0) out vec4 fragColor;\n"
    COMMON_VARS
    "\n"
    "float edgeFactor() {\n"
    "  vec3 d = fwidth(lCoord);\n"
    "  vec3 a3 = smoothstep(vec3(0.0), d * 1.5, lCoord);\n"
    "  return min(min(a3.x, a3.y), a3.z);\n"
    "}\n"
    "\n"
    "void main()\n"
    "{\n"
    "  // ambient\n"
    "  vec3 ambient = lightColor;\n"
    "\n"
    "  // diffuse\n"
    "  vec3 norm = normalize(Normal);\n"
    "  float diff = max(dot(norm, lightDir), 0.0);\n"
    "  vec3 diffuse = diffuseStrength * diff * lightColor;\n"
    "\n"
    "  vec3 result = (ambient) * Color;\n"
    " //fragColor = mix(vec4(0,0,0,1), vec4(result, Opaque), edgeFactor());\n"
    " fragColor = vec4(result, Opaque);\n"
    "}\n";

  return generate_shader_program(vertexShaderSource, fragmentShaderSource);
}


// simulate lit sphere
inline int get_lit_point_shader()
{
  const char *vertexShaderSource =
    SHADER_LAYOUT
    "out float gl_ClipDistance[" XSTR(NUM_CP) "];\n"
    "\n"
    "out vec3  Pos;\n"
    "out vec3  FragPos;\n"
    "out vec3  Color;\n"
    "out float Opaque;\n"
    "out vec4  matProps;\n"
    "out float ptSz;\n"
    "\n"
    "uniform vec3 lightDir;\n"                  \
    "uniform mat4 model;\n"
    "uniform mat4 view;\n"
    "uniform mat4 projection;\n"
    "uniform vec4 clipEqn[" XSTR(NUM_CP) "];\n"
    "uniform vec4 materialProps[" XSTR(MAX_NUM_SURFS) "];\n"
    "\n"
    "void main()\n"
    "{\n"
    "  Color    = aColor.xyz;\n"
    "  Opaque   = aColor.w;\n"
    "\n"
    "  matProps = materialProps[matNo];\n"
    "\n"
    "  FragPos  = vec3(model * vec4(aPos, 1.0));\n"
    "  gl_Position = view * vec4(FragPos, 1.0);\n"
    "  ptSz     = gl_PointSize;\n"
    "\n"
    "  for( int i=0; i<" XSTR(NUM_CP) "; i++ ){\n"
    "    gl_ClipDistance[i] = dot(vec4(aPos,1), clipEqn[i]);\n"
    "  }\n"
    "}\n";

  const char *fragmentShaderSource =
    GLSL_VERSION
    GLSL_PREC
    "in vec3 Color;\n"
    "in vec3 FragPos;\n"
    "in float Opaque;\n"
    "in float ptSz;\n"
    "in vec4 matProps;\n"
    "layout(location=0) out vec4 fragColor;\n"
    "//layout (depth_less) out float gl_FragDepth;\n"
    COMMON_VARS
    "void main()\n"
    "{\n"
    "\n"
    "    // compute normal of fragment as if on sphere\n"
    "    vec2 coord = 2.0*(gl_PointCoord - vec2(0.5,0.5));\n"
    "    coord.x   *= -1.;\n"
    "    float l    = length(coord);\n"
    "    if (l >1.0)\n"
    "      discard;\n"
    "    float z      = sqrt(1.-l*l);\n"
    "    vec3  norm   = vec3( coord, z );\n"
    "    gl_FragDepth = gl_FragCoord.z - " XSTR(FAUX_Z) "*z;"
    "\n"
    "    float ldot   = dot(norm, lightDir);\n"
    "\n"
    "    float ambient = ambientStrength;\n"
    "    if( ldot > 0.0 ) {\n"
    "      float spec     = matProps[0] * specularStrength * pow(ldot, matProps[2]);\n"
    "      float diffuse  = matProps[1] * diffuseStrength * ldot;\n"
    "      vec3 result    = ( (ambient+diffuse)*Color + spec ) * lightColor;\n"
    "      fragColor      = vec4(result, Opaque);\n"
    "    } else {\n"
    "      fragColor      = vec4( ambient * Color * matProps[3], Opaque);\n"
    "    }\n"
    "}\n";

  return generate_shader_program(vertexShaderSource, fragmentShaderSource);
}


template <class V>
inline void set_zero_4x4_mat(V mat[4][4])
{
  mat[0][0] = V(0.0), mat[0][1] = V(0.0), mat[0][2] = V(0.0), mat[0][3] = V(0.0);
  mat[1][0] = V(0.0), mat[1][1] = V(0.0), mat[1][2] = V(0.0), mat[1][3] = V(0.0);
  mat[2][0] = V(0.0), mat[2][1] = V(0.0), mat[2][2] = V(0.0), mat[2][3] = V(0.0);
  mat[3][0] = V(0.0), mat[3][1] = V(0.0), mat[3][2] = V(0.0), mat[3][3] = V(0.0);
}

template <class V>
inline void set_identity_4x4_mat(V mat[4][4])
{
  mat[0][0] = V(1.0), mat[0][1] = V(0.0), mat[0][2] = V(0.0), mat[0][3] = V(0.0);
  mat[1][0] = V(0.0), mat[1][1] = V(1.0), mat[1][2] = V(0.0), mat[1][3] = V(0.0);
  mat[2][0] = V(0.0), mat[2][1] = V(0.0), mat[2][2] = V(1.0), mat[2][3] = V(0.0);
  mat[3][0] = V(0.0), mat[3][1] = V(0.0), mat[3][2] = V(0.0), mat[3][3] = V(1.0);
}

template <class V>
inline void set_CMS_translation_4x4_mat(V x, V y, V z, V mat[4][4])
{
  set_identity_4x4_mat(mat);
  mat[3][0] = x;
  mat[3][1] = y;
  mat[3][2] = z;
}

template <class V>
inline void set_CMS_scale_4x4_mat(V s, V mat[4][4])
{
  set_identity_4x4_mat(mat);
  mat[0][0] = s;
  mat[1][1] = s;
  mat[2][2] = s;
}

template <class V>
inline void set_RMS_translation_4x4_mat(V x, V y, V z, V mat[4][4])
{
  set_identity_4x4_mat(mat);
  mat[0][3] = x;
  mat[1][3] = y;
  mat[2][3] = z;
}

template <class V>
inline void transpose_4x4_mat(V mat[4][4])
{
  V m01 = mat[0][1], m02 = mat[0][2], m03 = mat[0][3];
  V m10 = mat[1][0], m12 = mat[1][2], m13 = mat[1][3];
  V m20 = mat[2][0], m21 = mat[2][1], m23 = mat[2][3];
  V m30 = mat[3][0], m31 = mat[3][1], m32 = mat[3][2];

  mat[0][1] = m10, mat[0][2] = m20, mat[0][3] = m30;
  mat[1][0] = m01, mat[1][2] = m21, mat[1][3] = m31;
  mat[2][0] = m02, mat[2][1] = m12, mat[2][3] = m32;
  mat[3][0] = m03, mat[3][1] = m13, mat[3][2] = m23;
}


inline void set_shader_int(int ID, const char* name, int value)
{
  glUniform1i(glGetUniformLocation(ID, name), value);
}

inline void set_shader_float(int ID, const char* name, float value)
{
  glUniform1f(glGetUniformLocation(ID, name), value);
}

inline void set_shader_vec2(int ID, const char* name, float value[2])
{
  glUniform2fv(glGetUniformLocation(ID, name), 1, &value[0]);
}

inline void set_shader_vec2(int ID, const char* name, float x, float y)
{
  glUniform2f(glGetUniformLocation(ID, name), x, y);
}

inline void set_shader_vec3(int ID, const char* name, float value[3])
{
  glUniform3fv(glGetUniformLocation(ID, name), 1, &value[0]);
}

inline void set_shader_vec3(int ID, const char* name, float x, float y, float z)
{
  glUniform3f(glGetUniformLocation(ID, name), x, y, z);
}

inline void set_shader_vec4(int ID, const char* name, float value[4])
{
  glUniform4fv(glGetUniformLocation(ID, name), 1, &value[0]);
}

inline void set_shader_vec4(int ID, const char* name, float x, float y, float z, float w)
{
  glUniform4f(glGetUniformLocation(ID, name), x, y, z, w);
}

inline void set_shader_mat2(int ID, const char* name, float mat[4])
{
  glUniformMatrix2fv(glGetUniformLocation(ID, name), 1, GL_FALSE, mat);
}

inline void set_shader_mat3(int ID, const char* name, float mat[9])
{
  glUniformMatrix3fv(glGetUniformLocation(ID, name), 1, GL_FALSE, mat);
}

inline void set_shader_mat4(int ID, const char* name, float mat[16])
{
  glUniformMatrix4fv(glGetUniformLocation(ID, name), 1, GL_FALSE, mat);
}

inline void set_shader_view_matrices(int ID, void *context)
{
  set_shader_mat4(ID, "view", ModelViewMat[context].m);
  set_shader_mat4(ID, "projection", ProjectionMat[context].m);
}


#endif
