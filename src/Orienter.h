#ifndef ORIENTER_H
#define ORIENTER_H

#include "Directionizer.h"
#include "ClipPlane.h"

class  Orienter : public Directionizer
{
    private:
        int           _cp; //current clipping plane
    public:
        Orienter(int x, int y, int w, int h, const char *l = 0);
        virtual ~Orienter(){}
        virtual void draw();
        void    cp(int c);
        int     cp(){return _cp; }
};

#endif
