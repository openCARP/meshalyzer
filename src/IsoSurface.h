#ifndef ISOSURFACE_H
#define ISOSURFACE_H

#include "Model.h"
#include <vector>

class IsoSurface : public Surfaces {
    public:
        IsoSurface(){ _index = 0;_p = new PPoint;
          _rd.back_lit=1;
          _fresh = true;
        }
        ~IsoSurface();
        void calc( Model *, DATA_TYPE *, double, std::vector<bool>&, int, double *branch=NULL  );
        void draw(bool,bool=false);
        double isoval(){ return _val; } 
        void clear(GLfloat *n =NULL); 
	    int tm() const { return _tm; }
        void saveAux( std::string, int pt_offset=0 );
        bool fresh(){return _fresh;}
        int  npts(void) {return _p->num();}
        using Surfaces::buffer;
        GLfloat* allocate_buffer(){count_tris(); _rd.buffer(numtri()); _rd.opaque(-1); 
                                                                  return _rd.buffer();} 
        GLfloat* buffer(){return _rd.buffer();} 
        void init_buff(void *c){_rd.setup_render_data(c);}
        void update_buff(){_rd.update_nodalbuff();}
        void render(){_fresh=false;_rd.mesh_render(false);}
        void translucent( bool tl ){ if(tl){_rd.opaque(0);}else{_rd.opaque(-1);}}
        void material( GLfloat *matprop ) {_rd.set_material(0,matprop);}
	
    private: // member variables
      double                 _val=0;        //!< data value of surface
      int                    _tm=-1;        //!< time when surface calculated
      bool                   _fresh = false; //!< surface calculated but never drawn
      RenderTris             _rd;           //!< VBO data
};

#endif

