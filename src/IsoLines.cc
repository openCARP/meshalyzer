#include "IsoLines.h"
#include <map>
#include "VecData.h"

#ifdef _OPENMP
#include <omp.h>
omp_lock_t isoline_lock;
bool       acl_init=false;
#endif

void
flip_cnnx( MultiPoint *c )
{
  int n0 = c->obj()[0];
  int n1 = c->obj()[1];
  int nn[2] = { n1, n0 };
  c->redefine( nn );
}

inline void 
use_node( int e, std::vector<bool>& bv, std::vector<int>&node, std::vector<MultiPoint*>&ele )
{
  bv[e]   = true;
  node.push_back( ele[e]->obj()[0] );
  node.push_back( ele[e]->obj()[1] );
}


/**
 * @brief combine lines
 *
 * @param ln line list
 * @param l0 first line
 * @param l1 second line
 * @param n  node list
 *
 * node list is rearranged and \p ln modified
 *
 * \return true iff lists modified
 */
bool
line_flipjoin( std::vector<std::pair<int,int>>&ln, int l0, int l1, std::vector<int>&n  )
{
  int l0s =  n[ln[l0].first*2];
  int l0e =  n[ln[l0].second*2+1];
  int l1s =  n[ln[l1].first*2];
  int l1e =  n[ln[l1].second*2+1];

  if( l0s!=l1s && l0e!=l1s && l0s!=l1e && l0e!=l1e ) return false;

  bool pre  = l0s==l1s || l0s==l1e;
  bool flip = l0s==l1s || l0e==l1e;

  size_t l0sz = 2*(ln[l0].second-ln[l0].first+1);
  size_t l1sz = 2*(ln[l1].second-ln[l1].first+1);
  int   *l0p  = n.data()+ln[l0].first*2;
  int   *l1p  = n.data()+ln[l1].first*2;
  
  //copy 
  int *tmp  = (int *)malloc( l1sz*sizeof(int) );
  if( flip ) {
    int *l2pe = n.data()+ln[l1].second*2+1;
    for( int i=0; i<l1sz; i++ )
      tmp[i] = *l2pe--;
  } else
    memcpy( tmp, l1p, l1sz*sizeof(int) );

  if(pre){
    memmove( l0p+l1sz, l0p, (l1p-l0p)*sizeof(int) );
    memcpy( l0p, tmp, l1sz*sizeof(int) );
  } else {
    memmove( l0p+l0sz+l1sz, l0p+l0sz, (l1p-l0p-l0sz)*sizeof(int) );
    memcpy( l0p+l0sz, tmp, l1sz*sizeof(int) );
  }

  ln.erase(ln.begin()+l1);
  ln[l0].second += l1sz/2;
  for( int i=l0+1; i<l1; i++ ) {
    ln[i].first  += l1sz/2;
    ln[i].second += l1sz/2;
  }
  return true;
}

/** determine if the branch cut passes through the element 
 *
 *  \param d   data values
 *  \param n   number of data
 *  \param min range min  
 *  \param max range max
 *  \param tol allowed difference
 *
 *  \return true iff the branch passes thorugh the element
 */
bool
cross_branch( DATA_TYPE *d, int n, double min, double max, double tol )
{
  float mine=d[0], maxe=d[0];

  for( int i=0; i<n; i++ ) {
    if( d[i] > maxe ) 
        maxe = d[i];

    if( d[i] < mine ) 
        mine = d[i];
  }
  if( maxe-mine > (1.-tol)*(max-min) )
    return true;

  return false;
}



/**
 * @brief calculate the isoline segments
 *
 * @param s          the surface
 * @param dat        data on nodes
 * @param restricted do not draw lines if the surface not visible
 * @param surfno     surface number
 *
 * @return  number of line segments added
 */
int 
IsoLine :: process( Surfaces *s, DATA_TYPE *dat, bool restricted, int surfno )
{
  int num_lines=0;
#ifdef _OPENMP
  if( !acl_init ) {
    omp_init_lock(&isoline_lock);
    acl_init = true;
  }
#endif

#pragma omp parallel for 
  for( int i=0; i<_nl; i++ ){

    PPoint    lpts;
    PPoint   *lpp = &lpts;
    EdgePtMap epm;
    std::vector<MultiPoint *> val_poly;
    double val = _nl==1? _v0: _v0 + i*(_v1-_v0)/(float)(_nl-1.);

    for( int j=0; j<s->num(); j++ ) {
      if( restricted && !s->visible() ) continue;
      int npoly;
      MultiPoint **lpoly = s->ele(j)->isosurf( dat, val, npoly, lpp, epm );
      if( npoly && _branch ) {
        const int *nodes= s->ele(j)->obj();
        DATA_TYPE edat[MAX_NUM_SURF_NODES];
        for( int i=0; i< s->ele(j)->ptsPerObj(); i++ ){
          edat[i] = dat[nodes[i]];
        }
        if(cross_branch(edat,s->ele(j)->ptsPerObj(), _branch_range[0], _branch_range[1], _branch_tol))
          continue;
      }
      for( int k=0; k<npoly; k++ ) {
        val_poly.push_back(lpoly[k]);
#pragma omp atomic
        num_lines++;
      }
      delete[] lpoly;
    }
    if( val_poly.size() ) {
      make_lines( val_poly, lpts, val, surfno );
      for( auto p : val_poly ) delete p;
    }
  }
  return num_lines;
}


/**
 * @brief for a surface, determine the isolines
 *
 * @param s   surface
 * @param dat data
 *
 * @return number of connections added
 *
 * \post for each element i, \p _val[i] is the isovalue
 */
int 
IsoLine :: process( CutSurfaces *s, DATA_TYPE *dat )
{
  int num_lines=0;
#ifdef _OPENMP
  if( !acl_init ) {
    omp_init_lock(&isoline_lock);
    acl_init = true;
  }
#endif

#pragma omp parallel for 
  for( int i=0; i<_nl; i++ ){
    double              val = _nl==1? _v0: _v0 + i*(_v1-_v0)/(float)(_nl-1.);
    std::vector<MultiPoint*> val_poly;
    PPoint              lpts;
    EdgePtMap           epm;
    
    for( int j=0; j<s->num(); j++ ) {

      DATA_TYPE idata[s->ele(j)->ptsPerObj()];
      for ( int v=0; v<s->ele(j)->ptsPerObj(); v++ ) {
        if( _branch )
            idata[v] = s->interpolate( j, dat, v, _branch_range );
        else
            idata[v] = s->interpolate( j, dat, v );
      }
      int npoly;
      MultiPoint **lpoly = s->ele(j)->isosurf( idata, val, npoly, &lpts, epm, false );
      if( !npoly ||
          (_branch && cross_branch(idata,s->ele(j)->ptsPerObj(), _branch_range[0], _branch_range[1], _branch_tol)) )
          continue;
      for( int k=0; k<npoly; k++ ) {
        val_poly.push_back(lpoly[k]);
#pragma omp atomic
        num_lines++;
      }
      delete[] lpoly;
    }
    if( val_poly.size() ) {
      make_lines( val_poly, lpts, val );
      for( auto p : val_poly )
        delete p;
    }
  }
  return num_lines;
}


/**
 * @brief globalize points by checking if point already exists and
 *        try to join segments into lines
 *
 * @param elist     element to check
 * @param lpts      points defining elements in local numbering
 * @param val       value of isoline
 * @param sno       surfno
 *
 * \post \p _cnnx and \p _pts are updated
 *
 * \note this routine is written to use a lock to be OMP safe
 */
void
IsoLine::make_lines( std::vector<MultiPoint*>&elist, PPoint &lpts, float val, int sno )
{
  int extra_dup=0;

  // identify nodes which are really close to each other
  std::vector<int> o2n(lpts.num());
  for( int i=0; i<lpts.num(); i++ ) 
    o2n[i] = i;
  for( int i=0; i<lpts.num(); i++ ) {
    if( o2n[i] != i ) continue;
    for( int j=i+1; j<lpts.num(); j++ ) {
      if( o2n[j] != j ) continue;
      if( equal_pt( lpts[i], lpts[j], _pt_tol ) )  {
        o2n[j] = i;
        extra_dup++;
      }
    }
  }

  // renumber nodes in elements, removing elements which are very short
  for( auto e=elist.begin(); e != elist.end(); ) {
    int lnode0 = (*e)->obj()[0];
    int lnode1 = (*e)->obj()[1];
    int nn[2] = { o2n[lnode0], o2n[lnode1] };
    if( nn[0] == nn[1] ) {           // delete zero length segments
      delete *e;
      e = elist.erase(e);
    } else {
      (*e)->redefine( nn );
      ++e;
    }
  }

  std::vector<std::pair<int,int>> lines;
  std::vector<int>  nodes;

  if( !_3D ) {
    nodes.resize(elist.size()*2);
    auto ni = nodes.begin();
    for( auto e=elist.begin(); e != elist.end(); e++ ) {
      *ni++ = (*e)->obj()[0];
      *ni++ = (*e)->obj()[1];
    }
  } else {
    // everything below here is to form lines which is important in 3D
    _lined = true;

    // find previous and next connections
    std::map<MultiPoint*,int> next;
    std::map<MultiPoint*,int> prev;
    std::vector<bool>fixed(elist.size(),false); //!< is orientation fixed?
    for( int e=0; e<elist.size(); e++ ) {
      auto *ele = elist[e];
      int n0     = ele->obj()[0];
      int n1     = ele->obj()[1];
      bool fprev = prev.count(ele)==1;
      bool fnext = next.count(ele)==1;
      fixed[e]   = true;
      if( fnext && fprev ) continue;
      for( int te=e+1; te<elist.size(); te++ ) {
        auto tele = elist[te];
        if( !fnext && tele->obj()[0]==n1 && !prev.count(tele) ) {
          next[ele]  = te;
          prev[tele] = e;
          fnext=true;
          fixed[te] = true;
          if( fnext && fprev ) break;
          continue;
        }
        if( !fprev && tele->obj()[1]==n0 && !next.count(tele) ) {
          prev[ele]  = te;
          next[tele] = e;
          fprev=true;
          fixed[te] = true;
          if( fnext && fprev ) break;
          continue;
        }
        if( !fnext && tele->obj()[1]==n1 && !fixed[te] && !prev.count(tele)) {
          flip_cnnx( tele );
          next[ele]  = te;
          prev[tele] = e;
          fnext=true;
          fixed[te] = true;
          if( fnext && fprev ) break;
          continue;
        }
        if( !fprev && tele->obj()[0]==n0 && !fixed[te] && !next.count(tele) ) {
          flip_cnnx( tele );
          prev[ele]  = te;
          next[tele] = e;
          fprev=true;
          fixed[te] = true;
          if( fnext && fprev ) break;
          continue;
        }
      }
    }

    // now organize into lines
    std::vector<bool> used( elist.size(), false );
    int          lcnnx = 0;
    for( int e=0; e<elist.size(); e++ ) {
      if( !used[e] && next.count(elist[e]) && !prev.count(elist[e]) ) {
        int fcnnx = lcnnx;
        use_node( e, used, nodes, elist );
        lcnnx++;
        int nele  = next[elist[e]];
        while( next.find(elist[nele]) != next.end() ){
          use_node( nele, used, nodes, elist );
          lcnnx++;
          nele       = next[elist[nele]];
        }
        use_node( nele, used, nodes, elist );
        lines.push_back(std::make_pair(fcnnx, lcnnx));
        lcnnx++;
      }
    }

    // join lines if possible
    for( int i=0; i+1<lines.size(); i++ )
      for( int j=i+1; j<lines.size();j++ )
        if( line_flipjoin(lines, i, j, nodes) ) {
          i = -1;
          break;
        }

    // connections not in any line
    for( int e=0; e<elist.size(); e++ ) 
      if( !used[e] ) {
        nodes.push_back( elist[e]->obj()[0] );
        nodes.push_back( elist[e]->obj()[1] );
      }
  }

  // append new points on to old, renumbering them
#ifdef _OPENMP
  omp_set_lock(&isoline_lock);
#endif
  for( auto l : lines ) 
    _cnnx->add_line( l.first+_cnnx->num(), l.second+_cnnx->num() );
  int np = _pts.num();
  for( auto &n : nodes ) n+= np;
  _pts.add( lpts.pt(), lpts.num() ); 
  _val.resize( _pts.num(), val );
  int c0 = _cnnx->num();
  _cnnx->add( elist.size(), nodes.data() );
  _cnnx_surf.insert( {sno,std::make_pair(c0,_cnnx->num()-1)} );
#ifdef _OPENMP
  omp_unset_lock(&isoline_lock);
#endif
}


IsoLine::~IsoLine()
{
  delete _cnnx;
}


void IsoLine::buffer_lines( Colourscale *cs, GLfloat size )
{
  _rd.threeD( _3D );
  _rd.lineWidth = size;
  std::vector<GLushort> reg(_cnnx->num());
  for( auto m = _cnnx_surf.begin(); m != _cnnx_surf.end(); m++ ) 
    for( int c=m->second.first; c<=m->second.second; c++ )
      reg[c] = m->first;
  _cnnx->buffer( _rd, _color, cs, _val.data(), NULL, 0, reg.data() ); 
  _rd.update_nodalbuff();
}


// save as an Aux grid
void IsoLine::auxMesh( const char *fname )
{ 
  std::string basename = fname;
  size_t pos = basename.find( ".pts_t");
  if( pos == basename.length()-6 )
    basename.erase( pos );

  // count the number of points
  
  std::string ptfile  = basename+".pts_t";
  std::string datfile = basename+".dat_t";
  FILE *pout = fopen( ptfile.c_str(), "w" );
  FILE *dout = fopen( datfile.c_str(), "w" );
  fprintf( dout, "1\n%d\n", _pts.num() );
  fprintf( pout, "1\n%d\n", _pts.num() );
  for( int i=0; i<_pts.num(); i++ ) {
    const GLfloat *p = _pts[i];
    fprintf( pout, "%f %f %f\n", p[0], p[1], p[2] );
    fprintf( dout," %f\n", _val[i] );
  }
  fclose( pout );
  fclose( dout );

  std::string elemfile = basename+".elem_t";
  FILE *eout = fopen( elemfile.c_str(), "w" );
  fprintf( eout, "1\n%d\n", (int) _cnnx->num() );
  for( int i=0; i<_cnnx->num(); i++ ) {
    fprintf( eout, "Ln" );
    const int *np = _cnnx->obj(i);
    fprintf( eout," %d",   np[0] );
    fprintf( eout," %d\n", np[1] );
  }
  fclose( eout );

}

void 
IsoLine::clear()
{ 
  _rd.clear();
  _cnnx_surf.clear();
}


IsoLine &
IsoLine::operator +=(IsoLine &iso)
{
  if( _3D == iso._3D ) _rd += iso._rd;
  return *this;
}
