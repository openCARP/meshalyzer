#include "Surfaces.h"
#include <string.h>
#include "VecData.h"
#include <algorithm>
#include <vector>
#include <iterator>
#include <set>
#include "legacyGL.h"




/** find the z depth of a point (assume w=1)
 *
 * \param m row of the ModelViewProjection 
 * \param v vertex (x,y,z)
 *
 * \return the z depth buffer coordinate
 */
inline GLfloat 
z_proj( const GLfloat* m, const GLfloat *v )
{
  GLfloat z=m[3];
  for( int i=0; i<3; i++ ) z += m[i]*v[i];
  return z;
}


/** determine the data pointer offset
 *
 * \param cmode  coloring mode
 * \param _sele  source element
 * \param ele    element index
 *
 * \return offset
 */
inline int 
dp_offset( char cmode, int ele, std::vector<long> &sele )
{
  if( cmode&CM_ELEM  )
    return sele[ele];
  else if( cmode&CM_COL_ELEM )
    return ele;
  else
    return 0;
}


/**
 * @brief  set the material colour properties
 */
void
Surfaces :: set_material( )
{
  GLfloat diffuseb[4], specularb[4];

  CSET( diffuseb,  _diffuse[0]*_backlight,  _diffuse[3]  );
  CSET( specularb, _specular[0]*_backlight, _specular[3] );
}


void Surfaces :: fillcolor( float r, float g, float b, float a )
{
  _fillcolor[0] = r;
  _fillcolor[1] = g;
  _fillcolor[2] = b;
  _fillcolor[3] = a;
}


//! set outline colour
void Surfaces :: outlinecolor( float r, float g, float b, float a )
{
  _outlinecolor[0] = r;
  _outlinecolor[1] = g;
  _outlinecolor[2] = b;
  _outlinecolor[3] = a;
}


/** set the vertex normals for the surface
 *
 * \param pt all points
 *
 */
void Surfaces::determine_vert_norms( PPoint& pt )
{
  std::vector<bool> has_norm(pt.num());  // if elements attached to node
  _vert.clear();
  std::vector<GLfloat> tvn(pt.num()*3, 0.);

  const GLfloat* n;
  for ( auto ele : _ele ) {
    if ( (n=ele->nrml(0)) == NULL ) continue;
    const int *pnt = ele->obj();
    for ( int j=0; j<ele->ptsPerObj(); j++ ) {
      vec_addTo( tvn.data()+3*pnt[j], n );
      has_norm[pnt[j]] = true;
    }
  }
  // count \# nodes in surface
  short_float *shvn = (short_float*)calloc( pt.num()*3, sizeof(short_float) );
  for ( int i=0; i<pt.num(); i++ ) {
    if ( has_norm[i] ) {
      _vert.insert(i);
      SHORT_VCP( shvn+i*3, normalize(tvn.data()+i*3) );      
    }
  }
  free(_vertnorm);
  _vertnorm = shvn;
}


/**
 * @brief sort elements by z depth
 *
 * @param stride element drawing stride
 * @param sort   do we really sort?
 */
void Surfaces::zsort( void *context, int stride, bool sort )
{
  _zlist.resize( (_ele.size()+stride-1)/stride );

  if( sort ) {
    // build modelview projection matrix - only compute row to determine z
    GLfloat mvp[4]{};
    GLfloat *proj=ProjectionMat[context].m, *modview=ModelViewMat[context].m;
    for( int i=0; i<4; i++ )
      for(int j=0; j<4; j++ )
        mvp[i] += modview[2+j*4]*proj[4*i+j]; // column major order

    if( memcmp(mvp, _oldmvp, sizeof(mvp) ) || stride != _oldstride) {
      memcpy( _oldmvp, mvp, sizeof(mvp) );
    }
#pragma omp parallel for 
    for ( int i=0; i<_ele.size(); i+=stride ){
      const PPoint *pts = _ele[i]->pt();
      _zlist[i] = {i,0,_index};
      const int *nn     = _ele[i]->obj();
      for( int k=0; k<3; k++ )
        _zlist[i].z += z_proj(mvp,pts->pt(nn[k]));
    }
    std::sort( _zlist.begin(), _zlist.end(),[](const vtx_z a, const vtx_z b){return a.z<b.z;} );

  } else {
    for ( int i=0; i<_ele.size(); i+=stride )
      _zlist[i/stride] = { i, 0, _index };
  }
  _oldstride = stride;
}


/**
 * @brief determine element data by averaging the vertex data
 *
 * @param ptdata data on vertices
 *
 * @return the data for the elements
 */
DATA_TYPE * 
Surfaces::to_elem(DATA_TYPE *ptdata)
{
  _eledat.assign(_ele.size(),0.);
#pragma omp parallel for
  for( int e=0; e<_ele.size(); e++ ) {
    const int *n = _ele[e]->obj();
    for( int j=0; j<_ele[e]->ptsPerObj(); j++ )
      _eledat[e] += ptdata[n[j]];
    _eledat[e] /= (float)_ele[e]->ptsPerObj();
  }
  return _eledat.data();
}


 /**
 * @brief  Colour by region assuming opaque
 *
 * @param reg  regions
 * @param buf  VBO buffer
 */
void
Surfaces::buffer_reg( RRegion **reg, GLfloat *&buf )
{
  for ( int i=0; i<_ele.size(); i++ ) {
    GLfloat *col = reg[_reg[i]]->get_color(VolEle);
    buf += _ele[i]->buffer( 0, _vertnorm, buf, _index, CM_ELEM )*VBO_ELEM_SZ*3;
  }
  _zlist.clear();
}

 /**
 * @brief  Colour volume elements by region assuming opaque
 *
 * @param reg  regions
 * @param buf  colour VBO
 *
 * @post \p buf points one past last element colour
 */
void
Surfaces::colour_reg( RRegion **reg, GLubyte *&buf )
{
  for ( int i=0; i<_ele.size(); i++ ) {
    GLfloat *col = reg[_reg[i]]->get_color(VolEle);
    GLubyte bcol[4];
    memcpy( buf, float2ubyte( col, bcol, 255 ), 4 );
    buf += 4;
  }
}

/** buffer the surface
 *  \param fill     fill colour
 *  \param cs       colour scale
 *  \param dat      if flat, element data, else nodal data (NULL for nodata display)
 *  \param dataopac data opacity
 *  \param buffer   buffer in which to place data
 *  \param cmode    colouring mode
 *
 *  \pre  the elements need to have been sorted by calling ::zsort()
 *  \post the buffer reference points just past the entry added
 *  \post the sorted list, \p _list, is cleared
 */
void Surfaces::buffer( GLfloat *&buffer, char cmode )
{
  for ( auto &a :_zlist )
    buffer_elem( a.i, buffer, cmode );
  _zlist.clear();
}


/** fill the surface colour buffer
 *
 *  \param fill     fill colour
 *  \param cs       colour scale
 *  \param dat      if flat, element data, else nodal data (NULL for nodata display)
 *  \param dataopac data opacity
 *  \param buffer   buffer in which to place data
 *  \param cmode    colouring mode
 *
 *  \pre  the elements need to have been sorted by calling ::sort()
 *  \post the buffer reference points just past the entry added
 */
void Surfaces::colour( GLfloat *fill, Colourscale *cs, DATA_TYPE *dat,
                     dataOpac* dataopac, GLubyte *&buffer, char cmode )
{
  for( int a=0; a<num(); a++) 
    colour_elem( a, fill, cs, dat, dataopac, buffer, cmode );
}


/** buffer colour for opaque surface elements and optionally the geometry
 *
 *  \param opaque        transparency limit for opaque
 *  \param fill          fill colour
 *  \param cs            colour scale
 *  \param dat           nodal data (NULL for nodata display)
 *  \param dataopac      data opacity
 *  \param colbuf[inout] buffer in which to place colour data
 *  \param buffer[inout] buffer in which to place geometry data, can be NULL
 *  \param cmode         colouring mode
 *
 *  \post the buffer references points just past the last entry added
 *  \post opaque elements are removed from \p _zlist
 */
void Surfaces::buffer_opaque( float opaque, GLfloat *fill, Colourscale *cs, DATA_TYPE *dat,
                     dataOpac* dataopac, GLubyte *& colbuf, GLfloat *&buffer, char cmode )
{
  bool deadtrans  = cs->deadTrans();
  bool deadopaque = cs->deadOpaque();
  bool dataopacON = dataopac ? dataopac->on() : false;

  if( (!dat && fill[3]<opaque) ||
      ( dat && fill[3]<opaque && !deadopaque ) )
      return;

  std::set<int> solid;

  for ( int j=0; j<_zlist.size(); j++ ) {
    bool      eleopac = true;
    vtx_z    &a       = _zlist[j];
    if( !dat ) 
      eleopac = fill[3]>=opaque;
    else {
      SurfaceElement *s  = _ele[a.i];
      const int      *n  = s->obj();
      DATA_TYPE      *dp = dat + dp_offset( cmode, a.i, _sele );
      int loopcnt        = cmode&(CM_FLAT|CM_ELEM|CM_COL_ELEM) ? 1 : s->ptsPerObj();
      for( int i = 0; i<loopcnt; i++ ) {
        DATA_TYPE datum = dp[loopcnt==1 ? 0 : n[i]]; 
        if( (dataopacON && dataopac->alpha(datum)<opaque) ||
            (deadtrans   && cs->isDead(datum)           )  ){
          eleopac = false;
          break;
        }
      }
    }
    if( eleopac ) {
      solid.insert(j);
      colour_elem( a.i, fill, cs, dat, dataopac, colbuf, cmode );
      if( buffer ) buffer_elem( a.i, buffer, cmode );
    }
  }
  for( auto i=solid.rbegin(); i != solid.rend(); i++ )
    _zlist.erase(_zlist.begin()+(*i));
}


/** draw an element in the surface
 * \param  ele      the element index
 *  \param fill     fill colour
 *  \param cs       colour scale
 *  \param dat      nodal or elemental data (NULL for nodata display)
 *  \param dataopac data opacity
 *  \param buf[out] buffer to fill
 *  \param cmode    colour mode
 *
 *  \pre the elements need to have been sorted by calling ::sort()
 *  \post \p buf is advanced by the number of bytes written
 */
void Surfaces::colour_elem( int ele, GLfloat *fill, Colourscale *cs, DATA_TYPE *dat,
                            dataOpac* dataopac, GLubyte *&buf, char cmode )
{
  if( dat ) dat += dp_offset( cmode, ele, _sele );
  buf += _ele[ele]->bufcol( 0, fill, cs, dat, dataopac, buf, cmode )*VBO_RGBA_SZ*3;
}


/** draw an element in the surface
 *  \param ele      the element index
 *  \param buf[out] buffer to fill
 *  \param cmode    colour mode
 *
 *  \pre the elements need to have been sorted by calling ::sort()
 *  \post \p buf is advanced by the number of floats written
 */
void Surfaces::buffer_elem( int ele, GLfloat *&buf, char cmode )
{
  const short_float *nrml = (cmode&CM_FLAT)?_ele[ele]->sh_nrml():_vertnorm;
  buf += _ele[ele]->buffer( 0, nrml, buf, _index, cmode )*VBO_ELEM_SZ*3;
}


/** redraw elements through which the branch cut passes with flat shading 
 *
 * \param range  range of cut
 * \param data   data to display
 * \param cs     colour scale
 * \param stride output stride
 * \param opac   data opacity    
 */
void 
Surfaces ::correct_branch_elements( GLdouble *range, DATA_TYPE *data,
                         Colourscale *cs, int stride, dataOpac *opac )
{
#define BRANCH_TOL  0.2
  bool cross_branch( DATA_TYPE *d, int n, double min, double max, double tol );

  DATA_TYPE d[MAX_NUM_SURF_NODES*2];
  for ( int i=0; i<_ele.size(); i+=stride ) {
    const int *n = _ele[i]->obj();
    for( int j=0; j<_ele[i]->ptsPerObj(); j++ )
      d[j] = data[n[j]];
    if( cross_branch( d, _ele[i]->ptsPerObj(), range[0], range[1], BRANCH_TOL ) )
      assert(0);
  }
}


/** flip the normals
 */
void Surfaces :: flip_norms()
{
  unsigned char shift = sizeof(_vertnorm[0])*8-1;
  for( size_t i=0; i<_p->num()*3; i++ )
    _vertnorm[i] ^= 1U << shift;
}


/** write the surface to a file 
 * \param of output stream
 */
void
Surfaces :: to_file( std::ofstream &of )
{
    of << num() << "  " << label() << std::endl;
 
    for( auto &e : _ele ) {
      const int* n=e->obj();
      if( e->ptsPerObj()==3 ) 
        of << "Tr";
      else
        of << "Qd";
      for( int k=0; k< e->ptsPerObj(); k++ )
        of << " " << n[k];
      of << std::endl;
    }  
}


/** count the number of equivalent triangles 
 *
 *  \post _numtri is updated
 */
void
Surfaces :: count_tris()
{
  _numtri=0;
  for( auto &e: _ele )
    _numtri += e->ntris();
}

/** copy constructor */
Surfaces::Surfaces( Surfaces &s ){ 
  memcpy( this, &s, sizeof(Surfaces));
  _vertnorm=(short_float*)malloc(_p->num()*3); 
  memcpy( _vertnorm, s._vertnorm, _p->num()*3*sizeof(short_float)); 
  _vert  = s._vert;
  _ele   = s._ele;
  _sele  = s._sele;
  _label = s._label;
  _zlist = s._zlist;
}


/**
 * @brief Assign surface elements to a region
 *
 * @param reg regions
 * @param n   number of regions
 */
void
Surfaces::assign_reg( RRegion **reg, int n ){
  
  _reg.resize(_ele.size(),-1);
#pragma omp parallel for
  for( int i=0; i<_ele.size(); i++ ) {
    int r;
    for( r=0; r<n; r++ ) {
      int j;
      const int* o=_ele[i]->obj();
      for( j=j<_ele[i]->ptsPerObj()-1; j>=0; j-- )
        if( !reg[r]->pt_member(o[j]) )
          break;
      if( j<0 ) {
        _reg[i]=r;
        break;
      }
    }
  }
}

