#include "CutSurfaces.h"
#include <string.h>
#include "VecData.h"
#include <stdlib.h>

const int ADD_INC = 1000;

CutSurfaces::CutSurfaces(GLfloat *n) 
{
  fillcolor( 1., 0, 0.1 );
  outlinecolor( 0.125, 0.8, 0.7 );
  memcpy( _norm, n, 3*sizeof(GLfloat) );
  for( int i=0; i<3; i++ )
    _shnorm[i] = shortFromFloat( _norm[i] );
  _rd.set_material( 0, (const GLfloat[]){0,0,0,1} );
  _rd.ambient = 1.;
  _rd.back_lit = 1.;
}


/** add a Surface element to the surface
 *
 *  \param se   the element
 *  \param ni   list for data interpolation
 *  \param sele element being cut
 *
 *  \pre each element pointer points to a single element and not a list
 *
 *  \note points are not shared between elements
 */
void
CutSurfaces :: addEle( SurfaceElement *se, Interpolator<DATA_TYPE> *ni, int sele )
{
  _ele.push_back(se);
  _ptarr.push_back(const_cast<GLfloat *>(const_cast<PPoint*>(se->pt())->pt()));
  _interp.push_back(ni);
  _sele.push_back(sele);
}


/** assume that since everything is temporary, it will be our perogative
 * to destroy it all
 */
CutSurfaces::~CutSurfaces()
{
  for( auto &e : _ele )
    delete e;
  for( auto &p : _ptarr )
    delete p;
  for( auto &i : _interp )
    delete[] i;
}


void 
CutSurfaces::clear( GLfloat *n )
{
  if ( !_ele.empty() ) 
    for ( int i=0; i<_ele.size(); i++ ) {
      delete _ele[i];
      delete[] _interp[i];
    }
  _ele.clear();
  _sele.clear();
  _ptarr.clear();
  _interp.clear();
  _zlist.clear();
  _rd.clear(); 
  _p = NULL;
  if( n ) memcpy( _norm, n, 3*sizeof(GLfloat) );
  _fresh = true;
}


void
/**
 * @brief recolour surface 
 *
 * @param col        cut surface
 * @param showdata   whether surface is datified
 * \param update     copy colours to GPU
 */
CutSurfaces::recolor( const GLfloat *col, bool showdata, bool update )
{
  GLubyte *buff = _rd.colbuffer();
  for( int i=0; i<_rd.numVtx(); i++ ){
    buffer_alpha( buff+VBO_RGBA_SZ*i, col[3] );
    if( !showdata ) 
      buffer_RGB( buff+VBO_RGBA_SZ*i, col );
  }
  if( update ) _rd.update_col();
}

