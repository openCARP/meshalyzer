#ifndef MY_SCROLL_H
#define MY_SCROLL_H

#include <FL/Fl.H>
#include <FL/Fl_Scroll.H>
#include <cstdio>

#define SCROLL_GAP 5

/** \class MyScroll MyScroll.h
 * \brief Vertical scroll widget with dynamic additions and collapsing
 *
 * This class manages a vertical scroll which is one widget wide.
 * Widgets can be added dynamically, and also widgets can be replaced by  
 * a substitute button widget, and later restored. This allows collapsing widget groups.
 */
class MyScroll : public Fl_Scroll
{
  public:
    MyScroll( int x, int y, int w, int h, const char *L=0 ):Fl_Scroll(x,y,w,h,L){}
    void       insert( Fl_Widget* w, int indent, int gap=SCROLL_GAP );
    void       append( Fl_Widget* w, int indent, int gap=SCROLL_GAP, Fl_Widget *after=NULL );
    void       remove( Fl_Widget *, int gap=SCROLL_GAP );
    Fl_Widget* collapse( Fl_Widget *, int bh, const char* );
    Fl_Widget* expand( Fl_Widget *, Fl_Widget * );
    static     int all;       
    void       scroll_to( Fl_Widget * );
  private:
    int        shift( int, int );
};

#endif
