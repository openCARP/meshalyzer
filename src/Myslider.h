/** \class Myslider
 * \brief slider which responds to keyboard events
 */
#ifndef MYSLIDER_H

#define MYSLIDER_H
#include <FL/Fl_Value_Slider.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Value_Output.H>

class Myslider : public Fl_Group
{
    Fl_Slider       slider;
    Fl_Value_Output valout;
  public:
    Myslider( int, int, int, int, const char *L=0 );
    int handle( int );
    float value( void );
    void  value( int );
    void maximum( int );
    float maximum( void );
    void minimum( int );
    float minimum( void );
    void step( double );
};

class Strideslide : public Fl_Group
{
    Fl_Value_Slider slider;
    Fl_Value_Output valout;
    Fl_Box     		samplab;
    int quantity;

  public:
    Strideslide( int, int, int, int, char *L=0 );
    int value( void );
    void  value( int );
    void update( void );
    void setnum( int );
    void color( int );
};

#endif
