#ifndef CUTSURFACES_H
#define CUTSURFACES_H

#include <stdio.h>
#include <stdlib.h>
#include "DrawingObjects.h"
#include "Surfaces.h"
#include "Interpolator.h"


/** Class for a temporary cut plane
 *
 *  It is assumed that vertices will be changed if the plane is moved as
 *  well as all the elements
 */
class CutSurfaces : public Surfaces
{
  public:
    CutSurfaces(GLfloat *n);
    ~CutSurfaces();
    void get_vert_norms( GLfloat *vn ){}
    void determine_vert_norms( PPoint & ){}
    void addEle( SurfaceElement *e, Interpolator<DATA_TYPE>*, int=-1 );
    DATA_TYPE  interpolate( int e, DATA_TYPE *d, int p )
    {
      return _interp[e][p].interpolate( d );
    }
    DATA_TYPE  interpolate( int e, DATA_TYPE *d, int p, double *bc )
    {
      return _interp[e][p].interpolate( d, bc );
    }
    const GLfloat* norm(){ return _norm; }
    const short_float* sh_norm(){ return _shnorm; }
    void clear(GLfloat *n =NULL); 
    bool fresh(){return _fresh;}
    void fresh( bool f ) { _fresh = f; }
    GLfloat* allocate_buffer(){count_tris(); _rd.buffer(numtri()); _rd.opaque(-1); 
                                                             return _rd.buffer();} 
    GLfloat* buffer(){return _rd.buffer();} 
    size_t buffer_sz(){return _rd.numVtx()*_rd.vtx_size();}  //!< \return buffer size in floats 
    GLubyte* colbuff(){return _rd.colbuffer();} 
    GLuint   vtx_pos( GLfloat *vbo ){return (vbo-_rd.buffer())/VBO_ELEM_SZ;}
    void init_buff(void *c){_rd.setup_render_data(c);}
    void update_buff(){_rd.update_nodalbuff();}
    void update_colbuff(){_rd.update_col();}
    void render(){_fresh=false;_rd.mesh_render(false);}
    void recolor( const GLfloat *col, bool, bool=true );
    void translucent( bool tl ){ if(tl){_rd.opaque(0);}else{_rd.opaque(-1);}}
    void light( GLfloat a ){_rd.lighting(a,.7,.7,3.,1);}

    void colour_elem(int e, GLubyte*& cbf, GLfloat *c,Colourscale *cs,DATA_TYPE *dat, dataOpac* dop, char cm=0){ 
                      Surfaces::colour_elem(e,c,cs,dat,dop,cbf,0); }        
  protected:
    std::vector<GLfloat*>                 _ptarr;
    GLfloat                               _norm[3];
    short_float                           _shnorm[3];
    std::vector<Interpolator<DATA_TYPE>*> _interp;
    bool                                  _fresh = true;
    RenderTris _rd;
};

#endif
