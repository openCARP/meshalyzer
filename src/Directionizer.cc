#include "Directionizer.h"
#include <cmath>

#include "TBmeshWin.h"

Directionizer::Directionizer(int x, int y, int w, int h, const char *l )
    : Fl_Gl_Tb_Window(x, y, w, h, l)
{
  _dl=0;
  mode(FL_ALPHA|FL_RGB|FL_OPENGL3|FL_MULTISAMPLE);
}




/**
 * @brief  fill VBO to draw an arrow starting at (0,0,0) and extending along z
 *
 * @param stick        stick length
 * @param head         head length (=0 for no head)
 * @param stick_rad    stick radius
 * @param head_rad     head radius
 * @param numdiv       number of radial subdivisions
 * @param color        colour of arrow
 *
 * @post buffer points to the entry in the buffer after the arrow
 */
void 
Directionizer::arrow( GLfloat stick, GLfloat head, GLfloat stick_rad, GLfloat head_rad, 
                                           int numdiv, const GLfloat* colour             )
{
  int ntri = arrow3D_ntri( numdiv, head!=0 );
  GLfloat *buff = _rd->buff_append(ntri);
  _rd->buff_3Darrow( buff, stick, head, stick_rad, head_rad, numdiv, colour );
}


void
Directionizer::hoops(const GLfloat *colour)
{
  const float anginc = 5.;
  int         nseg   = int( 360/anginc );
  GLubyte    *colb;
  GLfloat    *buff   = _rd_lines->buff_append( nseg*6, &colb );

  V3f pt;
  SHORTVEC( nx, 1, 0, 0 );
  for( int j=0; j<nseg; j++ ) {
    float i=anginc*j;
    pt = V3f( 0, cos(i*M_PI/180.), sin(i/180*M_PI) );
    buff += buff_vtx( buff, colb, pt.e, nx, colour, 0 );
    pt = V3f( 0, cos((i+anginc)*M_PI/180.), sin((i+anginc)/180*M_PI) );
    buff += buff_vtx( buff, colb, pt.e, nx, colour, 0 );
  }
  SHORTVEC( ny, 0, 1, 0 );
  for( int j=0; j<nseg; j++ ) {
    float i=anginc*j;
    pt = V3f( cos(i/180*M_PI), 0, sin(i/180*M_PI) );
    buff += buff_vtx( buff, colb, pt.e, ny, colour, 0 );
    pt = V3f( cos((i+anginc)/180*M_PI), 0, sin((i+anginc)/180*M_PI) );
    buff += buff_vtx( buff, colb, pt.e, ny, colour, 0 );
  }
  SHORTVEC( nz, 0, 0, 1 );
  for( int j=0; j<nseg; j++ ) {
    float i=anginc*j;
    pt = V3f( cos(i/180*M_PI), sin(i/180*M_PI), 0 );
    buff += buff_vtx( buff, colb, pt.e, nz, colour, 0 );
    pt = V3f( cos((i+anginc)/180*M_PI), sin((i+anginc)/180*M_PI), 0 );
    buff += buff_vtx( buff, colb, pt.e, nz, colour, 0 );
  }
}

void
Directionizer::gl_settings()
{
  static M4f validMV;

  if( !valid() ) {
    valid( 1 );
    glLineWidth( 1. );
    glPointSize( 10.0 );
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    glEnable( GL_DEPTH_TEST );
    glEnable( GL_LINE_SMOOTH );
    glEnable( GL_BLEND );
    glEnable( GL_MULTISAMPLE );
    glDepthFunc( GL_LEQUAL );
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glViewport(0,0,pixel_w(),pixel_h());
    identity_mv(context());
    ortho_proj( context(), -1.05, 1.05, -1.1, 1.1, -10, 10 );
    validMV = ModelViewMat[context()];
    glClearColor( 1, 1, 1, 1 );
  }

  ModelViewMat[context()] = validMV;
  glClear( GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT );

}


/**
 * @brief     recolor the bufer
 *  
 * @param buf buffer
 * @param n   number of triangles
 * @param col color
 *
 * @post buf will be advanced
 */
void
Directionizer::recolor( GLubyte* &buf, int n, GLubyte *col )
{
  for( int i=0; i<3*n; i++ ) {
    memcpy( buf, col, 4 );
    buf += 4;
  }
}


/** set up the graphics since it is not guaranteed that a valid OpenGL
 *  context will be available in the constructor
 */
void 
Directionizer:: init_graphics()
{
  GLfloat discol[4] = { 0, 1., 0, 0.5 };
  GLubyte arrcol[4] = { 255, 0, 0, 255 };
  _rd->setup_render_data();
  arrow( 1.6, 0.4, 0.1, 0.2, 10, (const GLfloat[]){1,0,0,1} );
  const int ndiv = 72;
  GLfloat* buff = _rd->buff_append(ndiv);
  int nf = _rd->buff_cone( buff, ndiv, 1., 0, 0, 1., (const GLfloat[]){0, 1, 0, 0.5} );
  _rd->opaque(_rd->num()-ndiv);
  _rd->update_nodalbuff();
  _rd->diffuse = 0.8;
  _rd->specular = 0.8;
  _rd->ambient = 0.8;
  _rd->back_lit = 1;
  _rd->set_material( 0, (const GLfloat[]){ 1, 1., 80., 1 } );
  _rd->update_nodalbuff();
  
  _rd_lines->setup_render_data();
  hoops( (const GLfloat[]){0,0,0,1} );
  _rd_lines->opaque(-1);
  _rd_lines->update_nodalbuff();
  _rd_lines->diffuse  = 0.8;
  _rd_lines->specular = 0.8;
  _rd_lines->ambient  = 0.8;
  _rd_lines->back_lit = 1;
  _rd_lines->lineWidth      = 1.;
  GLfloat props[4] = { 1, 1., 80., 1 };
  _rd_lines->set_material( 0, props );
  _rd_lines->update_nodalbuff();
}


void
Directionizer:: draw()
{ 
  // get a new context when window is closed and then a new one created
  if ( context_valid()==0 ) {
    if( _rd )       delete _rd;
    if( _rd_lines ) delete _rd_lines;
    _rd       = new RenderTris(context());
    _rd_lines = new RenderLines(context());
  }

  gl_settings();
  
  // rotate clipping plane only if the Directionizer was moved, and 
  // not if the model was rotated
  if( !trackball.transformed ){ 
     if( win ) win->redraw();
  }
  
  if( !_eyeSys ) trackball.Rotation(_view); // adjust for model rotation
  trackball.DoTransform(context());

  if( !_rd_lines->inited() )
    init_graphics();
   
  _rd->mesh_render();
  _rd_lines->mesh_render();

  if( !_eyeSys ) 
    trackball.Rotation(_view.GetConjugate()); // remove model rotation
}


int
Directionizer :: handle( int event )
{
  Fl_Gl_Window::handle(event); // without this, no redraw() called, but why?
  return  Fl_Gl_Tb_Window::handle(event);
}

