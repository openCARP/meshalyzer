#ifndef DATAONDEMAND_H
#define DATAONDEMAND_H

#include "IGBheader.h"
#include <map>
#include <cmath>
#include <string>
#include <exception>
#include <algorithm>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "DataClass.h"
#include "AsciiReader.hpp"

static const size_t IGB_OFFSET = 1024;



/**
 * @brief find maximum element ignoring NaN
 *
 * @tparam T array type
 * @param v0 start
 * @param v1 one past last element
 *
 * @return the maximum element
 */
template<class T>
T vector_max_nan( T* v0, T* v1 ){
    auto v_iter = std::max_element(v0, v1,
    [] (auto x, auto y)
    {
        return x < y ? true : std::isnan(x);
    });
    return *v_iter;
}


/**
 * @brief find minimum element ignoring NaN
 *
 * @tparam T array type
 * @param v0 start
 * @param v1 one past last element
 *
 * @return the minimum element
 */
template<class T>
T vector_min_nan( T* v0, T* v1 ){
    auto v_iter = std::min_element(v0, v1,
    [] (auto x, auto y)
    {
        return x < y ? true : std::isnan(y);
    });
    return *v_iter;
}


/** Class for reading data requested from the file on disk */
template<class T>
class DataOnDemand: public DataClass<T>
{
  public:
    virtual T      max(int t){return extremum(t,true);}	//!< maximum data value at a time
    virtual T      max(){assert(0);}                    //!< maximum data value at a time
    virtual T      min(int t){return extremum(t,false);}//!< minimum data value at a time
    virtual T      min(){assert(0);}                    //!< minimum data value at a time
    virtual T*     slice(int=-1);                       //!< pointer to time slice of data
    virtual void   time_series( int, T* );              //!< time series for a point
    virtual void   increment(int){}                     //!< time slice increment

    DataOnDemand(const char *fn, int, int=-1);
    virtual ~DataOnDemand(){free(data);if(databuf)free(databuf);}

  private:
    IGBheader   *hdr=NULL;
    AsciiReader *asciiRdr=NULL;
    char        *databuf=NULL;
    const char  *_scanstr;             //!< format code to read ascii file
    const int    bufsz=1024;            
    char         buf[1024];            //!< storage tor read ascii line
    T            extremum(int,bool);
    using DataClass<T> :: data;
    using DataClass<T> :: maxtm;
    using DataClass<T> :: last_tm;
    using DataClass<T> :: slice_size;
    using DataClass<T> :: filename;
    using DataClass<T> :: _elebased;
    using DataClass<T> :: _dt;
    using DataClass<T> :: _t0;
    using DataClass<T> :: _ftype;
#ifdef USE_HDF5
    hid_t         hin=-1;
    unsigned int  grid_idx;
#endif
};

template<class T>
DataOnDemand<T>::DataOnDemand( const char *fn, int npts, int neles )
{
  if( neles<0) neles = npts;

  bool            IGBdata;
  int             j = 0;
  FILE           *in;
  std::string          scanner;
  std::map<int,std::string> CGfiles;
  std::map<int,std::string>::iterator CGp;
#ifdef USE_HDF5
  struct ch5s_nodal_grid info;
#endif

  filename       = fn;
  _ftype         = FileTypeFinder( fn );

  if ( _ftype == FThdf5 )  {
#ifdef USE_HDF5
    hin = H5Fopen(filename.substr(0, filename.find_last_of(":")).c_str(), H5F_ACC_RDONLY,
                            H5P_DEFAULT);
    if( hin == H5I_INVALID_HID )
      throw(1);
#endif
  } else {
    if ( (in=fopen(fn, "r")) == NULL )
      throw( 1 );
  }

  // ugly but I don't know what else to do besides specialization which is ugly
  if (      typeid(T) == typeid(double) ) _scanstr = "%lf";
  else if ( typeid(T) == typeid(float) )  _scanstr = "%f";
  else if ( typeid(T) == typeid(int) )    _scanstr = "%d";
  else if ( typeid(T) == typeid(short) )  _scanstr = "%hd";
  else if ( typeid(T) == typeid(long) )   _scanstr = "%ld";
  else exit(1);

  // open file
  if ( _ftype == FTIGB || _ftype == FTDynPt) {
    hdr = new IGBheader( in, true );
    if ( hdr->slice_sz() == npts ) {
      slice_size = npts;
      _elebased  = false;
    }else if ( hdr->slice_sz() == neles ) {
      slice_size = neles;
      _elebased  = true;
    } else {
      maxtm      = -1;
      throw PointMismatch(npts, neles, hdr->slice_sz());
    }
    if ( _ftype==FTDynPt && hdr->num_components()!=3 ) {
      maxtm = -1;
      throw PointMismatch(hdr->num_components(),3);
    }
    struct stat statstr;
    stat( filename.c_str(), &statstr );
    maxtm = (statstr.st_size-1024)/slice_size/hdr->data_size()-1;
    if( maxtm<0 ){
      throw EmptyDataFile();
    }
    _dt = hdr->inc_t();
    _t0 = hdr->org_t();
    databuf = (char *)malloc( slice_size*hdr->data_size() );
  } else if ( _ftype == FTfileSeqCG ) {
    CG_file_list( CGfiles, fn );
    CGp = CGfiles.begin();
    scanner = "%*f %*f";
    scanner += _scanstr;
  } else if( _ftype == FThdf5 ) {
    std::string gtype;
#ifdef USE_HDF5
    if( parse_HDF5_grid( fn, gtype, grid_idx ) || gtype!="nodal" )
      throw(1);
    ch5s_nodal_grid_info( hin, grid_idx, &info );
    _dt = info.time_delta;
    maxtm = info.time_steps-1;
    slice_size = info.num_nodes;
#endif
    _t0 = 0;
  } else if( _ftype == FTascii ) {
    asciiRdr = new AsciiReader;
    asciiRdr->read_file( filename );
        
    if( asciiRdr->num_lines()<npts && asciiRdr->num_lines()<neles ){
      maxtm = -1;
      throw PointMismatch(npts, neles, asciiRdr->num_lines());
    } else if( asciiRdr->num_lines()>=npts && !(asciiRdr->num_lines()%npts) ) {
      slice_size = npts;
      _elebased  = false;
    } else if( !neles || asciiRdr->num_lines()<neles || 
               (asciiRdr->num_lines()>=npts && asciiRdr->num_lines()%neles) ) {
      slice_size = npts;
      _elebased  = false;
    } else {
      slice_size =  neles;
      _elebased = true;
    }
    maxtm = asciiRdr->num_lines()/slice_size - 1;
  }

  size_t datasz = slice_size*sizeof(T)*(hdr?hdr->num_components():1);
  data    = (T *)malloc( datasz );
  last_tm = 0;
  
  // get one slice
  switch( _ftype ) {
      case FTIGB:
      case FTDynPt:
          hdr->read_data( data, 1, databuf );
          break;
      case FTascii:
          for( int i=0; i<slice_size; i++ ) {
            asciiRdr->get_line( i, buf, bufsz );
            if( sscanf(buf, _scanstr,  data+i ) != 1 ) {
              maxtm = -1;
              throw( 1 );
            }
          }
          break;
#ifdef USE_HDF5
      case FThdf5:
          ch5s_nodal_read( hin, grid_idx, 0, 0, data );
          break;
#endif
      default:
          assert(0);
  }
}


/** return an extremum 
 *
 * \param tm  frame number
 * \param max determine max?, min o.w.
 */
template<class T>
T DataOnDemand<T>::extremum( int tm, bool max )
{
  if ( tm>maxtm ) return 0;

  //T* (*exfcn)(T*, T*);
  T (*exfcn)(T*, T*);
  if( max )
    //exfcn = std::max_element<T*>;
    exfcn = vector_max_nan<T>;
  else
    exfcn = vector_min_nan<T>;

  if( last_tm == tm )
    return exfcn( data, data+slice_size );
 
  // analyze a temporary slice
  T* sdata;
  if( _ftype==FTIGB || _ftype==FTDynPt ) {
    sdata = (T*)malloc(slice_size*hdr->data_size());
    hdr->data_frame( tm );
    hdr->read_data( sdata, 1, NULL );
  } else if( _ftype == FTascii ) {
    sdata = (T*)malloc(slice_size);
    for( int i=0; i<slice_size; i++ ) {
      sscanf( asciiRdr->peek(i+slice_size*tm), _scanstr, sdata+i );
    }
#ifdef USE_HDF5
  } else if( _ftype==FThdf5 ) {
      sdata = (T*)malloc(slice_size);
      ch5s_nodal_read( hin, grid_idx, tm, tm, sdata );
#endif
  } else {
    assert(0);
  }

  T maxdat = exfcn( sdata, sdata+slice_size ) ;
  free( sdata );
  return maxdat;
}


template<class T>
T* DataOnDemand<T>::slice( int tm )
{
  if ( tm>maxtm )
    return NULL;
  if( tm == last_tm || tm == -1 )
    return data;

  if( _ftype == FTIGB || _ftype == FTDynPt ) {
    hdr->data_frame( tm );
    hdr->read_data( data, 1, databuf );
  } else if( _ftype == FTascii ) {
    for( int i=0; i<slice_size; i++ ) {
      sscanf( asciiRdr->peek(i+slice_size*tm), _scanstr, data+i );
    }
#ifdef USE_HDF5
  } else if( _ftype==FThdf5 ) {
      ch5s_nodal_read( hin, grid_idx, tm, tm, data );
#endif
  } else
    assert(0);

  last_tm = tm;
  return data;
}


/**
 * @brief return time series for a point
 *
 * @tparam T          type to receive time series
 * @param[in]  offset node number
 * @param[out] buffer  
 */
template<class T>
void DataOnDemand<T>::time_series( int offset, T* buffer )
{
  if( _ftype == FTIGB ) {
    FILE *in = (FILE *)hdr->fileptr();
    size_t slsz = hdr->slice_sz()*hdr->data_size();
    char datum[hdr->data_size()];
    offset *=hdr->data_size();
    for( size_t i=0; i<=maxtm; i++ ) {
       fseek( in, IGB_OFFSET+offset+i*slsz, SEEK_SET );
       fread( datum, hdr->data_size(), 1, in );
       if ( hdr->endian() != hdr->systeme() ) hdr->swab( datum, 1 );
       buffer[i] = hdr->convert_buffer_datum<T>( datum, 0 );
    }
  } else if( _ftype == FTascii ) {
    for( int i=0; i<=maxtm; i ++ ) 
      sscanf( asciiRdr->peek(offset+slice_size*i), _scanstr, buffer+i );
#ifdef USE_HDF5
  } else if( _ftype == FThdf5 ) {
    ch5s_nodal_read_time_series( hin, grid_idx, offset, buffer );
#endif
  }
}
#endif
