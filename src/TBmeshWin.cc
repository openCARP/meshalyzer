#define GL_GLEXT_PROTOTYPES
#include "trimesh.h"
#include <signal.h>
#include <fstream>
#include <sstream>
#include <set>
#include <algorithm>
#include <array>
#include <vector>
#ifdef __WIN32__
#  include <GL/glext.h>
#endif
#ifdef USE_HDF5
#include <ch5/ch5.h>
#include <HDF5DataBrowser.h>
#endif
#include "Frame.h"
#include "render_utils.h"
#include "legacyGL.h"
#include "DataOnDemand.h"
#include "ScreenTxt.h"
#include <FL/names.h>


#define HITBUFSIZE   10000
#define OPAQUE_LIMIT 0.95       //!< consider opaque if alpha level above this
const int MAX_SELECT_ATTEMPTS=3;
#define MAX_SURFELE_REALTIME 100000 //!< max \#ele's to draw while spinning

#define REDRAW(A)  (_redraw_state&(A))

unsigned int TBmeshWin::MAX_MESSAGES_READ = 100;
bool has_ext( std::string, const char * );

#ifdef OSMESA
    static OSMesaContext  osmesa_ctx = NULL;
#endif

GLfloat BlackOpq[] = {0.,0.,0.,1};


/** possible translucency 
 *
 * \param obj   object to consider
 * \param col   colour of undatified object
 * \param mixed determine if both translucent and opaque objects are possible
 *
 * \return true iff translucency is possible, if mixed is false
 * \return true iff both translucent and opaque objects are possible, if mixed is true
 */
bool 
TBmeshWin::translucency( Object_t obj, GLfloat *col, bool mixed )
{
  bool showData = datadst&(1<<obj) && have_data!=NoData; 
  bool dopac_on = dataopac->dop[obj].on();
  bool deadopac = cs->deadRange() && cs->deadColour()[3]<OPAQUE_LIMIT;

  if( mixed )
      return showData && (dopac_on || deadopac );
  else {
      bool alpha = col[3]<OPAQUE_LIMIT;
      return  ( !showData && alpha )              ||
              ( showData  && dopac_on )           || 
              ( showData  && !dopac_on && alpha ) ||
              ( showData  && deadopac );
  }
}


void animate_cb( void *v )
{
  TBmeshWin* mw = (TBmeshWin *)(((void **)v)[0]);
  Controls* ctrl = (Controls *)(((void **)v)[1]);
  if ( mw->frame_skip == 0 )
    return;

  int ntm = mw->tm + mw->frame_skip;
  if ( !mw->anim_loop && (ntm<0 || ntm>ctrl->tmslider->maximum()) ) {
    ntm -= mw->frame_skip;
    mw->frame_skip = 0;
  } else {
    if ( ntm<0 ) 
      ntm = ctrl->tmslider->maximum();
    else if( ntm>ctrl->tmslider->maximum() )
      ntm = 0;

    mw->signal_links( 1 );
  }
  mw->set_time( ntm );
  Fl::add_timeout( mw->frame_delay, animate_cb, v );
}



/**
 * @brief fill vector with vertices
 *
 * @param ntri   number of tris
 * @param snl    each row has node numbers
 * @param pt     vertex positions
 * @param vtxvec vector to populate
 *
 * @return the raw array filled with the vertices (\p vtxvec data)
 */
GLfloat*
fill_tri_vtx( int ntri, int snl[][MAX_NUM_SURF_NODES+1], PPoint &pt, std::vector<GLfloat> &vtxvec )
{
  vtxvec.resize(9*ntri);
  GLfloat *vtx = vtxvec.data();

  for( int i=0; i<ntri; i++ )
    for( int j=0; j<3; j++ )
      memcpy( vtx+i*9+j*3, pt[snl[i][j]], 3*sizeof(GLfloat) );
  return vtx;
}


/**
 * @brief determine the action if the play button is pressed on the control widget
 *
 * @param fs     frame increment
 * @param ctrl   control widget
 * @param repeat repeat increment 
 */
void
TBmeshWin::animate_skip( int fs, void *ctrl, bool repeat )
{
  static void* vp[2];

  if ( fs && dataBuffer != NULL)dataBuffer->increment( fs );

  if ( frame_skip ) {
    frame_skip = fs;
    return;
  }

  if ( ctrl==NULL )
    return;

  vp[0] = this;
  vp[1] = ctrl;
  frame_skip = fs;
  anim_loop = repeat;

  if ( tm+frame_skip >= contwin->tmslider->maximum() )
    tm = -frame_skip;
  else if ( tm+frame_skip < 0 )
    tm = int(contwin->tmslider->maximum())-frame_skip;
  animate_cb( &vp );
}


/**
 * @brief change the object to be highlighted
 *
 * @param obj            object type
 * @param a              index of object
 * @param update_widget  update the widget?
 */
void
TBmeshWin::highlight( Object_t obj, int a, bool update_widget )
{
  if( a >= model->number(obj) ) return;
  hilight[obj] = a;
  if( update_widget ) {
    if( obj == Vertex )
      contwin->verthi->value(a);
    else if( obj == SurfEle )
      contwin->elehi->value(a);
    else if( obj == VolEle )
      contwin->tethi->value(a);
    else if( obj == Cnnx )
      contwin->cnnxhi->value(a);
    else if( obj == Cable )
      contwin->cabhi->value(a);
  } 
  redraw(VBO_Hilight);
  if ( hinfo->window->visible() ) hiliteinfo();
  if ( obj == Vertex ) {
    if( timeplotter->window->shown() ) timeplot();
    if( timeplotter->graph->crvi_vis() )static_curve_info_cb(timeplotter->graph);
  }
  if( have_data == NoData ) return;
  Object_t o=!dataBuffer->ele_based()?Vertex:(model->numVol()?VolEle:SurfEle);
  if( obj==o ) contwin->vertvalout->value( data[a] );
}


TBmeshWin ::TBmeshWin(int x, int y, int w, int h, const char *l )
    : Fl_Gl_Tb_Window(x, y, w, h, l), datadst(ObjFlg[Surface]), cs( new Colourscale( 64 ) ),
    _cutsurface(new CutSurfaces*[NUM_CP] ), model(new Model()),
    renderMode(RENDER)
{
  hinfo      = new HiLiteInfoWin(this);
  dataopac   = new DataOpacity( this );
  cplane     = new ClipPlane( this );
  isosurfwin = new IsosurfControl(this);
  deadData   = new DeadDataGUI(this,cs,VBO_Colour);
  timeplotter= new PlotWin("Time series",this);
  cbgui      = new ColBarGUI( this );

  memset( hilight, 0, sizeof(int)*maxobject );
  bgd( 1. );
  for ( int i=0; i<NUM_CP; i++ ) _cutsurface[i]=NULL;
  mode(FL_RGB|FL_DOUBLE|FL_DEPTH|FL_ALPHA|FL_OPENGL3|FL_MULTISAMPLE);
}


TBmeshWin::~TBmeshWin()
{
  if (tmLink != NULL)
    delete tmLink;

  // need to delete all stack allocated objects
  if (model != NULL)
    delete model;
}

void TBmeshWin :: draw()
{
  static M4f validMV;

  if (!valid()) {
    valid(1);
    identity_mv(context());
    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    glPolygonOffset( 2., 2. );
    glEnable( GL_DEPTH_TEST );
#if !defined(__APPLE__) && !defined(OSMESA)
    glEnable(GL_POINT_SPRITE);                       // enables gl_PointCoord in shader
#endif
    glFrontFace( GL_CW );
    glCullFace(GL_BACK);
    if( backface_culling )
        glEnable(GL_CULL_FACE);
    else
        glDisable(GL_CULL_FACE);

    if ( facetshading || SELECT(renderMode) ) {			// faster but no anti-aliasing
      glDisable( GL_LINE_SMOOTH );
      glDisable( GL_MULTISAMPLE );
    } else {
      glEnable( GL_LINE_SMOOTH );
      glEnable( GL_MULTISAMPLE );
    }
    glDepthFunc(SELECT(renderMode)?GL_LESS:GL_LEQUAL);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    if ( SELECT(renderMode) )		// for picking and vertex list
      glClearColor( 0, 0, 0, 0 );
    else
      glClearColor( bc[0], bc[1], bc[2], bgd_trans?0:1 );

    GLint rbwidth=w(), rbheight=h();
    if( _offscreen ) {
#ifndef OSMESA
      glGetRenderbufferParameteriv( GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH,  &rbwidth );
      glGetRenderbufferParameteriv( GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &rbheight );
#endif
      glViewport( 0, 0, rbwidth, rbheight );
    } else 
      glViewport( 0, 0, pixel_w(), pixel_h() );
    float maxdim = model->maxdim();
    if( rbwidth>rbheight ) {
      _aspect_xf = float(rbwidth)/float(rbheight);
      _aspect_yf = 1.;
    } else {
      _aspect_xf = 1.;
      _aspect_yf = float(rbheight)/float(rbwidth);
    }
    float xdim   = maxdim*_aspect_xf;
    float ydim   = maxdim*_aspect_yf;
    ortho_proj( context(), -xdim, xdim, -ydim, ydim, -maxdim*5, maxdim*5 );
    validMV = ModelViewMat[context()];
  } 
  ModelViewMat[context()] = validMV;

  _rd_op.setup_render_data(context());
  _rd_wf.setup_render_data(context());

  framenum++;

  glClear( GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT );

  trackball.DoTransform(context());

  for ( int i=0; i<NUM_CP; i++ ) {
    if ( cplane->on(i) ) {
      clip_on( i );
      clip_eqn( i, cplane->plane(i) );
    } else {
      clip_off( i );
    }
  }
  cplane->setView( trackball.qRot );
  contwin->lightorient->setView( trackball.qRot );
  if( headlamp_mode ){
    light_dir( context(), trackball.qRot.GetConjugate()*contwin->lightorient->Rot() );
  } else { 
    light_dir( context(), contwin->lightorient->Rot() );
  }

  // determine if we are spinning or have just finished spinning
  static bool was_spinning = false;
  _spinning = Fl::event_state(FL_BUTTON1);
  if( trackball.isSpinning && (_spinning||was_spinning) )
    _redraw_state |= VBO_View;
  Display_t actual_disp = disp; 
  was_spinning = _spinning;

  if ( have_data != NoData ) {
    if ( (data = dataBuffer->slice(tm)) == NULL )
      data = dataBuffer->slice(dataBuffer->max_tm());
    Object_t o = !dataBuffer->ele_based() ? Vertex : (model->numVol() ? VolEle : SurfEle);
    contwin->dispVertVal(data[hilight[o]]);
  }

  if ( autocol == true ) {
    optimize_cs();
    contwin->mincolval->value(cs->min());
    contwin->maxcolval->value(cs->max());
  }

  if ( actual_disp == asTetMesh ) {

    draw_vol_mesh();

  } else if ( actual_disp == asSurface ) {

    for ( int s=0; s<model->_numReg; s++ ) {

      RRegion *reg = model->region(s);

      if( !reg->show(Vertex) || !reg->visible() || !reg->show(Cnnx) ) 
        model->pt.setVis(reg->pt_membership());

      if ( reg->show(Cnnx) && model->_cnnx->num() )
        draw_cnnx(reg);

      draw_vertices(reg);
    }
    draw_vertices(NULL);
  }

  if ( renderMode&RENDER ){
    draw_cut_planes();
    draw_iso_surfaces( );
  }
  
  draw_surfaces( _rd_op, _trans_elems, false );

  if ( renderMode&RENDER ){
    draw_surfaces( _rd_wf, _trans_wf, true );
    draw_iso_lines();
    draw_highlights();
    draw_clip_planes();
    if( vecdata != NULL ) vecdata->draw(tm,model->maxdim(),_redraw_state,context());
  }
  if( renderMode!=PICK_VTX && auxGrid) {
    auxGrid->draw( tm, _redraw_state, context() );
    contwin->updateAuxGrid(  auxGrid->num_vert() );
  }
  if ( renderMode&RENDER ){
    TBmeshWin::draw_axes();
    if( _scalebar ) _scalebar->render(context());
    draw_cbar();
  }

  glFlush();
  _redraw_state = VBO_None;
}


/** draw the mesh as a volume element outlines
 */
void
TBmeshWin::draw_vol_mesh()
{
  static RenderLines rd_mv;
  rd_mv.threeD( false );
  const bool datcolor = datadst&VolEle_flg && have_data!=NoData;

  if( rd_mv.setup_render_data(context()) || REDRAW(VBO_Visible) ) {

    rd_mv.clear();
    std::set<std::pair<int,int>> edges;
    for ( int r=0; r<model->_numReg; r++ ) {

      if ( !model->region(r)->visible() ) continue;
      model->pt.setVis(model->region(r)->pt_membership());

#pragma omp parallel for  
      for( int i=0; i<model->numVol(); i++ ) {
        VolElement *v = model->_vol[i];
        if( v->visible() ) {
          int ne;
          const int *elst = v->edges( ne );
          const int *nlst = v->obj();
          for( int e=0; e<ne; e++ ){
            int n0 = nlst[*elst++];
            int n1 = nlst[*elst++];
#pragma omp critical
            edges.insert( std::pair<int,int>( std::min(n0,n1), std::max(n0,n1)));
          }
        }
      }
      GLfloat *buff = rd_mv.buff_append(edges.size()*2);
      SHORTVEC( n,1,0,0 );
      for( auto &e : edges ) {
        buff += rd_mv.buff_vtx( buff, model->pt[e.first],  n, model->region(r)->get_color(VolEle), 0 );
        buff += rd_mv.buff_vtx( buff, model->pt[e.second], n, model->region(r)->get_color(VolEle), 0 );
      }
    }
    rd_mv.opaque(-1);
  }
  rd_mv.update_nodalbuff();
  rd_mv.lineWidth=model->region(0)->size(Cnnx);
  rd_mv.mesh_render();
}


// draw_iso_surfaces()
void TBmeshWin::draw_iso_surfaces()
{
  if( have_data == NoData ) return;

  glDisable(GL_CULL_FACE );

  for( int s=0; s<2; s++ ) {

    if( !isosurfwin->isoOn(s) ) continue;

    float bflight = isosurfwin->backlight(s);
    GLfloat mat[4] = { 0.75, 0.6, 80, bflight };
    bool dirty =  isosurfwin->issDirty(s);

    for ( int r=0; r<model->_numReg; r++ ) {
      RRegion *reg = model->region(r);
      IsoSurface*& iso = !s ? reg->_iso0 : reg->_iso1;
      if( iso==NULL ) 
        iso = new IsoSurface();

      if( dirty || iso->tm()!=tm || iso->fresh() ){
        iso->clear();
        iso->calc( model, data, isosurfwin->isoval(s),
                reg->ele_membership(), tm, _branch_cut?_branch_range:NULL );
        iso->init_buff(context());
        iso->update_buff();
        iso->allocate_buffer();
      }
      iso->fillcolor( isosurfwin->issColor(s) );
      iso->material( mat );
      iso->draw(_redraw_state&VBO_Isosurf_Color, facetshading);
    } 
  }
  if( backface_culling ) glEnable(GL_CULL_FACE);
}


/** draw the surfaces
 */
void
TBmeshWin::draw_surfaces( RenderTris &rd, std::vector<vtx_z> &trans, bool wireframe )
{
  model->pt.setVis( true );

  bool translucent =  false;
  Object_t obj = wireframe ? SurfEle : Surface;

  for ( int s=0; s<model->numSurf(); s++ ) {
    Surfaces *sf = model->surface(s);
    GLfloat props[] = {sf->specular(), sf->diffuse(), sf->shiny(), sf->backlight()};
    rd.set_material( s, props );

    if(translucency( obj, wireframe?sf->outlinecolor():sf->fillcolor()))
      translucent = true;
  }

  if( translucent && rd.num()>MAX_SURFELE_REALTIME && _spinning ){
    rd.mesh_render(wireframe);
    return;
  }

  bool recolour = REDRAW(VBO_Colour);
  bool newView  = REDRAW(VBO_Position|VBO_Visible|VBO_Opacity)    || 
                  ( REDRAW(VBO_View)&&translucent )               ||
                  ( recolour&&dataopac->dop[obj].on() );

  if( newView )  { // refill buffer
    int ntris   = 0;
    for ( int s=0; s<model->numSurf(); s++ ) { 
      Surfaces *surf = model->surface(s);
      bool son = wireframe ? surf->outline() : surf->filled();
      if ( surf->visible() && son ) 
        ntris += surf->numtri();
    }
    rd.buffer(ntris);
    rd.opaque(-1);
    trans.clear();
  }

  if( !rd.num() ) return;

  GLfloat *vbobuf = rd.buffer();
  GLubyte *colbuf = rd.colbuffer();

  for ( int s=0; s<model->numSurf(); s++ ) {

    Surfaces *sf = model->surface(s);
    if ( !sf->visible() ) continue;

    if ( wireframe ? sf->outline() : sf->filled() ) {
      if( newView )  {
        if( draw_surface(sf, vbobuf, colbuf, wireframe, false ) ) {
          // merge elements into list, keeping order
          unsigned int te_sz = trans.size();
          trans.resize( te_sz+sf->zl_sz() );
          std::copy( sf->zl_begin(), sf->zl_end(), trans.begin()+te_sz );
          std::inplace_merge( trans.begin(), trans.begin()+te_sz, 
               trans.end(), [](const vtx_z a, const vtx_z b){return a.z<b.z;} );
        }
        rd.opaque(rd.opaque()-sf->num_trans_tri());
      } else if( recolour ) {
        draw_surface(sf, vbobuf, colbuf, wireframe, true );
      }
    }
  }

  draw_sorted_elements( rd, trans, newView, wireframe, recolour );
}


/**
 * @brief Draw a surface 
 *        if an element is translucent, put it into a list
 *        if opaque, fill the buffer immediately
 *
 * @param sf       surface
 * @param posbuf   VBO buffer for position,normal
 * @param colbuf   VBO buffer for colour
 * @param wf       wireframe rendition
 * @param recolour update colour only?
 *
 * @return the number of translucent elements added
 *
 * @post   \p vbobuf points to one past last opaque element
 * @post   the surface \p _zlist has all translucent elements with their z value
 */
bool
TBmeshWin::draw_surface(Surfaces* sf, GLfloat *&posbuf, GLubyte *&colbuf, bool wf, bool recolour )
{
  int      stride   = 1;
  Object_t obj      = wf ? SurfEle : Surface;
  GLfloat *s_colour = wf ? sf->outlinecolor() : sf->fillcolor();
  bool     showData = datadst&(wf?SurfEdge_flg:Surface_flg) && have_data!=NoData; 
  bool     on_tr    = translucency( obj, s_colour );
  GLfloat *flNull   = NULL;

  if( !recolour ) sf->zsort( context(), stride, on_tr );
  char cmode = CMODE(facetshading, dataBuffer && dataBuffer->ele_based(), col_by_elem);
  DATA_TYPE *dp = NULL;
  if( showData && renderMode&RENDER ){
    dp = col_by_elem ? sf->to_elem( data ) : data; 
  }
  if( !(renderMode&RENDER) ) s_colour = BlackOpq;

  if( !on_tr ) {
    // all elements are opaque
    sf->colour( s_colour, cs, dp, NULL, colbuf, cmode );
    if( !recolour )
      sf->buffer( posbuf, cmode );
  } else {
    sf->buffer_opaque( OPAQUE_LIMIT, s_colour, cs, dp, dataopac->dop+obj, colbuf, 
                                                     recolour?flNull:posbuf, cmode );
  }
  return recolour ? 0 : sf->zl_sz();
}


/**
 * @brief Draw elements which have first been sorted by z depth
 *
 * @param elems        sorted element list
 * @param newView      view needs rebuffering?
 * @param wf           wireframe?
 * @param only_recolor recolour elements?
 */
void
TBmeshWin::draw_sorted_elements( RenderTris &rd, std::vector<vtx_z> &elems, bool newView, 
                                bool wf, bool recolor )
{
  if( !elems.size() && !rd.opaque() ) return;

  bool           showData = datadst&(wf?SurfEdge_flg:Surface_flg) && have_data!=NoData; 
  const GLfloat *vn       = NULL;
  int            prev_s   = -1;
  Surfaces      *sf;

  if( (newView || recolor) && renderMode&RENDER ) {
    GLfloat *vbobuf = rd.transBuffer();
    GLubyte *colbuf = rd.transColBuffer();
    GLfloat *scol;
    std::map<int,std::vector<GLfloat>> edata;
    for( auto &e : elems ) {
      if( e.s != prev_s ) {
        prev_s = e.s;
        sf     = model->surface(e.s);
        scol   = wf ? sf->outlinecolor() : sf->fillcolor();
      }
      if( newView ){
        sf->buffer_elem( e.i,vbobuf, 
                          CMODE(facetshading,dataBuffer && dataBuffer->ele_based(),col_by_elem));
      }  
      sf->colour_elem( e.i, scol, cs, showData?data:NULL,
             dataopac->dop+Surface, colbuf,
                        CMODE(facetshading,dataBuffer && dataBuffer->ele_based(),col_by_elem));
    }
    if( newView ) {
      rd.update_nodalbuff();
    } else if( recolor )
      rd.update_col();
  }

  rd.mesh_render(wf);
}


// draw connections
void TBmeshWin::draw_cnnx(RRegion* sf)
{
  if ( !(renderMode&RENDER) ) return;

  Connection *cnnx = model->_cnnx;
  cnnx->size( sf->size(Cnnx) );
  cnnx->threeD( sf->threeD(Cnnx) );
  _rd_cnnx.threeD( sf->threeD(Cnnx) );
  _rd_cnnx.lineWidth = cnnx->size();
  bool col_update = REDRAW(VBO_Cnnx_Color) || (REDRAW(VBO_Colour) && datadst&Cnnx_flg);
  bool update     = (REDRAW(VBO_Position) && model->pt.dynamic()) || !cnnx->threeD() ||
                     REDRAW(VBO_Cnnx|VBO_Visible);
  char cmode      = CMODE(facetshading, dataBuffer && dataBuffer->ele_based(), col_by_elem);

  if( _rd_cnnx.setup_render_data(context()) || update) {
    _rd_cnnx.clear();
    _rd_cnnx.sides( sf->threeD(Cnnx)*CNNX_NUM_SIDES );
    cnnx->buffer( _rd_cnnx, sf->get_color(Cnnx), cs, 
                  datadst&Cnnx_flg?data:NULL, dataopac->dop+Cnnx, cmode );
    _rd_cnnx.update_nodalbuff();
  } else if( col_update ) {
    cnnx->recolour(  _rd_cnnx, sf->get_color(Cnnx), cs, 
                  datadst&Cnnx_flg?data:NULL, dataopac->dop+Cnnx, cmode );
    _rd_cnnx.update_col();
  }
  _rd_cnnx.mesh_render();
}


/**
 * @brief  draw vertices for a region
 *
 * @param reg  region if NULL, render
 */
void TBmeshWin::draw_vertices(RRegion* reg)
{
  static bool  first    = true;    //!< first visible region since last redraw?
  PPoint      &pt       = model->pt;
  static int   numTrans = 0;
  std::vector<bool> draw_vert;

  if ( renderMode&RENDER ) {

    dataOpac &dop = dataopac->dop[Vertex];
    bool update = _rd_pt.setup_render_data(context())           ||
                  REDRAW(VBO_Pt_Color|VBO_Position|VBO_Visible) || 
                  (REDRAW(VBO_Colour) && datadst&Vertex_flg)    ||
                  REDRAW(VBO_Pt) ;

    if( !reg ) {
      _rd_pt.radius(pt.size());
      _rd_pt.threeD(threeD(Vertex));
      if( !first && _rd_pt.num() && update ){
        _rd_pt.update_nodalbuff();
        _rd_pt.opaque( _rd_pt.num()-numTrans );
        first = true;
      }
      _rd_pt.mesh_render();
      return;
    }

    pt.threeD( reg->threeD(Vertex) );
    pt.size( reg->size(Vertex) );

    if( update || _rd_pt.dirty() ) {
      if( first ) {
        _rd_pt.clear();
        first = false;
        numTrans = 0;
      }
      
      if( !reg->show(Vertex) || !reg->visible()) return;

      if( !_draw_all_vert ) trim_vertices( draw_vert, reg->show(Cnnx) );
      
      bool translucent = translucency( Vertex, reg->get_color(Vertex) ); 
      
      int nvis = pt.buffer( _rd_pt, 0, pt.num()-1, reg->get_color(Vertex), cs, 
                                         datadst&Vertex_flg?data:NULL, 1, &dop );
      if( translucent ) {
        numTrans += nvis;
      } else if( numTrans ) {
        _rd_pt.buff_rotate(nvis);  // move just drawn opaque elements to the beginning of the buffer
      }
    }

  } else if( renderMode == PICK_VTX ) {                                       // SELECT

    if( !reg ) {
      _select.update_nodalbuff();
      _rd_pt.opaque(-1);
      _select.mesh_render();
      first = true;
      return;
    }

    _select.setup_render_data(context());
    if( first ) {
      _select.clear();
      first = false;
    }
    if( !reg->visible() ) return;

    if( !_draw_all_vert ) trim_vertices( draw_vert, reg->show(Cnnx) );
    
    unsigned int bp=_select.num();
    _select.buff_append( count(pt.vis()->begin(), pt.vis()->end(), true) );
    for ( int i=0; i<model->pt.num(); i++ )
      if ( pt.vis(i) ) _select.buff_vtx( bp++, pt[i], i );
  }
}


/**
 * @brief draw the colour bar in the model window
 */
void
TBmeshWin::draw_cbar()
{
  if( cbgui->disp_on_model() ) {
    float x0, y0;
    cbgui->get_pos( x0, y0 );
    cbgui->cb->ogl_render(x0, y0, this->w(), this->h());
  }
}


/** remove point visibility if not in a surface or connection
 * 
 * \param visvert   work vector
 * \param draw_cnnx are connections drawn in this region
 * \post 
 */
void
TBmeshWin:: trim_vertices( std::vector<bool>& visvert, bool draw_cnnx )
{
  visvert.assign( model->pt.num(), false );

  // connection nodes
  if( draw_cnnx ) {
    const int *cidx = model->_cnnx->obj();
    for( int i=0; i<2*model->_cnnx->num(); i++,cidx++ )
      visvert[*cidx] = model->pt.vis(*cidx);
  }

  // surface nodes
  for ( int s=0; s<model->numSurf(); s++ ) {
    if( !model->surface(s)->visible() ) continue;
    for( auto sn : model->surface(s)->vertices() )
      visvert[sn] = model->pt.vis(sn);
  }
  model->pt.setVis( visvert );
}


void
TBmeshWin::draw_highlights()
{
  if ( !hilighton ) return;

  if( _redraw_state&VBO_Hilight ) {
    _rd_hi.clear();
    int snl[12][MAX_NUM_SURF_NODES+1]; // max 12 tris per element surface

    // hilighted vertex
    float hpt_sz = model->max_size(Vertex);
    if( !model->threeD(Vertex,0) ) {
      hpt_sz *= model->maxdim()/w();
      hpt_sz /= trackball.GetScale();
    }
    hpt_sz+= 2;
    
    _rd_hi.pt( hilight[Vertex], model->pt[hilight[Vertex]], hpt_sz, true );

    // draw highlighted cnnx if one
    if ( model->_cnnx->num()>1 ) {
      const int* c = model->_cnnx->obj( hilight[Cnnx] );
      GLfloat csz =  model->max_size(Cnnx);
      _rd_hi.cnnx(  hilight[Cnnx], model->pt[c[0]], model->pt[c[1]], 
              csz+std::max(3.,0.2*csz), model->threeD(Cnnx)?CNNX_NUM_SIDES:0 );
    }

    // draw highlighted cable if one
    if ( model->_cnnx->num_lines()>1 ) {
      auto ln = model->_cnnx->lines()[ hilight[Cable] ];
      GLfloat csz =  model->max_size(Cnnx);
      _rd_hi.line( hilight[Cable], ln.first, ln.second, model->_cnnx->obj(), 
              model->pt.pt(), csz+std::max(2.,0.1*csz), model->threeD(Cnnx)?CNNX_NUM_SIDES:0 );
    }

    // highlighted surface ele
    if ( model->numSurf() ) {
      int lsurf, lele;
      lele = model->localElemnum( hilight[SurfEle], lsurf );
      int ntri =  model->surface(lsurf)->ele(lele)->ntris();
      GLfloat buff[3*ntri*VBO_ELEM_SZ];
      GLfloat col[4] = {1,1,1,1};
      ntri = model->surface(lsurf)->ele(lele)->buffer( 0, NULL, buff, 0 );
      _rd_hi.surfele( ntri, buff, true, hilight[SurfEle] );
    }

    // draw highlighted volume element
    if ( model->numVol() ) {
      int ntri = model->_vol[hilight[VolEle]]->surfaces( snl, true);
      std::vector<GLfloat> vtx;
      fill_tri_vtx( ntri, snl, model->pt, vtx );
      _rd_hi.volele( ntri, vtx.data(), fill_hitet, hilight[VolEle] );
    }
    if (model->numVol() && vert_asc_obj==VolEle ) {
      //draw volume elements associated with highlighted node

      if ( fill_assc_obj ) {
#pragma omp parallel for
        for ( int i=0; i<model->numVol(); i++ )
          for ( int j=0; j<model->_vol[i]->ptsPerObj(); j++ )
            if ( model->_vol[i]->obj()[j] == hilight[Vertex] ) {
              int ntri = model->_vol[i]->surfaces( snl, true);
              std::vector<GLfloat> vtx;
              fill_tri_vtx( ntri, snl, model->pt, vtx );
#pragma omp critical
              _rd_hi.volele( ntri, vtx.data(), true );
            }
      }
      GLfloat black[]= { 0, 0, 0, 1 };
#pragma omp parallel for
      for ( int i=0; i<model->numVol(); i++ )
        for ( int j=0; j<model->_vol[i]->ptsPerObj(); j++ )
          if ( model->_vol[i]->obj()[j] == hilight[Vertex] ) {
            int ntri = model->_vol[i]->surfaces( snl, true);
            std::vector<GLfloat> vtx;
            fill_tri_vtx( ntri, snl, model->pt, vtx );
#pragma omp critical
            _rd_hi.volele( ntri, vtx.data(), false );
          }

    } else if ( model->numSurf() && vert_asc_obj != Nothing &&
            (!model->numVol() || vert_asc_obj==SurfEle) ) {
      //draw elements associated with highlighted node
      for ( int s=0; s<model->numSurf(); s++ ) {
        std::vector<SurfaceElement*>ele = model->surface(s)->ele();
        for ( int i=0; i<model->surface(s)->num(); i++ ) {
          for ( int j=0; j<ele[i]->ptsPerObj(); j++ )
            if ( ele[i]->obj()[j] == hilight[Vertex] ) {
              GLfloat buff[ele[i]->ntris()*VBO_ELEM_SZ*3];
              GLfloat col[4] = {1,1,1,1};
              int ntri = ele[i]->buffer( 0, NULL, buff, 0 );
              _rd_hi.surfele( ntri, buff, fill_assc_obj, -1 );
            }
        }
      }
    }
    _rd_hi.setup_render_data(context());
    _rd_hi.update_nodalbuff();
  }
  _rd_hi.mesh_render();
}


// draw_axes
void TBmeshWin::draw_axes()
{
  if( ! _axes ) return;
 
  static RenderTris   axes;
  static RenderBuffer axis(1);            //!< axis in z-orientation
  axes.setup_render_data(context());

  const float stick_len = 1.5;
  const float head_len  = 0.2;
  const int   nsides    = 16;                           //!< \#vertices to draw "X","Y","Z"

  if( axis.num() == 0 ) {
    axis.buffer( arrow3D_nvtx( nsides, true ) );
    axes.buffer( arrow3D_nvtx( nsides, true )/axis.prim_size() );
    GLfloat *buff = axis.buffer();
    axis.buff_3Darrow( buff, stick_len, head_len, 0.05, 0.1, nsides, (const GLfloat[4]){0.15,0,1,1});
    axes.set_material( 0, (const GLfloat[]){1,.8,80,1});
    axes.top(true);
  }

  GLfloat    size    = screen_dim()*_axsz;
  Quaternion inv     = trackball.qRot.GetConjugate();
  V3f        offset  = -trackball.GetOrigin() + inv.Rotate(V3f(_axx,_axy,0)*screen_dim()) -
                       inv.Rotate(V3f(trackball.GetTranslation()))/trackball.GetScale();
  
  static GLfloat osize    = -1;
  static V3f     o_offset(std::nanf(""), 0, 0);
  if( osize!=size || o_offset!=offset ){
    osize        = size;
    o_offset     = offset;
    GLfloat *vtx = axes.buffer();
    GLfloat nv[3];
    short_float nn[3];
    short_float *n;
    for( int i=0; i<axis.numVtx(); i++ ) {
      GLfloat* v = axis.buffer()+i*VBO_ELEM_SZ;
      scale( assign( nv, v[2], v[1], -v[0] ), size );
      vec_add( nv, offset.data(), nv );
      n = (short_float*)((char*)v + VBO_NORM_OFFSET);
      assign( nn, n[2], n[1], negShort(n[0]) );
      GLfloat c[4] = {1,0,0,1};         // X = red
      vtx += axes.buff_vtx( vtx, nv, nn, c, 0 );
    }
    for( int i=0; i<axis.numVtx(); i++ ) {
      GLfloat* v = axis.buffer()+i*VBO_ELEM_SZ;
      scale( assign( nv, v[0], v[2], -v[1] ), size );
      vec_add( nv, offset.data(), nv );
      n = (short_float*)((char*)v + VBO_NORM_OFFSET);
      assign( nn, n[0], n[2], negShort(n[1]) );
      GLfloat c[4] = {0,1,0,1};          // Y = green
      vtx += axes.buff_vtx( vtx, nv, nn, c, 0 );
    }
    for( int i=0; i<axis.numVtx(); i++ ) {
      GLfloat* v = axis.buffer()+i*VBO_ELEM_SZ;
      scale( assign( nv, v ), size );
      vec_add( nv, offset.data(), nv );
      short_float* n = (short_float*)((char*)v + VBO_NORM_OFFSET);
      assign( nn, n[0], n[1], n[2] );
      GLfloat c[4] = {0.15,0,1,1};          // Z = blue
      vtx += axes.buff_vtx( vtx, nv, nn, c, 0 );
    }
    axes.opaque(-1);
    axes.update_nodalbuff();
  }
  axes.mesh_render();

  static ScreenTxt st;
  float lw, lh, ldip;
  float fsz       = 48.*sqrt(_axsz);
  float lbl_dist  = ((stick_len+head_len)/2.+0.05)*_axsz;
  // displacement due to trackball dragging
  V3f   dtrans    = trackball.GetTranslation()/model->maxdim();
  // displacement due to changing center of rotation
  auto  displace  = (offset+trackball.GetOrigin())/screen_dim();

  // x axis label
  V3f rxp = trackball.qRot.Rotate( V3f(lbl_dist, 0, 0)+displace )+dtrans;
  st.colour((const GLfloat[4]){1.,0.,0.,1});
  ldip = st.size("X", w(), h(), fsz, lw, lh );
  st.render("X", rxp.X()/asp_xf()-lw/2-ldip, rxp.Y()/asp_yf()-lh/2., w(), h(), fsz);

  // y axis label
  rxp = trackball.qRot.Rotate(  V3f(0, lbl_dist, 0)+displace )+dtrans;
  st.colour((const GLfloat[4]){0.,1.,0.,1});
  ldip = st.size("Y", w(), h(), fsz, lw, lh );
  st.render("Y", rxp.X()/asp_xf()-lw/2, rxp.Y()/asp_yf()-lh/2., w(), h(), fsz);
  
  // z axis label
  rxp = trackball.qRot.Rotate( V3f(0., 0, lbl_dist)+displace )+dtrans;
  st.colour((const GLfloat[4]){0.,0.,1.,1});
  ldip = st.size("Z", w(), h(), fsz, lw, lh );
  st.render("Z", rxp.X()/asp_xf()-lw/2, rxp.Y()/asp_yf()-lh/2., w(), h(), fsz);
}


const int shift_time_scale=10;

int TBmeshWin::handle( int event )
{
  //std::cout << fl_eventnames[event] << std::endl;
  if( renderMode & MV_AXES ) {
    bool shift = Fl::event_state(FL_SHIFT);
    switch (Fl::event_key()) {
        case FL_Up:
            if(shift) 
              _axsz += _axsz<1-_axds/2. ? _axds : 0;
            else
              _axy  += _axy<2-_axdx/2. ? _axdx : 0;
            break;
        case FL_Down:
            if(shift) 
              _axsz -= _axsz>_axds/2. ? _axds : 0;
            else
              _axy  -= _axy>-2.+_axdx/2. ? _axdx : 0;
            break;
        case FL_Left:
            _axx -= _axx>-2+_axdx/2. ? _axdx : 0;
            break;
        case FL_Right:
            _axx += _axx<2-_axdx/2. ? _axdx : 0;
            break;
        case 'r':
            _axx  = _axy = 0;
            _axsz = 1.;
            break;
        default :
            return 0;
    }
    redraw(VBO_Axes);
    return 1;
  } else if ( SELECT(renderMode) ) {
    if ( event==FL_PUSH ) {      //picking
      static int    num_attempts=0;
      static Frame *vtx;
      if( !num_attempts ){
        vtx = new Frame( this, pixel_w(), pixel_h(), true, false );
        vtx->fill_buffer( pixel_w(), pixel_h(), true );
      }
      if( process_hits(*vtx) || num_attempts++==MAX_SELECT_ATTEMPTS ){
        num_attempts=0;
        delete vtx;
        if( renderMode == PICK_AUX ) {
          auxGrid->select(false);
          contwin->auxPckVtx->color(132);
          contwin->auxPckVtx->redraw();
        } else if( renderMode == PICK_VTX ) {
          contwin->pickvert->color(132);
          contwin->pickvert->redraw();
        }
        renderMode = RENDER;
        invalidate();		// reset the viewport and all that for the next redraw
        redraw(VBO_Visible);
      }
    }
    return 1;					// ignore all other events in this mode

  } else if ( event == FL_KEYBOARD || event == FL_SHORTCUT ) {

    int newtm;
    int k=Fl::event_key();
    float tshift=shift_time_scale;
    if( Fl::event_state()&FL_CTRL ) tshift*=tshift;

    switch ( Fl::event_key()) {
      case FL_Right:
        newtm = int(contwin->tmslider->value()+
                    contwin->frameskip->value()*
                    ((Fl::event_state()&FL_SHIFT)?tshift:1));
        if ( newtm <= contwin->tmslider->maximum() && newtm>=0 ) {
          signal_links( 1 );
          set_time( newtm );
        }
        return 1;
        break;
      case FL_Left:     
        newtm = int(contwin->tmslider->value()-
                    contwin->frameskip->value()*
                    ((Fl::event_state()&FL_SHIFT)?tshift:1));
        if ( newtm <= contwin->tmslider->maximum() && newtm>=0 ) {
          signal_links( -1 );
          contwin->tmslider->redraw();
          set_time( newtm );
        }
        return 1;
        break;
      case 'o':   // optimize colour scale
        optimize_cs();
        contwin->mincolval->value(cs->min());
        contwin->maxcolval->value(cs->max());
        return 1;
        break;
      case 'p':     // select a vertex
        contwin->pickvert->color(128);
        contwin->pickvert->redraw();
        select_vertex();
        return 1;
        break;
      case 'r':    // reread data
        if( !dataBuffer ) return 1;
        fl_cursor( FL_CURSOR_WAIT );
        Fl::check();
        get_data( dataBuffer->file().c_str(), contwin->tmslider );
        if ( timeplotter->window->shown() ) timeplot();
        fl_cursor( FL_CURSOR_DEFAULT );
        Fl::check();
        return 1;
        break;
      case 'c':  // put controls on top of window
        if( contwin->separate() ){
          contwin->window->hide();
          contwin->window->position( flwin->x_root(), flwin->y_root() );
          contwin->window->show();
        }
        return 1;
        break;
      case 'h': // highlight sync keyboard shortcut
        SendHiliteSyncMessage();
        return 1;
        break;
      case 't': // time sync keyboard shortcut
        SendTimeSyncMessage();
        return 1;
        break;
      case 'v': // view port keyboard shortcut 
		SendViewportSyncMessage();
		return 1;
		break;
      default:
        return 0;
        break;
    }
  } else if ( event == FL_DND_ENTER || event == FL_DND_LEAVE   ||
              event == FL_DND_DRAG  || event == FL_DND_RELEASE   ) {          // do dragNdrop
    return 1;
  } else if ( event == FL_PASTE ) {          // get dropped file name
    std::string f0 = Fl::event_text();
    //std::cout << f0 << std::endl;
    size_t m = f0.find("://");
    if( m != std::string::npos )
      f0 = f0.substr(m+3);
    while( f0.size()>0 ) {
      size_t m = f0.find("\n");
      std::string file = f0.substr(0,m);
      f0 = (m==std::string::npos)? "" : f0.substr(m+1);
      if( has_ext(file, ".mshz") ){
        contwin->restore_state(file.c_str());
#ifdef USE_HDF5
      } else if( has_ext(file, ".datH5") ) {
        HDF5DataBrowser *brow =  new HDF5DataBrowser( file.c_str(), this );
        contwin->add_recent(file.c_str(),HDF5);
#endif
      } else if( has_ext(file, ".vpts") ) {
        getVecData(contwin->tmslider, file.c_str());
        contwin->add_recent(file.c_str(),VECT);
      } else if( has_ext(file, ".pts")   || 
              has_ext(file, ".pts_t")  ) {
        bool fake = has_ext(file.c_str(), ".pts");
        if( !readAuxGrid( contwin->tmslider, file.c_str(), fake ) ) {
          contwin->auxgridgrp->activate();
          contwin->add_recent(file.c_str(),AUX);
        }
      } else if( has_ext(file.c_str(), ".igb") ) {
        get_data(file.c_str(), contwin->tmslider );
        contwin->add_recent(file.c_str(),IGB);
      } else if ( has_ext( file.c_str(), ".dat" ) ) {
        get_data(file.c_str(), contwin->tmslider );
        contwin->add_recent(file.c_str(),ASCII);
      } else if ( has_ext( file.c_str(), ".dynpt" ) ){
        read_dynamic_pts( file.c_str(), contwin->tmslider );
        contwin->add_recent(file.c_str(),DYN_PTS);
      }
    }
    return 1;
  } else{										    // do trackball thing
    if ( event == FL_PUSH)   // send key presses to widget
      Fl::focus(this->flwin);
    if( !_norot || Fl::event_button()!=FL_LEFT_MOUSE  || Fl::event_state(FL_SHIFT) ||
                   Fl::event_state(FL_CTRL)                                            )
      return Fl_Gl_Tb_Window::handle(event);
  }
  return 0;
}


/** set trackball and window information when reading a new model
 *  
 *  \param flwindow window identifier
 *  \param modname  model name
 */
void  TBmeshWin::set_windows( Fl_Window *flwindow )
{
  flwin = flwindow;
  flwintitle = "meshalyzer: ";
  std::string modname = model->file();
  auto i0=modname.rfind("/");
  if ( i0 < std::string::npos ) modname=modname.substr(i0+1,std::string::npos);
  flwintitle += modname;
  flwin->label( flwintitle.c_str() );

  // set the dimensions for the trackball
  float maxdim = model->maxdim();
  const GLfloat *poff = model->pt_offset();
  trackball.mouse.SetOglPosAndSize(-maxdim, maxdim, 2*maxdim, 2*maxdim );
  trackball.size = maxdim;
  trackball.SetOrigin( -poff[0], -poff[1], -poff[2] );
  cplane->set_dim( maxdim*1.3 );

  if( model->twoD() ){ 
    _norot=true;
    contwin->norot->set();
    contwin->illBut->value(0);
    contwin->illBut->do_callback();
    //illunate(false);
  } else
    illuminate();
    
  disp = asSurface;
}


/** read in the geometrical description
 *
 * \param flwindow
 * \param fnt      base file to open
 * \param base1    points begin numbering at 0
 * \param no_elems do not read element file
 */
void TBmeshWin::read_model( Fl_Window *flwindow, const char* fnt, 
		bool no_elems, bool base1 )
{
  std::string fname = fnt;

  if ( fnt == NULL || !fname.length() ) {
      
    // if available, go to the first Model Dir directory
    char  *moddir=NULL;
    if( getenv("MESHALYZER_MODEL_DIR") ) {
      moddir = strdup( getenv("MESHALYZER_MODEL_DIR") );
      char *p = strchr( moddir, ':' );
      if( p ) *p = '\0';
    }
  
    Fl_File_Chooser modchooser( moddir, "*.pts*", Fl_File_Chooser::SINGLE, "Pick one" ); 
    modchooser.show();
    while( modchooser.shown() )
      Fl::wait();

    fname = modchooser.value();
    if (fname == "") {
      fprintf(stderr, "No file selected.  Exiting.\n");
      exit(0);
    }
  }

#ifdef USE_VTK
  if ( has_ext(fname, ".vtu") ) {
      if ( !model->read_vtu( fname.c_str(), no_elems ) ) return;
  } else if ( has_ext(fname, ".vtk") ) {
      if ( !model->read_vtu( fname.c_str(), no_elems, true ) ) return;
  } else
#endif
    {if ( !model->read( fname.c_str(), base1, no_elems ) ) return;}

  set_windows( flwindow );
}


#ifdef USE_HDF5
void TBmeshWin::read_model(Fl_Window* flwindow, hid_t hdf_file, bool no_elems,
    bool base1)
{
  if (!model->read(hdf_file, base1, no_elems)) return;
  set_windows( flwindow );
}
#endif


/** add a surface by reading in a .tri(s) file or a .surf file
 *
 * \param file  file containing surface elements
 *
 * \return \#surfaces added
 */
int TBmeshWin :: add_surface( const char *fn )
{
  int nsa = -1; 
  if( has_ext( fn, ".tri" ) || has_ext( fn, ".tris" ) )
    nsa=model->add_surface_from_tri( fn );
  else 
    if( has_ext( fn, ".surf" ) )
      nsa=model->add_surface_from_surf( fn );
  if( nsa<=0 )
    fl_alert( "No compatible surfaces found in %s", fn );
  return nsa;
}


/*
 * return color of object for surface s
 */
GLfloat* TBmeshWin:: get_color( Object_t obj, int s )
{
  if ( s<0 ) s=0;

  if ( obj == VolEle )
    return tet_color;
  else if ( obj == SurfEle && model->numSurf() )
    return model->surface(s)->outlinecolor();
  else if ( obj == Surface && model->numSurf() )
    return model->surface(s)->fillcolor();
  else
    return model->get_color(obj, s );
}


void TBmeshWin:: set_color( Object_t obj, int s, float r, float g, float b, float a )
{
  if ( obj==VolEle ) {
    tet_color[0] = r;
    tet_color[1] = g;
    tet_color[2] = b;
    tet_color[3] = a;
  } 
  model->set_color( obj, s, r, g, b, a );
}


void TBmeshWin :: visibility( bool* reg, bool a )
{
  for ( int i=0; i<model->_numReg; i++ )
    if ( reg[i] )
      model->visibility( i, a );
  // redetermine the interpolated cutting planes
  for ( int i=0; i<NUM_CP; i++ )
    if ( cplane->drawIntercept(i) )
      determine_cutplane(i);
  redraw(VBO_Visible);
}


void TBmeshWin :: opacity( int s, float opac )
{
  model->opacity( s, opac );
  redraw(VBO_Colour);
}


/** determine the type of data reader to open
 *
 *  if the file is too big to fit into memory, use a threaded reader
 *
 * \param fn filename
 *
 */
DataReaderEnum TBmeshWin::getReaderType( const char *fn )
{
  size_t getFreePages(void);
  int    getNumberTimes( const char * );

  size_t memoryAvail = getFreePages() +
                     numframes*model->pt.num()*sizeof(DATA_TYPE)/getpagesize();

  size_t memreq = getNumberTimes( fn );

  memreq *= model->pt.num()*sizeof(DATA_TYPE)/ getpagesize();

  if ( memoryAvail < memreq )
    return OnDemand;
  else
    return AllInMem;
}

/** read in a data file 
 *
 * \param fn     file name
 * \param mslide time slider widget
 *
 * \returns non-zero iff no error
 */
int
TBmeshWin :: get_data( const char *fn, Myslider *mslide )
{
  DataClass<DATA_TYPE>* newDataBuffer=NULL;

  try {
        //newDataBuffer = new DataAllInMem<DATA_TYPE>( fn, model->pt.num(),model->base1() );
        newDataBuffer = new DataOnDemand<DATA_TYPE>( fn, model->pt.num(),
               (model->numVol()?model->numVol():model->numSurfEle())+model->num_cnnx() ); 
  } 
  catch( CompressedFileExc cf ) {
    fl_alert( "Please uncompress data file: %s", cf.file.c_str() );
    return 1;
  }
  catch ( PointMismatch pm ) {
    fl_alert( "%s\nPoints number mismatch: %s", fn, pm.print() );  
    return 2;
  }
  catch ( EmptyDataFile pm ) {
    fl_alert( "%s\n: %s", fn, pm.print() );  
    return 2;
  }
  catch (...) {
    fl_alert("Unable to open data file: %s", fn );
    return 3;
  }

  if( max_time(ScalarDataGrid)>0 && newDataBuffer->max_tm()>0 && 
                                         newDataBuffer->max_tm()!=max_time() ) {
    fl_alert("%s","Incompatible number of frames in data" );
    delete newDataBuffer;
    return 4;
  }

  if ( dataBuffer != NULL ) delete dataBuffer;
  dataBuffer = newDataBuffer;

  numframes = dataBuffer->max_tm()+1;
  if ( numframes >1)
    have_data = Dynamic;
  else if ( numframes == 1 )
    have_data = Static;
  else
    return 0;
  if ( tm>=numframes )
    tm = 0;
  data = dataBuffer->slice(tm);

  timevec = (DATA_TYPE *)realloc( timevec, numframes*sizeof(DATA_TYPE) );

  if ( mslide != NULL ) {
    mslide->maximum( max_time() );
    mslide->value( tm );
    mslide->redraw();
  }

  if ( contwin->read_recalibrate->value() ) {
    optimize_cs();
    contwin->mincolval->value(cs->min());
    contwin->maxcolval->value(cs->max());
  }

  // window title
  std::string fname = fn;
  auto i0 = fname.rfind("/");
  if ( i0 < std::string::npos ) fname= fname.substr(i0+1,std::string::npos);
  i0 = flwintitle.find(" --- ");
  flwintitle = flwintitle.substr(0,i0)+" --- "+fname;
  flwin->label( flwintitle.c_str() );
  
  isosurfwin->islDirty(true);
  isosurfwin->issDirty(true);

  timeplotter->datafile( fn );
  if ( timeplotter->window->shown() ) 
    timeplot();

  if( dataBuffer->ele_based() ) {
    std::cerr << "Finding volume associated with surface elements"<<std::endl;
    model->associate_surf_with_vol();
    std::cerr << "Finished\n"<<std::endl;
  }

  redraw(VBO_Colour);

  return 0;
}

void TBmeshWin :: optimize_cs(bool update)
{
  if( update ) {
    contwin->mincolval->value(cs->min());
    contwin->maxcolval->value(cs->max());
  } else {
    if( dataBuffer==NULL ) return;
    cs->calibrate( dataBuffer->min(tm), dataBuffer->max(tm) );
  }
  redraw(VBO_Colour);
}

void TBmeshWin :: randomize_color( Object_t obj )
{
  model->randomize_color( obj );
  redraw(VBO_Colour);
}


void TBmeshWin :: hiliteinfo()
{
  model->hilight_info( hinfo, hilight, data, data&&dataBuffer->ele_based() );
}


/** when something is clicked in the hilight info window, determine if it is a new object to 
 *  be highlighted
 *
 *  \param n number of selected line in window
 */
void
TBmeshWin :: select_hi( int n )
{
  if ( !n ) return;
  const char *txt = hinfo->text(n);
  Fl::copy( txt, strlen(txt), 2 );

  // only process selected line if first character is a number
  int d;
  if ( sscanf( txt, "%d", &d )!=1 )
    return;

  int i=n;
  Object_t objtype;
  MyValueInput *obin;

  while ( i>=1 )
    if ( strstr(  hinfo->text(i), "node" ) ) {
      objtype=Vertex;
      obin = contwin->verthi;
      break;
    } else if ( strstr(  hinfo->text(i), "Attached surface" ) ) {
      objtype=SurfEle;
      obin = contwin->elehi;
      break;
    } else if ( strstr(  hinfo->text(i), "volume" ) ) {
      objtype=VolEle;
      obin = contwin->tethi;
      break;
    } else if ( strstr(  hinfo->text(i), "cable" ) ) {
      objtype=Cable;
      obin = contwin->cabhi;
      break;
    } else if ( strstr(  hinfo->text(i), "connection" ) ) {
      objtype=Cnnx;
      obin = contwin->cnnxhi;
      break;
    } else if ( strstr(  hinfo->text(i), "surf" ) ) {
      objtype=Surface;
      i = 0;				// ignore selection
      break;
    } else
      i--;

  int ho;
  if ( i>1 && sscanf( txt, "%d", &ho )==1 ) {
    int lineno = hinfo->topline();
    obin->value(ho);
    highlight(objtype, ho );
    hinfo->topline( lineno );
  }
}

void TBmeshWin::output_png( const char* fn, Sequence *seqwidget, bool trim )
{
  bool sequence = (seqwidget!=NULL);
  int start=tm;
  int stop;
  static std::string foutname;
  foutname=fn;

  if ( sequence ) {
    Fl::flush();
    seqwidget->movieprog->minimum( 0 );
    seqwidget->movieprog->maximum( 1. );
    seqwidget->movieprog->value( 0 );
    seqwidget->movieprog->redraw();
    seqwidget->window->label(fn);
    seqwidget->window->show();
    seqwidget->window->redraw();
    Fl::flush();
    stop = (int)seqwidget->lastFrameNum->value();
  } else {
    stop = tm;
    if( !has_ext(fn, ".png") )
      foutname += ".png";
  }


  flwin->show();
  int update_period = 5;	// number of frames after which to update progress
  int last_update=tm-update_period-10;
  int frameskip=int(contwin->frameskip->value());
  Frame frame( this, pixel_w(), pixel_h()  );
  for ( ; tm<=stop; tm+=frameskip ) {

    if( model->pt.num_tm() ) model->pt.time(tm);//dynamic points

    if ( sequence ) {
      foutname = fn;
      std::string::size_type i0 = foutname.rfind(".");
      if ( i0 < std::string::npos ) foutname= foutname.substr(0, i0);
      char number[6];
      snprintf( number, 6, "%05d", tm );
      foutname = foutname + number + ".png";

      if ( tm-last_update>update_period ) {
        seqwidget->update( (float)(tm-start)/(stop-start));
        Fl::check();
        last_update = tm;
      }
      Fl::check();
      if( Fl::pushed()==seqwidget->abortButton || seqwidget->abort() )break;
    }
    sully(VBO_Time);
    frame.dump( foutname, trim );
  }
  if ( sequence ) {
    seqwidget->update(1.);
    Fl::check();
  }
  tm= start;
  if( model->pt.num_tm() ) model->pt.time(tm);//dynamic points
  redraw(VBO_Time);
  Fl::flush();
}


/** control lighting in the model
 *
 *  \param max maximum model dimension
 *
 *  \Todo Lighting is messed up big time. For so,e reason, back and front are
 *        mixed up but if I switch the normals, it does not reverse front
 *        and back face lighting, but mmakes it all bad
 */
void
TBmeshWin::illuminate(bool on)
{
  lightson = on;

  GLfloat diff_intensity = 0;
  GLfloat spec_intensity = 0;
  GLfloat am             = 1;
  GLfloat fresexp        = 3;

  if ( on ) {
    diff_intensity = contwin->diffuseslide->value();
    spec_intensity = contwin->specularslide->value();
    am             = contwin->ambientslide->value();
    fresexp        = contwin->fresnelslide->value();
  }

  _rd_wf.ambient = am;
  _rd_op.lighting( am, diff_intensity, spec_intensity, fresexp, lit_bf );
  _rd_cnnx.lighting( am, diff_intensity, spec_intensity, fresexp, lit_bf );
  _rd_pt.lighting( am, diff_intensity, spec_intensity, fresexp, lit_bf );

  if( auxGrid ) auxGrid->lighting( am, diff_intensity, spec_intensity, fresexp, lit_bf );

  redraw();
}


/** read in vector data
 * 
 * \param vp      slider
 * \param vptfile basename of vector data file
 *
 * \return nonzero if an error
 */
int
TBmeshWin::getVecData( void *vp, const char* vptfile )
{
  VecData* newvd;

  try {
    newvd = new VecData( vptfile );
  } catch (...) {
    return 1;
  }
  // make sure there is data and it matches the number of time instances
  if( max_time(VecDataGrid)>0 && newvd->maxtime()>0 && 
                                            newvd->maxtime() != max_time() ) {
    fl_message( "Number of times in vector data does not agree" );
    delete newvd;
    return 2;
  }

  if ( vecdata != NULL ) {
    *newvd = vecdata;
    delete vecdata;
  }

  vecdata = newvd;
  vecdata->optimize_len( tm );
  ((Myslider *)vp)->maximum( max_time() );
  contwin->update_vecdata( vecdata );
  contwin->window->redraw();
  redraw(VBO_Vector);
  return 0;
}


/**
 * @brief iread in auxiliary grid
 *
 * @param vp     time slider   
 * @param agfile file to read
 * @param fake   inot a true auxiliary file, just a regiular model
 *
 * @return 
 */
int
TBmeshWin::readAuxGrid( void *vp, const char* agfile, bool fake )
{
  AuxGrid* newAuxGrid;

  try {
    if( dataBuffer ) {
        newAuxGrid = new AuxGrid( agfile, this, auxGrid, fake, dataBuffer->t0(), dataBuffer->dt() );
    }else
        newAuxGrid = new AuxGrid( agfile, this, auxGrid, fake );
  } catch (...) {
    return 1;
  }

  // make sure there is data and it matches the number of time instances
  if( max_time(AuxDataGrid)>0 && newAuxGrid->num_tm()>1 && 
                                        newAuxGrid->num_tm()-1 != max_time() ) {
    fl_message( "Number of times in Aux Grid does not agree" );
    delete newAuxGrid;
    return 2;
  }
  if( auxGrid ) {
    newAuxGrid->cs.scale( auxGrid->cs.scaleStr() );
    delete auxGrid;
    newAuxGrid->update_plotter();
  }

  auxGrid = newAuxGrid;
  illuminate(illuminated());
  ((Myslider *)vp)->maximum( max_time() );
  contwin->window->redraw();
  redraw(VBO_Aux);
  return 0;
}


/** return maximum time to display
 *
 * \note There are possibly 4 grids which must all agree on the number of time
 *       instances. Grids with no data or only 1 instance of data
 *       are compatible with any number of time frames since they are
 *       static
 */
int TBmeshWin::max_time( GridType ignore )
{
  if( ignore!=ScalarDataGrid && have_data == Dynamic )
    return numframes-1;
  
  if( ignore!=VecDataGrid && vecdata && vecdata->maxtime()>0 )
    return vecdata->maxtime();

  if( ignore!=AuxDataGrid && auxGrid && auxGrid->num_tm()>0 )
    return auxGrid->num_tm()-1;

  if( ignore!=DynPtGrid && DynPtGrid && model->pt.num_tm()>0 )
    return model->pt.num_tm()-1;

  return 0;
}


// enter "vertex selection by mouse" mode
void
TBmeshWin:: select_vertex()
{
  renderMode = PICK_VTX;
}


/**
 * @brief process vertex selection
 *
 * @param frame scene rendered to memory
 *
 * @return true iff a vertex was selected
 */
bool
TBmeshWin::process_hits( Frame &frame )
{
  static std::vector<int> dx, dy;
  static bool init = false;
  const int search_radius = 30;

  int x = Fl::event_x()*pixels_per_unit();
  int y = Fl::event_y()*pixels_per_unit();

  if( !init ) {
    // build list of points to chck around selected point
    // displacements are ordered from closest to farthest
    init = true;
    for( int i=0; i<search_radius; i++ )
      for( int j=0; j<=i; j++ ){
        dx.push_back(i); dy.push_back(j);
        if( i ) { dx.push_back(-i); dy.push_back( j); }
        if( j ) { dx.push_back( i); dy.push_back(-j); }
        if( i && j ) { dx.push_back(-i); dy.push_back(-j); }

        if( i==j )continue;

        dx.push_back(j); dy.push_back(i);
        if( j ) { dx.push_back(-j); dy.push_back( i); }
        if( i ) { dx.push_back( j); dy.push_back(-i); }
        if( i && j ) { dx.push_back(-j); dy.push_back(-i); }
      }
  }

  std::array<GLubyte,4> pickedCol;
  int select_vtx=-1;
  int dp = 0;
  int maxVtx = renderMode==PICK_VTX?model->pt.num():auxGrid->num_vert();
  while( (select_vtx<0 || select_vtx>=maxVtx) && dp<dx.size() ) {
    int tx = x + dx[dp];
    int ty = pixel_h()-1-y + dy[dp++];

    if( tx<0 || tx>=pixel_w() || ty<0 || ty >=pixel_h() )
      continue;
     
    frame.pixel_color( tx, ty, pickedCol.data() );
    select_vtx = _select.col2i( pickedCol.data() );
  }

  if( renderMode == PICK_VTX ) {
    if( select_vtx>=0 && select_vtx<model->pt.num() ) {
      highlight( Vertex, select_vtx );
      contwin->verthi->value( select_vtx );
      if ( timeplotter->window->shown() ) timeplot();
      return true;
    }
  } else if ( renderMode == PICK_AUX ) {
    if( select_vtx>=0 && select_vtx<auxGrid->num_vert() ) {
      float v;
      auxGrid->highlight_vertex( select_vtx, v, true );
      contwin->auxhivert->value( select_vtx );
      contwin->auxvertval->value( v );
      return true;
    }
  }
  return false;
}


/** dump the clipping planes and vertices which were drawn into a file
 *
 *  the file has the format: \n
 *  no_clipping_planes   \n
 *  a b c d   (coefficients for the plane 1 as defined in OpenGL) \n
 *  a b c d   (coefficients for the plane 2 as defined in OpenGL) \n
 *  .  \n
 *  .  \n
 *  a b c d   (coefficients for last plane as defined in OpenGL) \n
 *  visible_node1   \n
 *  visible_node2   \n
 *  visible_node3   \n
 *  .   \n
 *  .   \n
 *  .   \n
 *  visible_node?
 */
void TBmeshWin::dump_vertices(const char *fname)
{
  std::vector<bool> ptDrawn(model->pt.num(), false );

  for ( int s=0; s<model->_numReg; s++ ) {
    
    RRegion *reg = model->region(s);
    if( !reg->show(Vertex) || !reg->visible()) return;

    model->pt.setVis(reg->pt_membership());
    std::vector<bool> draw_vert;
    if( !_draw_all_vert ) trim_vertices( draw_vert, reg->show(Cnnx) );

    for( int p=0; p<model->pt.num(); p++ ) 
      if( model->pt.vis(p) ) {
        for( int i=0; i<NUM_CP; i++ ) 
          if( clipping(i) && dot(CLIP_EQN[i],model->pt[p])+CLIP_EQN[i][3]<0 ){
            model->pt.vis(p, false);
            break;
          }
        ptDrawn[p] = ptDrawn[p] || model->pt.vis(p);
      }
  }

  std::ofstream of( fname );

  //output the clipping planes
  int cp_on = 0;
  for ( int i=0; i<NUM_CP; i++ ) 
	if( cplane->on(i) ) cp_on++;
  of << cp_on << std::endl;
  for ( int i=0; i<NUM_CP; i++ ) {
	GLfloat   ctr[4];
    if ( cplane->on(i) ) {
      auto cp_coeff = cplane->plane(i);
      for ( int j=0; j<3; j++ ) of << cp_coeff[j] << " ";
	  // adjust interecept for centering
	  model->pt.offset(ctr);
	  of << cp_coeff[3]+V3f(cp_coeff).Dot(V3f(ctr)) << std::endl;
    } 
  }

  // output the vertices
  of << count( ptDrawn.begin(), ptDrawn.end(), true ) << std::endl;
  for ( int i=0; i<model->pt.num(); i++ ) if ( ptDrawn[i] ) of << i << std::endl;

  of.close();
}


// output a PS or PDF file
void TBmeshWin::output_pdf( char *fn, bool PDF )
{
}


// plot the time series for a vertex
void TBmeshWin::timeplot()
{
  if ( have_data==NoData || numframes<2 ) return;

  dataBuffer->time_series( hilight[Vertex], timevec );
  timeplotter->window->show();
  timeplotter->set_data(  hilight[Vertex], numframes, timevec, tm, 
                               dataBuffer->dt(), dataBuffer->t0() );
  timeplotter->window->redraw();
}


/** Draw the clipping plane
 *
 * \param cp the clipping plane
 */
void TBmeshWin::draw_clip_planes()
{
  const GLfloat clipPlaneOpacity = 0.5;

  static RenderTris clip_planes;
  if( clip_planes.setup_render_data(context()) )
    clip_planes.set_material(0,(const GLfloat[]){0.,0.7,80.,1.});
  clip_planes.clear();

  for ( int cp=0; cp<NUM_CP; cp++ ) {
    if ( !cplane->on(cp) || !cplane->visible(cp) ) 
      continue;

    GLfloat   planeColor[4] = { 0, 1, 0, clipPlaneOpacity }; //translucent green

    int v0, v1, vf;
    GLfloat* x = cplane->plane(cp);
    if ( fabs(x[2])>fabs(x[0]) && fabs(x[2])>fabs(x[1]) ) {//mostly in z-direction
      v0 = 0; v1 = 1; vf = 2;
    } else if ( fabs(x[1]) > fabs(x[0]) ) {	// mostly in y direction
      v0 = 2; v1 = 0; vf = 1;
    } else {									// mostly in x direction
      v0 = 1; v1 = 2; vf = 0;
    }

    GLfloat vert[4][3];
    const GLfloat *poff = model->pt_offset();
    for ( int i=0; i<4; i++ ) {
      vert[i][v0] = 2*(2*(!i||i==3)-1)*model->maxdim()+poff[v0];
      vert[i][v1] = 2*(2*(i>1)-1)*model->maxdim()+poff[v1];
      vert[i][vf] = -(x[v0]*vert[i][v0]+x[v1]*vert[i][v1]+x[3])/x[vf];
    }

    GLfloat *buff = clip_planes.buff_append(2);
    SHORTVEC( shx, x[0], x[1], x[2] );
    clip_planes.buff_vtx( buff,               vert[0], shx, planeColor, 0 ); 
    clip_planes.buff_vtx( buff+VBO_ELEM_SZ*1, vert[1], shx, planeColor, 0 ); 
    clip_planes.buff_vtx( buff+VBO_ELEM_SZ*2, vert[2], shx, planeColor, 0 ); 
    clip_planes.buff_vtx( buff+VBO_ELEM_SZ*3, vert[2], shx, planeColor, 0 ); 
    clip_planes.buff_vtx( buff+VBO_ELEM_SZ*4, vert[3], shx, planeColor, 0 ); 
    clip_planes.buff_vtx( buff+VBO_ELEM_SZ*5, vert[0], shx, planeColor, 0 ); 
  }
  if( !clip_planes.num() ) return;
  bool cpon[NUM_CP];
  clip_off(cpon);
  clip_planes.opaque(0);
  clip_planes.update_nodalbuff();
  clip_planes.mesh_render();
  clip_on( cpon );

}


/** write out the frame buffer after a change to it is has been made
 *
 * \param fname base name for output files
 */
void TBmeshWin::record_events( char* fn )
{
  unsigned long old_framenum=framenum;
  int           num=0;

  Frame frame( this, pixel_w(), pixel_h() );
  while ( recording ) {
    Fl::wait();						 // process events one at a time
    if ( old_framenum != framenum ) { // see if the draw routine has been called
      old_framenum = framenum;
      std::string fname = fn;
      std::string::size_type i0 = fname.rfind(".");
      if ( i0 < std::string::npos ) fname = fname.substr(0, i0);
      char fnum[32];
      snprintf( fnum, 32, "%05d", num++ );
      fname += fnum;
      frame.write( pixel_w(), pixel_h(), fname );
    }
  }
  std::stringstream msg;
  msg << num << " frames output";
  fl_alert( "%s", msg.str().c_str() );
}


/** change visibility of a region
 *
 *  \param region lists of region integers
 *  \param nr     \#regions
 *  \param on     whether a region is visible
 */
void TBmeshWin::region_vis( int *region, int nr, bool* on )
{
  for ( nr--; nr>=0; nr-- )
    model->region(nr)->visible( on[nr] );
  redraw(VBO_Visible);
}


/** surface visibility
 *
 * \param l list of surfaces affected
 * \param v visibility
 */
void
TBmeshWin::surfVis( std::vector<int>&l, bool v )
{
  for ( int i=0; i<l.size(); i++ )
    model->surface(l[i])->visible(v);
  redraw(VBO_Visible);
}


/** surface element filled
 *
 * \param l list of surfaces affected
 * \param v visibility
 */
void
TBmeshWin::surfFilled( std::vector<int>& l, bool f )
{
  for ( int i=0; i<l.size(); i++ )
      model->surface(l[i])->filled(f);
  redraw(VBO_Visible);
}


/** surface element outlined
 *
 * \param l list of surfaces affected
 * \param f outlined
 */
void
TBmeshWin::surfOutline( std::vector<int>&l, bool f )
{
  for ( int i=0; i<l.size(); i++ )
      model->surface(l[i])->outline(f);
  redraw(VBO_Visible);
}


/** surface element outline color
 *
 * \param l list of surfaces affected
 * \param c colour
 */
void
TBmeshWin::surfOutColor( std::vector<int>&l, GLfloat* c )
{
  for ( int i=0; i<l.size(); i++ )
    model->surface(l[i])->outlinecolor(c[0],c[1],c[2],c[3]);
  redraw(VBO_Colour);
}


/** surface element fill color
 *
 * \param l list of surfaces affected
 * \param c colour
 */
void
TBmeshWin::surfFillColor( std::vector<int>&l, GLfloat *c )
{

  for ( int i=0; i<model->numSurf(); i++ )
    model->surface(l[i])->fillcolor(c[0],c[1],c[2],c[3]);
  redraw(VBO_Colour);
}


/** draw the cut planes
 *
 * \pre cut surface elments have been computed in determine_cutplane()
 */
void
TBmeshWin::draw_cut_planes( )
{
  glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
  glEnable( GL_LINE_SMOOTH );
  glEnable( GL_POLYGON_SMOOTH );

  if( isosurfwin->isolineOn->value() )
    glEnable(GL_POLYGON_OFFSET_FILL );

  bool on_tr = (dataopac->dop[Surface].on()||_cutplane_col[3]<OPAQUE_LIMIT);
 
  for ( int i=0; i<NUM_CP; i++ ) {
    if ( !cplane->drawIntercept(i) || !_cutsurface[i]->num()) continue;

    CutSurfaces *cut = _cutsurface[i];
    bool showData    = cplane->datafied(i) && have_data!=NoData;
    bool recolour    = (showData && REDRAW(VBO_Colour)) || (!showData&&REDRAW(VBO_Clip_Color));
    char cmode       = CMODE(facetshading, dataBuffer && dataBuffer->ele_based(), col_by_elem);

    // buffer the geometrical information
    if( cut->fresh() || REDRAW(VBO_Clip) ){   //1st use after recalculation or clip plane changed
      cut->init_buff(context());
      GLfloat *vbo = cut->allocate_buffer(); 
      for ( int e=0; e<cut->num(); e++ ) {
        // copy normal for all nodes
        short_float n[cut->ele(e)->ptsPerObj()*3];
        memcpy( n, cut->sh_norm(), 3*sizeof(n[0]) );
        for ( int v=1; v<cut->ele(e)->ptsPerObj(); v++ )
          memcpy( n+3*v, n, 3*sizeof(n[0]) );
        vbo += cut->ele(e)->buffer( 0, n, vbo, 0, cmode )*3*VBO_ELEM_SZ;
      }
    }

    // colour the plane
    if( cut->fresh()                                    ||     // first use after recalculated
            REDRAW(VBO_Clip)                            ||     // clipping plane changed position
            recolour                                      ){   // data displayed changed
      GLubyte *cbo = cut->colbuff(); 
      for ( int e=0; e<cut->num(); e++ ) {
        DATA_TYPE idata[cut->ele(e)->ptsPerObj()];
        if( cmode&CM_ELEM ) {
          for ( int v=0; v<cut->ele(e)->ptsPerObj(); v++ ) 
            idata[v] = data[cut->src_ele(e)];
        }else if ( showData ) {
          for ( int v=0; v<cut->ele(e)->ptsPerObj(); v++ ) {
            if( _branch_cut )
              idata[v] = cut->interpolate( e, data, v, _branch_range );
            else
              idata[v] = cut->interpolate( e, data, v );
          }
        }
        cut->colour_elem(e,cbo,_cutplane_col,cs,showData?idata:NULL,dataopac->dop+Surface,cmode);
      }
    }
  }

  bool surf_clips[NUM_CP];
  if( !_clip_cut_surface ) clip_off(surf_clips);

  for ( int i=0; i<NUM_CP; i++ ) {
    CutSurfaces *cut = _cutsurface[i];
    if ( !cplane->drawIntercept(i) || !cut->num()) continue;

    if(_clip_cut_surface) clip_off( i );

    bool showData = cplane->datafied(i) && have_data!=NoData;
    bool recolour = (showData && REDRAW(VBO_Colour)) || (!showData&&REDRAW(VBO_Clip_Color));

    if( cut->fresh() || REDRAW(VBO_Clip) )
      cut->update_buff();
    else if( recolour )
      cut->update_colbuff();

    cut->translucent( on_tr );
    cut->render();

    if(_clip_cut_surface) clip_on( i );
  }

  if( !_clip_cut_surface ) clip_on(surf_clips);
}


/** determine the cutting plane based on point visibility
 *
 *  \param cp index of cutting plane
 *
 *  \note We are assuming the number of points does not change between
 *        calls
 */
void
TBmeshWin :: determine_cutplane( int cp )
{
  static bool fst=true;
  static char* cpvis;
  if ( fst ) {
    cpvis = new char[model->pt.num()];
    fst = false;
  }
  memset( cpvis, 0, model->pt.num()*sizeof(char) );

  GLfloat* cpd = cplane->plane(cp);
  GLfloat  mag = magnitude( cpd );
  GLfloat  cpf[4];
  for ( int j=0; j<4; j++ ) cpf[j] = cpd[j]/mag;

  if ( _cutsurface[cp] == NULL )
    _cutsurface[cp] = new CutSurfaces( cpf );
  else {
    _cutsurface[cp]->clear(cpf);
  }

  // first determine which points belong to visible regions
  for ( int r=0; r<model->_numReg; r++ ) {
    if ( !model->region(r)->visible() ) continue;
    std::vector<bool>& memb = model->region(r)->pt_membership();
#pragma omp parallel for
    for ( int i=0; i<memb.size(); i++ )
      if ( memb[i] ) cpvis[i]=true;
  }

  // determine the points clipped by the plane
  // for visible points: ax+by+cz+d > 0
  const GLfloat *pp = model->pt.pt();
  for ( int j=0; j<model->pt.num(); j++ )
    if ( cpvis[j] && (dot( pp+3*j, cpf )<-cpf[3]) )
      cpvis[j] = false;

  for ( int r=0; r<model->_numReg; r++ ) {
    if( !model->region(r)->visible() ) 
      continue;
#pragma omp parallel for
    for ( int e=0; e<model->numVol(); e++ ) {
      if( model->region(r)->ele_member(e) ) {
        Interpolator<DATA_TYPE> *interp;
        SurfaceElement *se = model->_vol[e]->cut( cpvis, cpf, interp );
        if ( se!= NULL ) {
#pragma omp critical
          _cutsurface[cp]->addEle( se, interp, e );
        }
      }
    }
  }
}


/** draw isovalue lines
 *
 *  we check if the values in the widget have changed so that we do not
 *  recompute lines needlessly
 */
void
TBmeshWin::draw_iso_lines()
{
  IsosurfControl *isc = isosurfwin;

  if( have_data==NoData || !((bool)isc->isolineOn->value()) )
    return;

  bool update  = _redraw_state&(VBO_Isoline|VBO_Isoline_Color);
       update |=  isc->islDirty() || !isoline || tm!=isoline->tm();

  if( update){
    if( isc->islDirty() || !isoline || tm!=isoline->tm() || VBO_Isoline ) {
      if( isoline ) delete isoline;
      isoline=new IsoLine( isc->isolineVal0->value(), isc->isolineVal1->value(),
            isc->isoNumLines->value(), tm );
      isoline->closePt( isc->isoptclose->value() );
    }
    isoline->threeD( isc->threeD() );
    isoline->color( isc->islColor() );
    if( _branch_cut ) 
      isoline->branch( true, _branch_range[0], _branch_range[1] );
    isoline->clear();

    for ( int s=0; s<model->numSurf(); s++ ) {
      if( !model->surface(s)->visible() && isc->restricted() )continue;
      if( isc->islDirty() || VBO_Isoline ){
        isoline->process( model->surface(s), data, !isc->isoLineRestrict->value(), s+1);
        isoline->set_clip( s+1, surfclip(s) ); // surf 0 is for cut planes
      }
    }
    isoline->init_buff( context() );
    isoline->buffer_lines( isc->islDatify->value()?cs:NULL, isc->islThickness() );
  }
  isoline->translucent(isc->islColor()[3]<0.95);
  isoline->render();

  static IsoLine *cutplane_iso[NUM_CP] = {NULL};
  bool            clipstat[6];
  if( !_clip_cut_surface ) clip_off( clipstat );

  // draw isolines on cut surfaces
  for( int i=0; i<NUM_CP; i++ ) {
    if( cplane->drawIntercept(i) && _cutsurface[i] != NULL ){
      if( _redraw_state&VBO_Clip || update || cutplane_iso[i]==NULL ) {

        if( cutplane_iso[i] ) delete cutplane_iso[i];
        cutplane_iso[i] = new IsoLine( *isoline );
        cutplane_iso[i]->process( _cutsurface[i], data );
        cutplane_iso[i]->buffer_lines( isc->islDatify->value()?cs:NULL, isc->islThickness() );
        cutplane_iso[i]->translucent(isc->islColor()[3]<0.95);
        cutplane_iso[i]->init_buff(context());
        cutplane_iso[i]->update_buff();
      }
      // turn off clipping when we draw on the clipping plane or we will get z-fighting 
      if( _clip_cut_surface ) clip_off( i );
      cutplane_iso[i]->render();
      if( _clip_cut_surface ) clip_on( i );
    }
  }
  if( !_clip_cut_surface ) clip_on( clipstat );
  isc->islDirty( false );
}


void TBmeshWin::CheckMessageQueue(){
  
  LinkMessage::CmdMsg msg;
  
  unsigned int numberOfMsgRead = 0;
   
  while ((tmLink->ReceiveMsg(msg) == 0) &&
	   (numberOfMsgRead < MAX_MESSAGES_READ)) {
	
	ProcessLinkMessage(msg);

	numberOfMsgRead++;
  }  
}


int TBmeshWin::ProcessLinkMessage(const LinkMessage::CmdMsg& msg)
{
  if ( msg.command == LinkMessage::LINK ) {
    tmLink->add_link(msg.newlink);
  } else if( msg.command == LinkMessage::UNLINK ) {
    tmLink->unlink(msg.senderPid);
  } else if(  msg.command == LinkMessage::DIFFUSE_LINK ) {
     tmLink->diffuse_link( msg.senderPid );
  } else if( msg.command == LinkMessage::VIEWPORT_SYNC ) {
    trackball.SetScale(msg.trackball.scale);
    V3f modtrans = msg.trackball.trans*model->maxdim();
    trackball.SetTranslation(modtrans.X(), modtrans.Y(), modtrans.Z() );
    trackball.qRot = msg.trackball.qRot*model->syncRefRot();
    trackball.qSpin = msg.trackball.qSpin;
    redraw(VBO_View);
  } else if( msg.command == LinkMessage::TIME_SYNC ) {
    int newTm = msg.sliderTime;
    if (newTm > contwin->tmslider->maximum()) {
      newTm = contwin->tmslider->maximum();
    } else if (newTm < 0) {
      newTm = 0;
    }
    contwin->tmslider->value(newTm);
    set_time(newTm);
  } else if( msg.command == LinkMessage::COLOUR_SYNC ) { 
    cs->calibrate( msg.colour.min, msg.colour.max );
    cs->size( msg.colour.levels );
    contwin->mincolval->value(cs->min());
    contwin->maxcolval->value(cs->max());
    contwin->numcolev->value(cs->size());
    contwin->cstype->value( msg.colour.scale );
    contwin->cstype->mvalue()->do_callback(contwin->cstype);
    deadData->showDeadData->value(msg.colour.deadDataOn);
    deadData->showDeadData->do_callback();
    deadData->minValid->value(msg.colour.minDeadOn);
    deadData->maxValid->value(msg.colour.maxDeadOn);
    deadData->nan->value(msg.colour.ignoreNaN);
    deadData->minValidVal->value(msg.colour.minDead);
    deadData->maxValidVal->value(msg.colour.maxDead);
    deadData->deadopac->value(msg.colour.deadopac);
    deadData->dead_color->rgb(msg.colour.deadcol[0],
               msg.colour.deadcol[1], msg.colour.deadcol[2] );
    deadData->window->redraw();
    deadData->deadApp->do_callback();
    redraw(VBO_Colour);
  } else if( msg.command == LinkMessage::CLIP_SYNC ) {
    std::stringstream cpinfo;
    Quaternion Qrefinv = model->syncRefRot().GetConjugate();
    for( int i=0; i<6; i++ ) {
      V3f cn = Qrefinv.Rotate(msg.clip.cnorm[i]);
      cpinfo << cn.X() << " " << cn.Y() << " " << cn.Z() << " " 
             << msg.clip.inter[i] << " " << msg.clip.state[i] << std::endl;
    }
    cplane->set_CPs( cpinfo );
    redraw();
  } else if( msg.command == LinkMessage::WINSZ_SYNC ) { 
    contwin->tb_winsz( msg.winsz.w, msg.winsz.h );
  } else if( msg.command == LinkMessage::HILITE_SYNC ) {
    highlight( Vertex, msg.hilight.vtx, true );
    if(model->number(Cable))      highlight( Cable,   msg.hilight.cable,   true );
    if(model->number(Cnnx))       highlight( Cnnx,    msg.hilight.cnnx,    true );
    if(model->number(Surface))    highlight( SurfEle, msg.hilight.surfEle, true );
    if(model->number(VolEle))     highlight( VolEle,  msg.hilight.volEle,  true );
    float val;
    if(auxGrid && msg.hilight.auxVtx>=0 ) auxGrid->highlight_vertex( msg.hilight.auxVtx, val );
  } else if( msg.command == LinkMessage::TMPLOT_SYNC ) {
    if( timeplotter ) timeplotter->graph->set_X( msg.tmplot.x0, msg.tmplot.x1 );
    if( auxGrid ) auxGrid->time_X( msg.tmplot.x0, msg.tmplot.x1 ); 
  } else {
    return -1;
  }
  return 0;
}

void TBmeshWin::SendClipSyncMessage()
{
  std::stringstream cpinfo;
  LinkMessage::CmdMsg msg;

  cplane->get_CPs( cpinfo );
  for( int i=0; i<6; i++ ) {
    V3f cpnorm;
    cpinfo >> cpnorm;
    msg.clip.cnorm[i]  = model->syncRefRot().Rotate(cpnorm);
    cpinfo >> msg.clip.inter[i];
    cpinfo >> msg.clip.state[i];
  }
  msg.command = LinkMessage::CLIP_SYNC;
  if( tmLink ) tmLink->SendMsgToAll(msg);
}


void TBmeshWin::SendViewportSyncMessage()
{
  if( !tmLink ) return;

  // create a viewport sending
  float scale = 0.0;
  V3f v3f_trans;
  V3f p3f_origin;
  Quaternion qSpin;
  Quaternion qRot;

  // retrieve vals
  scale = trackball.GetScale();
  v3f_trans = trackball.GetTranslation();
  qRot = trackball.GetRotation(); 
  qSpin = trackball.qSpin;

  LinkMessage::CmdMsg msgToSend;
  msgToSend.trackball.scale = scale;
  msgToSend.trackball.trans = v3f_trans/model->maxdim();
  msgToSend.trackball.qSpin = qSpin;
  msgToSend.trackball.qRot = qRot*model->syncRefRot().GetConjugate();
  
  msgToSend.command = LinkMessage::VIEWPORT_SYNC;
  
  tmLink->SendMsgToAll(msgToSend);
}

void TBmeshWin::SendTimeSyncMessage()
{
  LinkMessage::CmdMsg msgToSend;
  msgToSend.sliderTime = tm;
  
  msgToSend.command = LinkMessage::TIME_SYNC;
  
  if( tmLink ) tmLink->SendMsgToAll(msgToSend);
}

void TBmeshWin::SendColourSyncMessage()
{
  LinkMessage::CmdMsg msgToSend;
  msgToSend.command              = LinkMessage::COLOUR_SYNC;
  msgToSend.colour.min           = cs->min();
  msgToSend.colour.max           = cs->max();
  msgToSend.colour.scale         = contwin->cstype->ivalue();
  msgToSend.colour.levels        = cs->size();
  DeadRange dr;
  double dmin, dmax;
  cs->get_deadRange( dmin, dmax, dr );
  msgToSend.colour.deadDataOn    = cs->deadRange();
  msgToSend.colour.ignoreNaN     = (dr&DEAD_NaN) > 0;
  msgToSend.colour.minDeadOn     = (dr&DEAD_MIN) > 0;
  msgToSend.colour.maxDeadOn     = (dr&DEAD_MAX) > 0;
  msgToSend.colour.minDead       = dmin;
  msgToSend.colour.maxDead       = dmax;
  msgToSend.colour.deadcol[0]    = cs->deadColour()[0];
  msgToSend.colour.deadcol[1]    = cs->deadColour()[1];
  msgToSend.colour.deadcol[2]    = cs->deadColour()[2];
  msgToSend.colour.deadopac      = cs->deadColour()[3];
  
  if( tmLink ) tmLink->SendMsgToAll(msgToSend);
}


void TBmeshWin::SendWinszSyncMessage()
{
  LinkMessage::CmdMsg msgToSend;
  msgToSend.winsz.w = w();
  msgToSend.winsz.h = h();
  
  msgToSend.command = LinkMessage::WINSZ_SYNC;
  
  if( tmLink ) tmLink->SendMsgToAll(msgToSend);
}


void TBmeshWin::SendHiliteSyncMessage()
{
  LinkMessage::CmdMsg msgToSend;
  msgToSend.hilight.vtx     = contwin->verthi->value();
  msgToSend.hilight.surfEle = contwin->elehi->value();
  msgToSend.hilight.volEle  = contwin->tethi->value();
  msgToSend.hilight.cnnx    = contwin->cnnxhi->value();
  msgToSend.hilight.cable   = contwin->cabhi->value();
  msgToSend.hilight.auxVtx  = auxGrid ? auxGrid->highlight() : -1;
  
  msgToSend.command = LinkMessage::HILITE_SYNC;
  
  if( tmLink ) tmLink->SendMsgToAll(msgToSend);
}


void TBmeshWin::SendTmPlotSyncMessage()
{
  if( !timeplotter  && !auxGrid && !auxGrid->plottable() ) return;

  LinkMessage::CmdMsg msgToSend;
  double c, d;
  if( timeplotter )
    timeplotter->graph->range(msgToSend.tmplot.x0,msgToSend.tmplot.x1,c,d);
  else
    auxGrid->time_get_X(msgToSend.tmplot.x0,msgToSend.tmplot.x1);
    
  msgToSend.command = LinkMessage::TMPLOT_SYNC;
  
  if( tmLink ) tmLink->SendMsgToAll(msgToSend);
}

extern sem_t *meshProcSem;

/** signal the time linked meshalyzer instances
 *
 *  \param dir direction
 */
void
TBmeshWin::signal_links( int dir ) 
{
  if( meshProcSem==SEM_FAILED ) return;

  // make sure we start at zero
  int numsem=0;
  sem_getvalue( meshProcSem, &numsem );
  for( int i=0; i<numsem; i++ )
    sem_wait( meshProcSem ); 

  int num_pending=0;

  for( auto it=timeLinks.begin(); it!=timeLinks.end(); it++ ){
    if( kill( *it, dir>0?SIGUSR1:SIGUSR2 ) )
      timeLinks.erase( *it );
    else
      num_pending++;
  }

  for( int i=0; i<num_pending; i++ )
    sem_wait( meshProcSem );
}


/** update the time
 *
 * \param a new time index
 *
 * \pre a is a valid time index
 * \return false if not a valid time index
 */
bool
TBmeshWin:: set_time(int a)
{
  if( a > max_time() )
    return false;

  tm=a;
  if (timeplotter!=NULL)timeplotter->highlight(tm);
  contwin->tmslider->value(tm);
  contwin->tmslider->redraw();
  if ( timeplotter->window->shown() ) timeplotter->highlight( tm );
  if( auxGrid && auxGrid->data() && contwin->auxautocalibratebut->value() ) {
    contwin->auxmincolval->value( auxGrid->cs.min() );
    contwin->auxmaxcolval->value( auxGrid->cs.max() );
  }
  if( model->pt.dynamic() ) {
    model->pt.time(tm);
    for( int i=0; i<6; i++ )
        if(cplane->drawIntercept(i)) determine_cutplane(i);
  }
  if ( hinfo->window->visible() &&
       ( model->pt.dynamic() || have_data==Dynamic    ) )
    hiliteinfo();

  redraw( VBO_Time );
  return true;
}


/** read in the dynamic points file
 *
 * \param fn the file name 
 *
 * \return nonzero on failure
 *
 * \note the dynamic points file is an IGB file of data type IBG_VEC3_f
 */
int
TBmeshWin :: read_dynamic_pts( const char *fn, Myslider *mslide )
{
  try {
    model->pt.dynamic( fn, numframes );
  }
  catch( FrameMismatch fm ) {
    fl_alert( "Incompatible number of time frames. Expected %d but got %d\n", fm.expected, fm.got );
    return 1;
  }
  catch( PointMismatch pm ) {
    fl_alert( "Incompatible number of points. %s\n", pm.print() );
    return 2;
  }

  model->pt.time(tm);
  mslide->maximum( max_time() );
  numframes = mslide->maximum()+1;
  mslide->redraw();

  return 0;
}


/** manage branch cuts 
 *
 * \param min
 * \param max
 * \param tol
 */
void
TBmeshWin :: branch_cut(double min, double max, float tol)
{
  if( min != 0. || max != 0. ){
    _branch_cut = true; 
    _branch_range[0] = min;
    _branch_range[1] = max;
  } else
    _branch_cut = false; 

  isosurfwin->islDirty(true);
  isosurfwin->issDirty(true);

  redraw(VBO_Colour);
}


/**
 * @brief center view on specified vertex
 *
 * @param vtx index of vertex
 */
void
TBmeshWin :: ctr_on_vtx( int vtx )
{
  const GLfloat *p = model->pt.pt(vtx);
  trackball.SetOrigin( -p[0], -p[1], -p[2] );
  trackball.SetTranslation( 0., 0., 0. );
  redraw(VBO_View);
}


/** 
 * @brief save isosurface to file as auxilliary grid
 *
 * \param fn   filename
 * \param surf which isosurface
 */
void
TBmeshWin :: saveAux( char *fn, int surf )
{
  int numEle=0, numPt=0;
  
  for ( int r=0; r<model->_numReg; r++ ) {
    IsoSurface *iso = !surf ? model->region(r)->_iso0 : model->region(r)->_iso1;
    if( iso ) {
      numEle += iso->num();
      numPt  += iso->npts();
    }
  }
  if( !numPt ) return;
  
  std::string basename = fn;
  size_t pos = basename.find( ".pts_t");
  if( pos == basename.length()-6 )
    basename.erase( pos );

  std::string ptfile = basename+".pts_t";
  FILE *pout = fopen( ptfile.c_str(), "w" );
  fprintf( pout, "1\n%d\n", numPt );
  fclose( pout );

  std::string elemfile = basename+".elem_t";
  FILE *eout = fopen( elemfile.c_str(), "w" );
  fprintf( pout, "1\n%d\n", numEle );
  fclose( eout );

  int pt_offset = 0;
  for ( int r=0; r<model->_numReg; r++ ) {
    IsoSurface *iso = !surf ? model->region(r)->_iso0 : model->region(r)->_iso1;
    iso->saveAux( basename, pt_offset );
    pt_offset += iso->npts();
  }
}


/**
 * @brief return what the colour scale applies to
 *
 * @param cschoice pointer widget requesting update
 *
 * @return a textual description
 */
void
TBmeshWin::cs_adj( CS_Choice *cschoice, const char *scale ) 
{
  unsigned long flags=0;

  if( contwin->cstype == cschoice ){
    cs->scale(scale);
    flags = VBO_Colour|VBO_Isoline_Color;
  } else if( contwin->veccstype == cschoice ) {
    if( vecdata ) vecdata->cs->scale(scale);
    flags = VBO_Vector;
  } else if( contwin->auxcstype == cschoice ) {
    if( auxGrid ) auxGrid->cs.scale(scale);
    flags = VBO_Aux;
  } else {
    std::cerr << "unknown colour scale" << std::endl;
    throw 1;
  }
  redraw(flags);
}



/**
 * @brief set if a surface respects a clipping plane
 *
 * @param surf surface number
 * @param cp   clipping plane number
 * @param val  respect?
 *
 */
void
TBmeshWin:: surfclip( int surf, int cp, bool val )
{
  model->surface(surf)->clip( cp, val );
  _rd_op.clip_obey( surf, cp, val );
  _rd_wf.clip_obey( surf, cp, val );
  if( isoline ) isoline->set_clip(surf, _rd_op.clip_obey( surf ));
}


void *
TBmeshWin::context()
{
#ifdef OSMESA
                            return osmesa_ctx;
#elif !defined(__APPLE__)
                            return Fl_Gl_Window::context();
#else
                            return NULL;
#endif
}

void 
TBmeshWin::context( void *c )
{
#ifdef OSMESA
                            osmesa_ctx = static_cast<OSMesaContext>(c);
#elif !defined(__APPLE__)
                            Fl_Gl_Window::context(c);
#endif
}


/**
 * @brief remove the grid and its associated data
 *
 * @param g grid to remove
 */
void
TBmeshWin::clear_grid(GridType g)
{
  bool         changed=false;
  unsigned int reason=0;

  if( g==AuxDataGrid && auxGrid ) {
    delete auxGrid;
    auxGrid = NULL;
    changed = true;
    contwin->auxgridgrp->deactivate();
    reason = VBO_Aux;
  } else if( g==VecDataGrid && vecdata ) {
    delete vecdata;
    vecdata = NULL;
    changed = true;
    contwin->vectorgrp->deactivate();
    reason = VBO_Vector;
  } else if( g==DynPtGrid && model->pt.dynamic() ) {
    model->pt.clear_dynamic();
    changed = true;
    reason = VBO_Visible;
  } else if( g==ScalarDataGrid && dataBuffer ) {
    delete dataBuffer;
    dataBuffer = NULL;
    data = NULL;
    have_data = NoData;
    changed = true;
    reason = VBO_Visible;
  } else if( g==TmAnnotate ) {
    _tm_annot.clear();    
    timeplotter->clear_annots();
  }
  if( changed ) {
    contwin->tmslider->maximum( max_time() );
    numframes = contwin->tmslider->maximum()+1;
    contwin->tmslider->redraw();
    model->pt.time(tm);
    redraw(reason);
  }
}


void
TBmeshWin::read_tm_annos( const char *atmfile )
{
#ifdef USE_HDF5
  if( strstr( atmfile, ":tm_annot/" )  ){
    std::string atmf(atmfile);
    hid_t hin = H5Fopen(atmf.substr(0, atmf.find_last_of(":")).c_str(), 
                                           H5F_ACC_RDONLY, H5P_DEFAULT);
    if( hin<1 ) return;
    int idx;
    try {
      idx = std::stod( atmf.substr( atmf.find_last_of("/")+1 ) );
    }
    catch(...) { return; }
    char **anns;
    if( ch5_annot_read( hin, idx, &anns ) ) return;
    ch5_close( hin );
    std::string txt;
    int i=0;
    while( anns[i] )
      txt += anns[i++];
    ch5_text_free( &anns );
    _tm_annot.resize(model->number(Vertex));
    try {
      for( int i=0; i<model->number(Vertex); i++ ) {
        size_t pos;
        _tm_annot[i] = std::stof( txt, &pos );
        txt = txt.substr(pos);
      }
    }
    catch(...){ _tm_annot.clear(); }
  } else {
#endif
    AsciiReader atm(atmfile); 
    if( atm.num_lines() == model->number(Vertex) ){
      _tm_annot.resize(model->number(Vertex));
      try{
        for( int i=0; i<model->number(Vertex); i++ )
          _tm_annot[i] = std::stod( atm[i] );
      }
      catch(...) {
        _tm_annot.clear();
      }
    }
#ifdef USE_HDF5
  }
#endif
}
