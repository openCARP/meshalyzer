#ifndef DATACLASS_H
#define DATACLASS_H

#include <zlib.h>
#include "IGBheader.h"
#include <map>
#include <string>
#include <exception>


enum DataReaderEnum {AllInMem, Threaded, OnDemand};
enum fileType { FTIGB=1, FTascii, FTfileSeqCG, FTDynPt, FThdf5, FTother };

fileType FileTypeFinder ( const char *fn );
void CG_file_list( std::map<int,std::string>&filelist, const char *fn );
int  parse_HDF5_grid( const char *fn, std::string& type, unsigned int& index );

class FrameMismatch
{
    public:
        int expected, got;
        FrameMismatch( int a, int b ){expected=a;got=b;}
};

class PointMismatch
{
    private:
        bool single = true;
        int expected1, got, expected2;
        char msg[1024];
    public:
        PointMismatch( int a, int b ){expected1=a;got=b;}
        PointMismatch( int a, int b, int c ){expected1=a;expected2=b;got=c;single=false;}
        const char* print(){
          if(single)snprintf( msg, 1024, "Expected %d but got %d", expected1, got);
          else snprintf( msg, 1024, "Expected %d or %d but got %d", expected1, expected2, got);
          return msg; }
};

class EmptyDataFile
{
    private:
        const char* msg = "No data in file" ;
    public:
        EmptyDataFile(){}
        const char* print(){ return msg; }
};


/** The basic class for reading data */
template<class T>
class DataClass
{
  public:
    virtual T      max(int)=0;	                  //!< maximum data value at a time
    virtual T      max()=0;	                      //!< maximum data value at a time
    virtual T      min(int)=0;                    //!< minimum data value at a time
    virtual T      min()=0;                       //!< minimum data value at a time
    virtual T*     slice(int)=0;                  //!< pointer to time slice of data
    virtual void   time_series( int, T* )=0;      //!< time series for a point
    virtual void   increment(int)=0;              //!< time slice increment
    int      max_tm(){return maxtm;}              //!< maximum allowable time
    int      slice_sz(){return slice_size;}       //!< size of 1 time slice
    void     slice_sz(int a){slice_size=a;}       //!< set size of slice
    std::string   file(){return filename;}             //!< return the file name
    float    t0(void){ return _t0; }              //!< initial time read
    float    dt(void){ return _dt; }              //!< time increment
    fileType filetype(void){ return _ftype; }     //!< type of file
    bool     ele_based(void){ return _elebased; } //!< is data element based?

    DataClass(){}
    virtual ~DataClass(){}

  protected:
    T*       data=NULL;       //!< data
    int      maxtm=0;         //!< number of time slice
    int      last_tm=-1;      //!< last time that can be requested
    int      slice_size=0;    //!< amountof data in one time slice
    float    _dt=1;           //!< time increment
    float    _t0=0;           //!< initial time read in
    std::string   filename;        //!< file containing data
    fileType _ftype;          //!< file type
    bool     _elebased=false; //!< element data read in?
};

#endif
