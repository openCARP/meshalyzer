#include "DrawingObjects.h"

const int prism_num_edge=9;
const int prism_edges[prism_num_edge][2] =
  {
    {0,1},{1,2},{2,0},{0,3},{1,5},{2,4},{3,4},{4,5},{5,3}
  };
const int prism_iso_table[][17] = {
    { 0 },
    { 1, 3, 0, 1, 0, 2, 0, 3 },
    { 1, 3, 1, 0, 1, 2, 1, 5 },
    { 1, 4, 0, 2, 1, 2, 1, 5, 0, 3 },
    { 1, 3, 0, 2, 1, 2, 2, 4 },
    { 1, 4, 0, 3, 2, 4, 2, 1, 1, 0 },
    { 1, 4, 0, 2, 0, 1, 1, 5, 2, 4 },
    { 1, 3, 0, 3, 2, 4, 1, 5 },
    { 1, 3, 0, 3, 3, 4, 3, 5 },
    { 1, 4, 0, 2, 0, 1, 3, 5, 3, 4 },
    { 2, 3, 0, 1, 1, 5, 0, 3, 3, 1, 2, 3, 5, 3, 4 }, 
    { 1, 4, 4, 3, 3, 5, 1, 5, 1, 2 },
    { 2, 3, 0, 3, 0, 2, 2, 4, 3, 2, 1, 3, 5, 3, 4 },
    { 1, 5, 0, 1, 5, 3, 3, 4, 4, 2, 2, 1 },
    { 2, 4, 0, 3, 0, 2, 2, 4, 3, 4, 3, 0, 1, 3, 5, 1, 5 },
    { 1, 4, 2, 4, 1, 5, 3, 5, 3, 4 },
    { 1, 3, 3, 4, 4, 5, 2, 4 },
    { 2, 3, 0, 3, 3, 4, 4, 2, 3, 0, 1, 0, 2, 4, 5 },
    { 2, 3, 3, 4, 4, 5, 2, 4, 3, 2, 1, 1, 0, 1, 5 },
    { 2, 3, 0, 2, 0, 3, 3, 4, 4, 1, 2, 2, 4, 4, 5, 1, 5 },
    { 1, 4, 0, 2, 2, 1, 5, 4, 4, 3 },
    { 1, 5, 0, 3, 3, 4, 4, 5, 2, 1, 1, 0 },
    { 1, 5, 0, 1, 0, 2, 3, 4, 4, 5, 5, 1 },
    { 1, 4, 0, 3, 3, 4, 4, 5, 5, 1 },
    { 1, 4, 0, 3, 2, 4, 4, 5, 5, 3 },
    { 1, 5, 2, 4, 3, 5, 4, 5, 0, 1, 0, 2 },
    { 2, 4, 1, 5, 5, 4, 4, 2, 2, 1, 3, 0, 1, 0, 3, 3, 5 },
    { 2, 3, 1, 2, 2, 0, 3, 5, 3, 2, 4, 4, 5, 1, 5 },
    { 1, 5, 0, 3, 3, 5, 5, 4, 1, 2, 0, 2 },
    { 1, 4, 0, 1, 1, 2, 4, 5, 5, 3 },
    { 2, 3, 0, 3, 3, 5, 1, 5, 3, 0, 1, 0, 2, 5, 4 },
    { 1, 3, 1, 5, 5, 4, 5, 3 }, 
    { 1, 3, 1, 5, 5, 4, 5, 3 },
    { 2, 3, 0, 3, 3, 5, 1, 5, 3, 0, 1, 0, 2, 5, 4 },
    { 1, 4, 0, 1, 1, 2, 4, 5, 5, 3 },
    { 1, 5, 0, 3, 3, 5, 5, 4, 1, 2, 0, 2 },
    { 2, 3, 1, 2, 2, 0, 3, 5, 3, 2, 4, 4, 5, 1, 5 },
    { 2, 4, 1, 5, 5, 4, 4, 2, 2, 1, 3, 0, 1, 0, 3, 3, 5 },
    { 1, 5, 2, 4, 3, 5, 4, 5, 0, 1, 0, 2 },
    { 1, 4, 0, 3, 2, 4, 4, 5, 5, 3 },
    { 1, 4, 0, 3, 3, 4, 4, 5, 5, 1 },
    { 1, 5, 0, 1, 0, 2, 3, 4, 4, 5, 5, 1 },
    { 1, 5, 0, 3, 3, 4, 4, 5, 2, 1, 1, 0 },
    { 1, 4, 0, 2, 2, 1, 5, 4, 4, 3 },
    { 2, 3, 0, 2, 0, 3, 3, 4, 4, 1, 2, 2, 4, 4, 5, 1, 5 },
    { 2, 3, 3, 4, 4, 5, 2, 4, 3, 0, 1, 1, 2, 1, 5 },
    { 2, 3, 0, 3, 3, 4, 4, 2, 3, 0, 1, 0, 2, 4, 5 },
    { 1, 3, 3, 4, 4, 5, 2, 4 },
    { 1, 4, 2, 4, 1, 5, 3, 5, 3, 4 },
    { 2, 4, 0, 3, 0, 2, 2, 4, 3, 4, 3, 0, 1, 3, 5, 1, 5 },
    { 1, 5, 0, 1, 5, 3, 3, 4, 4, 2, 2, 1 },
    { 2, 3, 0, 3, 0, 2, 2, 4, 3, 2, 1, 3, 5, 3, 4 },
    { 1, 4, 4, 3, 3, 5, 1, 5, 1, 2 },
    { 2, 3, 0, 1, 1, 5, 0, 3, 3, 1, 2, 3, 5, 3, 4 }, 
    { 1, 4, 0, 2, 0, 1, 3, 5, 3, 4 },
    { 1, 3, 0, 3, 3, 4, 3, 5 },
    { 1, 3, 0, 3, 2, 4, 1, 5 },
    { 1, 4, 0, 2, 0, 1, 1, 5, 2, 4 },
    { 1, 4, 0, 3, 2, 4, 2, 1, 1, 0 },
    { 1, 3, 0, 2, 1, 2, 2, 4 },
    { 1, 4, 0, 2, 1, 2, 1, 5, 0, 3 },
    { 1, 3, 1, 0, 1, 2, 1, 5 },
    { 1, 3, 0, 1, 0, 2, 0, 3 },
    { 0 }
};
const int prism_num_surf = 5;
const int prism_surface_table[][4] = {
   {0,1,2,-1}, {0,2,4,3}, {2,1,5,4}, {1,5,3,0}, {3,4,5,-1}
};


/** read in the point file */
bool Prism :: read( const char *fname )
{
  gzFile in;

  try {
    in = openFile( fname, "tetras" );
  } catch (...) {return false;}

  const int bufsize=1024;
  char buff[bufsize];
  if ( gzgets(in, buff, bufsize) == Z_NULL ) return false;
  sscanf( buff, "%d", &_n );
  _node    = new int[6*_n];
  _region = new int[_n];

  for ( int i=0; i<_n; i++ ) {
    gzgets(in, buff, bufsize);
    if ( sscanf( buff, "%d %d %d %d %d %d %d", _node+6*i, _node+6*i+1,
                 _node+6*i+2, _node+6*i+3, _node+6*i+4, _node+6*i+5, _region+i ) < 7 ) {
      // if region not specified, assign a default of -1
      _region[i] = -1;
    }
  }
  gzclose(in);
  return true;
}


/** draw the faces of an element not contaiining a node
 *
 * \param cn do not draw faces associated with this node
 */
void Prism::draw_out_face( int cn )
{
#if 0
  for ( int i=0; i<_n*_ptsPerObj; i++ )
    if ( _node[i] == cn ) {
      int nn = i%_ptsPerObj;
      int elenode = i - nn;

      glBegin(GL_TRIANGLES);
      if ( nn>2 ) {
        glVertex3fv( _pt->pt(_node[elenode+3]) );
        glVertex3fv( _pt->pt(_node[elenode+4]) );
        glVertex3fv( _pt->pt(_node[elenode+5]) );
      } else {
        glVertex3fv( _pt->pt(_node[elenode+0]) );
        glVertex3fv( _pt->pt(_node[elenode+1]) );
        glVertex3fv( _pt->pt(_node[elenode+2]) );
      }
      glEnd();
      glBegin(GL_QUADS);
      if ( nn==1 || nn==5 ) {
        glVertex3fv( _pt->pt(_node[elenode+0]) );
        glVertex3fv( _pt->pt(_node[elenode+2]) );
        glVertex3fv( _pt->pt(_node[elenode+4]) );
        glVertex3fv( _pt->pt(_node[elenode+3]) );
      } else if ( nn==0 || nn==3 ) {
        glVertex3fv( _pt->pt(_node[elenode+2]) );
        glVertex3fv( _pt->pt(_node[elenode+1]) );
        glVertex3fv( _pt->pt(_node[elenode+5]) );
        glVertex3fv( _pt->pt(_node[elenode+4]) );
      } else {
        glVertex3fv( _pt->pt(_node[elenode+0]) );
        glVertex3fv( _pt->pt(_node[elenode+3]) );
        glVertex3fv( _pt->pt(_node[elenode+5]) );
        glVertex3fv( _pt->pt(_node[elenode+1]) );
      }
      glEnd();
    }
#endif
}


/** determine if a clipping plane intersects an element.
 *
 *  \param pd     visible points
 *  \param cp     clipping plane, cp[0]x + cp[1]y +cp[2]z + cp[3] = 0
 *  \param e      the element in the list
 *  \param interp construct to interpolate data
 *
 *  \return a surface element if intersection, NULL otherwise
 *
 *  \post interp may be allocated
 */
SurfaceElement*
Prism::cut( char* pd, GLfloat* cp,
            Interpolator<DATA_TYPE>* &interp, int e )
{
  return planecut( pd, cp, interp, prism_num_edge, prism_edges, e );
}


const int* 
Prism::iso_polys(unsigned int index)
{
  return prism_iso_table[index];
}


/* 
 * return list of lists of nodes defining the bounding surfaces
 *
 * \param ft   face table to be filled in
 * \param v    element number
 * \param tris surfaces as tris?
 *
 * \param pointer to a vector of vectors
 */
int
Prism::surfaces( int ft[][MAX_NUM_SURF_NODES+1], bool tris, int v )
{
  return make_surf_nodelist( v, ft, prism_num_surf,
          int(prism_surface_table[1]-prism_surface_table[0]), 
                      (const int **)prism_surface_table, tris );
}


const int* 
Prism::edges(int &n)
{
  n = prism_num_edge;
  return (const int*)prism_edges;
}
