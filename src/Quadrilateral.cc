#include "DrawingObjects.h"

GLubyte *nul_ubyte = NULL;

const int quad_iso_table[][11] = {
    { 0 },
    { 1, 2, 0, 1, 0, 3 },
    { 1, 2, 0, 1, 1, 2 },
    { 1, 2, 0, 3, 1, 2 },
    { 1, 2, 1, 2, 2, 3 },
    { 2, 2, 0, 1, 2, 3, 2, 1, 2, 0, 3 },
    { 1, 2, 0, 1, 2, 3 },
    { 1, 2, 0, 3, 2, 3 },
    { 1, 2, 0, 3, 2, 3 },
    { 1, 2, 0, 1, 2, 3 },
    { 2, 2, 0, 1, 2, 3, 2, 1, 2, 0, 3 },
    { 1, 2, 1, 2, 2, 3 },
    { 1, 2, 0, 3, 1, 2 },
    { 1, 2, 0, 1, 1, 2 },
    { 1, 2, 0, 1, 0, 3 },
    { 0 }
};


/** draw one Quadrilaterals and do not do glBegin/glEnd
 *
 *  \param p0       index of first quadrilateral to draw
 *  \param nrml     if flat, element normal, else vertex normals  (NULL for none)
 *  \param buffer   buffer entry in which to place data
 *  \param surf     surface we are in
 *  \param cmode    colour mode
 */
int Quadrilateral::buffer( int p0, const short_float* nrml,  GLfloat *buffer, 
                                                  int surf, char cmode )
{
  if ( p0>=_n ) return 0;

  int i = 4*p0;
  if ( !_pt->vis(_node[i]) || !_pt->vis(_node[i+1]) || 
       !_pt->vis(_node[i+2]) || !_pt->vis(_node[i+3])  )
    return 0;

  SHORTVEC( zn, 0, 0, 1 );
  for ( int j=0; j<3; j++ ) {

    int node  = _node[i+j];
    const short_float *n = nrml?((cmode&CM_FLAT)?nrml:nrml+node*3):zn;
    buff_vtx( buffer+j*VBO_ELEM_SZ, nul_ubyte, _pt->pt(node), n, nul_ubyte, surf );

    node  = _node[i+(j+2)%4];
    n     = nrml?((cmode&CM_FLAT)?nrml:nrml+node*3):zn;
    buff_vtx( buffer+(j+3)*VBO_ELEM_SZ, nul_ubyte, _pt->pt(node), n, nul_ubyte, surf );
  }
  return 2;
}


/** draw one Quadrilaterals and do not do glBegin/glEnd
 *
 *  \param p0       index of first quadrilateral to draw
 *  \param c        colour to use if no data
 *  \param cs       colour scale
 *  \param data     data associated with nodes, element if flat (NULL for no data display)
 *  \param dataopac data opacity
 *  \param buffer   buffer entry in which to place data
 *  \param cmode    colour mode
 */
int Quadrilateral::bufcol( int p0, GLfloat *c, Colourscale* cs,
                          DATA_TYPE* data, dataOpac* dataopac,
                          GLubyte *buffer, char cmode, bool safe )
{
  if ( p0>=_n ) return 0;

  int i = 4*p0;
  if ( safe && (!_pt->vis(_node[i])   || !_pt->vis(_node[i+1]) || 
                !_pt->vis(_node[i+2]) || !_pt->vis(_node[i+3]))    )
    return 0;

  for ( int j=0; j<3; j++ ) {

    GLubyte bcol[4];

    int cnode = CM_COL( cmode, p0, _node[i], _node[i+j] );
    colour( cnode, c, data, cs, dataopac, bcol );  
    memcpy( buffer+j*4, bcol, 4 );

    cnode = CM_COL( cmode, p0, _node[i+(j+2)%4], _node[i] );
    colour( cnode, c, data, cs, dataopac, bcol );  
    memcpy( buffer+(j+3)*4, bcol, 4 );
  }
  return 2;
}


/** read in the triangle file */
bool Quadrilateral :: read( const char *fname )
{
  gzFile in;

  try {
    in = openFile( fname, "tris" );
  } catch (...) { return false; }

  const int bufsize=1024;
  char      buff[bufsize];
  int       nold=_n, nele;

  while ( gzgets(in, buff, bufsize) != Z_NULL ) {
    sscanf( buff, "%d", &nele );
    _n += nele;
    _node  = (int *)realloc( _node, _n*3*sizeof(int) );
    _nrml = (GLfloat*)realloc( _nrml, _n*sizeof(GLfloat)*3 );
    for ( int i=_n-nele; i<_n; i++ ) {
      gzgets(in, buff, bufsize);
      sscanf( buff, "%d %d %d %*d", _node+3*i, _node+3*i+1, _node+3*i+2 );
    }
  }
  gzclose(in);

  compute_normals( nold, _n-1 );

  return true;
}


/** increase the number of triangles
 *
 * \param fn   file name with elements to be added
 *
 * \return true if successful
 */
bool Quadrilateral :: add( const char* fn )
{
 return false;
}


const int* 
Quadrilateral::iso_polys(unsigned int index)
{
  return quad_iso_table[index];
}
    
    
void  
Quadrilateral::rev_order(int a)
{
  int *n = _node+a*4;
  int tmp = n[3];
  n[3] = n[0];
  n[0] = tmp;
  tmp  = n[2];
  n[2] = n[1];
  n[1] = tmp;
}
