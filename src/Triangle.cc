#include "DrawingObjects.h"
#include "gzFileBuffer.h"

static const int tri_iso_table[][6] = { 
  {0,0,0,0,0,0},{1,2,0,1,0,2},{1,2,1,0,1,2},{1,2,1,2,0,2},
  {1,2,2,0,2,1},{1,2,0,1,2,1},{1,2,0,1,0,2},{0,0,0,0,0,0} };

/** fill VBO with colour for one Triangle
 *
 *  \param p0       index of triangle to draw
 *  \param c        colour to use if no data
 *  \param cs       colour scale
 *  \param data     data associated with nodes or elements (NULL for no data display)
 *  \param dataopac data opacity (NULL for none)
 *  \param buffer   buffer entry in which to place data
 *  \param cmode    colouring mode
 *  
 *  \return number of triangles buffered
 */
int Triangle::bufcol( int p0, GLfloat *c, Colourscale* cs,
                     DATA_TYPE* data, dataOpac* dataopac,
                     GLubyte *buffer, char cmode, bool safe )
{
  if ( p0>=_n ) return 0;
  int i=3*p0; 
  const std::vector<bool>*vis = _pt->vis();
  if ( !safe || ((*vis)[i] && (*vis)[i+1] && (*vis)[i+2] )) {
    for ( int j=0; j<3; j++ ) {
      GLubyte col[4];
      int     nn    = _node[i++];
      int     cnode = CM_COL( cmode, p0, _node[i-j-1], nn);
      memcpy(buffer+j*4, colour(cnode,c,data,cs,dataopac,col), 4);
    }	    
    return 1;
  } 
  return 0;
}

/** fill VBO with data for one Triangle
 *
 *  \param p0       index of triangle to draw
 *  \param nrml     if \p flat, element normal, else vertex normals (NULL for none)
 *  \param buffer   buffer entry in which to place data
 *  \param surf     surface index
 *  \param cmode    colouring mode
 *  
 *  \return number of triangles buffered
 */
int Triangle::buffer( int p0, const short_float* nrml, GLfloat *buffer, 
                                          int surf, char cmode )
{
  GLubyte *nul_ubyte=NULL;

  if ( p0>=_n ) return 0;
  int i=3*p0; 
  const std::vector<bool>*vis = _pt->vis();
  if ( (*vis)[i] && (*vis)[i+1] && (*vis)[i+2] ) {
    short_float zn[3] = {0, 0, 0};
    for ( int j=0; j<3; j++ ) {
      int            nn    = _node[i++];
      const short_float *n = nrml?((cmode&CM_FLAT)?nrml:nrml+nn*3):zn;
      buff_vtx( buffer+j*VBO_ELEM_SZ, nul_ubyte, _pt->pt(nn), n, nul_ubyte, surf );
    }	    
    return 1;
  } 
  return 0;
}


/** read in the triangle file */
bool Triangle :: read( const char *fname )
{
  gzFile in;

  try {
    in = openFile( fname, "tris" );
  } catch (...) { return false; }

  const int bufsize=1024;
  char      buff[bufsize];
  int       nold=_n, nele;

  gzFileBuffer file(in);
  while ( file.gets(buff, bufsize) != Z_NULL ) {
    sscanf( buff, "%d", &nele );
    _n += nele;
    _node  = (int *)realloc( _node, _n*3*sizeof(int) );
    _nrml = (GLfloat*)realloc( _nrml, _n*sizeof(GLfloat)*3 );
    for ( int i=_n-nele; i<_n; i++ ) {
      file.gets(buff, bufsize);
      sscanf( buff, "%d %d %d %*d", _node+3*i, _node+3*i+1, _node+3*i+2 );
    }
  }
  gzclose(in);

  compute_normals( nold, _n-1 );

  return true;
}


/** increase the number of triangles
 *
 * \param fn   file name with elements to be added
 *
 * \return true if successful
 */
bool Triangle :: add( const char* fn )
{
  int    nele;
  std::string nfn = fn;

  try {
    nele = countInFile( fn );
  } catch (...) { return false; }

  const int bufsize=1024;
  char      buff[bufsize];

  _node  = (int *)realloc( _node,  3*(_n+nele)*sizeof(int) );
  _nrml = (GLfloat*)realloc( _nrml, (_n+nele)*sizeof(GLfloat)*3 );

  int nold = _n;
  _n += nele;

  gzFile in = gzopen( fn, "r" );

  for ( int i=_n-nele; i<_n; i++ ) {
    if ( gzgets(in, buff, bufsize)==Z_NULL )
      throw 1;
    if ( sscanf( buff, "%d %d %d %*d", _node+3*i, _node+3*i+1, _node+3*i+2 )<3 )
      i--;
  }

  gzclose(in);

  // remove tri or tris suffix
  if ( nfn.substr( nfn.size()-4 ) == ".tri" )
    nfn.erase( nfn.size()-3 );
  else if ( nfn.substr( nfn.size()-5 ) == ".tris" )
    nfn.erase( nfn.size()-4 );

  compute_normals( nold, _n-1 );

  return true;
}


/** Count the number of triangles in a file
 *
 *  if the first line contains less than 3 integers, assume it is the number
 *  of Triangles
 *
 *  \param fn file name 0 must have .tri or .tris suffix
 *
 *  \return the \# of elements
 */
int Triangle::countInFile( const char* fn )
{
  int        numtri=0;
  int        a, b, c;
  static int bufsize=1024;
  char       buff[bufsize];
  gzFile     in;

  if ( (in=gzopen( fn, "r" )) == NULL )
    throw(1);

  gzgets(in, buff, bufsize);
  if ( sscanf( buff, "%d %d %d", &a, &b, &c )==3 ) {
    numtri = 1;
    while ( gzgets(in, buff, bufsize) != Z_NULL ) {
      if ( sscanf( buff, "%d %d %d", &a, &b, &c ) == 3 ) numtri++;
    }
  } else {
    do {
      numtri += a;
      for ( int i=0; i<a; i++ ) gzgets(in, buff, bufsize);
    } while ( gzgets( in, buff, bufsize)!=Z_NULL &&
              sscanf( buff, "%d %d %d", &a, &b, &c )==1 );
  }

  gzclose( in );

  if ( numtri < 1 )
    throw(1);

  return numtri;
}


const int* Triangle::iso_polys(unsigned int index)
{
  return tri_iso_table[index];
}


void  
Triangle::rev_order(int a)
{
  int *n = _node+a*3;
  int tmp = n[0];
  n[0] = n[1];
  n[1] = tmp;
}
