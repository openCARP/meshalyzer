#include "MyScroll.h"
#include "FL/Fl_Button.H"
#include <iostream>

int MyScroll::all = -1000000;         //!< apply to all widgets

/**
 * @brief callback for button to restore original widget
 *
 * @param w    original widget
 * @param data array of widgets: scroll, button
 *
 * \post button widget deleted
 */
void
expand_cb( Fl_Widget *w, void *data )
{
  auto widget = (Fl_Widget**)data;
  auto scroll = (MyScroll*)widget[0];
  scroll->expand( widget[1], w );
  delete widget;
}


/**
 * @brief shift widgets in the Y direction
 *
 * @param limit  threshold y-ordinate for shifting
 * @param offset amount to shift
 *
 * @return total height of widgets
 */
int
MyScroll::shift( int limit, int offset )
{
  int height = 0;
  for( int i=0; i<children()-2; i++ ) { 
    auto ch = child(i);
    if( ch->y() > limit )
      ch->position( ch->x(), ch->y()+offset );
    if( ch->y()+ch->h() > height )
      height = ch->y()+ch->h();
  }
  return height;
}


/**
 * @brief add a widget to the top of the scroll
 *
 * @param w      widget to add
 * @param indent amount to indent
 * @param gap    interwidget gap
 */
void
MyScroll::insert( Fl_Widget *w, int indent, int gap )
{
  shift( MyScroll::all, w->h()+gap );
  w->position( indent, 0 );
  add( w );
  w->show();
  redraw();
}


/**
 * @brief append a widget to a scroll dynamically
 *
 * @param w      widget to add
 * @param indent left offset
 * @param gap    distance from bottom widget
 * @param after  if NULL, append at bottom, else after this widget
 *
 * @return callback function to implement
 */
void
MyScroll::append( Fl_Widget *w, int indent, int gap, Fl_Widget *after )
{
  int height;

  if( after ) {
    if( find(after) == children() ) return;
    height = after->y() + after->h();
    shift( after->y(), w->h()+gap );
  } else {
    height = shift(MyScroll::all, 0);
  }

  w->position( indent, height+gap );
  add( w );
  w->show();
  redraw();
}


/**
 * @brief replace a widget by a button
 *
 * @param w   widget to replace
 * @param bh  height of replacement button
 * @param lab button label
 *
 * \return button created
 *
 * \post \p w is removed from the scroll and hidden, but not deleted
 * \post clicking the button will restore the widget
 */
Fl_Widget *
MyScroll::collapse( Fl_Widget *w, int bh, const char* lab )
{
  if( find(w) == children() ) return NULL;
  shift(  w->y()+1, -w->h()+bh );
  auto but = new Fl_Button( 0, w->y(), this->w(), bh );
  but->align(FL_ALIGN_INSIDE|FL_ALIGN_CENTER);
  but->copy_label( lab );
  auto **cbws = new Fl_Widget*[2];
  cbws[0] = this;
  cbws[1] = w;
  but->callback( expand_cb, (void*)cbws );
  Fl_Group::remove( w );
  w->hide();
  add( but );
  redraw();
  return but;
}


/**
 * @brief restore widget replaced by button 
 *
 * This is normally called by clicking the replacement button. 
 *
 * @param w   widget to replace button
 * @param but button to be replaced
 *
 * @post \p but is destroyed
 *
 * @return the widget inserted
 */
Fl_Widget *
MyScroll::expand( Fl_Widget *w, Fl_Widget *but )
{
  int idx = find(but);
  if( idx == children() ) return NULL;
  shift(  but->y(), w->h()-but->h() );
  w->position( w->x(), but->y() );
  delete( but );
  add( w );
  w->show();
  redraw();
  return w;
}


/**
 * @brief remove a widget from the scroll
 *
 * @param w   widget to remove
 * @param gap gap between widgets
 *
 * @post widget is hidden but not destroyed
 */
void
MyScroll::remove( Fl_Widget *w, int gap )
{
  if( find(w) == children() ) return;

  shift( w->y(), -w->h()-gap );
  Fl_Group::remove(w);
  w->hide();
  redraw();
}



/**
 * @brief scroll to make a widget visible
 *
 * @param w the widget to be visible
 */
void
MyScroll::scroll_to( Fl_Widget*w )
{
  auto ypos = w->y();
  auto ht   = w->h();
  if( ht<h() ) ypos -= h()-ht;
  if( ypos<0 ) ypos=0;
  Fl_Scroll::scroll_to(0, ypos);
  redraw();
}
