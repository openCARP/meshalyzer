#include "Colourscale.h"
#include "colour_maps.h"
#include <iostream>
#include <vector>

/** \note map of deprecated colour scale indices to colour map names, do not modify */
std::map<int,std::string> cs_indx_map = {
      { CS_HOT           , "Hot"           },
      { CS_GREY          , "Grey"          },
      { CS_RGREY         , "RGrey"         },
      { CS_GGREY         , "GGrey"         },
      { CS_BGREY         , "BGrey"         },
      { CS_RAINBOW       , "Rainbow"       },
      { CS_BL_RAINBOW    , "Bl_Rainbow"    },
      { CS_COLD_HOT      , "Cold_Hot"      },
      { CS_CG            , "CG"            },
      { CS_MATLAB_REV    , "MATLAB_REV"    },
      { CS_MATLAB        , "MATLAB"        },
      { CS_ACID          , "Acid"          },
      { CS_P2G           , "Pink2Green"    },
      { CS_VIRIDIS       , "Viridis"       },
      { CS_VIRIDIS_LIGHT , "Viridis_light" },
      { CS_MAGMA         , "Magma"         },
      { CS_INFERNO       , "Inferno"       },
      { CS_DISTINCT      , "Distinct"      },
      { CS_PuOr          , "PuOr"          },
      { CS_TWILIGHT      , "Twilight"      },
      { CS_KINDLMANN     , "Kindlmann"     }
    };


Colourscale :: Colourscale( int ts, CScale_t tcs )
{
  scale(tcs);
  size( ts );
}


Colourscale :: Colourscale( int ts, std::string tcs )
{
  scale(tcs);
  size( ts );
}


Colourscale :: Colourscale( std::vector<GLfloat[3]>& m )
{
  scaletype = "Custom";
  size( m.size() );
  for( int i=0; i<m.size(); i++ ) {
    memcpy( cmap[i], m[i], 3*sizeof(GLfloat) );
  }
  calibrate(0,m.size()-1);
}

void Colourscale::  scale( CScale_t ce )
{
  scaleIdx=ce;
  scale(cs_indx_map[ce]);
} 

void Colourscale :: calibrate( double tmin, double tmax )
{
  maxdat = tmax;
  mindat = tmin;

  if ( maxdat != mindat ) {
    a = (static_cast<float>(n))/(maxdat-mindat);
    b = -a*mindat;
  } else {
    a = 0.;
    b = 0.;
  }
  _loaded = false;
}


void Colourscale :: scale( std::string cs )
{
  int   i;
  float intrvl, val;
  float ispan = static_cast<float>(n);
  float step  = ispan/(ispan-1.);

  scaletype = cs;

  if( cs =="Grey" ){  							/* bw */
    for ( i=0; i<n; i++ )
      cmap[i][0] = cmap[i][1] = cmap[i][2] = ((float)i)/(ispan-1.);
  }
  else if( cs =="RGrey" ){  							/* red */
    for ( i=0; i<n; i++ ) {
      cmap[i][0] = ((float)i)/(ispan-1.);
      cmap[i][1] = cmap[i][2] = 0;
    }
  } else if( cs =="GGrey" ){  							/* green */
    for ( i=0; i<n; i++ ) {
      cmap[i][1] = ((float)i)/(ispan-1.);
      cmap[i][0] = cmap[i][2] = 0;
    }
  } else if( cs =="BGrey" ){  							/* blue */
    for ( i=0; i<n; i++ ) {
      cmap[i][2] = ((float)i)/(ispan-1.);
      cmap[i][0] = cmap[i][1] = 0;
    }
  } else if( cs =="Hot" ){ 
    intrvl = ispan/3.;
    for( i=0; i<n; i++  ) {
      val = i*step/intrvl;
      if ( val<1. ) {
        cmap[i][0] = val;
        cmap[i][1] = cmap[i][2] = 0;
      } else if ( val<2 ) {
        cmap[i][0] = 1;
        cmap[i][1] = val-1.0;
        cmap[i][2] = 0;
      } else { 
        cmap[i][0] = 1;
        cmap[i][1] = 1;
        cmap[i][2] = val-2;
      }
    }
  } else if( cs =="Rainbow" ){ 
    intrvl = ispan/6.;
    for( i=0; i<n; i++  ) {
      val = i*step;
      if( i<intrvl ) {
        cmap[i][0] =  1-val/intrvl;
        cmap[i][1] = 0;
        cmap[i][2] = 1;
      } else if( val<2*intrvl ) {
        cmap[i][0] = 0;
        cmap[i][1] = (val-intrvl)/intrvl;
        cmap[i][2] = 1;
      } else if( val<3*intrvl ) {
        cmap[i][0] = 0;
        cmap[i][1] = 1;
        cmap[i][2] = 1-(val-2*intrvl)/intrvl;
      } else if( val<4*intrvl ) {
        cmap[i][0] = (val-3*intrvl)/intrvl;
        cmap[i][1] = 1;
        cmap[i][2] = 0;
      } else if( val<5*intrvl ) {
        cmap[i][0] = 1;
        cmap[i][1] = 1-(val-4*intrvl)/intrvl;
        cmap[i][2] = 0;
      } else {
        cmap[i][0] = 1;
        cmap[i][1] = 0;
        cmap[i][2] =(val-5*intrvl)/intrvl;
      }
    }
  } else if( cs =="Cold_Hot" ){ 
    intrvl = ispan/2.;
    for( i=0; i<n; i++  ) {
      val = i*step;
      if ( val<intrvl ) {
        cmap[i][2] =  1;
        cmap[i][0] =  cmap[i][1] = val/intrvl ;
      } else {
        cmap[i][0] = 1;
        cmap[i][1] =  cmap[i][2] = (2*intrvl-val)/intrvl ;
      }
    }
  } else if( cs =="CG" ){ 
    intrvl = ispan/4.;
    for( i=0; i<n; i++  ) {
      val = i*step;
      if ( val<intrvl ) {
        cmap[i][0] = 0;
        cmap[i][1] = val/intrvl;
        cmap[i][2] = 1;
      } else if ( val<2*intrvl ) {
        cmap[i][0] = 0;
        cmap[i][1] = 1;
        cmap[i][2] = (2*intrvl-val)/intrvl;
      } else if ( val<3*intrvl ) {
        cmap[i][0] = (val-2*intrvl)/intrvl;
        cmap[i][1] = 1;
        cmap[i][2] = 0;
      } else {
        cmap[i][0] = 1;
        cmap[i][1] = (4*intrvl-val)/intrvl;
        cmap[i][2] = 0;
      }
    }
  } else if( cs =="MATLAB" ){  // should be CS_MATLAB_REV according controls window
    intrvl = ispan/8.;
    for( i=0; i<n; i++  ) {
      val = i*step;
      if ( val<intrvl ) {
        cmap[i][0] = 0.5 + val/(2*intrvl);
        cmap[i][1] = 0;
        cmap[i][2] = 0;
      } else if ( val<3*intrvl ) {
        cmap[i][0] = 1;
        cmap[i][1] = (val-intrvl)/(2*intrvl);
        cmap[i][2] = 0;
      } else if ( val<5*intrvl ) {
        cmap[i][0] = (5*intrvl-val)/(2*intrvl);
        cmap[i][1] = 1;
        cmap[i][2] = (val-3*intrvl)/(2*intrvl);
      } else if ( val<7*intrvl ) {
        cmap[i][0] = 0;
        cmap[i][1] = (7*intrvl-val)/(2*intrvl);
        cmap[i][2] = 1;
      }else {
        cmap[i][0] = 0;
        cmap[i][1] = 0;
        cmap[i][2] = (9*intrvl-val)/(2*intrvl);;
      }
    }
  } else if( cs =="Bl_Rainbow" ){ 
    intrvl = ispan/6.;
    for( i=0; i<n; i++  ) {
      val = i*step/intrvl;
      if ( val<1. ) {
        cmap[i][0] = 0;
        cmap[i][1] = 0;
        cmap[i][2] = val ;
      } else if( val<2. ) {
        cmap[i][0] = 0;
        cmap[i][1] = val-1;
        cmap[i][2] = 1;
      } else if( val<3 ) {
        cmap[i][0] = 0;
        cmap[i][1] = 1;
        cmap[i][2] = 3.-val;
      } else if( val<4 ) {
        cmap[i][0] = val-3.;
        cmap[i][1] = 1;
        cmap[i][2] = 0;
      } else if ( val<5 ) {
        cmap[i][0] = 1;
        cmap[i][1] = 5.-val;
        cmap[i][2] = 0;
      } else {
        cmap[i][0] = 1;
        cmap[i][1] = val-5.;
        cmap[i][2] = val-5.;
      }
    }
  } else if( cs =="Acid" ){ 
    intrvl = ispan/2.;
    for( i=0; i<n; i++  ) {
      val = i*step/intrvl;
      if ( val<1. ) {
        cmap[i][0] = cmap[i][2] = 1.0;
        cmap[i][1] = val ;
      } else {
        cmap[i][0] = cmap[i][1] = 1.0;
        cmap[i][2] = 2-val;
      }
    }
  } else if( cs =="B2G2R" ){ 
    intrvl = ispan/2.;
    for( i=0; i<n; i++  ) {
      val = i*step/intrvl;
      if ( val<1. ) {
        cmap[i][0] = 0.;
        cmap[i][1] = val;
        cmap[i][2] = 1.0 - val;
      } else {
        cmap[i][0] = val - 1.;
        cmap[i][1] = 2. - val;
        cmap[i][2] = 0;
      }
    }
  } else if( cs =="Pink2Green" ){ 
    for ( i = 0; i < n; i++ ) {
      cmap[i][0] = cmap[i][2] = 1.0 - (((float)i)/(ispan-1.));
      cmap[i][1] = ((float)i)/(ispan-1);
    }
  } else if( cs =="Viridis" ){ 
    interp_map( viridis );
  } else if( cs =="Viridis_light" ){ 
    interp_map( viridis_light );
  } else if( cs =="Magma" ){ 
    interp_map( magma );
  } else if( cs =="Inferno" ){ 
    interp_map( inferno );
  } else if( cs =="Distinct" ){ 
    {
      // cheap Latin Hypercube Sampling of RGB colorspace
      srand(1);
      const float min_br = 0.1;
      const float max_br = 1.;
      std::vector<float> rgb[3];
      for ( i = 0; i < 3; i++ ) 
        rgb[i].resize(n);
      for ( i = 0; i < n; i++ ) 
        rgb[0][i] = rgb[1][i] = rgb[2][i] = min_br + (max_br-min_br)/n*(i+0.5);
      for ( i=0; i<n; i++ ) {
        for( int j=0; j<3; j++ ) {
          int ri = rand()%(n-i);
          cmap[i][j] = rgb[j][ri];
          rgb[j].erase(rgb[j].begin()+ri);
        }
      }
    }
  } else if( cs =="PuOr" ){ 
    interp_map( PuOr );
  } else if( cs =="Twilight" ){ 
    interp_map( twilight );
  } else if( cs =="Kindlmann" ){ 
    interp_map( Kindlmann );
  } else if( cs =="CET_I1" ){ 
    interp_map( CET_I1 );
  } else if( cs =="Turbo" ){ 
    interp_map( turbo_colormap );
  } else if( cs =="Plasma" ){ 
    interp_map( plasma_cmap );
  } else if( cs =="CoolWarm" ){ 
    interp_map( coolwarm_cmap );
  } else if( cs =="RdBu" ){ 
    interp_map( RdBu_cmap );
  } else if( cs =="PiYG" ){ 
    interp_map( PiYG_cmap );
  } else if( cs =="BrBG" ){ 
    interp_map( BrBG_cmap );
  } else if( cs =="Durrer" ){ 
    interp_map( Durrer_cmap );
  } else if( cs =="Custom" ){ 
    ;
  } else {
    std::cerr << "Unknown colour scale: " << cs << std::endl;
    exit(1);
  }
  if( _rev ){
    GLfloat tmp[3];
    for( int i=0; i<n/2; i++ ){
      memcpy( tmp, cmap[i], sizeof(GLfloat)*3 );
      memcpy( cmap[i], cmap[n-1-i], sizeof(GLfloat)*3 );
      memcpy( cmap[n-1-i], tmp, sizeof(GLfloat)*3 );
    }
  }
}


void Colourscale :: size( int s )
{
  if ( s<3 && scaletype != "Custom" ) return;

  if ( n ) {
    for ( int i=0; i<n; i++ ) delete[] cmap[i];
    delete[] cmap;
  }

  n = s;
  cmap = new GLfloat* [n];
  for ( int i=0; i<n; i++ ) cmap[i] = new GLfloat[4];

  calibrate( mindat, maxdat );
  scale( scaletype );
  _loaded = false;
}


/**
 * @brief return index into colour map
 *
 * @param val data value
 *
 * @retval -1 dead colour
 */
int
Colourscale :: colorIdx( double val)
{
  if( !NO_DEAD && (
           ( _deadRange&DEAD_MIN && val<_deadMin ) ||
           ( _deadRange&DEAD_MAX && val>_deadMax ) ||
           ( _deadRange&DEAD_NaN && std::isnan(val) ) 
                  )                                  ){
    return -1;
  }
  int indx=int(a*val+b);
  if ( indx<0 ) return 0;
  else if ( indx>=n ) return n-1;
  return indx;
}


/**
 * @brief determine colour
 *
 * @param val   data value
 * @param alpha opacity value
 *
 * @return RGBA colour
 */
GLfloat* 
Colourscale :: colorvec( double val, float alpha )
{
  int indx = colorIdx( val );

  if( indx == -1 ) 
    return _deadColour;
  else {
    cmap[indx][3] = alpha;
    return cmap[indx];
  }
}


void
Colourscale :: output_png( const char *filename )
{
  std::string foutname(filename);
  if( foutname.size()<5 || foutname.rfind(".png")!=foutname.size()-4 )
    foutname += ".png";
  PNGwrite cbar( fopen( foutname.c_str(), "w") );
  int y        = 128;
  int colwidth = 1024/n;
  if (!colwidth)
    colwidth = 1;
  int x        = colwidth*n;
  cbar.size( x, y );
  unsigned char buffer[x*y*3];
  for( int line = 0; line<y; line++ ){
    for( int i=0; i<n; i++ ) {
      unsigned char r=lroundf(cmap[i][0]*255);
      unsigned char g=lroundf(cmap[i][1]*255);
      unsigned char b=lroundf(cmap[i][2]*255);
      for( int j=0; j<colwidth; j++ ) {
        buffer[(i*colwidth+j+line*x)*3]   = r;
        buffer[(i*colwidth+j+line*x)*3+1] = g;
        buffer[(i*colwidth+j+line*x)*3+2] = b;
      }
    }
  }
  cbar.write( buffer, 1 );
}


void  
Colourscale::deadColour( GLfloat *dc, GLfloat dopac )
{
  memcpy( _deadColour, dc, 3*sizeof(GLfloat) );
  _deadColour[3] = dopac;
  _loaded = false;
}

void
Colourscale::deadRange( double min, double max, bool nan, DeadRange dr )
{
  _deadMin   = min;
  _deadMax   = max;
  _deadNaN   = nan;
  _deadRange = dr;
  _loaded    = false;
}
