#ifndef DRAWINGOBJECT_H
#define DRAWINGOBJECT_H

#include "render_utils.h"
#include "objects.h"
#include <vector>
#include <set>
#include <unordered_map>
#include <utility>
#include <algorithm>
#include "drawgl.h"
#include "Colourscale.h"
#include "DataOpacity.h"
#include <zlib.h>
#include "Interpolator.h"
#include "DataAllInMem.h"
#include "ThreadedData.h"
#include "short_float.h"
#ifdef USE_HDF5
#include <ch5/ch5.h>
#endif

#define BIN_HDR_SIZE 1024

static const float OPAQ_LIM = 0.95;

struct hash_pair { 
    template <class T1, class T2> 
    size_t operator()(const std::pair<T1, T2>& p) const
    { 
        auto hash1 = std::hash<T1>{}(p.first); 
        auto hash2 = std::hash<T2>{}(p.second); 
        return hash1 ^ hash2; 
    } 
}; 

struct edgecomp { 
    template <class T1, class T2> 
    bool operator()(const std::pair<T1, T2>& p0, const std::pair<T1, T2>& p1) const
    { 
      if( p0.first < p1.first ) return true;
      if( p1.first < p0.first ) return false;
      return p0.second < p1.second;
    } 
}; 

typedef std::pair<int,int> EdgePair;
typedef std::unordered_map< EdgePair, int, hash_pair > EdgePtMap;
typedef std::set<EdgePair,edgecomp> EdgeSet;

gzFile openFile( const char*, const char* );

GLubyte * float2ubyte( const GLfloat *cf, GLubyte *cb, float alpha=-1 );

#define MAX_NUM_SURF 6          // per element
#define MAX_NUM_SURF_NODES 4    // per element

// colouring modes
#define CM_FLAT 1
#define CM_ELEM 2
#define CM_COL_ELEM 4
#define CMODE(F,E,C) ((F)*CM_FLAT+(E)*CM_ELEM+(C)*CM_COL_ELEM)
/** \def CM_COL(C,E,F,S) 
 *  determine index of colour depending on shading
 *  \param C colouring mode
 *  \param E element data
 *  \param F color of first vertex
 *  \param S vertex colour
 */
#define CM_COL(C,E,F,S)  (C&CM_ELEM)?(E):((C&CM_COL_ELEM)?(E):(S))

class DrawingObj
{
  public:
    DrawingObj() {}
    virtual ~DrawingObj() {}

    virtual bool   read( const char * ) = 0;
    int    num() const { return _n; }		//!< get \#objects

    void   translucency( bool );            //!< set tranlucency
    void   size( float s ){ _size = s; }
    float  size( void ){ return _size; }
    void   threeD( bool b ){ _3D = b; }
    bool   threeD( void ){ return _3D; }
    const GLfloat* colour( int i, const GLfloat *defcol, DATA_TYPE *data, Colourscale *cs, dataOpac *dopac ) {
        if(data&&cs)return cs->colorvec(data[i],(dopac&&dopac->on())?dopac->alpha(data[i]):defcol[3]);return defcol; }
    GLubyte* colour( int i, const GLfloat *defcol, DATA_TYPE *data, Colourscale *cs, dataOpac *dopac, GLubyte *b );
  protected:
    int   _n = 0;			                    //!< \# objects
    float _size = 1;                            //!< size to draw objects
    bool  _3D = false;
};


class PPoint: public DrawingObj
{
  public:
    PPoint() {}
    virtual ~PPoint() { if ( _pts ) free(_pts); _pts = 0; }

    int              buffer( RenderSpheres &, int, int, const GLfloat *, Colourscale*,
                                                       DATA_TYPE*, int, dataOpac* );
    virtual bool     read( const char * );
#ifdef USE_HDF5
    virtual bool     read( hid_t );
#endif
    const   GLfloat* pt( int p=0 ){ return _pts+p*3; }
    const   GLfloat* pt( int p ) const { return _pts+p*3; }
    const   std::vector<bool>* vis() const { return &_visible; }
    const   GLfloat* offset() const { return _offset; }
    GLfloat* operator[] (int i){ return _pts+3*i; }
    void    setVis( bool v ){ _visible.assign(_visible.size(), v ); }
    void    setVis( std::vector<bool>& v ){ _visible=v; }
    bool    vis( int n ) const { return _visible[n]; }
    void    vis( int n, bool b ) { _visible[n]=b; }
    void    offset( const GLfloat*o )  { memcpy(_offset,o,sizeof(GLfloat)*3); }
    void    base1(bool b){ _base1 = b; }
    void    add( const GLfloat *, int n=1 );
    void    set( int n, GLfloat *p ){ memcpy(_pts+n*3,p,3*sizeof(GLfloat)); }
    void    time( int a );
    int     time(){ return _tm; }
    int     dynamic( const char *, int );
    bool    dynamic( void ){return _dynPt != NULL;}
    void    clear_dynamic(void){if(_dynPt){delete _dynPt; _dynPt=NULL;}}
    int     num_tm(){ return _dynPt? _dynPt->max_tm()+1:0; }
    void    recenter();
    void    clear(){_n=0;free(_pts);_pts=NULL;}
  private:
    GLfloat*     _pts=NULL;	       //!< point list
    std::vector<bool>_visible;          //!< points which get drawn
    GLfloat      _offset[3];       //!< centering offset
    bool         _base1=false;     //!< true for base 1
    int         _maxtm;            //!< maximum time
    int         _tm=-1;            //!< current time
    GLfloat  _min[3]={0,0,0},
             _max[3]={0,0,0};//!< bounding box
    DataClass<float>* _dynPt=NULL; //!< dynamic point data
    bool readASCII( const char* );
    bool readBinary( const char* );
};


class MultiPoint : public DrawingObj
{
  public:
    MultiPoint( PPoint *p, int n, int e ):_pt(p),_ptsPerObj(n),_node(NULL), _nedge(e) {init_lock();}
    virtual ~MultiPoint() { if ( _node ) free(_node); _node = NULL; }

    const int* obj( int n=0 ) { return _node+n*_ptsPerObj; }
    int     ptsPerObj(){ return _ptsPerObj; }
    void    add( int *n );
    const   PPoint* pt(){ return _pt; }
    void    define( const int *nl, int n=1 );
    void    redefine( const int *nl, int n=0 );
    bool    isoEdges( DATA_TYPE *dat, DATA_TYPE val, EdgePtMap &epm );
    float   edge_interp( int, int, DATA_TYPE*, DATA_TYPE, GLfloat* );
    MultiPoint **isosurf( DATA_TYPE *d, DATA_TYPE val, int &, PPoint *epts, EdgePtMap &epm, bool global_pt=true );
    MultiPoint **isosurfaces( DATA_TYPE *dat, DATA_TYPE val, int &npoly, PPoint *epts, EdgePtMap &epm );
    virtual const int*iso_polys(unsigned int)=0; 
    virtual int bytes()=0;
    GLfloat* color( int, GLfloat *, DATA_TYPE *, Colourscale *, dataOpac *);
    bool     visible(int n=0){const int* node=obj(n);for(int i=0;i<_ptsPerObj;i++)if(!_pt->vis(node[i]))return false;return true;}
  protected:
    int *  _node=NULL;	    //!< list of nodes defining objects
    PPoint* _pt;            //!< pointer to point list
    int    _ptsPerObj;		//!< \#nodes to define one object
    int    _nedge;          //!< \#edges
    void   init_lock();     //!< initialize OMP lock
    static bool _init_lock; //!< is lock initialized?
};


class Connection : public MultiPoint
{
  public:
    Connection(PPoint *p):MultiPoint(p,2,1) {}
    virtual ~Connection() {}

    void             add( int, int );	 //!< add a connection
    void             add( int, int* );   //!< add connections
    void             buffer( RenderLines&, const GLfloat *, Colourscale*,
                             DATA_TYPE*, dataOpac* =NULL, char=0, GLushort* =NULL );
    void             recolour( RenderLines&, const GLfloat *, Colourscale*,
                                             DATA_TYPE*, dataOpac* =NULL, char=0 );
    virtual DrawingObj *isosurf( DATA_TYPE *d, DATA_TYPE val ){return NULL;}
    virtual bool     read( const char * );
    bool             read_cables( const char * );
    bool             read_lines( const char *fname, bool replace=true );
    void             make_lines();
    virtual int      bytes(){return sizeof(Connection);}
#ifdef USE_HDF5
    virtual bool     read( hid_t hdf_file );
#endif
    const int *iso_polys(unsigned int index){return NULL;}
    int   add_line( int a, int b ){_lines.push_back(std::make_pair(a,b));_line_color.push_back(new GLfloat[4]);return _lines.size();}
    int   num_lines() const {return _lines.size(); }
    GLfloat *line_color( int i ){ if(i>=0&&i<_lines.size()){return _line_color[i];}else return NULL; }
    const std::vector<std::pair<int,int>>& lines(){ return _lines ;}

  private:
    std::vector<std::pair<int,int>> _lines;
    std::vector<GLfloat *> _line_color;
    DATA_TYPE* dat_to_elem( std::vector<DATA_TYPE>& edat, DATA_TYPE *dat ) {
      edat.resize(num());
      for( int i=0; i<num(); i++ ) edat[i] = (dat[_node[i*2]]+dat[_node[i*2+1]])/2.;
      return edat.data();
    }
    
};


// closed convex polygons
class SurfaceElement : public MultiPoint
{
  public:
    SurfaceElement(PPoint *p, int n):MultiPoint(p,n,n),_nrml(NULL),_shnrml(NULL) {}
    virtual ~SurfaceElement(){if(_nrml) delete[] _nrml;}

    virtual void  compute_normals( int, int );
    const   void  nrml( GLfloat *n ){ _nrml=n; };
    const   void  nrml( short_float *n ){ _shnrml=n; };
    const   GLfloat* nrml( int a=0 ) {return _nrml==NULL?NULL:_nrml+3*a; }
    const   short_float* sh_nrml( int a=0 ) {return _shnrml==NULL?NULL:_shnrml+3*a; }
    virtual int   buffer( int, const short_float *, GLfloat *, int, char=0 )=0;
    virtual int   bufcol( int, GLfloat*, Colourscale*, DATA_TYPE*,
                               dataOpac*, GLubyte *, char=0, bool safe=false )=0;
    void          read_normals( int, int, const char * );
    int           ntris() { return _ptsPerObj-2; } //!< equivalent #triangles
    virtual void  rev_order(int a=0)=0;
  protected:
    GLfloat*     _nrml=NULL;
    short_float* _shnrml=NULL;
};

class PolyGon : public SurfaceElement
{
  public:
    PolyGon( PPoint *p, int n ) : SurfaceElement(p,n) {}
    virtual ~PolyGon() {}

    virtual int  buffer( int,  const short_float *, GLfloat*, int, char=0 );
    virtual int  bufcol( int, GLfloat*, Colourscale*, DATA_TYPE*,
                              dataOpac*, GLubyte *, char=0, bool=false );
    virtual bool read( const char * ){return true;}
	static const int  _zero;
    const int* iso_polys(unsigned int index){return &_zero;}
    int     bytes(){ return sizeof(PolyGon);}
    virtual void  rev_order(int a=0);
};


class Triangle : public SurfaceElement
{
  public:
    Triangle(PPoint *p):SurfaceElement(p,3) {}
    virtual ~Triangle() {}
    virtual int      buffer( int, const short_float *, GLfloat *, int, char=0 );
    virtual int      bufcol( int, GLfloat*, Colourscale*, DATA_TYPE*,
                                  dataOpac*, GLubyte *, char=0, bool safe=false );
    virtual bool     read( const char * );
    virtual DrawingObj *isosurf( DATA_TYPE *d, DATA_TYPE val ){return NULL;}
    bool     add( const char * );
    const int*       iso_polys(unsigned int);
    int     bytes(){ return sizeof(Triangle);}
    virtual void  rev_order(int a=0);
  protected:
    int         countInFile( const char * );
};


class Quadrilateral : public SurfaceElement
{
  public:
    Quadrilateral(PPoint *p):SurfaceElement(p,4) {}
    virtual ~Quadrilateral() {}

    virtual int      buffer( int, const short_float *, GLfloat *, int, char=0 );
    virtual int      bufcol( int, GLfloat*, Colourscale*, DATA_TYPE*,
                                   dataOpac*, GLubyte *, char=0, bool safe=false );
    virtual bool     read( const char * );
    bool     add( const char * );
    const int*       iso_polys(unsigned int);
    int     bytes(){ return sizeof(Quadrilateral);}
    virtual void  rev_order(int a=0);
  protected:
    int         countTrisInFile( const char * );
};


//! Volume elements
class VolElement : public MultiPoint
{
  public:
    VolElement( PPoint *p, int n, int e ):MultiPoint( p, n, e ) {}
    virtual ~VolElement() {}

    const   int* region() const { return _region; }
    int  region(int a) const { return _region[a]; }
    void region(int a, int b) { _region[a] = b; }
    void add( int *n, int r=-1 );
    virtual void draw_out_face( int )=0;
    virtual SurfaceElement *cut( char*, GLfloat*, Interpolator<DATA_TYPE>*&, int e=0 )=0;
    virtual int surfaces(int [][MAX_NUM_SURF_NODES+1], bool=false, int v=0) = 0;
    virtual const int* edges(int &) = 0;
  protected:
    int*        _region=NULL;	//!< region for each element
    Connection* _edges;         //!< egdes for drawing elements
    SurfaceElement* planecut( char*, GLfloat*, Interpolator<DATA_TYPE>*&, int, const int [][2], int e  );
    int make_surf_nodelist(int, int[][MAX_NUM_SURF_NODES+1], const int, int, const int **, bool=false);
};


class Tetrahedral : public VolElement
{
  public:
    Tetrahedral(PPoint *p ): VolElement(p,4,6) {}
    virtual ~Tetrahedral() {}

    virtual bool     read( const char * );
    virtual void     draw_out_face( int );
    virtual SurfaceElement* cut(char*,GLfloat*,Interpolator<DATA_TYPE>*&,int=0);
    const int* iso_polys( unsigned int );
    virtual int bytes(){ return sizeof(Tetrahedral);}
    virtual int surfaces(int [][MAX_NUM_SURF_NODES+1], bool t=false, int v=0);
    virtual const int* edges(int &);
};

class Prism : public VolElement
{
  public:
    Prism(PPoint *p ): VolElement(p,6,9) {}
    virtual ~Prism() {}

    virtual bool     read( const char * );
    virtual void draw_out_face( int );
    virtual SurfaceElement* cut(char *,GLfloat*,Interpolator<DATA_TYPE>*&,int);
    const   int*     iso_polys(unsigned int);
    virtual int surfaces(int [][MAX_NUM_SURF_NODES+1], bool t=false, int v=0);
    virtual int     bytes(){ return sizeof(Prism);}
    virtual const int* edges(int &);
};


class Hexahedron : public VolElement
{
  public:
    Hexahedron(PPoint *p ): VolElement(p,8,12) {}
    virtual ~Hexahedron() {}

    virtual bool     read( const char * );
    virtual void draw_out_face( int );
    virtual SurfaceElement* cut(char *,GLfloat*,Interpolator<DATA_TYPE>*&,int);
    const int* iso_polys( unsigned int );
    virtual int surfaces(int [][MAX_NUM_SURF_NODES+1], bool t=false, int v=0);
    virtual int bytes(){ return sizeof(Hexahedron);}
    virtual const int* edges(int &);
};

class Pyramid : public VolElement
{
  public:
    Pyramid(PPoint *p ): VolElement(p,5,8) {}
    virtual ~Pyramid() {}

    virtual bool     read( const char * );
    virtual void     draw_out_face( int );
    virtual SurfaceElement* cut(char*, GLfloat*, Interpolator<DATA_TYPE>*&,int);
    const int* iso_polys( unsigned int );
    virtual int surfaces(int [][MAX_NUM_SURF_NODES+1], bool t=false, int v=0);
    virtual int bytes(){ return sizeof(Pyramid);}
    virtual const int* edges(int &);
};

#endif
