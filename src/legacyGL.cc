#include "legacyGL.h"
#include <map>

const GLenum CLIP_DISTANCE[] = {
    GL_CLIP_DISTANCE0, GL_CLIP_DISTANCE1, GL_CLIP_DISTANCE2,
    GL_CLIP_DISTANCE3, GL_CLIP_DISTANCE4, GL_CLIP_DISTANCE5
};


GLfloat CLIP_EQN[NUM_CP][4]; 

// global modelview and projection matrices indexed by OpenGL context
// column major order
std::map<void*,M4f>        ModelViewMat;
std::map<void*,M4f>        ProjectionMat;
std::map<void*,Quaternion> LightDir;


/**
 * @brief make sure context matrices exist
 *
 * @param cntxt
 */
void
add_context_mats( void *cntxt )
{
  if( !ModelViewMat.count(cntxt) ) {
    ModelViewMat[cntxt]  = M4f();
    ProjectionMat[cntxt] = M4f();
    LightDir[cntxt]      = Quaternion();
  }
}


/**
 * @brief set light direction
 *
 * @param cntxt the context
 * @param ld    direction
 */
void 
light_dir( void *cntxt, const Quaternion &ld )
{
  add_context_mats( cntxt );
  LightDir[cntxt] = ld;
}


/**
 * @brief get the matrix for an OpenGL context and create them
 *        if they do not exist
 *
 * @param mat   projection or modelview matrix
 * @param cntxt OpenGL context 
 *
 * @return 
 */
M4f &context_mat( std::map<void*,M4f> &mat, void *cntxt )
{
  add_context_mats( cntxt );
  return mat[cntxt];
}


/**
 * @brief load clipping plane equation: Ax + By + cZ + D = 0
 *
 * @param i       clipping plane number
 * @param coeffs  A,B,C,D
 */
void
clip_eqn( int i, GLfloat* coeffs )
{
  if( i>=0 && i<NUM_CP )
    memcpy( CLIP_EQN[i], coeffs, sizeof(GLfloat)*4 );
}


void
ortho_proj( void *context,
            GLfloat left,  GLfloat right,
            GLfloat bottom, GLfloat top,
            GLfloat near,   GLfloat far  )
{
  M4f ortho;
  ortho.e[0][0] =  2./(right-left);
  ortho.e[1][1] =  2./(top-bottom);
  ortho.e[2][2] = -2./(far-near);
  ortho.e[3][0] = -(right+left)/(right-left);
  ortho.e[3][1] = -(top+bottom)/(top-bottom);
  ortho.e[3][2] = -(far+near)/(far-near);
  M4f &m = context_mat( ModelViewMat, context );
  m *= ortho;
} 


void translate_mv( void *context, GLfloat x, GLfloat y, GLfloat z ) 
{
  M4f translate;
  translate.e[3][0] = x;
  translate.e[3][1] = y;
  translate.e[3][2] = z;
  M4f &m = context_mat( ModelViewMat, context );
  m *= translate;
}


void 
scale_mv( void *context, GLfloat x, GLfloat y, GLfloat z ) 
{
  M4f scale;
  scale.e[0][0] = x;
  scale.e[1][1] = y;
  scale.e[2][2] = z;
  M4f &m = context_mat( ModelViewMat, context );
  m *= scale;
}


void
mult_mv( void *context, M4f mm )
{
  M4f &m = context_mat( ModelViewMat, context );
  m *= mm;
}


/**
 * @brief set ModelView matrix to identity
 *
 * @param context
 */
void
identity_mv( void *context )
{
  M4f &m = context_mat( ModelViewMat, context );
  m.Identity();
}


/**
 * @brief determine if clippling plane is enabled
 *
 * @param i the clipping plane
 *
 * @return 
 */
bool 
clipping( int i )
{
  return glIsEnabled( CLIP_DISTANCE[i] );
}

/**
 * @brief turn off all clipping planes and save states
 *
 * @param b vector of length >=NUM_CP to hold states
 */
void 
clip_off( bool *b )
{
  for( int i=0; i<NUM_CP; i++ ){
    b[i] = clipping(i);
    clip_off(i);
  }
}


/**
 * @brief turn off a clipping plane
 *
 * @param i the plane
 */
bool
clip_off( int i )
{
  bool rv = clipping(i);
  glDisable( CLIP_DISTANCE[i] );
  return rv;
}


bool
clip_on( int i )
{
  bool rv = clipping(i);
  glEnable( CLIP_DISTANCE[i] );
  return rv;
}


/**
 * @brief turn on multiplanes
 * 
 * @param b vector of boolean values, 1 per plane
 *
 * \note \p b must be of at least length NUM_CP
 */
void
clip_on( bool *b )
{
  for( int i=0; i<NUM_CP; i++ )
    if( b[i] )
      glEnable( CLIP_DISTANCE[i] ); 
}

Vector3D<GLfloat> 
view_mult( void *context, Vector3D<GLfloat> dir )
{
  return ModelViewMat[context].MultiplyDirectionVector(dir);
}
