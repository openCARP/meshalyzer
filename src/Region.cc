#include "Surfaces.h"
#include "Region.h"
#include <string.h>

int RRegion_sort( const void *a, const void *b )
{
  return (*(RRegion **)a)->label() - (*(RRegion **)b)->label();
}

void RRegion:: initialize( int n, int nvol, int l )
{
  is_visible = true;
  _label = l;
  _iso0 = NULL;
  _iso1 = NULL;
  set_color( Vertex, 0., 1., 0. );
  set_color( Cable, 0., 0., 1. );
  set_color( Cnnx, 1., 0., 1. );
  set_color( VolEle, 0., 0., 1. );
  memset( startind, 0, maxobject*sizeof(int)  );
  memset( endind,   0, maxobject*sizeof(int)  );
  memset( showobj,  0, maxobject*sizeof(bool) );
  memset( _3D,      0, maxobject*sizeof(bool) );
  _member.resize(n,false);
  _elemember.resize(nvol,false);
  for( int i=0; i<maxobject; i++ ) _size[i] = 1.;
}


/** constructor
 *
 * \param v   volume list
 * \param n   number of points
 * \param l   label for region
 */
RRegion::RRegion( std::vector<VolElement *>&v, int n, int l )
{
  startind[VolEle] = -1;

  initialize( n, v.size(), l );
//#pragma omp parallel for
  for ( int j=0; j<v.size(); j++ ) {
    if( v[j]->region(0) != l ) continue;

    _elemember[j] = true;
    const int *node = v[j]->obj();
    for ( int i=0; i<v[j]->ptsPerObj(); i++,node++ ) {
      _member[*node] = true;
    }
    endind[VolEle] = j;
    if ( startind[VolEle]<0 ) 
      startind[VolEle] = j;
  }

  startind[Vertex] = -1;
  for ( int i=0; i<n; i++ )
    if ( _member[i] == true ) {
      //endind[Vertex] = i;
      if ( startind[Vertex]<0 ) {
        startind[Vertex] = i;
        break;
      }
    }
}


/** constructor
 *
 * \param n number of points
 * \param nv number volume elements
 * \param l label for region
 * \param b initial value
 */
RRegion::RRegion( int n, int nv, int l, bool b )
{
  initialize( n, nv, l );
  _member.assign(n, b);
  _elemember.assign(nv, b);
}


void RRegion :: set_color( Object_t obj, float r, float g, float b, float a )
{
  color[obj][0] = r;
  color[obj][1] = g;
  color[obj][2] = b;
  color[obj][3] = a;
}



/** assign/unassign an element to a region
 *
 *  \param v point membership list
 *  \param e element number
 *  \param b membership
 */
void RRegion :: ele_member(std::vector<VolElement *>&v, int e, bool b )
{
  _elemember[e] = b;
  for ( int i=0; i<v[e]->ptsPerObj(); i++ ) {
    _member[v[e]->obj()[i]] = b;
  }
}
