#ifndef RENDER_UTILS_H
#define RENDER_UTILS_H

#define MAX_NUM_SURFS 256
#define MAX_COLOURS   1200

// fineness of entities drawn
#define CNNX_NUM_SIDES 8
#define SPHERE_REFINE  2

const float POINT_BACK_LIMIT = -0.95;

/// what has triggered the redraw
enum Sullied : unsigned int { VBO_None          = 0,       // nothing
                              VBO_View          = 1<<0,    // viewport
                              VBO_Opacity       = 1<<1,    // surface opacity toggled 
                              VBO_Position      = 1<<2,    // point positions
                              VBO_Visible       = 1<<3,    // object visibility
                              VBO_Time          = 1<<4,    // time
                              VBO_Hilight       = 1<<5,    // highlighted object
                              VBO_Vector        = 1<<6,    // vector display attribute
                              VBO_Aux           = 1<<7,    // Auxilliary grid attribute
                              VBO_Aux_Color     = 1<<8,    // Auxilliary grid attribute
                              VBO_Lighting      = 1<<9,    // lighting property
                              VBO_Clip          = 1<<10,    // clipping plane
                              VBO_Clip_Color    = 1<<11,   // clipping plane colour
                              VBO_Isosurf       = 1<<12,   // isosurface attribute
                              VBO_Isosurf_Color = 1<<13,   // isosurface colour
                              VBO_Isoline       = 1<<14,   // isoline attribute
                              VBO_Isoline_Color = 1<<15,   // isoline colour
                              VBO_Cnnx_Color    = 1<<16,   // connection colour
                              VBO_Cnnx          = 1<<17,   // connection colour
                              VBO_Pt_Color      = 1<<18,   // vertex colour
                              VBO_Pt            = 1<<19,   // vertex visibility
                              VBO_VoxEle        = 1<<20,   // voxele display
                              VBO_Axes          = 1<<21,   // axes
                              VBO_Colour        = 1<<22,   // simple surface colour change
                              VBO_Scalebar      = 1<<23,   // scale bar change
                              VBO_ColBar        = 1<<24,   // scale bar change
                              VBO_All           = ~(0u) }; // everything

#ifdef OSMESA
#  include <GL/osmesa.h>
#elif defined(__APPLE__)
#    define GL_SILENCE_DEPRECATION
# include <OpenGL/gl3.h>
#else
#  if defined(WIN32) || defined(__CYGWIN__)
#    define GLEW_STATIC 1
#  endif
#  include <GL/glew.h>
#endif
#ifdef USE_AMD
#    define NORM_FLOAT
#endif
#include "Quaternion.h"
#include "short_float.h"
#include <vector>
#include <algorithm>           // std::rotate

static GLuint  SEP_ELEM = ~(0u);  // max unsigned integer 
static GLfloat *nulptr   = NULL;
static GLubyte *nulUBptr = NULL;

#define VBO_ELEM_SZ        ((VBO_ENT_OFFSET+VBO_ENT_SZ+sizeof(GLfloat)-1)/sizeof(GLfloat))//!< vertex data size in floats
#define VBO_XYZ_OFFSET     0   //!< byte offset for vertex position
#define VBO_NORM_OFFSET   (VBO_XYZ_SZ)   //!< byte offset for normal 
#define VBO_XYZ_SZ        12
#ifdef NORM_FLOAT
#define VBO_NORM_SZ       12
#else
#define VBO_NORM_SZ       6
#endif
#define VBO_ENT_OFFSET    (VBO_NORM_OFFSET+VBO_NORM_SZ)     //!< byte offset for surface 
#define VBO_ENT_SZ        sizeof(GLushort)

#define VBO_COLOR_SZ       3                                //!< number of colour entities
#define VBO_OPAQ_OFFSET   (VBO_COLOR_SZ)                    //!< byte offset for alpha channel 
#define VBO_OPAQ_SZ        1                                //!< sizee of opaque
#define VBO_RGBA_SZ       (VBO_COLOR_SZ+VBO_OPAQ_SZ) 

#define NODALBUFF_UPD_VTX 1
#define NODALBUFF_UPD_NRML 2
#define NODALBUFF_UPD_COL 4

#define XY_PLANE 1
#define XZ_PLANE 2
#define YZ_PLANE 4
#define BOGUS_VAL -666.

/**
 * @brief vertex buffer object rendering
 */
class render_data
{
    public: 
      // ps=vertices/object
      render_data(int ps,void *c=NULL):prim_sz(ps),_context(c),_esz(ps*VBO_ELEM_SZ){
                            for(int i=0; i<MAX_NUM_SURFS;i++){
                                    set_material(i,(const float[]){BOGUS_VAL,0,0,0});
                                    _clip_use[i]=~0;}                              }
      ~render_data();

      // light settings
      GLfloat           ambient   = 0.4f;
      GLfloat           diffuse   = 0.2f;
      GLfloat           specular  = 0.7f;
      GLint             back_lit  = 0;      //!< 0=cull, 1=unlit, 2=lit
      GLfloat           fresnel   = 3.;     //!< Fresnl lighting exponent
      GLfloat           lineWidth = 2.;

      /**
       * @brief set properties for a surface
       *
       * @param s     surface index
       * @param props specular,diffuse,shininess,back_light
       */
      void set_material( int s, const GLfloat *props ){ 
        assert( s<MAX_NUM_SURFS );
        for( int i=0; i<4; i++ ) material[s][i]=props[i];
        _dirty = true;
      }
 
      bool material_unset( int s ) { return material[s][0]==BOGUS_VAL; }
      void clear(){ nodalbuff.clear(); _n = n_opaque = 0; }
      bool inited(){ return init; }
      bool setup_render_data(void *c=NULL, bool p=false, bool eib=false);
      virtual void init_nodalbuff(bool);
      virtual void mesh_render( bool wireframe=false )=0;

      int num(){return _n;}              //!< \return number of elements buffered
      int numVtx(){return _n*prim_sz;}   //!< \return number of vertices buffered
      int prim_size(){ return prim_sz; } //!< \return number of vertices per element
      
      /// \return pointer to start of geometry memory
      GLfloat *buffer(){ return nodalbuff.data(); }
      /// \return pointer to start of colour memory
      GLubyte *colbuffer(){ return colbuff.data(); }                                       
      /// portion of geometry buffer containing translucent elements
      GLfloat *transBuffer() { return nodalbuff.data() + n_opaque*VBO_ELEM_SZ;}  
      /// portion of buffer containing translucent colour
      GLubyte *transColBuffer() { return colbuff.data() + n_opaque*4; }        
      /// resize buffer for \p n elements
      GLfloat *buffer(int n){_n=n; nodalbuff.resize(n*_esz);colbuff.resize(n*prim_sz*4);return nodalbuff.data();}   
      /// append \p n elements \return pointer to new memory \post if specified, return new colour memory in \p b
      GLfloat *buff_append(int n, GLubyte **b=NULL){_n+=n; nodalbuff.resize(_n*_esz);colbuff.resize(_n*prim_sz*4);if(b){*b=colbuff.data()+(_n-n)*4*prim_sz;} return nodalbuff.data()+(_n-n)*_esz;} 
      /// put last \p N elements at start
      void     buff_rotate( int N ){ 
        std::rotate( nodalbuff.begin(), nodalbuff.begin()+nodalbuff.size()-N*prim_sz*VBO_ELEM_SZ, nodalbuff.end() );
        std::rotate( colbuff.begin(), colbuff.begin()+colbuff.size()-N*prim_sz*4, colbuff.end() );
      }
      int  buff_vtx( GLfloat *, const GLfloat *, const short_float *, const GLfloat *, GLushort );
      int  buff_vtx( GLfloat *, const GLfloat *, const short_float *, const GLubyte *, GLushort );
      int  buff_vtx( int, const GLfloat *, const short_float *, const GLfloat *, GLushort );
      int  buff_vtx( int, const GLfloat *, const short_float *, const GLubyte *, GLushort );
      /// buffer vertex colour \param vbo location in geometry buffer \param c colour
      int  buff_col( GLfloat *vbo, GLubyte *c){
        memcpy( colbuff.data()+(vbo-nodalbuff.data())/VBO_ELEM_SZ*VBO_RGBA_SZ, c, VBO_RGBA_SZ);
        return 4; }
      int  buff_cone( GLfloat* &, int, GLfloat, GLfloat, float, GLfloat, const GLfloat* );
      int  buff_3Darrow( GLfloat* &, GLfloat, GLfloat, GLfloat, GLfloat, int, 
                                        const GLfloat*, bool as_strip=false);
      void update_nodalbuff(){init_nodalbuff(true);}
      void update_col(bool unbind=true);

      void  opaque( int n ){ n_opaque = prim_sz * (n<0 ? _n : n ); } //!< \param n number of opaque elements
      unsigned int  opaque(){ return n_opaque/prim_sz; }             //!< \return number opaque elements
      bool  dirty(){ return _dirty; }                                //!< \return redraw needed?
      void  top(bool a){_on_top=a;}                                  //!< draw on top of all objects, unclipped
      void  context( void *c ){ _context=c; }                        //!< assign OpenGL context
      void *context(){ return _context; }                            //!< return OpenGL context
      int   num_trans(){ return _n - opaque(); }                     //!< \return number of translucent elements
      int   vtx_size() { return VBO_ELEM_SZ; }                       //!< \return number of floats/vertex
      void  lighting( float a, float d, float s, float f, int bl ) {
                     ambient=a; diffuse=d; specular=s; fresnel=f; back_lit=bl; }
      void  resurf( int vtx0, int vtx1, unsigned short s );
      GLint clip_obey(int s){ return _clip_use[s]; }                 //!< \return clip planes obeyed by surface \s
      void  clip_obey(int s, GLint c){ _clip_use[s]=c; }             //!< assign clip planes obeyed by surface \p s 
      /// set whether surface \p s obeys clip plane \p cp
      void  clip_obey(int s, int cp, bool obey){int b=1<<cp; if(obey) _clip_use[s]|=b; else _clip_use[s]&=~b;}

    protected:
      bool    init = false;                     //!< is structure initialized?
      int     shader_default, shader_wireframe; //!< shaders
      int     prim_sz=0;                        //!< number of vertices per element
      long    _n=0;                             //!< number of elements
      long    n_opaque=-1;                      //!< number of opaque vertices to render
      float   tfm[4][4];                        //!< transformation matrix
      std::vector<GLfloat> nodalbuff;           //!< CPU geometry buffer
      std::vector<GLubyte> colbuff;             //!< CPU colour buffer
      GLuint  gl_nodalbuff;                     //!< VBO for vertex data
      GLuint  gl_colbuf=0;                      //!< VBO for colour data
      GLuint  gl_vao;                           //!< VAO for data
      GLuint  _eib=0;                           //!< element index buffer
      GLuint  _dib=0;                           //!< element index buffer
      int     _esz;                             //!< element size in vertices
      bool    _dirty=true;                      //!< buffer needs repopulating
      bool    _on_top=false;                    //!< draw on top?
      GLfloat material[MAX_NUM_SURFS][4];       //!< material properties for each surface
      GLint   _clip_use[MAX_NUM_SURFS];         //!< which clipping planes to respect
      void   *_context;                         //!< OpenGL context
      size_t  _old_sz=0;                        //!< size of last buffer?

      void    set_clipping(int);
      void    finish_render();
      
      template<typename T>
      int buff_cyl( GLfloat* &buff, const GLfloat *vtx0, const GLfloat *vtx1, int ns, 
          GLfloat radius, const T *c0, const T *c1, int reg, bool join, 
          const GLfloat *next, bool strip=false, GLfloat* &cbuf=nulptr, GLubyte* &ccol=nulUBptr );
};


/** \class RenderBuffer 
 * \brief buffer data but do not draw it
 */
class RenderBuffer : public render_data
{
   public:
       RenderBuffer(int p=3):render_data(p,NULL){}
       virtual void init_nodalbuff(bool){}
       virtual void mesh_render( bool wireframe=false ){}
};


class RenderTris : public render_data 
{
    public:
      RenderTris(void *c=NULL):render_data(3,c){}
      void mesh_render( bool wireframe=false );
};


/** \class RenderLines 
 *
 *  \brief draw lines
 *
 *  This class can draw 3 types of lines:
 *     1. Simple 1D lines
 *     2. 3D cylinders defined as separate triangles
 *     3. 3D cylinders defined by TRIANGLE_STRIPS and TRIANGLE_FANs (end caps)
 *
 *  To set the line as 3D, call sides(n) with \p n >0 
 *  By default, 3D lines are drawn as separate triangles. To use strips, call as_strip(true)
 */
class RenderLines : public render_data 
{
    public:
      RenderLines(void *c=NULL):render_data(1,c){sides(0);diffuse=0.7;set_material(0,defmat);}
      bool setup_render_data(void *c=NULL){ return render_data::setup_render_data(c,false,true); }
      void init_nodalbuff(bool);
      void update_col(bool=true);
      void mesh_render(bool=false);
      void sides( int, int=0 );
      int  sides(){ return _num_sides; }
      bool threeD() { return _num_sides>=3; }
      void threeD(bool on) {if(on==threeD())return;sides(on?CNNX_NUM_SIDES:0);if(!on)_diskbuff.clear();}
      /** add line of n-segments */
      void buffer_line( GLfloat* vbobuff, GLubyte *, int n=1, bool=false );
      void adjust_n(){_n=nodalbuff.size()/VBO_ELEM_SZ;} // needed since no standard size
      RenderLines& operator += (RenderLines &rl);
      GLuint recolour_line( GLubyte *lbuff, int n, bool facet, GLuint, GLuint );
      void clear(){render_data::clear();_num_disk=0; _diskbuff.clear(); _cylind.clear(); }
      void as_strip( bool b ){ _strip = b; }
      void cyl_indices( GLuint, int );
      void def_light( int s ){ set_material(s,defmat); }
    private:
      std::vector<int>     _line_idx;             //!< vertex indices of line starts
      int                  _num_sides = 0; 
      int                  _num_disk  = 0;        //!< number of disks
      bool                 _strip     = false;    //!< draw as triangle strips
      GLfloat              defmat[4]  = {0.7, 0.7, 80., 1.};
      std::vector<GLfloat> _diskbuff;             //!< buffer to draw caps
      std::vector<GLubyte> _diskcolb;             //!< disk colour buffer
      std::vector<GLuint>  _diskind;              //!< indices to draw disks
      std::vector<GLuint>  _cylind;               //!< indices to draw cylinders
                                                  
      int   nCylVtx(){ return threeD() ? _num_sides : 2; }
      int   nDiskVtx(){ return threeD() ? _num_sides+2 : 0; } //!< #vertices/disk
      int   nLineVtx(int n){return threeD()?(_num_sides*(_strip?n+1:6*n+6)):2*n;} //!< #vertices/line
};


class RenderSpheres : public render_data 
{
    public:
      RenderSpheres(int ns=0,void *c=NULL):render_data(1,c){diffuse=0.7;set_material(0,defmat);}
      bool setup_render_data(void *c=NULL){ return render_data::setup_render_data(c,true); }
      void mesh_render(bool=false);
      void subdiv( int a );
      int  subdiv(){ return _nsubdiv; }
      bool threeD(){ return _nsubdiv>0; }
      void threeD( bool on ) { if(threeD()!=on) subdiv( on ? SPHERE_REFINE : 0 ); }
      void  radius( float r ){if(threeD()&&r!=_radius)_dirty=true;_radius=r; }
      float radius() { return _radius; }
      void buffer_sphere( GLfloat *, int, const GLfloat*, const GLfloat*, int );
    private:
      int              _nsubdiv=0;
      GLfloat           defmat[4] = {0.7, 0.7, 80., 1.};
      std::vector<GLfloat>        _vtx;  //!< icosahedron vertices
      std::vector<unsigned int>   _tri;  //!< icosahedron tris
      GLfloat          _radius = 1;
      int  buff_sphere( GLfloat* &, const GLfloat*, const GLfloat*, int, const unsigned int*,
                                                                  float, const GLfloat*, int );
};

class RenderHilights : public render_data
{
    public:
        RenderHilights(void *c=NULL):render_data(1,c){diffuse=0.7;ambient=0.9;specular=0.7;set_material(0,defmat);}
        void mesh_render(bool=false);
        void pt( unsigned int, const GLfloat *, GLfloat, bool );
        void cnnx( unsigned int, const GLfloat*, const GLfloat*, GLfloat, int );
        void line( unsigned int, int p0, int p1, const int*, const GLfloat*, GLfloat, int );
        void surfele( int n, const GLfloat *, bool, int=-1 );
        void volele( int n, const GLfloat *, bool, int=-1 );
        void update_nodalbuff();
        void clear();
    private:
        GLubyte _colPt[4]       = { 255,  26,  26, 255 };
        GLubyte _colCnnx[4]     = { 255, 255, 128, 255 };
        GLubyte _colLine[4]     = {  26, 255, 255, 255 };
        GLubyte _colHiSurf[4]   = {  34,   0, 230, 255 };
        GLubyte _colAssSurf[4]  = { 255,  26, 255, 255 };
        GLubyte _colSurfEdge[4] = {   0,   0,   0, 255 };
        GLubyte _colHiVol[4]    = {  33, 204,   0, 255 };
        GLubyte _colAssVol[4]   = { 255, 140,  26, 255 };
        GLubyte _colVolEdge[4]  = {   0,   0,   0, 255 };
        GLfloat _pt[3];
        GLfloat _cnnx[6];
        std::vector<GLfloat> _wireTri;
        std::vector<GLubyte> _wireCol;
        unsigned int         _hiPt;
        unsigned int         _hiCnnx;
        unsigned int         _hiLine;
        unsigned int         _hiSurf;
        unsigned int         _hiVol;
        int                  _ptRad     = 1.;
        int                  _cnnxRad   = 1.;
        int                  _lineRad   = 1.;
        int                  _nPtVtx    = 1;
        int                  _nCnnxVtx  = 0;
        int                  _nLineVtx  = 0;
        bool                 _line3D    = false;
        size_t               _n_filled;
        GLfloat               defmat[4] = {1., 0.7, 80., 1.};
        void buffer_hiEles(int, const GLfloat*, bool, const GLubyte*, const GLubyte*);
};


int  generate_shader_program(const char* vertex_source, const char* fragment_source);
void align_objects();
void render_cross(float x, float y, float z, float len, float line_width);
int  buff_vtx( GLfloat *, GLubyte *&, const GLfloat *, const short_float *, const GLfloat *, GLushort );
int  buff_vtx( GLfloat *, GLubyte *&, const GLfloat *, const short_float *, const GLubyte *, GLushort );
int  mk_icosahedron( int nsubdiv, std::vector<unsigned int>& tri, std::vector<GLfloat>& vtx );
inline int  arrow3D_ntri( int nd, bool head ){ return (head?5:4)*nd; }
inline int  arrow3D_nvtx( int nd, bool head, bool strip=false ){ return strip?(nd*2):((head?5:4)*nd*3); }
void buffer_alpha( GLubyte *, GLfloat );
void buffer_RGB( GLubyte *, const GLfloat*, bool alpha=false );

int  recolour_cyl( GLfloat* &, int, const GLubyte *, const  GLubyte *, bool, bool, bool=true, GLfloat* &a=nulptr  );

#include "VecData.h"
/**
 * @brief render a line segment as a cylinder. 
 * If joining to previous segment, the points on the end of the previous cylinder are used,
 * otherwise points on the plane normal to the cylinder axis passing through the starting vertex are generated.
 * If there is a following segment, the intersection plane between the two segments is the average of the 2
 * end cap planes passing through the second vertex, otherwise the end cap plane is used.
 * The points from the starting end are translated along the cylinder axis to the end plane.
 *
 * @param buff   destination buffer to fill
 * @param vtx0   starting vertex
 * @param vtx1   ending vertex
 * @param ns     number of sides
 * @param radius radius
 * @param c0     color vertex 0 (r,g,b,a)
 * @param c1     color vertex 1 (r,g,b,a)
 * @param join   join segment with previous one, else cap
 * @param next   next vertex in line, or NULL if none
 * @param strip  draw as a triangle strip
 * @param capbuf buffer to fill for end caps (as TRIANGLE_FAN)
 * @param capcol buffer to fill for end cap colours
 * @tparam T     type of colour data
 *
 * @return number of GLfloats added to buffer
 *
 * \pre  \p buff is large enough to hold cylinder: ( 3*\p ns*( (\p join?0:1) + 2 + (\p next==NULL?1:0) )*VBO_ELEM_SZ
 * \pre  if \p join, preceding cylinder is just before \p buff
 * \post \p buff points to one past the last entry
 * @post  if specified, \p capbuf and \p capcol are filled and incremented to the next entry
 */
template<typename T>
int 
render_data::buff_cyl( GLfloat* &buff, const GLfloat *vtx0, const GLfloat *vtx1, int ns, 
          GLfloat radius, const T *c0, const T *c1, int reg, bool join, 
          const GLfloat *next, bool strip, GLfloat* &capbuf, GLubyte* &capcol            )
{
  GLfloat *b0 = buff;

  // compute cylinder axis
  GLfloat axis[3];
  normalize(sub( vtx1, vtx0, axis ));
 
  // calculate cylinder points
  GLfloat*     pt[ns*2];
  short_float  n[ns*2][3];
  GLfloat      npts[ns*2][3];
  GLfloat      newn[ns*2][3];

  // do first end which may join to previous segment
  if( join ) {
    // if join, copy initial points from last segment
    GLfloat* pt0[ns];
    GLfloat* base = buff - VBO_ELEM_SZ*(strip?ns:ns*6-5);
    for( int i=0; i<ns; i++ ) {
      pt[i] = base + VBO_XYZ_OFFSET + i*VBO_ELEM_SZ*(strip?1:6);
      // figure out the normal of the old point to the new axis
      GLfloat d[3];
      GLfloat pa[3];
      sub( pt[i], vtx0, d );
      assign( pa, axis );
      normalize( sub( d, scale( pa, dot( axis, d ) ), newn[i] ) );
      SHORT_VCP( n[i], scale(newn[i],-1.) );
    }
  } else { // make new points
    // compute orthogonal radial directions
    GLfloat r0[3]={0.,0.,0.}, 
            r1[3]={0.,0.,0.},
            zp[3]={0,0,1},
            zn[3]={0,0,-1};
    if( equal_pt(axis, zp, 1.e-4f ) ||
        equal_pt(axis, zn, 1.e-4f )   )
      r0[0] = 1.;
    else 
      r0[2] = 1.;  
    GLfloat proj[3];
    sub( r0, scale( assign(proj,axis), dot(axis,r0) ), r0 ); 
    normalize(r0);
    cross( axis, r0, r1 );
    for( int i=0; i<ns; i++ ) {
      GLfloat tmpC[3], tmpS[3];
      assign( tmpC, r0 );
      assign( tmpS, r1 );
      float ang = 2*M_PI/(float)ns*i;
      add( scale( tmpC, cos(ang) ), scale( tmpS, sin(ang) ), newn[i] );
      scale( assign( tmpC, newn[i] ), radius );
      add( vtx0, tmpC, npts[i] );
      SHORT_VCP( n[i], scale( newn[i], -1. ) );
      pt[i] = npts[i];
    }
  }

  // do second end by translating points along axis direction
  GLfloat capn[3];
  assign( capn, axis );
  if( next ) {
    // calculate average plane of intersection
    normalize(sub( next, vtx1, capn ));
    if( dot( capn, axis)>POINT_BACK_LIMIT )  // does not point back on itself
      scale( add(axis, capn, capn), 0.5 );
  }
  float d = dot( capn, vtx1 );
  for( int i=0; i<ns; i++ ) {
    // find intersection of line through p[i] in axial 
    // direction with intersection plane
    // http://geomalgorithms.com/a05-_intersect-1.html
    GLfloat dx[3];
    GLfloat denom = dot( capn, axis );
    GLfloat num   = dot( capn, sub( pt[i], vtx1, dx ) );
    assign( dx, axis );
    add( pt[i], scale( dx, -num/denom ), npts[i+ns] );
    assign( n[ns+i], n[i] );
    pt[i+ns] = npts[i+ns];
  }

  // draw start cap if not joining
  if( !join ) {
    SHORTVEC( shaxis, axis[0], axis[1], axis[2] );
    if(strip) {
      for( int i=0; i<ns; i++ ) 
        buff += buff_vtx( buff, pt[i], n[i], c0, reg );
      if( capbuf ){
        capbuf += ::buff_vtx( capbuf, capcol, vtx0, shaxis, c0, reg );
        for( int i=0; i<=ns; i++ ) {
          capbuf += ::buff_vtx( capbuf, capcol, pt[i%ns], shaxis, c0, reg );
        }
      }
    }else if( !strip ){
      for( int i=0; i<ns; i++ ) {
        int j = (i+1)%ns;
        buff_vtx( buff,                vtx0, shaxis, c0, reg );
        buff_vtx( buff+1*VBO_ELEM_SZ, pt[i], shaxis, c0, reg );
        buff_vtx( buff+2*VBO_ELEM_SZ, pt[j], shaxis, c0, reg );
        buff += VBO_ELEM_SZ*3;
      }
    }
  }

  // make cylinder elements
  if(strip) {
    for( int i=0; i<ns; i++ ) 
      buff += buff_vtx( buff, pt[i+ns], n[i+ns], c1, reg );
  } else {
    for( int i=0; i<ns; i++ ) {
      int j=(i+1)%ns;
      buff_vtx( buff,               pt[j],    n[j],    c0, reg );
      buff_vtx( buff+1*VBO_ELEM_SZ, pt[i],    n[i],    c0, reg );
      buff_vtx( buff+2*VBO_ELEM_SZ, pt[ns+j], n[ns+j], c1, reg );
      buff_vtx( buff+3*VBO_ELEM_SZ, pt[i],    n[i],    c0, reg );
      buff_vtx( buff+4*VBO_ELEM_SZ, pt[ns+i], n[ns+i], c1, reg );
      buff_vtx( buff+5*VBO_ELEM_SZ, pt[ns+j], n[ns+j], c1, reg );
      buff += VBO_ELEM_SZ*6;
    }
  }

  // draw end cap?
  if( next==NULL ){
    scale( axis, -1. );
    SHORTVEC( shaxis, axis[0], axis[1], axis[2] );
    if(strip && capbuf) {
      capbuf += ::buff_vtx( capbuf, capcol, vtx1, shaxis, c1, reg );
      for( int i=0; i<=ns; i++ ) 
        capbuf += ::buff_vtx( capbuf, capcol, pt[2*ns-1-(i%ns)], shaxis, c1, reg );
    } else if(!strip) {
      for( int i=0; i<ns; i++ ) {
        int j = (i+1)%ns+ns;
        buff_vtx( buff,               vtx1,     shaxis, c1, reg );
        buff_vtx( buff+1*VBO_ELEM_SZ, pt[i+ns], shaxis, c1, reg );
        buff_vtx( buff+2*VBO_ELEM_SZ, pt[j],    shaxis, c1, reg );
        buff += VBO_ELEM_SZ*3;
      }
    }
  }

  return buff - b0;
}

#endif  // header guard
