/**
* @file asciireader.hpp
* @brief Class that reads an ascii file into memory to enable parallel parsing.
* @author Aurel Neic
* @version
* @date 2018-09-18
*/
#ifndef ASCII_READER_HPP
#define ASCII_READER_HPP

#include <stdio.h>
#include <errno.h>
#include <string.h>

#include <iostream>
#include <string>
#include <vector>



/// Class that reads an ascii file into memory to enable parallel parsing.
class AsciiReader {

  public:
  AsciiReader( ){}
  AsciiReader(std::string file, char error_act = 2){read_file(file, error_act);}
  /**
  * @brief Read a whole file into memory
  *
  * @param file  The file to read.
  * @param error_act flags:1=throw,2=print
  */
  void read_file(std::string file, char error_act = 2)
  {
    FILE* fd = fopen(file.c_str(), "r");
    if(!fd) {
      this->treat_file_error(file, errno, error_act);
      return;
    }
    // make sure it is not a directory
    struct stat path_stat;
    stat(file.c_str(), &path_stat);
    if( !S_ISREG(path_stat.st_mode) ) {
      this->treat_file_error(file, errno, error_act);
      return;
    }

    fileopened = file;

    size_t filesize = get_file_size(fd);
    bytebuffer.resize(filesize);

    // read the entire textfile into the byte buffer
    fread(bytebuffer.data(), filesize, 1, fd);
    fclose(fd);
    
    // we do not know how many lines the file holds, we approximate
    // 50 bytes per line
    buff_dsp.reserve(filesize / 50);

    int line_cnt = 0;
    buff_dsp.push_back(0);
    size_t idx = 0, dsp_idx= 0;

    while(idx < filesize) {
      line_cnt++;

      if(bytebuffer[idx] == '\n') {
        dsp_idx++;
        buff_dsp.push_back(buff_dsp[dsp_idx - 1] + line_cnt);
        line_cnt = 0;
        bytebuffer[idx] = '\0';
      }

      idx++;
    }
    bytebuffer.push_back('\0'); 
  }


  /**
   * @brief look at a line
   *
   * @param i line number
   *
   * @return pointer to line 
   */
  const char *operator[](size_t i) {
    return peek(i);
  }

  /**
  * @brief Return the number of read lines.
  *
  * @return Number of read lines.
  */
  size_t num_lines()
  {
    if(buff_dsp.size() == 0) return 0;
    else                     return buff_dsp.size() - 1;
  }

  /**
  * @brief Copy a line into a given buffer.
  *
  * @param line_nbr  The line number to copy.
  * @param buff      The provided buffer.
  * @param buffsize  The size of the provided buffer.
  *
  * @return
  */
  bool get_line(size_t line_nbr, char* buff, size_t buffsize)
  {
    if(bytebuffer.size() == 0)
      return false;

    if(line_nbr >= buff_dsp.size())
      return false;

    size_t linesize = buff_dsp[line_nbr + 1] - buff_dsp[line_nbr];

    if(buffsize <= linesize)
      return false;

    for(size_t i=0; i<linesize; i++)
      buff[i] = bytebuffer[buff_dsp[line_nbr]+i];

    buff[linesize] = '\0';

    return true;
  }

  const char* peek( size_t line_nbr )
  {
    if(bytebuffer.size()==0 || line_nbr>=buff_dsp.size())
      return NULL;

    return (const char*)(bytebuffer.data()+buff_dsp[line_nbr]);
  }

  void clear() {
    bytebuffer.clear();
    buff_dsp.clear();
    fileopened = "";
  }

  std::string file() { return fileopened; }
  private:
  std::vector<char> bytebuffer;   ///< The buffer holding the read file.
  std::vector<size_t>        buff_dsp;     ///< The displacement between lines.
  std::string                fileopened;

  /// Treat a file read error by parsing the errno error number.
  void treat_file_error(const std::string & file, int error_number, char error_act)
  {
    if( error_act&2 )
      fprintf(stderr, "Error: could not open %s for reading! Reason:\n"
                    "%s\n", file.c_str(), strerror(error_number));
    if(error_act&1)
      throw 1;
  }

  /// Get the size of the file behind the file-descriptor fd
  size_t get_file_size(FILE* fd)
  {
    size_t oldpos = ftell(fd);
    fseek(fd, 0L, SEEK_END);
    size_t sz = ftell(fd);
    fseek(fd, oldpos, SEEK_SET);

    return sz;
  }
};

#endif
