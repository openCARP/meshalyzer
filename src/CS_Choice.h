#ifndef CS_CHOICE_H
#define CS_CHOICE_H

#include <Colourscale.h>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Menu_Item.H>

extern std::map<int,std::string> cs_indx_map;

/** class CS_Choice
 *  \brief A pulldown menu for colour scales displaying the colour scales
 *  The choice can be specified by the name of the colour scale or the index into menu.
 */
class CS_Choice : public Fl_Choice 
{
    public:
      CS_Choice( int X, int Y, int W, int H, const char *L=0);
      void data( void *p ){ _data=p; }            
      void *data( ){ return _data; }
      std::string value();
      using Fl_Choice::value;
      int  ivalue(){return Fl_Choice::value();}  //!< get index into menu
      void value( int i){ Fl_Choice::value(i); } //!< set colour by menu index
      void value(std::string);      
      void update(bool rev);                     //!< reverse colour scale images
    private:
      void *_data = NULL;                         //!< model window
      Fl_Menu_Item *_csmenu = NULL;               //!< the colour scales available
      void make_menu( int, int, bool=false );                 //!< make the images
};


#endif
