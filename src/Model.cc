#include "Model.h"
#include <set>
#include <unordered_set>
#include <map>
#include <vector>
#include <string>
#include "DataOpacity.h"
#include <cstdio>
#ifdef HAVE_GL2PS
#include "gl2ps.h"
#endif
#include "VecData.h"
#include <sstream>
#include <FL/filename.H>
#include "gzFileBuffer.h"
#include "logger.hpp"
#include <algorithm>
#include <utility>
#include <queue>
#ifdef USE_VTK
#include <vtkSmartPointer.h>
#include<vtkUnstructuredGrid.h>
#include<vtkXMLUnstructuredGridReader.h>
#include<vtkCellData.h>
#include <vtkGenericDataObjectReader.h>
#endif

#ifdef _OPENMP
#include<omp.h>
#endif


std::vector<const char *> volIDs{ "Tt", "Py", "Pr", "Hx" };

bool has_ext( std::string, const char * );

/// for reading CARP binary format
typedef enum : int
{
  Tt,
  Hx,
  Oc,
  Py,
  Pr,
  Qd,
  Tr,
  Ln,
  Bogus
} elem_t;

/// get number of nodes for element types
inline short
get_nnodes(elem_t type)
{
  switch(type) {
    case Ln: return 2;
    case Tr: return 3;
    case Qd:
    case Tt: return 4;
    case Py: return 5;
    case Oc:
    case Pr: return 6;
    case Hx: return 8;
    case Bogus: return -1;
  }
  return -1;
}

/**
 * @brief add commas as thousands separators 
 *
 * @param num an integer
 *
 * @return \p num with commas added between every 3 digits
 */
std::string
commaize( int num )
{
  std::string  numstr = std::to_string(num);
  std::string  retnum;
  for( int i=numstr.size()-1; i>=0; i-- ){
    uint from_end=numstr.size()-1-i;
    retnum.insert(0, 1, numstr[i] );
    if( from_end && !((from_end+1)%3) && i )
      retnum.insert(0, "," );
  }
  return retnum;
}


/** return the element file name, if there is one
 *
 * \post if ascii, the file is read in \p _elemfile
 * */
std::string
Model::bin_or_ascii_elem( std::string fn )
{
  struct stat buffer;   
  if( stat(fn.c_str(), &buffer) || !S_ISREG(buffer.st_mode) ){     // provided filename is a regular file?
    if( fn.back() != '.' ) fn.push_back('.');
    fn.append("belem");
    if(stat (fn.c_str(), &buffer) != 0){   // binary elem file exists?
      fn.erase(fn.size()-strlen("belem"),1); // erase 'b' from "belem"
      if(stat (fn.c_str(), &buffer) != 0)  // ASCII file exists?
        return "";
    }
  }
  if( has_ext(fn, ".elem" ) ) _elemfile.read_file( fn, 2 );
  return fn;
}


struct Face {
  int nsort[MAX_NUM_SURF_NODES];  //!< sorted nodes
  int norig[MAX_NUM_SURF_NODES];  //!< nodes in original order
  int nnode;                      //!< number of nodes
  int elem;                       //!< surface element
};

int intcmp( const void *a, const void *b )
{
  return *(int *)a-*(int *)b; 
}


/** make sure label is legal */
int
check_label( int l, bool& illegal )
{
  if( l<0 ) {
    illegal = true;
    return 0;
  } else
    return l;
}


/**
 * @brief convert std::string to unsigned int
 *
 * @param p
 *
 * @return  int
 * @retval -1 failure
 * \post p is at white space or NULL if EOL
 */
inline
int naive(const char* &p) {
    int x = 0;
    while (*p == ' ') ++p;
    
    if( *p<'0' || *p>'9' ) return -1;

    while (*p >= '0' && *p <= '9') {
        x = (x*10) + (*p - '0');
        ++p;
    }
    while (*p == ' ') ++p;
    if( *p=='\0' ) p=NULL;
    return x;
}


/** compare if 2 faces equivalent */
bool operator==( const Face &A, const Face &B)
{
  if( A.nnode != B.nnode ) return  false;
  return !memcmp( A.nsort, B.nsort, A.nnode*sizeof(A.nsort[0]) );
}


/** generate hash value for a face, only use first 3 nodes */
const size_t hash_offset = 1000000000;
struct face_hash
{
  const int nn=3;

  size_t operator() ( const Face &A ) const {
    unsigned long key = A.nnode;
    for( int i=0; i<nn; i++ )
      key += A.nsort[i]*hash_offset*(i+1);
    return std::hash<unsigned long>()(key);
  }
};

typedef std::unordered_set<Face, face_hash> faceset;


/** make a face from a node list
 *
 * \param f[out] new face
 * \param n      number of nodes in surface face
 * \param orig   nodes in original order
 */
void
make_face( Face &f, int n, const int* orig, int e=-1  )
{
  f.nnode = n;
  memcpy( f.norig, orig, n*sizeof(int) );
  memcpy( f.nsort, orig, n*sizeof(int) );
  int *a = f.nsort;
  // sort the nodes
  if( n==3 ) {
    if (a[0]>a[1]) std::swap(a[0], a[1]);
    if (a[0]>a[2]) std::swap(a[0], a[2]);
    if (a[1]>a[2]) std::swap(a[1], a[2]);
  } else if( n==4 ) {
    if(a[0]>a[1]) std::swap(a[0],a[1]);
    if(a[2]>a[3]) std::swap(a[2],a[3]);
    if(a[0]>a[2]) std::swap(a[0],a[2]);
    if(a[1]>a[3]) std::swap(a[1],a[3]);
    if(a[1]>a[2]) std::swap(a[1],a[2]);
  } else
    qsort( f.nsort, n, sizeof(int), intcmp ); 
  f.elem = e;
}


/** read in a line from a file, ignoring lines beginning with "#"
 *
 * \warning (GD) lines are limited to 2048 bytes.
 * \warning not threadsafe
 * \return pointer to a static buffer
 */
char *
get_line( gzFile in )
{
  const int bufsize=2048;
  static char buf[bufsize];
  char *retval;
  do {
	retval = gzgets( in, buf, bufsize );
	if (retval == Z_NULL)
	  {
	    int err;
	    char const * errd = gzerror(in, &err);
	    std::cerr << errd << std::endl;
	    continue;
	  }
  } while( retval != Z_NULL && buf[0]=='#' );
  
  return retval==Z_NULL?NULL:buf;
}


/** format std::string for hilight window output 
 *
 * \param i  index
 * \param hi is this a hilighted index
 *
 * \return a pointer to a constant std::string
 */
char * const
format_hilite( int i, bool hi ) 
{
  static char txt[256];
  if( !hi )
    sprintf( txt, "\t%6d", i );
  else
    sprintf( txt,"@B%d\t%6d", FL_GRAY, i);
  return txt;
}


/** return a new unique region label
 */
int Model::new_region_label()
{
  int  r, newlabel=0; 
  while ( true ) {
    for ( r=0; r<_numReg; r++ )
      if ( _region[r]->label() == newlabel )
        break;
    if ( r==_numReg )
      break;
    newlabel++;
  }
  return newlabel;
}


Model::Model()
{
  for ( int i=0; i<maxobject; i++ ) {
    _outstride[i] = 1;
  }
}


/** read in the geometrical description
 *
 * \param flwindow
 * \param fnt      base file to open
 * \param base1    points begin numbering at 0
 * \param no_elems do not read element file
 */
const int bufsize=1024;
bool Model::read( const char* fnt, bool base1, bool no_elems )
{
  _base1 = base1;
  pt.base1( base1 );
  
  LOG_TIMER_RESET;
  try {
    pt.read( fnt );
  } catch (...) {
    fprintf(stderr, "Unable to read proper points file\n" );
    exit(1);
  }
  LOG_TIMER("Read Points");

  allvis.resize(pt.num());
  allvis.assign(pt.num(), true );

  _file = fnt;
  if( has_ext( _file, ".pts" ) )
    _file.erase(_file.size()-4);
  else if( has_ext( _file, ".bpts" ) )
    _file.erase(_file.size()-5);
  
  LOG_TIMER_RESET;
  std::string elemfile = bin_or_ascii_elem( _file ); 
  LOG_TIMER("Read Connections");

  LOG_TIMER_RESET;
  _cnnx   = new Connection( &pt );
  _cnnx->read( _file.c_str() );
  _cnnx->read_cables( _file.c_str() );
  add_cnnx_from_elem( elemfile );
  LOG_TIMER("Read Connections");

  if(!no_elems) {
    LOG_TIMER_RESET;
    no_elems = !read_elem_file( elemfile );
    LOG_TIMER("Read Elements");
  }

  read_region_file( _file );

  LOG_TIMER_RESET;
  determine_regions();
  LOG_TIMER("Determine Regions");
  
  LOG_TIMER_RESET;
  add_surface_from_elem( elemfile );
  LOG_TIMER("Add Surface from Elements");
  
  LOG_TIMER_RESET;
  add_surface_from_tri( _file );
  LOG_TIMER("Add Surface from Tri");
  
  LOG_TIMER_RESET;
  add_surface_from_surf( _file );
  LOG_TIMER("Add Surface from Surf");

  find_max_dim_and_bounds();

  _elemfile.clear();

  return true;
}

#ifdef USE_HDF5
bool Model::read(hid_t hdf_file, bool base1, bool no_elem)
{
  LOG_TIMER_RESET;
  pt.read(hdf_file);
  LOG_TIMER("Read Points");
  
  char *modelname;
  ch5_meta_get_name(hdf_file, &modelname);
  _file = modelname;
  free(modelname);

  allvis.resize(pt.num());
  allvis.assign(pt.num(), true);
  
  LOG_TIMER_RESET;
  _cnnx = new Connection(&pt);
  _cnnx->read(hdf_file);
  LOG_TIMER("Read Connections");
  
  LOG_TIMER_RESET;
  if (!no_elem) add_elements(hdf_file);
  LOG_TIMER("Read Elements");
  
  LOG_TIMER_RESET;
  add_regions(hdf_file);
  determine_regions();
  LOG_TIMER("Add + Determine Regions");
  
  LOG_TIMER_RESET;
  add_surfaces(hdf_file);
  LOG_TIMER("Add Surfaces");
  
  find_max_dim_and_bounds();
  
  return true;
}


void Model::add_surfaces(hid_t hdf_file) {
  int *elements;
  ch5_dataset dset_info;
  
  // From 2D elements
  if ( ch5m_elem_get_info(hdf_file, &dset_info) ){
    std::cerr << "Could not find elements dataset" << std::endl;
    return;
  }
  
  elements = (int*) malloc(sizeof(int) * dset_info.count * dset_info.width);
  if (elements == NULL) {
    std::cerr << "Could not allocate memory for elements" << std::endl;
    exit(1);
  }
  
  if ( ch5m_elem_get_all(hdf_file, elements) ){
    free(elements);
    std::cerr << "Could not read from elements dataset" << std::endl;
    return;
  }
  
  add_surfaces(elements, dset_info.count, dset_info.width, NULL);
  free(elements);
  
  // From explicitly defined surfaces
  int surfaceCount = ch5m_surf_get_count(hdf_file);
  for (int i = 0; i < surfaceCount; i++) {
    hid_t surf_id;
    ch5m_surf_open(hdf_file, i, &surf_id);
    
    ch5m_surf_get_elem_info(surf_id, &dset_info);
    
    elements = (int*) malloc(sizeof(int) * dset_info.count * dset_info.width);
    if (elements == NULL) {
      std::cerr << "Could not allocate memory for elements" << std::endl;
      exit(1);
    }

    char file_name_buf[255];
    char *surf_name;
    int name_result = ch5m_surf_get_name(surf_id, &surf_name);
    if (name_result != 1) {
      H5Fget_name(hdf_file, file_name_buf, 255);
      surf_name = (char*) malloc(sizeof(char) * 255);
      sprintf(surf_name, "%s:%d", fl_filename_name(file_name_buf), i);
    }
    if ( !ch5m_elem_get_all_by_dset(surf_id, elements) )
      add_surfaces(elements, dset_info.count, dset_info.width, surf_name);
    else
      std::cerr << "Could not read from surface " << i << " dataset" << std::endl;
    free(elements);
    free(surf_name);
    
    ch5m_surf_close(surf_id);
    
  }
}

void Model::add_surfaces(int *elements, int count, int max_width, char *name) {
  // Count up the number of elements in each region and build some lookup maps
  // for pairing regions to newly formed surfaces
  std::map<int,int> regionCounts;
  for (int i = 0; i < count; i++) {
    if ((elements[i * max_width] == CH5_TRIANGLE) || (elements[i * max_width] == CH5_QUADRILATERAL))
      regionCounts[elements[i * max_width + CH5_ELEM_REGION_OFFSET]]++;
  }
  if (regionCounts.size() == 0) return;
  
  std::map<int,int> regionToSurfaceIndex;
  std::map<int,int> elementCounts;
  std::map<int,int>::iterator regionCountsIter = regionCounts.begin();
  std::map<int,int>::iterator regionCountsEnd  = regionCounts.end();
  int newSurfaceCount = 0;
  int origSurfaceEnd  = _surface.size();
  while (true) {
    if (regionCountsIter->second != 0) {
      Surfaces *newSurf = new Surfaces(&pt);
      newSurf->num(regionCountsIter->second);
      if (name != NULL) newSurf->label(name);
      _surface.push_back(newSurf);
      regionToSurfaceIndex[regionCountsIter->first] = _surface.size() - 1;
      elementCounts[regionCountsIter->first] = 0;
      newSurfaceCount++;
    }
    if (++regionCountsIter == regionCountsEnd) break;
  }
  
  // Iterate through elements and populate the surfaces
  if (newSurfaceCount > 0) {
    for (int i = 0; i < count; i++) {
      int region = elements[i * max_width + CH5_ELEM_REGION_OFFSET];
      int surfaceIndex = regionToSurfaceIndex[region];
      int elementCount = elementCounts[region];
      switch (elements[i * max_width]) {
        case CH5_TRIANGLE:
          _surface[surfaceIndex]->addele(elementCount, new Triangle(&pt));
          break;
        
        case CH5_QUADRILATERAL:
          _surface[surfaceIndex]->addele(elementCount, new Quadrilateral(&pt));
          break;
        
        default:
          break;
      }
      if (_surface[surfaceIndex]->ele(elementCount) == NULL) continue;
      _surface[surfaceIndex]->ele(elementCount)->define(&elements[i * max_width + CH5_ELEM_DATA_OFFSET]);
      _surface[surfaceIndex]->ele(elementCount)->compute_normals(0,0);
      elementCounts[region]++;
    }
    
    for (int i = origSurfaceEnd; i < origSurfaceEnd + newSurfaceCount; i++) {
      _surface.at(i)->determine_vert_norms(pt);
      _surface.at(i)->count_tris();
      _surface.at(i)->assign_reg(_region, _numReg);
    }
  }
}
#endif // USE_HDF5

#ifdef USE_VTK

const char *VTK2CARP_etype[] = {
  " ",      // VTK_EMPTY_CELL       = 0,
  "Pt",     // VTK_VERTEX           = 1,
  "PP",     // VTK_POLY_VERTEX      = 2,
  "Ln",     // VTK_LINE             = 3,
  "Ca",     // VTK_POLY_LINE        = 4,
  "Tr",     // VTK_TRIANGLE         = 5,
  "TS",     // VTK_TRIANGLE_STRIP   = 6,
  "PG",     // VTK_POLYGON          = 7,
  "Px",     // VTK_PIXEL            = 8,
  "Qd",     // VTK_QUAD             = 9,
  "Tt",     // VTK_TETRA            = 10,
  "Vx",     // VTK_VOXEL            = 11,
  "Hx",     // VTK_HEXAHEDRON       = 12,
  "Pr",     // VTK_WEDGE            = 13,
  "Py",     // VTK_PYRAMID          = 14,
  "Pr",     // VTK_PENTAGONAL_PRISM = 15,
  "HP",     // VTK_HEXAGONAL_PRISM  = 16,
};


/** read in all types of elements 
 *
 * \param grid     VTU mesh
 * \param no_velem do not read volum elements
 *
 * \return true if surface elements present
 */
bool
Model:: read_objects(vtkUnstructuredGrid* grid, bool no_velem )
{
  int np = grid->GetNumberOfPoints();

  GLfloat  *p = new GLfloat[np*3];
  double *x;
  for( int i=0;i<np; i++ ){
    x = grid->GetPoint(i);
    for( int j=0; j<3; j++ )
      p[3*i+j] = x[j];
  }
  pt.add( p, np );

  allvis.resize(pt.num());
  allvis.assign(pt.num(), true );

  _cnnx       = new Connection( &pt );
  int numCell = grid->GetNumberOfCells(),
      nsurf   = 0,
      ncnnx   = 0,
      nvol    = 0;

  // count the element types
  for( int i=0; i<numCell; i++ ) {
    const char *eletype = VTK2CARP_etype[grid->GetCellType(i)];
    if(  !strcmp( eletype, "Tt" ) || !strcmp( eletype, "Hx" ) || 
         !strcmp( eletype, "Py" ) || !strcmp( eletype, "Pr" )   )
      nvol++;
    else if( !strcmp( eletype, "Tr" ) ||  !strcmp( eletype, "Qd" ) )
      nsurf++;
    else if( !strcmp( eletype, "Ln" ) ) {
      ncnnx++;
    } else {
      fprintf(stderr, "Unsupported element type: %s\n", eletype);
    }
  }

  if( no_velem ) nvol=0;
  _vol.resize(nvol);
  nvol = 0;

  if( nsurf ) {
    Surfaces *newSurf = new Surfaces(&pt);
    newSurf->label("default");
    _surface.push_back(newSurf);
    newSurf->num(nsurf);
    nsurf = 0;
  }

  vtkDataArray* regdata = grid->GetCellData()->GetArray("Regions");
  if( !regdata )
    regdata = grid->GetCellData()->GetArray("elemTag");  // Aurel's choice

  std::vector<int> cnnx;
  int reg = 0;
  int start_ln = -1;

  // read in elements
  for( int i=0; i<numCell; i++ ) {

    const char *eletype = VTK2CARP_etype[grid->GetCellType(i)];

    // convert points to int
    int n[10];
#if VTK_MAJOR_VERSION == 8
    vtkIdType *nvtk, nn;
    grid->GetCellPoints( i, nn, nvtk );
    for( int j=0; j<nn; j++ )
      n[j] = nvtk[j];
#else
    vtkSmartPointer<vtkIdList> nl =
      vtkSmartPointer<vtkIdList>::New();
    grid->GetCellPoints( i, nl );
    for( int j=0; j<nl->GetNumberOfIds(); j++ )
      n[j] = nl->GetId(j);
#endif

    if( regdata ) reg = (int)( regdata->GetTuple1(i) );

    if( !no_velem && !strcmp( eletype, "Tt" ) ) {
      _vol[nvol] = new Tetrahedral( &pt );
      _vol[nvol]->add( n, reg );
      nvol++;
    } else if( !no_velem && !strcmp( eletype, "Hx" ) ) {
      std::swap( n[5], n[7] );
      _vol[nvol] = new Hexahedron( &pt );
      _vol[nvol]->add( n, reg );
      nvol++;
    } else if( !no_velem && !strcmp( eletype, "Py" ) ) {
      _vol[nvol] = new Pyramid( &pt );
      _vol[nvol]->add( n, reg );
      nvol++;
    } else if( !no_velem && !strcmp( eletype, "Pr" ) ) {
      std::swap( n[1], n[2] );
      _vol[nvol] = new Prism( &pt );
      _vol[nvol]->add( n, reg );
      nvol++;
    } else if( !strcmp( eletype, "Tr" ) ) {
      std::swap( n[1], n[2] );
	  _surface[0]->addele(nsurf, new Triangle( &pt ));
	  _surface[0]->ele(nsurf)->define(n);
  	  _surface[0]->ele(nsurf)->compute_normals(0,0);
      nsurf++;
    } else if ( !strcmp( eletype, "Qd" )  ) {
      std::swap( n[1], n[3] );
	  _surface[0]->addele(nsurf, new Quadrilateral( &pt ));
      _surface[0]->ele(nsurf)->define(n);
      _surface[0]->ele(nsurf)->compute_normals(0,0);
      nsurf++;
    } else if( !strcmp( eletype, "Ln" ) ) {
      if( start_ln>=0 && cnnx.back()!=n[0] ){
        _cnnx->add_line( start_ln, cnnx.size()/2-1 );
        start_ln = -1;
      } else if( start_ln<0 && cnnx.back()==n[0] )
        start_ln =  cnnx.size()/2-1;
      cnnx.push_back(n[0]);
      cnnx.push_back(n[1]);
    } 
  }

  // add any connections found
  if( start_ln>=0 )_cnnx->add_line( start_ln, cnnx.size()/2-1 );
  if( cnnx.size() )
    _cnnx->add( cnnx.size()/2., cnnx.data() );

  // add any surface elements found
  if( nsurf ) {
    _surface[0]->num(nsurf);
    _surface[0]->determine_vert_norms( pt );
    _surface[0]->count_tris();
    _surface[0]->assign_reg(_region,_numReg);
    _surface[0]->label( "0" );
    return true;
  }

  return false;
}


bool 
Model::read_vtu( const char* filename, bool no_elem, bool legacy ) 
{
  vtkUnstructuredGrid* grid;

  if( legacy ) {
    vtkSmartPointer<vtkGenericDataObjectReader> reader =
      vtkSmartPointer<vtkGenericDataObjectReader>::New();
    reader->SetFileName(filename);
    reader->Update();
    grid = reader->GetUnstructuredGridOutput();
    read_objects(grid, no_elem );
  } else {
    vtkSmartPointer<vtkXMLUnstructuredGridReader> reader =
        vtkSmartPointer<vtkXMLUnstructuredGridReader>::New();
    reader->SetFileName(filename);
    reader->Update();
    grid = reader->GetOutput();
    read_objects(grid, no_elem );
  }

  _file = filename;
  _file.erase(_file.size()-4);
 
  determine_regions();
  
  find_max_dim_and_bounds();
  return true;
}


#endif


void Model::find_max_dim_and_bounds()
{
  const GLfloat *p = pt.pt();
  const GLfloat *offset = pt.offset();

  _maxdim = fabs(p[0]-offset[0]);
  for ( int i=0; i<pt.num()*3; i+=3 ) 
    for( int j=0; j<3; j++ )
      if ( fabs(p[i+j]-offset[j])>_maxdim ) 
        _maxdim = fabs(p[i+j]-offset[j]);
      
  int i=1;
  GLfloat z0 = pt.pt(0)[2];
  for( ; i<pt.num(); i++ )
    if( pt.pt(i)[2] != z0 )
      break;
  _2D = i==pt.num();
}


/** determine which points are part of all the regions
 *
 * \pre all objects (vertices, cables, etc.) have been read in
 */
void Model::determine_regions()
{
  if (!_vol.size() && !_numReg  ) {
    _region = (RRegion **)calloc( 1, sizeof(RRegion *) );
    _region[0] = new RRegion( pt.num() ); //0 ==default region label
    _numReg = 1;
    return;
  }

  std::unordered_set<int>reglabels;

  for ( int i=0; i<_vol.size(); i++ ) {
    auto res = reglabels.insert( _vol[i]->region(0) );
    if ( res.second ) {
      _numReg++;
      _region = (RRegion**)realloc( _region, sizeof(RRegion)*_numReg );
      _region[_numReg-1]=new RRegion(_vol,pt.num(),_vol[i]->region(0));
    }
  }

  // make sure that all points are included
  std::vector<bool> hasPtRegion(pt.num(),false);
#pragma omp parallel for
  for ( int i=0; i<pt.num(); i++ )
    for ( int r=0; r<_numReg; r++ )
      hasPtRegion[i] = hasPtRegion[i] || _region[r]->pt_member(i);

  for ( int i=0; i<pt.num(); i++ )
    if ( !hasPtRegion[i] ) { // point without a region detected
      _region = (RRegion**)realloc( _region, sizeof(RRegion)*(_numReg+1) );
      _region[_numReg] = new RRegion( pt.num(), _vol.size(), 
                                            new_region_label(), false );
      _numReg++;
      for ( int j=i; j<pt.num(); j++ )
        if ( !hasPtRegion[j] ) _region[_numReg-1]->pt_member( j, true );
      break;
    }

#ifdef _ARCH_PPC
  heapsort( _region, _numReg, sizeof(RRegion *), RRegion_sort );
#else
  qsort( _region, _numReg, sizeof(RRegion *), RRegion_sort );
#endif

  // find first connection in layer
  if ( _cnnx != NULL ) {
    for ( int r=0; r<_numReg; r++ ) {
      for ( int i=0; i<_cnnx->num(); i++ ) {
        const int *o = _cnnx->obj(i);
        if ( _region[r]->pt_member(o[0])==true ) {
          _region[r]->first(Cnnx,i);
          break;
        }
      }
    }
  }
}


/** add conections by selecting them from .elem file
 *
 *  \param fname element file
 */
int Model::add_cnnx_from_elem( std::string efile )
{
  if( has_ext(efile, ".belem") || !_elemfile.num_lines() ) 
    return 0;

  int    numcx=0, *cnnx=NULL;
  char   old_reg[10] = "Initial";
  int    start_ln    = -1;
  bool   reg_read    = false;
  for( int i=1; i<_elemfile.num_lines(); i++ ) {
	char reg[10];
	if( !strncmp(_elemfile.peek(i),"Ln",2) ) {
      if( !(numcx%10000) )
        cnnx = (int *)realloc( cnnx, (numcx/10000+1)*20000*sizeof(int) );
	  if(sscanf( _elemfile.peek(i),"%*s %d %d %s", cnnx+2*numcx, cnnx+2*numcx+1, reg)<3 )
		strcpy(reg,"EMPTY");
      else
        reg_read = true;
      if( start_ln>=0 && (strcmp( old_reg, reg ) || cnnx[2*numcx]!=cnnx[2*numcx-1]) ){
        _cnnx->add_line( start_ln, numcx-1 );
        start_ln = -1;
      } else if( start_ln<0 && !strcmp( old_reg, reg ) && cnnx[2*numcx]==cnnx[2*numcx-1]  )
        start_ln =  numcx-1;
      numcx++;
      if( strcmp( reg, "EMPTY" ) ) strcpy( old_reg, reg );
	} 
  }
  if( start_ln >= 0 ) _cnnx->add_line( start_ln, numcx-1 );
  _cnnx->add( numcx, cnnx );
  free( cnnx );

  if( numcx && !reg_read ) _cnnx->make_lines();

  return numcx;
}


/** add surface by selecting 2D elements from .elem file
 *
 *  A surface will be created for each different region specified.
 *  Also, all elements without a region specified will form a surface.
 *
 *  \param base model name
 */
int Model::add_surface_from_elem( std::string efile )
{
  if( has_ext(efile, ".belem") ) return 0;

  // Count the number of surface elements in each surface
  std::map<std::string,int> surfs;             // number of elements in each surface

  std::map<std::string,std::string> surfNameMap;
  for( int i=1; i<_elemfile.num_lines(); i++ ) {
    char surfnum[16]  ="";
    char surfname[256]="";
    auto buff = _elemfile.peek(i);
    if ( !strncmp( buff, "Tr", 2 ) ) {
      if ( sscanf(buff+3, "%*d %*d %*d %s %255[^\n]", surfnum, surfname )<1 )
        strcpy( surfnum, "EMPTY" );
      else if( surfname[0] )
        surfNameMap[surfnum] = surfname;
    } else if( !strncmp( buff, "Qd", 2 )) {
      if ( sscanf(buff+3, "%*d %*d %*d %*d %s %255[^\n]", surfnum, surfname )<1 )
        strcpy( surfnum, "EMPTY" );
      else if( surfname[0] )
        surfNameMap[surfnum] = surfname;
    }
    if ( strlen(surfnum) )
      surfs[surfnum]++;
  }
  if ( !surfs.size() ) return numSurf();

  // allocate new surfaces
  int oldnumSurf = _surface.size();
  for ( int s=oldnumSurf; s<oldnumSurf+surfs.size(); s++ )
    _surface.push_back( new Surfaces( &pt ) );
  std::map<std::string,int> surfmap;
  auto iter = surfs.begin();
  for ( int s=oldnumSurf; iter!=surfs.end(); iter++, s++ ) {
    _surface[s]->num(iter->second );
    if( surfNameMap.count(iter->first) )
        _surface[s]->label( surfNameMap[iter->first] );
    else {
      std::string sname;
      if( iter->first != "EMPTY" )
        sname = iter->first+"-";
      sname.append( _elemfile.file(), 0, _elemfile.file().size()-5 );
      _surface[s]->label( sname );
    }
    surfmap[iter->first] = s;   // map region to surface index
  }

  surfs.clear();                // now use for current element in each surface
  /*
   * note that we flip the node order to get the proper normal 
   * \todo Should not be this way!
   */
  for( int i=1; i<_elemfile.num_lines(); i++ ) {
    auto buff = _elemfile.peek(i);
	char etype[10],reg[10];
	int  idat[4];
	if( !strncmp(buff,"Tr",2) ) {
	  if(sscanf( buff,"%s %d %d %d %s", etype, idat, idat+2, idat+1, reg)<5 )
		strcpy(reg,"EMPTY");
	  _surface[surfmap[reg]]->addele(surfs[reg],new Triangle( &pt ));
	} else  if( !strncmp(buff,"Qd",2) ) {
	  if(sscanf( buff,"%s %d %d %d %d %s", etype, idat, idat+3, idat+2, 
				  idat+1, reg)<6 )
		strcpy(reg,"EMPTY");
	  _surface[surfmap[reg]]->addele(surfs[reg],new Quadrilateral( &pt ));
	} else
	  continue;  //ignore volume elements
	_surface[surfmap[reg]]->ele(surfs[reg])->define(idat);
	_surface[surfmap[reg]]->ele(surfs[reg])->compute_normals(0,0);
	_surface[surfmap[reg]]->src_ele(surfs[reg],i-1);
	surfs[reg]++;
  }

  for ( int s=oldnumSurf; s<_surface.size(); s++ ) {
    _surface[s]->determine_vert_norms( pt );
    _surface[s]->count_tris();
    _surface[s]->assign_reg(_region,_numReg);
  }

  for( int i=0; i<_surface.size(); i++ ) _surface[i]->index(i);
  return _surface.size();
}


/** find bounding surfaces for all the regions
 *
 *  We do this by adding the faces of all elements into a tree and removing a face if
 *  it appears twice
 *
 *  \return number of new surfaces
 */
int Model::add_region_surfaces()
{
  int numNewSurf=0;

  for( int r=0; r<_numReg; r++ )  {

#ifdef _OPENMP
    int numthrd = omp_get_max_threads();
#else
    int numthrd = 1;
#endif

    std::vector< faceset > facetree(numthrd);

#pragma omp parallel for 
    for( int e=0; e<_vol.size(); e++ ) {
#ifdef _OPENMP
      int thrd    = omp_get_thread_num();
#else
      int thrd    = 0;
#endif
      if( _region[r]->ele_member(e) ) {
        int faces[MAX_NUM_SURF][MAX_NUM_SURF_NODES+1];
        int ns = _vol[e]->surfaces( faces );
        for( int i=0; i<ns; i++ ) {
          Face newface;
          make_face( newface, faces[i][0], faces[i]+1 );
          faceset::iterator iter = facetree[thrd].find(newface);
          if (iter != facetree[thrd].end()) {
            facetree[thrd].erase(iter);
          } else {
            facetree[thrd].insert(newface);
          }
        }
      }
    }

#if 0
    // count the number of faces left in the list
    for( int s=1; s<numthrd; s++ ) {
      for(faceset::iterator sn=facetree[s].begin(); sn!=facetree[s].end(); sn++) { 
        faceset::iterator iter = facetree[0].find(*sn);
        if (iter != facetree[0].end()) {
          facetree[0].erase(iter);
        } else {
          facetree[0].insert(*sn);
        }
      }
    }
#else
    int nleft = numthrd;
    while( nleft>1 ) {
      int add = (nleft+1)/2;
      nleft /= 2;
#pragma omp parallel for num_threads(nleft)
      for( int s=0; s<nleft; s++ ) {
        for(auto sn=facetree[s+add].begin(); sn!=facetree[s+add].end(); sn++) { 
          faceset::iterator iter = facetree[s].find(*sn);
          if (iter != facetree[s].end()) {
            facetree[s].erase(iter);
          } else {
            facetree[s].insert(*sn);
          }
        }
      }
      nleft = add;
    }
#endif

    if( facetree[0].size() ) {   // convert the left over faces into a surface
      numNewSurf++;
      
      _surface.push_back( new Surfaces( &pt ) );
      _surface.back()->num( facetree[0].size() );

      int e=0;
      for(auto iter=facetree[0].begin(); iter!=facetree[0].end(); ++iter) {

        Face newface = *iter;
        if( newface.nnode == 3 ) {
          _surface.back()->addele(e, new Triangle( &pt ));
        } else if( newface.nnode==4 ) {
          _surface.back()->addele(e, new Quadrilateral( &pt ));
        } else {
          std::cout << newface.nnode;
          for( int n=0;n< newface.nnode; n++ )
            std::cout << "In element " << std::distance(facetree[0].begin(),iter) 
                        << " of " << facetree[0].size()
                        << ": " << newface.nnode << std::endl;
          assert(0);
        }

        _surface.back()->ele(e)->define( newface.norig );
        _surface.back()->ele(e)->compute_normals(0,0);
        e++;
      }
      _surface.back()->determine_vert_norms( pt );
      _surface.back()->count_tris();
      _surface.back()->assign_reg(_region,_numReg);
      std::stringstream regnum;
      regnum << "Reg " << _region[r]->label();
      _surface.back()->label( regnum.str() );

    }
  }
  for( int i=0; i<_surface.size(); i++ ) _surface[i]->index(i);
  return numNewSurf;
}


/** add a surface by reading in a .surf file
 * Also 
 *
 * \param fn     basename of file containing surface elements
 * \param single combine all surfaces into one
 *
 * \return     the \#surfaces added, -1 if not successful
 */
int Model::add_surface_from_surf( std::string fname, bool single )
{
  if( !has_ext(fname, ".surf") ) fname += ".surf";
  AsciiReader surffile;

  try{
    surffile.read_file(fname, 1);
  }
  catch(...){
    return -1;
  }

  int  nd[5], nele;
  int  surfnum = 0;
  char surflabel[bufsize];

  size_t ln=0;
  const char *line=surffile.peek(ln++);
  while ( line && sscanf(line, "%d", &nele )==1 ) {

    if( !_surface.size() || !single )
      _surface.push_back( new Surfaces( &pt ) );

    Surfaces *s = _surface.back();

    // use specified surface label if available
    if( sscanf( line, "%d %1023[^\n]", &nele, surflabel ) != 2 ) 
      sprintf( surflabel, "%s:%d", fl_filename_name(fname.c_str()), surfnum );
    s->label( surflabel );

    int currele = s->num();
    s->num(nele+currele);
    for ( int i=currele; i<nele+currele; i++ ) {
      int nl[5];
      char etype[12];
      line=surffile.peek(ln++);
      if ( line && sscanf(line, "%s %d %d %d %d %d",
                             etype, nl, nl+1, nl+2, nl+3, nl+4 ) < 4 ) {
        if(!single)_surface.pop_back();
        return surfnum;
      }
      if( !strcmp(etype, "Tr" ) )
        s->addele(i,new Triangle( &pt ));
      else if( !strcmp(etype, "Qd") )
        s->addele(i,new Quadrilateral( &pt ));
      else {
        if(!single) _surface.pop_back();
        return surfnum;
      }
      s->ele(i)->define(nl);
      if( !check_element( _surface.back()->ele(i) ) )  {
        if(!single) _surface.pop_back();
        return surfnum;
      }
      s->ele(i)->compute_normals(0,0);
    }
    if( !single ){
      s->determine_vert_norms( pt );
      s->count_tris();
      s->assign_reg( _region, _numReg );
    }
    surfnum++;
    line=surffile.peek(ln++);
  } 
  if( single ){
    _surface.back()->determine_vert_norms( pt );
    _surface.back()->count_tris();
    _surface.back()->assign_reg( _region, _numReg );
  }

  for( int i=0; i<_surface.size(); i++ ) _surface[i]->index(i);

  return surfnum;
}


/** add a surface by reading in a .tri file, also try reading a normal file
 *
 * \param fn   file containing tri's
 *
 * \return     the \#surfaces, -1 if not successful
 */
int Model::add_surface_from_tri( std::string fname )
{
  /* determine file name */
  FILE *in;
  AsciiReader trif;
  try{
    trif.read_file( fname.c_str(), 1 );
  }
  catch(int a){ 
    fname += ".tri";
    try{
      trif.read_file( fname.c_str(), 1 );
    }
    catch(int a){ 
      fname += "s";
      try{
        trif.read_file( fname.c_str(), 1 );
      }
      catch(int a){ 
        return -1;
      }
    }
  }

  int  nd[3], ntri;
  bool multi_surface = sscanf( trif.peek(0), "%d %d %d", &ntri, nd+1, nd+2 ) < 3;
  int surfnum = 0;

  if ( multi_surface ) {

    int offset = 1;
    do {
      _surface.push_back( new Surfaces( &pt ) );

      // use specified surface label if available
      char surflabel[1024];
      int nread = sscanf( trif.peek(0), "%d %1023[^\n]", &ntri, surflabel );
      if( !nread ) {
         _surface.pop_back();
         return surfnum;
      } else if( nread != 2 ) {
        sprintf( surflabel, "%s:%d", fl_filename_name(fname.c_str()), surfnum );
      }
      _surface.back()->label( surflabel );
      _surface.back()->num(ntri);

      for ( int i=0; i<ntri; i++ ) {
        _surface.back()->addele(i,new Triangle( &pt ));
        int nl[3];
        if ( sscanf( trif.peek(i+offset), "%d %d %d", nl, nl+1, nl+2 ) < 3 ) {
          _surface.pop_back();
          return surfnum;
        }
        _surface.back()->ele(i)->define(nl);
        if( !check_element( _surface.back()->ele(i) ) ) {
          _surface.pop_back();
          return surfnum;
        }
        _surface.back()->ele(i)->compute_normals(0,0);
      }
      _surface.back()->determine_vert_norms( pt );
      _surface.back()->count_tris();
      _surface.back()->assign_reg( _region, _numReg );
      surfnum++;
      offset += ntri;
    } while ( sscanf( trif.peek(offset++), "%d",&ntri)==1 );

  } else {  // 1 surface
    
    int nl[3];
    _surface.push_back( new Surfaces( &pt ) );
    _surface.back()->num(ntri);
    int curele = 0;
    do {
      if(  sscanf(trif.peek(curele), "%d %d %d",nl, nl+1, nl+2) < 3 ) {
        if( !curele ) {
        _surface.pop_back();
        return -1;
      } else
        break;
      }
	  _surface.back()->addele(curele,new Triangle( &pt ));
  	  _surface.back()->ele(curele)->define(nl);
	  _surface.back()->ele(curele)->compute_normals(0,0);
	}while( trif.peek(++curele) );
    _surface.back()->num(curele);
    _surface.back()->determine_vert_norms( pt );
    _surface.back()->count_tris();
    _surface.back()->assign_reg( _region, _numReg );
    std::stringstream slabel(fname);
    _surface.back()->label( slabel.str() );
    surfnum++;
  }
  for( int i=0; i<_surface.size(); i++ ) _surface[i]->index(i);
  return surfnum;
}


/** set if an object is shown in a region
 */
void Model::showobj( Object_t obj, bool *r, bool f )
{
  for ( int i=0; i<_numReg; i++ )
    if ( r[i] )
      _region[i]->show( obj, f );
}


/** return color of object for surface s
 *
 *  \param obj object type
 *  \param s   region number
 */
GLfloat* Model::get_color( Object_t obj, int s )
{
  return _region[s<0?0:s]->get_color(obj);
}


void Model::get_mat( int s, float &diff, float &spec, float &shine, float &back )
{
  diff  = _surface[s]->diffuse( );
  spec  = _surface[s]->specular( );
  shine = _surface[s]->shiny( );
  back  = _surface[s]->backlight( );
}


void Model::set_mat( int s, float diff, float spec, float shine, float back )
{
  _surface[s]->diffuse( diff );
  _surface[s]->specular( spec );
  _surface[s]->shiny( shine );
  _surface[s]->backlight( back );
}


void Model::set_color( Object_t obj, int s, float r, float g, float b, float a )
{
  if ( obj==Surface) {
    if ( s<0 )
      for ( int i=0; i<numSurf(); i++ )
        _surface[i]->fillcolor( r, g, b, a );
    else
      _surface[s]->fillcolor( r, g, b, a );
  } else if ( obj==SurfEle) {
    if ( s<0 )
      for ( int i=0; i<numSurf(); i++ )
        _surface[i]->outlinecolor( r, g, b, a );
    else
      _surface[s]->outlinecolor( r, g, b, a );
  } else {
    if ( s<0 )
      for ( int i=0; i<_numReg; i++ )
        _region[i]->set_color( obj, r, g, b, a );
    else
      _region[s]->set_color( obj, r, g, b, a );
  }
}

void Model::visibility( int r, bool a )
{
  _region[r]->visible(a);
}

void Model::opacity( int s, float opac )
{
  for ( int i=s<0?0:s; i<(s<0?_numReg:s+1); i++ ) {
    GLfloat *c = _region[i]->get_color(Cable);
    c[3] = opac;
    c = _region[i]->get_color(Vertex);
    c[3] = opac;
    c = _region[i]->get_color(Cnnx);
    c[3] = opac;
  }
}

void Model::randomize_color( Object_t obj )
{
  const double min_brightness=1.5;		// minimum brighness for colour
  int num;
  if ( obj==Surface || obj==SurfEle )
    num = numSurf();
  else if(obj==Cable) 
    num = _cnnx->num_lines();
  else
    num = _numReg;

  for ( int i=0; i<num; i++ ) {
    GLfloat *c;
    if ( obj==Surface )
      c = surface(i)->fillcolor();
    else if ( obj==SurfEle )
      c = surface(i)->outlinecolor();
    else if ( obj==Cable )
      c = _cnnx->line_color(i);
    else
      c = _region[i]->get_color( obj );
      c[0] = double(random())/RAND_MAX;
      c[1] = double(random())/RAND_MAX;
      c[2] = double(random())/RAND_MAX;
      float sum = c[0] + c[1] + c[2];
      if ( sum < min_brightness ) {		// ensure the surface is bright enough
        for( int i=0; i<3; i++ ) {
          c[i] *= min_brightness/sum;
          c[i] = std::min( 1.f, c[i] );
        }
      }
  }
}


Model::~Model()
{
  if (_cnnx) {
    delete _cnnx;
    _cnnx = 0;
  }

  for ( auto p : _vol ) 
    delete p;

  for ( int i=0; i<numSurf(); i++ )
    delete _surface[i];
  _surface.clear();
}


/** output highlight information
 *
 *  \param hinfo   window in which to output text
 *  \param hilight list of highlighted objects
 *  @param data    data values
 *  @param eledata true for data on elements, points o.w.
 */
void Model::hilight_info( HiLiteInfoWin* hinfo, int* hilight, DATA_TYPE* data, bool eledata )
{
  hinfo->clear();
  char* txt  = new char[256];
  std::string sepnum;

  /////////////////////////////
  // Vertex info
  std::set<int> att_nodes;        	// list nodes in attached objects
  sepnum = commaize(pt.num());
  sprintf(txt, "@b@C%6dVertex: %d of %s", FL_DARK_GREEN, hilight[Vertex],
          sepnum.c_str() );
  hinfo->add( txt );
  if ( data && !eledata ) {
    sprintf( txt, "value: %f", data[hilight[Vertex]] );
    hinfo->add( txt );
  }
  sprintf( txt, "( %.6g, %.6g, %.6g )", pt.pt()[hilight[Vertex]*3],
           pt.pt()[hilight[Vertex]*3+1], pt.pt()[hilight[Vertex]*3+2]);
  hinfo->add( txt );

  if ( _cnnx->num() ) {
    hinfo->add( "Attached connections:" );
    auto &ln = _cnnx->lines();
    int vtx_cable = -1;
    for ( int cnum=0; cnum<_cnnx->num();cnum++ ) {
      const int* conn = _cnnx->obj(cnum);
      if ( conn[0]==hilight[Vertex] || conn[1]==hilight[Vertex] ) {
        hinfo->add( format_hilite(cnum,cnum==hilight[Cnnx]) );
        att_nodes.insert(conn[conn[0]==hilight[Vertex]?1:0] );
        for( int i=0; i<ln.size(); i++ ) 
          if( cnum>=ln[i].first && cnum<=ln[i].second ){
            vtx_cable = i;
            break;
          }
      }
    }
    if( vtx_cable >= 0 ) {
      hinfo->add( "In cable:" ); 
      hinfo->add( format_hilite(vtx_cable,vtx_cable==hilight[Cable]) );
    }
  }
  
  if ( numSurf() ) {
    std::set<int> att_ele;
    for( int s=0; s<numSurf(); s++ ) {
      if( !surface(s)->visible() ||
          (surface(s)->vertices()).count(hilight[Vertex])==0 )
        continue;
      auto ele=surface(s)->ele();
#pragma omp parallel for
      for ( int j=0; j<ele.size(); j++ ) {
        const int* nl = ele[j]->obj();
        for ( int k=0; k<ele[j]->ptsPerObj(); k++ ) {
          if ( nl[k]==hilight[Vertex] ) {
            int globEle = globalElemnum( s, j );
#pragma omp critical
            {
              att_ele.insert( globEle );
              for ( int m=0; m<ele[j]->ptsPerObj(); m++ )
                att_nodes.insert(nl[m]);
            }
          }
        }
      }
    }
    if( att_ele.size() ) {
      hinfo->add( "Attached surface elements:" );
      for( auto e : att_ele ) 
        hinfo->add( format_hilite(e,e==hilight[SurfEle]) );
    }
  }

  if ( _vol.size() ) {
    std::set<int> att_vol;
#pragma omp parallel for
    for ( int i=0; i<_vol.size(); i++ ) {
      const int* tet=_vol[i]->obj();
      for ( int j=0; j<_vol[i]->ptsPerObj(); j++ )
        if ( tet[j]==hilight[Vertex] ) {
#pragma omp critical
          {
            att_vol.insert( i );
            for ( int k=0; k<_vol[i]->ptsPerObj(); k++ ) {
              att_nodes.insert(tet[k]);
            }
          }
        }
    }
    if( att_vol.size() ) {
      hinfo->add( "Attached volume elements:" );
      for( auto v: att_vol )
        hinfo->add( format_hilite(v,v==hilight[VolEle]) );
    }
  }

  if( att_nodes.size()>1 ) {
    hinfo->add( "Attached nodes:" );
    att_nodes.erase(hilight[Vertex]);
    for ( auto p: att_nodes ) 
      hinfo->add( format_hilite(p,false) );
  }
  hinfo->add( "" );
  // data extrema
  if( data )  {
    const char *src = eledata ? "element" : "vertex";
    hinfo->add( "Data Extrema" );
    DATA_TYPE *mind = std::min_element( data, data+pt.num() ); 
    sprintf( txt, "Minimum: %g @ %s", *mind, src );
    hinfo->add( txt );
    hinfo->add( format_hilite( mind-data, mind-data==hilight[Vertex]) );
    DATA_TYPE *maxd = std::max_element( data, data+pt.num() ); 
    sprintf( txt, "Maximum: %g @ %s", *maxd, src );
    hinfo->add( txt );
    hinfo->add( format_hilite( maxd-data, maxd-data==hilight[Vertex]) );
    hinfo->add( "" );
  }
  //End vertex info
  ///////////////////////////////////
 
  //Cables
  if ( _cnnx->num_lines() ) {
    sepnum = commaize(_cnnx->num_lines());
    sprintf(txt, "@b@C%6dCable: %d of %s", FL_CYAN, hilight[Cable],
            sepnum.c_str() );
    hinfo->add( txt );
    hinfo->add( "connections (first-last):" );
    auto ln = _cnnx->lines()[hilight[Cable]];
    hinfo->add( format_hilite( ln.first,  ln.first==hilight[Cnnx]) );
    hinfo->add( format_hilite( ln.second, ln.second==hilight[Cnnx]));
    hinfo->add( "" );
  }

  // Volume ELements
  if ( _vol.size() ) {
    int hivol=hilight[VolEle];
    sepnum = commaize(_vol.size());
    sprintf( txt, "@b@C%6dVolume Element: %d of %s", FL_RED, hivol, sepnum.c_str() );
    hinfo->add( txt );
    if ( eledata ) {
      sprintf( txt, "value: %f", data[hilight[VolEle]] );
      hinfo->add( txt );
    }
    hinfo->add( "nodes:\t" );
    const int* tet=_vol[hivol]->obj();
    for ( int i=0; i<_vol[hivol]->ptsPerObj(); i++ ) {
      char *line = format_hilite( tet[i], tet[i]==hilight[Vertex] ); 
      if ( data != NULL )
        sprintf(line+strlen(line), " -> %f", data[tet[i]] );
      hinfo->add( line );
    }
  }
  hinfo->add("");

  // SurfEles
  int elesurf, lele;
  if ( numSurf() ) {
    lele = localElemnum( hilight[SurfEle], elesurf );
    sepnum = commaize(number(SurfEle));
    sprintf(txt, "@b@C%6dSurface Element: %d of %s",
            FL_BLUE, hilight[SurfEle], sepnum.c_str() );
    if( numSurf()>1 ) {
      sepnum = commaize(surface(elesurf)->num());
      sprintf(txt+strlen(txt), " (%d of %s in %s)",
            lele, sepnum.c_str(), surface(elesurf)->label().c_str());
    }
    hinfo->add( txt );
    if ( eledata && !_vol.size() ) {
      sprintf( txt, "value: %f", data[hilight[SurfEle]] );
      hinfo->add( txt );
    }
    hinfo->add( "nodes:\t" );
    for ( int i=0; i<surface(elesurf)->ele(lele)->ptsPerObj(); i++ ) {
      int node=surface(elesurf)->ele(lele)->obj()[i];
      char *line = format_hilite( node, node==hilight[Vertex] ); 
      if ( data != NULL )
        sprintf(line+strlen(line), " -> %f", data[node] );
      hinfo->add( line );
    }
    //normal
    const GLfloat* n=surface(elesurf)->ele(lele)->nrml();
    if ( n != NULL ) {
      hinfo->add( "normal:\t" );
      sprintf( txt, "(%f, %f, %f)", n[0], n[1], n[2] );
      hinfo->add( txt );
    }
    hinfo->add( "" );
  }

  //Connections
  if ( _cnnx->num() ) {
    sepnum = commaize(_cnnx->num());
    sprintf(txt, "@b@C%6dConnection: %d of %s", FL_MAGENTA, hilight[Cnnx],
            sepnum.c_str() );
    hinfo->add( txt );
    hinfo->add( "nodes:\t" );
    for( int i=0; i<2; i++ ) {
      hinfo->add(format_hilite(_cnnx->obj(hilight[Cnnx])[i],
                         _cnnx->obj(hilight[Cnnx])[i]==hilight[Vertex]));
    }
    if( _cnnx->num_lines() ){ // are there cables?
      auto ln = _cnnx->lines();
      for( int i=0; i<ln.size(); i++ )
        if( hilight[Cnnx]>=ln[i].first && hilight[Cnnx]<=ln[i].second ){
          sprintf( txt, "in cable %d", i );
          hinfo->add( txt );
        }
    }
    hinfo->add("");
  }
  hinfo->window->show();
}


/** try reading in optional regions file
 *
 * \param fnb base file name
 */
void Model::read_region_file( std::string  fnb )
{
  int  numNewReg;

  gzFile in;
  if ( (in=gzopen((fnb+".region").c_str(), "r" )) != NULL ) {
    char buff[bufsize];
    gzgets(in, buff, bufsize);
    sscanf( buff, "%d", &numNewReg );
    _numReg += numNewReg;
    _region = (RRegion **)realloc( _region, _numReg*sizeof(RRegion*) );
    for ( int i=_numReg-numNewReg; i<_numReg; i++ ) {
      int firstpt, lastpt, label;
      gzgets(in, buff, bufsize);
      sscanf( buff, "%d %d %d", &firstpt, &lastpt, &label );
      _region[i] = new RRegion( pt.num(), 0, label, false );
      for ( int e=firstpt; e<=lastpt; e++ )
        _region[i]->pt_member(e,true);
    }
    gzclose(in);
  }
}


#ifdef USE_HDF5
void Model::add_regions(hid_t hdf_file)
{
  ch5_dataset dset_info;
  if (ch5m_regn_get_info(hdf_file, &dset_info))
    return;
  
  int readRegions[dset_info.count * dset_info.width];
  if ( ch5m_regn_get_all(hdf_file, readRegions) ) {
    std::cerr << "Could not read regions from HDF file" << std::endl;
    exit(1);
  }
  
  int oldNumReg = _numReg;
  _numReg += dset_info.count;
  _region = (RRegion**) realloc(_region, _numReg * sizeof(RRegion*));
  if (_region == NULL) {
    std::cerr << "Could not allocate memory for regions" << std::endl;
    exit(1);
  }
  
  for (int i = 0; i < dset_info.count; i++) {
    _region[oldNumReg + i] = new RRegion(pt.num(), 0, 0, false);
    for (int e = readRegions[i * 2]; e <= readRegions[i * 2 + 1]; e++)
      _region[oldNumReg + i]->pt_member(e, true);
  }
}
#endif


int Model::number(Object_t a )
{
  int nele=0;

  switch ( a ) {
    case Vertex:
      return pt.num();
    case Cnnx:
      return _cnnx->num();
    case Cable:
      return _cnnx->num_lines();
    case Surface:
      return _surface.size();
    case SurfEle:
      for ( int s=0; s<_surface.size(); s++ )
        nele += surface(s)->num();
      return nele;
    case VolEle:
      return _vol.size();
    case RegionDef:
      return _numReg;
    default:
      return 0;
  }
}


/** set vertex normals for a surface
 *
 * \param sp pointer to surface
 */
const short_float* 
Model::vertex_normals(Surfaces *sp)
{
  sp->get_vert_norms( _vertnrml );
  return _vertnrml;
}


/** read in an element file
 *
 * \param fn element file
 *
 * \return true if successful
 */
bool
Model::read_elem_file( std::string fname )
{
  if( fname.size() == 0 ) return false;

  if( has_ext( fname, ".belem" ) )
    return read_elem_binary( fname );

  bool  tets = false;
  int   nele;

  if( !_elemfile.num_lines() ) return false;
  sscanf( _elemfile.peek(0), "%d", &nele );

  std::vector<VolElement*> vol(nele, NULL);

  int surfe      = 0;
  bool unsup_ele = false;
  std::queue<std::pair<int,int> > new_cnnx;
  bool illegal_reg_label = false;

#pragma omp parallel for reduction(+:surfe) reduction(||:illegal_reg_label)
  for( int i=0; i<nele; i++ ) {
     
    char eletype[3] = "Tt"; //default assumes tetrahedral file
    int  n[9];
    elem_t  et = Bogus;

    auto buf = _elemfile.peek(i+1);
    if( !buf || !strlen(buf) ) continue;

    if( tets ) {
      auto p = buf;
      for (size_t j=0; p && j<5; j++)
        n[j] = naive(p);

    } else {

      eletype[0]=buf[0];
      eletype[1]=buf[1];
      auto p = buf+3;
      unsigned short j;
      for (j=0; p && j<9; j++)
        n[j] = naive(p);
      if(j<9) n[j] = 0; // default region 0
    }

    if( !strcmp( eletype, "Tt" ) ) {
      vol[i] = new Tetrahedral( &pt );
      vol[i]->add( n, check_label(n[4], illegal_reg_label) );
    } else if( !strcmp( eletype, "Hx" ) ) {
      vol[i] = new Hexahedron( &pt );
      vol[i]->add( n,  check_label(n[8], illegal_reg_label) );
	} else if( !strcmp( eletype, "Py" ) ) {
	  vol[i] = new Pyramid( &pt );
	  vol[i]->add( n,  check_label(n[5], illegal_reg_label) );
	} else if( !strcmp( eletype, "Pr" ) ) {
	  vol[i] = new Prism( &pt );
	  vol[i]->add( n,  check_label(n[6], illegal_reg_label) );
	} else if( !strcmp( eletype, "Tr" ) || !strcmp( eletype, "Qd" ) || 
                                           !strcmp( eletype, "Ln" )) {
	  // surface and line elements ignored
      surfe++;
	} else {
      if( !unsup_ele ) 
        fprintf(stderr, "Unsupported element type: %s\n", eletype);
      unsup_ele = true;
    }
  }
  if( illegal_reg_label )
    std::cerr << "Illegal volume label detected in elem file (must be positive integer) - using 0" << std::endl;

  if( unsup_ele ) {
    _vol.clear();
    return false;
  }

  for( auto &p : vol )
    if( p ) _vol.push_back(p);
  int ne = _vol.size();

  if( ne+surfe+new_cnnx.size()<nele) {
    fprintf( stderr, "Warning: truncated element file? stated elements: %d, "
             " surface elements read: %d, volume elements read: %d,"
             "line elments read: %d\n",
             nele, surfe, ne, (int) new_cnnx.size() );
  }

  return true;
}


bool 
Model::read_elem_binary( std::string elemf )
{
  FILE* ele_file = fopen( elemf.c_str(), "r" );
  char header[BIN_HDR_SIZE] = {};
  fread(header, sizeof(char), BIN_HDR_SIZE, ele_file);
  int nele;
  sscanf(header, "%d", &nele);
 
  std::vector<VolElement*> vol(nele, NULL);

  bool unsup_ele = false;
  bool illegal_reg_label = false;

  // for connections
  std::vector<int> cnnx;
  int old_reg  = -1;
  int start_ln = -1;

  // for surface elements
  int surfe      = 0;
  std::map<int,Surfaces*> surfmap;

//#pragma omp parallel for reduction(+:surfe) private(illegal_reg_label)
  for( int i=0; i<nele; i++ ) {

    int    n[9];
    elem_t et;

    fread(&et, sizeof(int), 1, ele_file);
    int nnodes = get_nnodes( et );
    fread(n, sizeof(int), nnodes+1, ele_file);

    // volume elements
    if( et==Tt ){
      vol[i] = new Tetrahedral( &pt );
    } else if( et==Hx ){
      vol[i] = new Hexahedron( &pt );
    } else if( et==Py ){
      vol[i] = new Pyramid( &pt );
    } else if( et==Pr ){
      vol[i] = new Prism( &pt );
    } else if( et==Tr || et==Qd ){
      // surface elements
      Surfaces *surf;
      auto surfit =surfmap.find( n[nnodes] );
      if( surfit == surfmap.end() ) { 
        surfmap[n[nnodes]] = new Surfaces( &pt );
        surf = surfmap[n[nnodes]];
        _surface.push_back(surf);
      } else
        surf = surfit->second;

      if( et==Tr ) {
        surf->appendEle(new Triangle( &pt ));
        std::swap( n[1], n[2] );
      } else  if( et==Qd ) {
        surf->appendEle(new Quadrilateral( &pt ));
        std::swap( n[1], n[3] );
      } 
      int lele=surf->num()-1;
      surf->ele(lele)->define(n);
      surf->ele(lele)->compute_normals(0,0);
      surf->src_ele(lele,i-1);

      surfe++;

    } else if( et==Ln ){ 
      // line elements
      int  numcx     = cnnx.size()/2;
      bool cont_line = numcx && cnnx.back()==n[0];
      cnnx.push_back(n[0]);
      cnnx.push_back(n[1]);
      if( start_ln>=0 && ( old_reg!=n[2] || !cont_line ) ){
        _cnnx->add_line( start_ln, numcx-1+_cnnx->num() );
        start_ln = -1;
      } else if( start_ln<0 && old_reg==n[2] && cont_line )
        start_ln =  numcx-1+_cnnx->num();
      old_reg = n[2];

    } else {

      if( !unsup_ele ) 
        fprintf(stderr, "Unsupported type for element %d: %d\n", i, et);
      unsup_ele = true;

    }
    if( vol[i] )  vol[i]->add( n, check_label(n[nnodes], illegal_reg_label) );
  }

  if( unsup_ele ) {
    _vol.clear();
    return false;
  }

  // finish up connections
  if( start_ln >= 0 ) _cnnx->add_line( start_ln, _cnnx->size()/2-1 );
  _cnnx->add( cnnx.size()/2, cnnx.data() );

  // finish surfaces
  for (auto const& x : surfmap){
    x.second->determine_vert_norms( pt );
    x.second->count_tris();
    x.second->assign_reg(_region, _numReg);
  }
  for( int i=0; i<_surface.size(); i++ ) _surface[i]->index(i);

  // only copy volume elements to _vol
  for( auto &p : vol )
    if( p ) _vol.push_back(p);

  // make sure we got everything
  if( _vol.size()+surfe+cnnx.size()/2<nele) {
    fprintf( stderr, "Warning: truncated element file? stated elements: %d, "
             " surface elements read: %d, volume elements read: %d,"
             "line elments read: %d\n",
             nele, surfe, _vol.size(), (int) _cnnx->size() );
  }

  return true;
}


#ifdef USE_HDF5
bool Model::add_elements(hid_t hdf_file)
{
  ch5_dataset info;
  if( ch5m_elem_get_info(hdf_file, &info) )
    return false;
  
  _vol.clear();
  _vol.resize(info.count,NULL);
  int  surfe    = 0;
  int  ne       = 0;
  int *elements = (int*) malloc(sizeof(int) * info.count * info.width);
  ch5m_elem_get_all(hdf_file, elements);
  
  for (int i = 0; i < info.count; i++) {

    switch (elements[i * info.width + CH5_ELEM_TYPE_OFFSET]) {
      case CH5_TETRAHEDRON:
        _vol[ne] = new Tetrahedral(&pt);
        break;
      
      case CH5_PYRAMID:
        _vol[ne] = new Pyramid(&pt);
        break;
      
      case CH5_PRISM:
        _vol[ne] = new Prism(&pt);
        break;
      
      case CH5_HEXAHEDRON:
        _vol[ne] = new Hexahedron(&pt);
        break;
      
      // As in read_elem_file, other primitives are ignored
      case CH5_TRIANGLE:
      case CH5_QUADRILATERAL:
        surfe++;
        break;
      
      // As in read_elem_file, unsupported types cause the function to clean
      // up and return
      default:
        std::cerr << "Unsupported element type (ch5m_element_type): " <<
          elements[i * info.width] << std::endl;
    	  _vol.clear();
          return false;
    }
    if ( _vol[ne] )
      _vol[ne++]->add(&elements[i * info.width + CH5_ELEM_DATA_OFFSET], 
              elements[i * info.width + CH5_ELEM_REGION_OFFSET]);
  }

  free(elements);

  // Shrink-fit elements array
  _vol.resize(ne);

  return true;
}
#endif


/** find the local surface element from the global number
 *
 * \param gele global element number
 * \param surf[out] surface containing the element
 *
 * \return the local element number in the surface,-1 if not in range
 * \post \p surf is the surface in which it was found
 */
int Model::localElemnum( int gele, int& surf )
{
  if( gele<0 )
    return -1;

  surf=0; 
  while( surf<_surface.size() && _surface[surf]->num()<=gele ) 
    gele -= _surface[surf++]->num();

  return surf==_surface.size() ? -1 : gele;
}



/** compute the global element number from the local
 *
 * \param surf surface containing the element
 * \param lnum local element number
 *
 * \return the global element number
 */
int Model::globalElemnum( int surf, int lnum )
{
  if( !surf && !lnum ) 
    return 0;

  if( surf<0 || surf>=_surface.size() )
    return -1;

  for( int i=0; i<surf; i++ )
    lnum += _surface[i]->num();

  return lnum;
}



/** delete a surface
 *
 *  \param s the number of the surface to delete
 */
void
Model::surfKill( int s )
{
  _surface.erase(_surface.begin()+s);
  for( int i=0; i<_surface.size(); i++ ) _surface[i]->index(i);
}


#define BUFSIZE 1024
/** read in one instant in time which is part of a element time file
 *
 *  \param pt_in   already open point file
 *  \param elem_in already opened element file
 *
 *  \return 
 */
bool Model :: read_instance( gzFile pt_in, gzFile elem_in )
{
  // read in points
  if( pt_in != NULL ) {
    int num_pt;
    sscanf( get_line(pt_in), "%d", &num_pt );
    GLfloat *p = new GLfloat[num_pt*3];
    for( int i=0; i< num_pt; i++ ) {
      sscanf( get_line(pt_in), "%f %f %f", p+i*3, p+i*3+1, p+i*3+2 );
    }
    pt.add( p, num_pt );
    pt.setVis( true );
    delete[] p;
  }

  // read in elements
  _surface.push_back( new Surfaces( &pt ) );
  _cnnx = new Connection( &pt );
  if( elem_in != NULL ) {
    int num_elem;
	sscanf( get_line(elem_in), "%d", &num_elem );
	std::vector<int> Cxpt;
	_vol.resize(num_elem);
    int numVol = 0;

	int  n[10];
    int  start_ln    = -1;
    int  prev_ln_reg = -1;
    bool read_ln_reg = false;
	for( int i=0; i<num_elem; i++ ) {
      char type[10];
      int nrd = sscanf( get_line(elem_in),"%s %d %d %d %d %d %d %d %d %d %d",
                        type, n, n+1, n+2, n+3, n+4, n+5, n+6, n+7, n+8, n+9 );
      if( !strcmp( type, "Ln") ) {
        Cxpt.push_back(n[0]);
        Cxpt.push_back(n[1]);
        if( nrd<4 ) continue;
        read_ln_reg = true;
        if( start_ln>=0 && n[2]!=prev_ln_reg ){
          _cnnx->add_line( start_ln, Cxpt.size()/2-2 );
          start_ln = Cxpt.size()/2-1;
        } else if( start_ln<0 )
          start_ln = Cxpt.size()/2-2;
        prev_ln_reg = n[2];
      } else if( !strcmp( type, "Tr" ) ) {
        _surface.back()->ele().push_back( new Triangle( &pt ) );
        _surface.back()->ele().back()->define( n );
        _surface.back()->ele().back()->compute_normals(0,0);
      } else if( !strcmp( type, "Qd" ) ) {
        _surface.back()->ele().push_back( new Quadrilateral( &pt ) );
        _surface.back()->ele().back()->define( n );
        _surface.back()->ele().back()->compute_normals(0,0);
      } else if( !strcmp( type, "Tt" ) ) {
        _vol[numVol] = new Tetrahedral( &pt );
        _vol[numVol]->add( n, n[4] );
        numVol++;
      } else if( !strcmp( type, "Hx" ) ) {
        _vol[numVol] = new Hexahedron( &pt );
        _vol[numVol]->add( n, n[8] );
        numVol++;
      } else if( !strcmp( type, "Py" ) ) {
        _vol[numVol] = new Pyramid( &pt );
        _vol[numVol]->add( n, n[5] );
        numVol++;
      } else if( !strcmp( type, "Pr" ) ) {
        _vol[numVol] = new Prism( &pt );
        _vol[numVol]->add( n, n[6] );
        numVol++;
      }
    }
    _vol.resize(numVol);
    // define connections
    _cnnx->define( Cxpt.data(), Cxpt.size()/2 );
    if( start_ln >= 0 ) _cnnx->add_line( start_ln, Cxpt.size()/2-1 );
    if( !read_ln_reg ) _cnnx->make_lines();

    for( auto s : _surface ) 
      if( s->num() ) s->determine_vert_norms( pt );
  }
  determine_regions();
  return true;
}


bool
Model::check_element( SurfaceElement *e )
{
  const int *n=e->obj();
  for( int i=0; i<e->ptsPerObj(); i++ )
    if( n[i] >= pt.num() )
      return false;

  return true;
}


/** get maximum size of object in model 
 *
 * \param o object
 *
 * \return maximum size
 */
float
Model::max_size( Object_t o )
{
  float ms = _region[0]->size(o);
  for( int i=1; i<_numReg; i++ ) 
    if( _region[0]->size(o)>ms ) ms = _region[0]->size(o);
  return ms;
}


/** determine the volume element to which a surface element is attached
 */
void
Model::associate_surf_with_vol()
{
  std::vector<bool> surfpt(pt.num());
  
  for( auto &surf : _surface ) {

    if( surf->vol_assoc()  ) continue;

    // determine nodes in surface, and put all the faces in an
    // unordered set
    surfpt.assign(pt.num(),false);
    faceset faces(surf->num());  
    auto elst = surf->ele();
#pragma omp parallel for
    for( int ei=0; ei<elst.size(); ei++ ) {
      auto e=elst[ei];
      Face face;
      const int *n = e->obj();
      make_face( face, e->ptsPerObj(), n, ei );
#pragma omp critical
      faces.insert(face);
      for( int i=0; i<e->ptsPerObj(); i++ )
        surfpt[n[i]] = true;
    }

    // for all volume elements, check if at least three nodes are in the
    // surface, and then look for faces of volume element in the face list
#pragma omp parallel for
    for( int v=0; v<_vol.size(); v++ ) {
      int facets[MAX_NUM_SURF][MAX_NUM_SURF_NODES+1];
      int nfacets = _vol[v]->surfaces( facets );
      for( int i=0; i<nfacets; i++ ) {
        int j;
        for( j=1; j<=facets[i][0]; j++ )
          if( !surfpt[facets[i][j]] )
            break;
        if( j<=facets[i][0] )
          continue;  // ignore facet if all nodes not in surface
        Face facet;
        make_face( facet, facets[i][0], facets[i]+1 );
#pragma omp critical 
        { 
          auto iter = faces.find(facet);
          if (iter != faces.end()) {
            surf->src_ele((*iter).elem, v);
            faces.erase(iter);
          }
        }
      }
    }

    surf->vol_assoc(true);
  }
}



int
Model::copy_surface( int s )
{
  Surfaces* surf = new Surfaces;
  *surf = *(_surface[s]);
  surf->index(_surface.size());
  std::string label = _surface[s]->label();
  label += "_cp";
  surf->label( label );
  _surface.push_back(surf);
  return surf->index();
}


#ifdef USE_HDF5
/** read in one instant in time which is part of a element  time file
 *
 *  \param hid_t     already open HDF5 file
 *  \param indx      grid index 
 *  \param tm        time to fetch
 *  \param data[out] data buffer which will be allocated
 *
 *  \note With HDF5, lines are not defined in the auxiliary grid but will
 *        be constructed if possible
 *
 *  \return true on success, false o.w.
 */
bool Model :: read_instance( hid_t hin, unsigned int indx, unsigned int tm,
                                      float* &data )
{
  ch5s_aux_time_step info;
  ch5s_aux_time_step_info( hin, indx, tm, &info );

  GLfloat *p   = new GLfloat[info.num_points*3];
  int  datawidth = info.max_element_width+2; // 1 for element type+1 for region

  int *ele = info.num_elements ? 
              new int[info.num_elements*datawidth] : NULL;
  data = info.has_data ? new float[info.num_points] : NULL ;

  ch5s_aux_read( hin, indx, tm, p, reinterpret_cast<unsigned int *>(ele), data);

  pt.add( p, info.num_points );
  pt.setVis( true );
  delete[] p;

  // make elements
  _surface.push_back( new Surfaces( &pt ) );
  _cnnx = new Connection( &pt );
  int start_ln = -1;

  if( info.num_elements ) {
	int  nvol=0;
	_vol.resize(info.num_elements);

    int  start_ln    = -1;
    int  prev_ln_reg = -1;
    bool read_ln_reg = false;
    int numCx = 0;
	std::vector<int> Cxpt;

	for( int i=0; i<info.num_elements; i++ ) {
      ch5m_element_type etype = 
                    static_cast<ch5m_element_type>(ele[i*datawidth]);
      if( etype==CH5_CONNECTION ) {
        Cxpt.push_back(ele[i*datawidth+2]);
        Cxpt.push_back(ele[i*datawidth+3]);
        int ln_reg = ele[i*datawidth+4];
        continue;
        read_ln_reg = true;
        if( start_ln>=0 && ln_reg!=prev_ln_reg ){
          _cnnx->add_line( start_ln, Cxpt.size()/2-2 );
          start_ln = Cxpt.size()/2-1;
        } else if( start_ln<0 )
          start_ln = Cxpt.size()/2-2;
        prev_ln_reg = ln_reg;
      } else if( etype==CH5_TRIANGLE ) {
        _surface.back()->ele().push_back( new Triangle( &pt ) );
        _surface.back()->ele().back()->define( ele+i*datawidth+2 );
        _surface.back()->ele().back()->compute_normals(0,0);
      } else if( etype==CH5_QUADRILATERAL ) {
        _surface.back()->ele().push_back( new Quadrilateral( &pt ) );
        _surface.back()->ele().back()->define( ele+i*datawidth+2 );
        _surface.back()->ele().back()->compute_normals(0,0);
      } else if( etype==CH5_TETRAHEDRON ){
        _vol[nvol] = new Tetrahedral( &pt );
        _vol[nvol]->add( ele+i*datawidth+2 );
        nvol++;
      } else if( etype==CH5_HEXAHEDRON ){
        _vol[nvol] = new Hexahedron( &pt );
        _vol[nvol]->add( ele+i*datawidth+2 );
        nvol++;
      } else if( etype==CH5_PYRAMID ){
        _vol[nvol] = new Pyramid( &pt );
        _vol[nvol]->add( ele+i*datawidth+2 );
        nvol++;
      } else if( etype==CH5_PRISM ){
        _vol[nvol] = new Prism( &pt );
        _vol[nvol]->add( ele+i*datawidth+2 );
        nvol++;
      } else {
        //fl_alert( "\nUnknown element type: %d\n\n", etype );
        _vol.clear();
        return false;
      }
    }
    _vol.resize(nvol);
    // define connections
    _cnnx->define( Cxpt.data(), Cxpt.size()/2 );
    if( start_ln >= 0 ) _cnnx->add_line( start_ln, numCx-1 );
    if( !read_ln_reg ) _cnnx->make_lines();

    for( auto s : _surface )  {
      if( s->num() ) s->determine_vert_norms( pt );
      s->index(0);
    }
  }
  determine_regions();
  return true;
}

#endif
