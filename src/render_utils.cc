#ifdef OSMESA
#  define  GL_GLEXT_PROTOTYPES
#  include <GL/osmesa.h>
#elif defined(__APPLE__)
#  define GL_SILENCE_DEPRECATION
#  include <OpenGL/gl3.h> 
#else
#  if defined(WIN32)
#    define GLEW_STATIC 1
#  endif
# include <GL/glew.h>
#endif

#include "render_func.h"
#include <string>
#include <iostream>
#include <sstream>
#include <set>
#include "VecData.h"
#include "short_float.h"

#define ICOSHX .525731112119133606 
#define ICOSHZ .850650808352039932
static GLfloat icoshVtx[12][3] = {    
   {-ICOSHX, 0.0, ICOSHZ}, {ICOSHX, 0.0, ICOSHZ}, {-ICOSHX, 0.0, -ICOSHZ}, {ICOSHX, 0.0, -ICOSHZ},    
   {0.0, ICOSHZ, ICOSHX}, {0.0, ICOSHZ, -ICOSHX}, {0.0, -ICOSHZ, ICOSHX}, {0.0, -ICOSHZ, -ICOSHX},    
   {ICOSHZ, ICOSHX, 0.0}, {-ICOSHZ, ICOSHX, 0.0}, {ICOSHZ, -ICOSHX, 0.0}, {-ICOSHZ, -ICOSHX, 0.0} 
};
static unsigned int icoshTri[20][3] = { 
   {0,4,1},  {0,9,4},  {9,5,4},  {4,5,8},  {4,8,1},    
   {8,10,1}, {8,3,10}, {5,3,8},  {5,2,3},  {2,7,3},    
   {7,10,3}, {7,6,10}, {7,11,6}, {11,0,6}, {0,1,6}, 
   {6,1,10}, {9,0,11}, {9,11,2}, {9,2,5},  {7,2,11} };


/**
 * @brief recolour a 3D cylinder
 *
 * @param buff   buffer to recolour cylinders
 * @param vtx0   starting vertex
 * @param vtx1   ending vertex
 * @param ns     number of sides
 * @param c0     color vertex 0 (r,g,b,a)
 * @param c1     color vertex 1 (r,g,b,a)
 * @param join   join segment with previous one, else cap
 * @param next   next vertex in line, or NULL if none
 * @param strip  draw as a triangle strip?
 * @param dbuff  buffer to recolour end caps if a triangle strip
 *
 * @return number of vertices added to \p vbo buffer
 *
 * \post \p buff points to one past the last entry
 */
int 
recolour_cyl( GLubyte* &buff, int ns, const GLubyte *c0, const  GLubyte *c1, bool join, bool next,
              bool strip, GLubyte* &dbuff  )
{
  GLubyte *b0 = buff;

  // cap if not joining
  if( !join ) {
    if( strip  ) {
      for( int i=0; i<ns; i++ ) {
        memcpy( buff, c0, VBO_RGBA_SZ );
        buff += VBO_RGBA_SZ;
      }
      if( dbuff ) {
        for( int i=0; i<ns+2; i++ ) {
          memcpy( dbuff, c0, VBO_RGBA_SZ );
          dbuff += VBO_RGBA_SZ;
        }
      }
    } else if(!strip){
      for( int i=0; i<ns; i++ ) {
        memcpy( buff,               c0, 4 );
        memcpy( buff+1*VBO_RGBA_SZ, c0, 4 );
        memcpy( buff+2*VBO_RGBA_SZ, c0, 4 );
        buff += VBO_RGBA_SZ*3;
      }
    }
  }

  // recolour cylinder elements
  if( strip ){
    for( int i=0; i<ns; i++ ) {
      memcpy( buff,             c1, 4*sizeof(GLubyte) );
      buff += VBO_RGBA_SZ;
    }
  }else{
    for( int i=0; i<ns; i++ ) {
      memcpy( buff,               c0, 4*sizeof(GLubyte) );
      memcpy( buff+1*VBO_RGBA_SZ, c0, 4*sizeof(GLubyte) );
      memcpy( buff+2*VBO_RGBA_SZ, c1, 4*sizeof(GLubyte) );
      memcpy( buff+3*VBO_RGBA_SZ, c0, 4*sizeof(GLubyte) );
      memcpy( buff+4*VBO_RGBA_SZ, c1, 4*sizeof(GLubyte) );
      memcpy( buff+5*VBO_RGBA_SZ, c1, 4*sizeof(GLubyte) );
      buff += VBO_RGBA_SZ*6;
    }
  }

  // draw end cap?
  if( !next ){
    if( strip && dbuff) {
      for( int i=0; i<ns+2; i++ ) {
        memcpy( dbuff, c1, sizeof(GLubyte)*4 );
        dbuff += VBO_RGBA_SZ;
      }
    } else if(!strip){
      for( int i=0; i<ns; i++ ) {
        memcpy( buff,               c1, 4*sizeof(GLubyte) );
        memcpy( buff+1*VBO_RGBA_SZ, c1, 4*sizeof(GLubyte) );
        memcpy( buff+2*VBO_RGBA_SZ, c1, 4*sizeof(GLubyte) );
        buff += VBO_RGBA_SZ*3;
      }
    }
  }
  return (buff-b0)/VBO_RGBA_SZ;
}


int generate_shader_program(const char* vertex_source, const char* fragment_source)
{
  // build and compile our shader program
  // ------------------------------------
  // vertex shader
  int vertexShader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertexShader, 1, &vertex_source, NULL);
  glCompileShader(vertexShader);
  // check for shader compile errors
  int success;
  char infoLog[512];

  glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
    std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
  }

  // fragment shader
  int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragmentShader, 1, &fragment_source, NULL);
  glCompileShader(fragmentShader);
  // check for shader compile errors
  glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
  if (!success)
  {
    glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
    std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
  }
  // link shaders
  int shaderProgram = glCreateProgram();
  glAttachShader(shaderProgram, vertexShader);
  glAttachShader(shaderProgram, fragmentShader);
  glLinkProgram(shaderProgram);
  // check for linking errors
  glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
  if (!success) {
    glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
    std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
  }

  glDeleteShader(vertexShader);
  glDeleteShader(fragmentShader);

  return shaderProgram;
}


/**
 * @brief copy vertex data to GPU
 *
 * @param unbind unbind buffer after transfer?
 */
void 
render_data::update_col(bool unbind)
{
  glBindBuffer(GL_ARRAY_BUFFER, gl_colbuf);
  glBufferData(GL_ARRAY_BUFFER, colbuff.size(), colbuff.data(), GL_DYNAMIC_DRAW);

  if(unbind)
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}


/**
 * @brief copy vertex data to GPU
 *
 * @param unbind unbind buffer after transfer?
 */
void 
render_data::init_nodalbuff(bool unbind)
{
  glBindBuffer(GL_ARRAY_BUFFER, gl_nodalbuff);
  glBufferData(GL_ARRAY_BUFFER, nodalbuff.size()*sizeof(float), nodalbuff.data(), GL_DYNAMIC_DRAW);
  static size_t r=0;
  //std::cout<<"nodal_buffer update " << r++ << std::endl;
  glBindBuffer(GL_ARRAY_BUFFER, gl_colbuf);
  glBufferData(GL_ARRAY_BUFFER, colbuff.size(), colbuff.data(), GL_DYNAMIC_DRAW);

  if(unbind)
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}


/**
 * @brief do all the ugly setting up for OPenGL including
 * defining the vertex buffer and binding to OGL context
 *
 * \param c   openGL context
 * \param pt  draw simulated 3D points
 * \param eib use buffer for point antialiasing
 *
 * \return true iff the initialization was performed
 */
bool 
render_data::setup_render_data(void *c, bool pt, bool eib)
{
  if(!init) {
#if !defined(__APPLE__) && !defined(OSMESA) 
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if (GLEW_OK != err) {
      std::cerr << "GLEW problem -- aborting!" << std::endl;
    }
#endif
    if( c ) _context=c;
    shader_default = get_default_lighting_shader();

    shader_wireframe = pt ? get_lit_point_shader() : get_wireframe_shader();

    set_identity_4x4_mat(tfm);

    glGenVertexArrays(1, &gl_vao);
    glBindVertexArray(gl_vao);
    glGenBuffers(1, &gl_nodalbuff);
    glGenBuffers(1, &gl_colbuf);

    if(eib){
      glPrimitiveRestartIndex(SEP_ELEM);
      glEnable(GL_PRIMITIVE_RESTART);
      glGenBuffers(1, &_eib);
      glGenBuffers(1, &_dib);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _eib);
    }

    init_nodalbuff(false);
    glBindBuffer(GL_ARRAY_BUFFER, gl_nodalbuff);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float)*VBO_ELEM_SZ, (void*)VBO_XYZ_OFFSET);
    glEnableVertexAttribArray(1);
#ifdef NORM_FLOAT
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(float)*VBO_ELEM_SZ, (void*)VBO_NORM_OFFSET);
#else
    glVertexAttribPointer(1, 3, GL_HALF_FLOAT, GL_FALSE, sizeof(float)*VBO_ELEM_SZ, (void*)VBO_NORM_OFFSET);
#endif
    glEnableVertexAttribArray(2);
    glVertexAttribIPointer(2, 1, GL_UNSIGNED_SHORT, sizeof(float)*VBO_ELEM_SZ, (void*)VBO_ENT_OFFSET);

    // set up the seperate colour VBO
    glBindBuffer(GL_ARRAY_BUFFER, gl_colbuf);
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(GLubyte)*4, (void*)0);
    
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    init = true;
    return true;
  } else
    return false;
}



render_data::~render_data()
{
  if( init ) {
    glDeleteBuffers( 1,  &gl_nodalbuff );
    glDeleteVertexArrays( 1, &gl_vao );
    glDeleteBuffers( 1,  &_eib );
    glDeleteProgram(shader_default);
    glDeleteProgram(shader_wireframe);
  }
}


void
render_data::set_clipping(int shader_program) 
{
  glUseProgram(shader_program);

  // now feed in the uniform input data (mostly the transformation matrices)
  // into the shader program
  set_shader_view_matrices(shader_program,_context);
  set_shader_mat4(shader_program, "model", &tfm[0][0]);

  // set up lighting
  GLfloat light_colour[4]= { 1.0f, 1.0f, 1.0f, 1.0f };
  V3f ld = light_dir(_context).Rotate( V3f(0,0,1) );
  set_shader_vec3(shader_program,  "lightDir",         ld.e          );
  set_shader_vec3(shader_program,  "lightColor",       light_colour  );
  set_shader_float(shader_program, "ambientStrength",  ambient       );
  set_shader_float(shader_program, "diffuseStrength",  diffuse       );
  set_shader_float(shader_program, "specularStrength", specular      );
  set_shader_int(shader_program,   "backLit",          back_lit      );
  set_shader_float(shader_program, "fresnelExp",       fresnel       );
 
  GLfloat noclip[4] = { 0.f, 0.f, 0.f, 1000.f };
  for( int i=0; i<NUM_CP; i++ ) {
    GLfloat *clipEqn = clipping(i)&&!_on_top ? CLIP_EQN[i]: noclip;
    std::stringstream clipEqnStr;
    clipEqnStr << "clipEqn[" << i << "]";
    set_shader_vec4(shader_program, clipEqnStr.str().c_str(), clipEqn);
  }
  
  // set up material properties for each surface
  for( int i=0; i<MAX_NUM_SURFS; i++ ){
    if( material[i][0] == BOGUS_VAL ) continue;
    std::stringstream materialStr;
    materialStr << "materialProps[" << i << "]";
    set_shader_vec4(shader_program, materialStr.str().c_str(), material[i]);
    std::stringstream clipStr;
    clipStr << "use_clip[" << i << "]";
    set_shader_int(shader_program, clipStr.str().c_str(), _clip_use[i]);
  }
}


/** finish rendering */
void
render_data :: finish_render()
{
  // unbind render data and shader
  glBindVertexArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0 );
  glUseProgram(0);

  _dirty = false;
  // don't know why the following are needed
  glDepthMask(GL_TRUE);
  glEnable(GL_BLEND);
}


/**
 * @brief assign surfaces to vertices
 *
 * @param vtx0 start vertex
 * @param vtx1 inclusive end vertex
 * @param s    new surface number
 *
 */
void
render_data :: resurf( int vtx0, int vtx1, unsigned short s )
{
  for( int i=vtx0; i<=vtx1; i++ ) {
    char *sptr = (char*)(nodalbuff.data() + i*VBO_ELEM_SZ);
    *((unsigned short *)(sptr+VBO_ENT_OFFSET)) = s;
  }
}

/** render the surface elements
 *
 * \param wireframe   render wireframe else solid elements
 *
 * \pre \p ntris is the number of triangles to render
 * \pre \p n_opaque is the number of opaque vertices (3/tri)
 * \pre \p nodalbuff is filled first with opaque vertices, followed by translucent
 * \pre \p material[i] has the info for surface[i]
 */
void 
RenderTris:: mesh_render( bool wireframe )
{
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glBindVertexArray(gl_vao);
  int shader_program = wireframe ? shader_wireframe : shader_default;
  if( wireframe ) {
    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    shader_program = shader_wireframe;
    glLineWidth(lineWidth);
  } else {
    shader_program = shader_default;
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
  }
  set_clipping(shader_program);

  //run the rendering pipeline, first opaque then translucent
  glDisable(GL_BLEND);
  glDepthMask(GL_TRUE);
  if( _on_top ) 
    glClear(GL_DEPTH_BUFFER_BIT);
  
  glDrawArrays(GL_TRIANGLES, 0, n_opaque);

  GLint ntrans_vtx = _n*3 - n_opaque;
  if(ntrans_vtx) {
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    glDrawArrays(GL_TRIANGLES, n_opaque, ntrans_vtx );
  }

  finish_render();
}
/** render the surface elements
 *
 * \param wireframe   render wireframe else solid elements
 *
 * \pre \p ntris is the number of triangles to render
 * \pre \p n_opaque is the number of opaque vertices (3/tri)
 * \pre \p nodalbuff is filled first with opaque vertices, followed by translucent
 * \pre \p material[i] has the info for surface[i]
 */
void 
RenderLines:: mesh_render( bool wf )
{
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glBindVertexArray(gl_vao);
  GLboolean cull;

  int shader_program;
  if( threeD() ) {
    shader_program = shader_default;
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    cull = glIsEnabled( GL_CULL_FACE );
    //if( !_strip || _diskind.size() ) 
      glEnable( GL_CULL_FACE );
  } else {
    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    shader_program = shader_wireframe;
    glLineWidth(lineWidth);
  }
  set_clipping(shader_program);

  //run the rendering pipeline, first opaque then translucent
  glDisable(GL_BLEND);
  glDepthMask(GL_TRUE);
  if( !threeD() )
    glDrawArrays(GL_LINES, 0, n_opaque);
  else {
    if( n_opaque ) {
      if( _strip ) {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _eib);
        glDrawElements( GL_TRIANGLE_STRIP, _cylind.size(),  GL_UNSIGNED_INT, 0 );
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _dib);
        glDrawElements( GL_TRIANGLE_FAN,   _diskind.size(), GL_UNSIGNED_INT, 0 );
      }else
        glDrawArrays(GL_TRIANGLES, 0, n_opaque );
    }
  }

  GLint ntrans_vtx = n_opaque==-1 ? 0 : (_n-n_opaque);
  if(ntrans_vtx) {
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    if( !threeD() )
      glDrawArrays(GL_LINES, n_opaque, ntrans_vtx );
    else {
      if( !n_opaque ) {
        if( _strip ){
          glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _eib);
          glDrawElements( GL_TRIANGLE_STRIP, _cylind.size(),  GL_UNSIGNED_INT, 0 );
          glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _dib);
          glDrawElements( GL_TRIANGLE_FAN,   _diskind.size(), GL_UNSIGNED_INT, 0 );
        } else
          glDrawArrays(GL_TRIANGLES, n_opaque, ntrans_vtx );
      }
    }
  }

  finish_render();

  if( threeD() && !cull ) glDisable(GL_CULL_FACE);
}


/**
 * @brief recolour a 3D line
 *
 * This is initially called with \p vbo set to NULL
 *
 * @param lbuff   buffer entries for all \p n+1 vertices 
 * @param n       number of segments in line
 * @param facet   facet shading
 * @param globInd number of vertices already buffered
 * @param line    current line number
 *
 * \post buffer colours are changed
 * \post vbo points one past the end of the filled buffer
 * \retval number of vertices added
 */
GLuint
RenderLines :: recolour_line( GLubyte *lbuff, int n, bool facet, GLuint globInd, GLuint line )
{
  GLuint vadded = 0;

  GLubyte *vbo  = colbuff.data()+globInd*VBO_RGBA_SZ; 

  GLubyte *dvbo = NULL;
  if( threeD() && _strip ) dvbo = _diskcolb.data()+line*nDiskVtx()*2*VBO_RGBA_SZ; 

  for( int i=0; i<n; i++ ) {
    GLubyte *col0 = lbuff+i*VBO_RGBA_SZ;
    GLubyte *col1 = facet ? col0 : col0+VBO_RGBA_SZ;
    if( threeD() ) {
      bool next = i<n-1;
      vadded += recolour_cyl( vbo, _num_sides, col0, col1, i, next, _strip, dvbo );
    } else {
      memcpy( vbo,             col0, 4*sizeof(GLubyte) );
      memcpy( vbo+VBO_RGBA_SZ, col1, 4*sizeof(GLubyte) );
      vbo += VBO_RGBA_SZ*2;
      vadded += 2;
    }
  }
  return vadded;
}

/**
 * @brief buffer a line
 *
 * @param lbuff buffer entries for segment vertices 
 * @param cbuff colour for vertices 
 * @param n     number of segments in line
 * @param facet facet shading
 *
 * \pre  \p lbuff and \p cbuff each are of size \p n+1 
 * \post buffer is increased in size to hold new line
 */
void
RenderLines :: buffer_line( GLfloat *lbuff, GLubyte *cbuff, int n, bool facet )
{
  GLuint  oldn = _n;
  GLubyte *cbo;
  GLfloat *vbo = buff_append(nLineVtx(n), &cbo);  

  GLfloat *cap_vbo = NULL;
  GLubyte *cap_col = NULL;
  if( threeD() ){
    if( _strip ) {
      _num_disk += 2;
      _diskbuff.resize( _num_disk*nDiskVtx()*VBO_ELEM_SZ );
      _diskcolb.resize( _num_disk*nDiskVtx()*VBO_RGBA_SZ );
      cap_vbo = _diskbuff.data() + (_num_disk-2)*nDiskVtx()*VBO_ELEM_SZ;
      cap_col = _diskcolb.data() + (_num_disk-2)*nDiskVtx()*VBO_RGBA_SZ;
    }

    for( int i=0; i<n; i++ ) {
      GLfloat *vtx0 = lbuff+i*VBO_ELEM_SZ;
      GLubyte *col0 = cbuff+i*VBO_RGBA_SZ;
      GLfloat *next = i<n-1 ? vtx0+2*VBO_ELEM_SZ : NULL;
      GLubyte *col1 = col0 + (facet ? 0 : VBO_RGBA_SZ);
      GLshort  reg  = *((GLshort*)((GLubyte*)vtx0+VBO_ENT_OFFSET)); 
      buff_cyl( vbo, vtx0, vtx0+VBO_ELEM_SZ, _num_sides, lineWidth, col0, col1, 
                                         reg, i, next, _strip, cap_vbo, cap_col );
    }
    cyl_indices( oldn, n );
  } else {
    for( int i=0; i<n; i++ ) {
      memcpy( vbo, lbuff+i*VBO_ELEM_SZ, 2*sizeof(GLfloat)*VBO_ELEM_SZ );
      if( facet ) {
        memcpy( cbo,             cbuff+i*VBO_RGBA_SZ, VBO_RGBA_SZ     );
        memcpy( cbo+VBO_RGBA_SZ, cbuff+i*VBO_RGBA_SZ, 2*VBO_RGBA_SZ   );
      } else
        memcpy( cbo, cbuff+i*VBO_RGBA_SZ, 2*VBO_RGBA_SZ               );
      vbo += 2*VBO_ELEM_SZ;
      cbo += 2*VBO_RGBA_SZ;
    }
  }
}


/**
 * @brief    change number of sides to draw segments
 *
 * @param n     number of sides per cylinder
 * \param extra extranumber of vertices to allocate per line segment
 *
 * @note the lines must be rebuffered for this to have an effect
 */
void
RenderLines :: sides( int n, int extra ) 
{
  if( _num_sides==n ) return;
  _num_sides = n<3 ? 0 : n;
  _dirty = true;
}


/**
 * @brief add two buffers together
 *
 * @param rl buffer to append
 *
 * \note if not specified, assume all lines are opaque
 */
RenderLines& 
RenderLines :: operator += (RenderLines &rl)
{
  if( threeD() != rl.threeD() ) throw 1;
  if( n_opaque == -1 )    n_opaque    = _n;
  if( rl.n_opaque == -1 ) rl.n_opaque = rl._n;
  nodalbuff.insert( _n==n_opaque?nodalbuff.end():nodalbuff.begin()+n_opaque*VBO_ELEM_SZ,
                     rl.nodalbuff.begin(), rl.nodalbuff.begin()+rl.n_opaque*VBO_ELEM_SZ );
  nodalbuff.insert( nodalbuff.end(), rl.nodalbuff.begin()+rl.n_opaque*VBO_ELEM_SZ, rl.nodalbuff.end() );
  _n += rl._n;
  n_opaque += rl.n_opaque;
  
  std::cout << "adding" << std::endl;
  return *this;
}


void 
RenderLines::update_col(bool unbind )
{
  glBindBuffer(GL_ARRAY_BUFFER, gl_colbuf);
  size_t cbuf = colbuff.size();
  size_t dbuf = _diskcolb.size();
  glBufferData(GL_ARRAY_BUFFER, cbuf+dbuf, NULL, GL_DYNAMIC_DRAW);
  glBufferSubData(GL_ARRAY_BUFFER, 0,    cbuf, colbuff.data());
  glBufferSubData(GL_ARRAY_BUFFER, cbuf, dbuf, _diskcolb.data());

  if( unbind ) 
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

/**
 * @brief copy vertex data to GPU
 *
 * @param unbind unbind buffer after transfer?
 */
void 
RenderLines::init_nodalbuff(bool unbind)
{
  // geometry VBO
  glBindBuffer(GL_ARRAY_BUFFER, gl_nodalbuff);
  size_t cbuf = nodalbuff.size()*sizeof(float);
  size_t dbuf = _diskbuff.size()*sizeof(float);
  glBufferData(GL_ARRAY_BUFFER, cbuf+dbuf, NULL, GL_DYNAMIC_DRAW);
  glBufferSubData(GL_ARRAY_BUFFER, 0,    cbuf, nodalbuff.data());
  glBufferSubData(GL_ARRAY_BUFFER, cbuf, dbuf, _diskbuff.data());
  // colour VBO
  update_col( false );

  if( threeD() && _strip ){
    // construct index list to draw disks
    _diskind.resize(_num_disk*(nDiskVtx()+1));
    auto dit = _diskind.begin();
    for( int i=0; i<_num_disk; i++ ){
      for( int j=0; j<nDiskVtx(); j++ )
        *dit++ = _n + i*nDiskVtx() + j;
      *dit++ = SEP_ELEM;
    }
    cbuf = _cylind.size()*sizeof(GLuint);
    dbuf = _diskind.size()*sizeof(GLuint);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _eib);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, cbuf, _cylind.data(),  GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _dib);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, dbuf, _diskind.data(), GL_DYNAMIC_DRAW);
  }
  
  if(unbind){
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  }
}


/** define element list to draw a line of cylinders as a TRIANGLE_STRIP
 *
 * @param origvtx #vtx before new line
 * @param n       number of cylindrical segments in line
 */
void RenderLines::cyl_indices( GLuint origvtx, int n )
{
  if( !threeD() || !_strip ) return;
  GLuint oldsz   = _cylind.size();
  _cylind.resize( oldsz+n*(2*_num_sides+3) );
  for( int i=0; i<n; i++ ) {
    _cylind[oldsz++] = origvtx;
    _cylind[oldsz++] = origvtx + _num_sides;
    for( int j=0; j<_num_sides; j++ ) {
      int k = (j+1)%_num_sides;
      _cylind[oldsz++] = origvtx + k;
      _cylind[oldsz++] = origvtx + k + _num_sides;
    }
    _cylind[oldsz++] = SEP_ELEM;
    origvtx += _num_sides;
  }
}


/**
 * @brief buffer a point
 *
 * @param vbo start of buffer
 * @param b   index of sphere in buffer
 * @param vtx vertex position
 * @param col colour
 * @param reg region
 *
 * \pre  buffer is allocated
 * \post buffer is filled, either 0 or 3D, for point \p b
 */
void
RenderSpheres :: buffer_sphere( GLfloat *vbo, int b, const GLfloat *vtx, const GLfloat *col, int reg )
{
  vbo += _esz*b; 
  if( !threeD() ) {              
    SHORTVEC( n,0,0,1 );
    vbo += buff_vtx( vbo, vtx, n, col, reg );
  } else {
    buff_sphere( vbo, vtx, _vtx.data(), _tri.size()/3, _tri.data(), _radius, col, reg );   
  }
}

/**
 * @brief define spherical approx by #subdisions of an icoshahedron
 *
 * @param nsd number of subdivisions, 0=0D, 1=20 faces, 2=80, 3=320, etc
 *
 * \post \p _tri and \p _vtx are populated for unit sphere at origin
 */
void
RenderSpheres::subdiv( int nsd )
{
  if( _nsubdiv == nsd ) return;
  if( threeD() ) _dirty = true;

  _nsubdiv=nsd;
  if( !_nsubdiv ) {
    _vtx.clear();
    _tri.clear();
    _esz = VBO_ELEM_SZ;
    prim_sz = 1;
    return;
  }
  prim_sz = mk_icosahedron( _nsubdiv, _tri, _vtx );
  _esz = prim_sz*VBO_ELEM_SZ;
}


void 
RenderSpheres:: mesh_render( bool wf )
{
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glBindVertexArray(gl_vao);
 
  GLboolean cull;
  int shader_program;

  if( threeD() ) {
    cull = glIsEnabled( GL_CULL_FACE );
    glEnable( GL_CULL_FACE );
    shader_program = shader_default;
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
  } else {
    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    shader_program = shader_wireframe;
    glPointSize(_radius);
  }
  set_clipping(shader_program);
  // adjust the light 
  V3f ld   = light_dir(_context).Rotate( V3f(0,0,1) );
  V3f ldxV = view_mult( _context, ld ).Normalize();
  set_shader_vec3(shader_program,  "lightDir", ldxV.e );
 
  //run the rendering pipeline, first opaque then translucent
  glDisable(GL_BLEND);
  glDepthMask(GL_TRUE);
  if( threeD() )
    glDrawArrays(GL_TRIANGLES, 0, n_opaque*prim_sz);
  else
    glDrawArrays(GL_POINTS, 0, n_opaque);

  int ntrans_vtx = (_n - n_opaque) * prim_sz;
  if(ntrans_vtx) {
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    if( threeD() )
      glDrawArrays(GL_TRIANGLES, n_opaque*prim_sz, ntrans_vtx);
    else
      glDrawArrays(GL_POINTS, n_opaque, ntrans_vtx );
  }
  
  if( threeD() && !cull ) glDisable(GL_CULL_FACE);
  finish_render();
}


#define SPHERE_REFINE_HI 3
void
RenderHilights::pt( unsigned int p, const GLfloat *vtx, GLfloat rad, bool threeD )
{
  SHORTVEC( n, 1, 0, 0 );
  assign( _pt, vtx );
  _hiPt  = p;
  _ptRad = rad;
  if( threeD ) {
    std::vector<unsigned int> _sphTri;
    std::vector<GLfloat>      _sphVtx;
    mk_icosahedron( SPHERE_REFINE_HI, _sphTri, _sphVtx );
    size_t osz = nodalbuff.size();
    GLfloat *buff = buff_append( _sphTri.size() );
    for( int s=0;s<_sphTri.size();s++) {
      GLfloat p[3];
      int node = _sphTri[s];
      add( scale( assign( p, _sphVtx.data()+node*3 ), _ptRad ), _pt, p );
      SHORT_VCP( n, _sphVtx.data()+node*3 );
      buff += buff_vtx( buff, p, n, _colPt, 0 );
      _nPtVtx++;
    }
  } else {
    GLfloat *buff = buff_append( 1 );
    buff_vtx( buff, _pt, n, _colPt, 0 );
    _nPtVtx = 1;
  }
}


void
RenderHilights::cnnx( unsigned int c, const GLfloat *vtx0, const GLfloat*vtx1, GLfloat rad, int ns )
{
  SHORTVEC( n, 1, 0, 0 );
  _hiCnnx    = c;
  _cnnxRad   = rad;
  bool _cnnx3D = ns>=3;
  size_t osz = nodalbuff.size();
  if( _cnnx3D ) {
    GLfloat *buff = buff_append(ns*4*3); 
    _nCnnxVtx = buff_cyl(buff, vtx0, vtx1, ns, rad, _colCnnx, _colCnnx, 0, false, NULL)/VBO_ELEM_SZ;
  } else {
    GLfloat *buff = buff_append(2); 
    buff += buff_vtx( buff, vtx0, n, _colCnnx, 0 );
    buff_vtx( buff, vtx1, n, _colCnnx, 0 );
    _nCnnxVtx=2;
  }
}


/**
 * @brief add this surface tris to render
 *
 * @param ntri     number of tris
 * @param ebuff    VBO format buffered eleemnts
 * @param fill     fill in surfele?
 * @param s        if >=0, highlight surface number
 */
void
RenderHilights::surfele( int ntri, const GLfloat* ebuff, bool fill, int s )
{
  if( s>=0 ) _hiSurf = s;
  buffer_hiEles( ntri, ebuff, fill,  s<0?_colAssSurf:_colHiSurf,
                          fill?_colSurfEdge:(s<0?_colAssSurf:_colHiSurf) );
}


/**
 * @brief Buffer a bunch of tris
 *
 * @param ntri       \#triangles
 * @param ebuff      buffer fo vertices
 * @param fill       fill the triangles?
 * @param solidCol   fill colour
 * @param outlineCol edge colour
 */
void
RenderHilights::buffer_hiEles( int ntri, const GLfloat* ebuff, bool fill, 
                             const GLubyte *solidCol, const GLubyte *outlineCol )
{
  _wireTri.resize( _wireTri.size() + ntri*3*VBO_ELEM_SZ );
  _wireCol.resize( _wireCol.size() + ntri*3*VBO_RGBA_SZ );
  GLfloat* wbuff = _wireTri.data() + _wireTri.size() - ntri*3*VBO_ELEM_SZ;
  GLubyte* cbuff = _wireCol.data() + _wireCol.size() - ntri*3*VBO_RGBA_SZ;
  GLfloat *nbuff=NULL;
  if( fill ) nbuff = buff_append( ntri*3 );
  for( int i=0; i<ntri; i++ ){
    GLfloat a[3], b[3], n[3];
    sub( ebuff+VBO_ELEM_SZ*2, ebuff, a );
    sub( ebuff+VBO_ELEM_SZ,   ebuff, b );
    normalize( cross( a, b, n ) );
    short_float shn[3];
    SHORT_VCP( shn, n );
    for( int j=0; j<3; j++ ){
      wbuff += ::buff_vtx( wbuff, cbuff, ebuff+(i*3+j)*VBO_ELEM_SZ, shn, outlineCol, 0 );
      if( fill )
        nbuff += buff_vtx( nbuff, ebuff+(i*3+j)*VBO_ELEM_SZ, shn, solidCol, 0 );
    }
  }
}


/**
 * @brief buffer surfaces of a volume element
 *
 * @param ntri number of triangles
 * @param vtx  vertices
 * @param fill fill in triangles?
 * @param v    if >=0, highlighted volume index
 *
 * @return 
 */
void
RenderHilights:: volele( int ntri, const GLfloat *vtx, bool fill, int v )
{
  if( v>=0 ) _hiVol = v;

  GLfloat *buff  = new GLfloat[ntri*3*VBO_ELEM_SZ];
  for( int i=0; i<ntri*3; i++ )
    memcpy( buff+i*VBO_ELEM_SZ, vtx+i*3, 3*sizeof(GLfloat) );

  buffer_hiEles( ntri, buff, fill, v<0?_colAssVol:_colHiVol, fill?_colVolEdge:(v<0?_colAssVol:_colHiVol) );

  delete[] buff;
}


/**
 * @brief render the buffer contents which are arranged as follows:
 *
 * [vtx of highlighted point if _nPtVtx==1 ]
 * [vtx0 of highlighted cnnx 
 *  vtx0 of highlighted cnnx if _nCnnxVtx==2 ]
 * [ vtx X n_filled ] filled triangles
 * [ vtx X (nodalbuff.size()/VBO_ELEM_SZ-n_filled-nPtVtx-nCnnxVtx) ] outlined triangles
 *
 * @param wf
 */
void 
RenderHilights:: mesh_render( bool wf )
{
  glBindVertexArray(gl_vao);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glDisable(GL_BLEND);
  glDepthMask(GL_TRUE);
  int sp = 0;

  size_t offset = 0;
  if( _nPtVtx==1 ) {
    set_clipping(sp=shader_wireframe);
    glPointSize(_ptRad);
    glDrawArrays(GL_POINTS, 0, 1);
  } else  {
    set_clipping(sp=shader_default);
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    glDrawArrays(GL_TRIANGLES, offset, _nPtVtx );
  }
  offset += _nPtVtx;

  if( _nCnnxVtx==2 ) {
    if( sp != shader_wireframe ) {
      set_clipping(sp=shader_wireframe);
    }
    glLineWidth( _cnnxRad );
    glDrawArrays(GL_LINES, offset, 2);
  } else if( _nCnnxVtx>2 ) {
    if( sp != shader_default ) {
      set_clipping(sp=shader_default);
    }
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    glDrawArrays(GL_TRIANGLES, offset, _nCnnxVtx );
  }
  offset += _nCnnxVtx;
  
  if( !_line3D ) {
    if( sp != shader_wireframe ) {
      set_clipping(sp=shader_wireframe);
    }
    glLineWidth( _lineRad );
    glDrawArrays(GL_LINES, offset, _nLineVtx);
    offset += _nLineVtx;
  }

  if( sp != shader_default ) {
    set_clipping(sp=shader_default);
  }
  glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
  glDrawArrays(GL_TRIANGLES, offset, _n_filled );
  offset += _n_filled;

  set_clipping(shader_wireframe);
  glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
  glDrawArrays(GL_TRIANGLES, offset, _wireTri.size()/VBO_ELEM_SZ );

}


void
RenderHilights::clear()
{
  render_data::clear();
  _wireTri.clear();
  _n_filled  = 0;
  _ptRad     = 0.;
  _cnnxRad   = 0.;
  _nCnnxVtx  = 0;
  _nPtVtx    = 0;
}


void
RenderHilights::update_nodalbuff()
{
  _n_filled = nodalbuff.size()/VBO_ELEM_SZ;
  _n_filled -= _nPtVtx + _nCnnxVtx;
  if( !_line3D ) _n_filled -= _nLineVtx;
  nodalbuff.insert(nodalbuff.end(), _wireTri.begin(), _wireTri.end());
  render_data::update_nodalbuff();
}


void
RenderHilights:: line( unsigned int hl, int p0, int p1, const int*cnnx, const GLfloat *pt, GLfloat rad, int ns )
{
  SHORTVEC( n, 1., 0, 0);
  _hiLine  = hl;
  _lineRad = rad;
  _line3D  = ns>=3;
  size_t osz = num();
  int    nseg = p1 - p0 + 1;
  GLfloat *buff;
  if( _line3D ) {
    buff = buff_append((nseg*ns*2+2*ns)*3) ;
    for( int p=p0; p<=p1; p++ )
      buff_cyl( buff, pt+3*cnnx[2*p], pt+3*cnnx[2*p+1], ns, rad, _colLine, _colLine, 
                                              0, p!=p0, p==p1?NULL:pt+3*cnnx[2*p+3] );
  } else {
    buff = buff_append( 2*nseg ); 
    for( int p=p0; p<=p1; p++ ){
      buff += buff_vtx( buff, pt+3*cnnx[2*p],   n, _colLine, 0 );
      buff += buff_vtx( buff, pt+3*cnnx[2*p+1], n, _colLine, 0 );
    }
  }
  _nLineVtx=(num()-osz);
}



/**
 * @brief subdivide icosahedron by a factor of 4
 *
 * \param snsubdiv[in]  \#times to subdivide,0=point,1=20,2=80,3=320,etc
 * \param tri[out]      triangles
 * \param vtx[out]      vertices
 *
 * \return number of vertices in triangles
 * \post \p tri abd \p vtx are allocated and populated
 */
int 
mk_icosahedron( int nsubdiv, std::vector<unsigned int>& tri, std::vector<GLfloat>& vtx )
{
  vtx.resize(12*3);
  tri.resize(20*3);
  for( int i=0; i<12; i++ ) 
    assign( vtx.data()+i*3, icoshVtx[i] );
  for( int i=0; i<20; i++ ) 
    assign( tri.data()+i*3, icoshTri[i] );

 for( int j=1; j<nsubdiv; j++ ) { // 4X more triangles per loop
   int ntri = tri.size()/3;
   tri.resize(ntri*4*3);
   int nvtx = vtx.size()/3;
   vtx.resize((nvtx+3*ntri)*3);
   int nloop=ntri;
   for( int i=0; i<nloop; i++ ) { 
     int a = tri[i*3];
     int b = tri[i*3+1];
     int c = tri[i*3+2];
     int d = nvtx;
     int e = d+1;
     int f = e+1;
     GLfloat *ax = vtx.data()+a*3;
     GLfloat *bx = vtx.data()+b*3;
     GLfloat *cx = vtx.data()+c*3;
     GLfloat *dx = vtx.data()+d*3;
     GLfloat *ex = dx+3;
     GLfloat *fx = ex+3;
     normalize( add( ax, bx, dx ) );
     normalize( add( bx, cx, ex ) );
     normalize( add( cx, ax, fx ) );
     tri[i*3]   = a;
     tri[i*3+1] = d;
     tri[i*3+2] = f;
     // new tris
     tri[ntri*3]   = d;
     tri[ntri*3+1] = b;
     tri[ntri*3+2] = e;
     ntri++;
     tri[ntri*3]   = e;
     tri[ntri*3+1] = c;
     tri[ntri*3+2] = f;
     ntri++;
     tri[ntri*3]   = f;
     tri[ntri*3+1] = d;
     tri[ntri*3+2] = e;
     ntri++;
     nvtx += 3;
   }
 }
 return tri.size();
}


/** fill vertex frame buffer array
 *
 * \param buff buffer to fill
 * \param xyz  point coordinates
 * \param nrml point normal
 * \param col  point colour
 * 
 * \return number of floats copied
 */
int
render_data :: buff_vtx( GLfloat *buff, const GLfloat *xyz, const short_float *nrml, const GLubyte *col, GLushort surf )
{
  GLubyte *cb = colbuff.data()+VBO_RGBA_SZ*(buff-nodalbuff.data())/VBO_ELEM_SZ;
  return ::buff_vtx( buff, cb, xyz, nrml, col, surf ); 
}


/** fill vertex frame buffer array
 *
 * \param buff buffer to fill
 * \param xyz  point coordinates
 * \param nrml point normal
 * \param col  point colour
 * 
 * \return number of floats copied
 */
int
render_data :: buff_vtx( GLfloat *buff, const GLfloat *xyz, const short_float *nrml, const GLfloat *col, GLushort surf )
{
  GLubyte *cb = colbuff.data()+VBO_RGBA_SZ*(buff-nodalbuff.data())/VBO_ELEM_SZ;
  return ::buff_vtx( buff, cb, xyz, nrml, col, surf ); 
}


/** fill vertex frame buffer array
 *
 * \param index of vtx to buffer
 * \param xyz  point coordinates
 * \param nrml point normal
 * \param col  point colour
 * 
 * \return number of floats copied
 */
int
render_data :: buff_vtx( int e, const GLfloat *xyz, const short_float *nrml, const GLfloat *col, GLushort surf )
{
  char *bp = (char *)nodalbuff.data()+VBO_ELEM_SZ*e;
  GLubyte *cb = colbuff.data()+VBO_RGBA_SZ*e; 
  return ::buff_vtx( (GLfloat *)bp, cb, xyz, nrml, col, surf ); 
}


/** fill vertex frame buffer array
 *
 * \param v    index of vertex to fill
 * \param xyz  point coordinates
 * \param nrml point normal
 * \param col  point colour
 * 
 * \return number of floats copied
 */
int
render_data :: buff_vtx( int v, const GLfloat *xyz, const short_float *nrml, const GLubyte *col, GLushort surf )
{
  char *bp = (char *)nodalbuff.data()+VBO_ELEM_SZ*v;
  GLubyte *cbuf = colbuff.data()+VBO_RGBA_SZ*v; 
  return ::buff_vtx( (GLfloat *)bp, cbuf, xyz, nrml, col, surf );
}


/** fill vertex frame buffer array
 *
 * \param buff buffer to fill
 * \param xyz  point coordinates
 * \param nrml point normal
 * \param col  point colour
 * 
 * \post cb is incremented by GL_RGBA_SZ
 *
 * \return number of floats copied
 */
int
buff_vtx( GLfloat *buff, GLubyte *&cb, const GLfloat *xyz, const short_float *nrml, const GLfloat *col, GLushort surf )
{
  GLubyte shcol[4] = { (GLubyte)(col[0]*255), (GLubyte)(col[1]*255), 
                       (GLubyte)(col[2]*255), (GLubyte)(col[3]*255) };
  return buff_vtx( buff, cb, xyz, nrml, shcol, surf ); 
}

/** fill vertex frame buffer array
 *
 * \param buff buffer to fill
 * \param xyz  point coordinates
 * \param nrml point normal
 * \param col  point colour
 * 
 * \post cb is incremented by GL_RGBA_SZ
 *
 * \return number of floats copied
 */
int
buff_vtx( GLfloat *buff, GLubyte *&cb, const GLfloat *xyz, const short_float *nrml, const GLubyte *col, GLushort surf )
{
  char    *bp = (char *)buff;
  memcpy( bp+VBO_XYZ_OFFSET, xyz, VBO_XYZ_SZ );
#ifdef NORM_FLOAT
  GLfloat lnrml[3];
  FLOAT_VCP( lnrml, nrml );
  memcpy( bp+VBO_NORM_OFFSET , lnrml, VBO_NORM_SZ );
#else
  memcpy( bp+VBO_NORM_OFFSET , nrml, VBO_NORM_SZ );
#endif
  memcpy( bp+VBO_ENT_OFFSET, &surf, VBO_ENT_SZ );

  if( cb ) {
    memcpy( cb, col, VBO_RGBA_SZ );
    cb += VBO_RGBA_SZ;
  }

  return VBO_ELEM_SZ;
}

/**
 * @brief  buffer a sphere
 *
 * @param buff   buffer to fill
 * @param ctr    center of sphere
 * @param vtx    point positions on unity sphere at origin
 * @param ntri   number of triangles
 * @param tris   connectivity of vertices
 * @param radius radius of sphere
 * @param col    colour of sphere
 * @param reg    region
 *
 * @return number of floats buffered
 */
int
RenderSpheres::buff_sphere( GLfloat* &buff, const GLfloat *ctr, const GLfloat* vtx, int ntri,
             const unsigned int* tris, float radius, const GLfloat *col, int reg )
{
  for( int i=0; i<ntri*3; i++ ){
    GLfloat P[3], n[3];
    scale( assign(P, assign(n, vtx+tris[i]*3)), radius );
    add( P, ctr, P );
    short_float shn[3];
    SHORT_VCP( shn, scale(n, -1) );
    buff += buff_vtx( buff, P, shn, col, reg );
  }
  return ntri*3*VBO_ELEM_SZ;
}



/**
 * @brief draw a cone/disk in x,y plane
 *
 * @param buff   VBO buffer
 * @param ndiv   number of subdivisions
 * @param rad    radius
 * @param zb     height of base
 * @param zp     height of peak
 * @param nz     normal (+/- 1)z
 * @param colour r,g,b,a
 *
 * \return number of floats buffered
 *
 * \post buff is advanced to one past end of cone/disk
 */
int
render_data :: buff_cone( GLfloat* &buff, int ndiv, GLfloat rad, float zb, float zp,
                                                 GLfloat nz, const GLfloat* colour ) 
{
  GLfloat x[ndiv+1], y[ndiv+1];
  GLfloat ang = 2*M_PI/float(ndiv);
  for( int i=0; i<ndiv+1; i++ ) {
    x[i] = cos( ang*i );
    y[i] = sin( ang*i );
  }

  GLubyte *nub = NULL;
  GLfloat c[3] = { 0, 0, zp };
  GLfloat zn = copysign( rad/sqrt(rad*rad+(zb-zp)*(zb-zp)), nz );
  GLfloat xyn = sqrt(1.-zn*zn);
  GLfloat n[3];
  for( int i=0; i<ndiv; i++ )  {
    GLfloat a[3] = {0, 0, zb };
    a[0] = x[i+(nz>0)]*rad;
    a[1] = y[i+(nz>0)]*rad;
    GLfloat b[3] = {0, 0, zb };
    b[0] = x[(i+(nz<0))]*rad;
    b[1] = y[(i+(nz<0))]*rad;
    GLfloat xm = (x[i]+x[(i+1)])/2;
    GLfloat ym = (y[i]+y[(i+1)])/2;
    scale( normalize( assign( n, xm, ym, 0 ) ), xyn );
    n[2] = zn;
    short_float shn[3];
    SHORT_VCP( shn, scale( n, -1 ) );
    buff += buff_vtx( buff,c, shn, colour, 0 );
    assign( n, x[i+(nz>0)]*xyn, y[i+(nz>0)]*xyn, zn ); 
    SHORT_VCP( shn, scale( n, -1 ) );
    buff += buff_vtx( buff, a, shn, colour, 0 );
    assign( n, x[i+(nz<0)]*xyn, y[i+(nz<0)]*xyn, zn ); 
    SHORT_VCP( shn, scale( n, -1 ) );
    buff += buff_vtx( buff, b, shn, colour, 0 );
  }
  return 3*ndiv*VBO_ELEM_SZ;
}


/**
 * @brief  fill buffer to draw an arrow centered at (0,0,0) and extending along z
 *
 * @param buffer       buffer to fill
 * @param stick        stick length
 * @param head         head length (=0 for no head)
 * @param stick_rad    stick radius
 * @param head_rad     head radius
 * @param numdiv       number of radial subdivisions
 * @param color        colour of arrow
 * @param _strip       draw as TRIANGLE_STRIP
 *
 * @post buffer filled
 * \pre  buffer must be allocated to hold arrow
 *
 * \return number of GLfloats buffered
 */
int 
render_data :: buff_3Darrow( GLfloat* &buff, GLfloat stick, GLfloat head, GLfloat stick_rad, 
                    GLfloat head_rad, int numdiv, const GLfloat* colour, bool as_strip )
{
  GLfloat* buff0 = buff;
  bool     draw_head = head>0;

  GLfloat vtx0[3] = {0,0,0}, vtx1[3]={0,0,0}, vtx2[3]={0,0,0};
  vtx0[2] = -(stick+head)/2.;
  vtx1[2] =  (stick-head)/2.;
  vtx2[2] =  vtx1[2]+1;
  int nfloat = buff_cyl( buff, vtx0, vtx1, numdiv, stick_rad, colour, colour, 0, false, 
                                                        draw_head?vtx2:NULL, as_strip );

  if( !draw_head ) return nfloat;

  // draw base of head
  buff_cone( buff, numdiv, head_rad, (stick-head)/2, (stick-head)/2., -1., colour );
  // draw head
  buff_cone( buff, numdiv, head_rad, (stick-head)/2, (stick+head)/2.,  1., colour );

  return buff - buff0;
}


/**
 * @brief enter alpha value into vertex buffer array
 *
 * @param buf     start of buffer entry
 * @param alpha   opacity value
 */
void 
buffer_alpha( GLubyte *buf, GLfloat alpha )
{
  GLubyte a = alpha*255;
  memcpy( buf+3, &a, 1 );
}


void 
buffer_RGB( GLubyte *buf, const GLfloat *c, bool alpha )
{
  GLubyte col[4] = { (GLubyte)(c[0]*255), (GLubyte)(c[1]*255),
                     (GLubyte)(c[2]*255), (GLubyte)(c[3]*255) };
  memcpy( buf, col, alpha?VBO_RGBA_SZ:3 );
}


