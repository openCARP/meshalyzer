#ifndef FRAME_H
#define FRAME_H

#include <string>
#include "PNGwrite.h"
#ifdef OSMESA
#include <GL/osmesa.h>
#endif

class TBmeshWin;

class  Frame {
    public:
      Frame( TBmeshWin *t, int, int, bool=false, bool=true );
      ~Frame();
      int write( std::string s ){return write(_w,_h,s);}
      int write( std::string s, int t){return write(_w, _h, s, t);}
      int write( int, int, std::string, int f=-1 );
      int write( int, int, std::string, int, int,int stride=1 );
      void dump( std::string s, bool tr=false ){dump(_w,_h,s, tr);}
      void dump( int, int, std::string, bool=false );
      void pixel_color( int x, int y, GLubyte *color ){
                     memcpy( color, _buffer+(x+_w*y)*4, 4*sizeof(GLubyte ) );}
      void fill_buffer( int w, int h, bool alpha=false );
      GLubyte *tightBB( int &nx, int &ny, GLubyte *bgd, int align );
    private:
     GLubyte* _buffer = NULL;
     int  _w;
     int  _h;
     bool _full_alpha=false;
     TBmeshWin *_tbwm;
     void delete_objs();
#ifdef USE_FBO
     bool _fbo = true;
     GLuint _fb, _color_rb, _depth_rb;
     GLuint _fbms, _color_rbms, _depth_rbms;
#else
     bool _fbo = false;
#endif
};

#include "TBmeshWin.h"
#endif
