#include "DrawingObjects.h"

const int tet_num_edge=6;
static int tet_edges[tet_num_edge][2] =
  {
    {0,1},{1,2},{2,0},{0,3},{1,3},{2,3}
  };
const int tetra_iso_table[][15] = {
    { 0 }, // 0000
    { 1, 3, 0, 3, 0, 2, 0, 1 },
    { 1, 3, 1, 3, 0, 1, 1, 2 },
    { 2,
      3, 0, 3, 0, 2, 1, 3,
      3, 1, 3, 0, 2, 1, 2 },
    { 1, 3, 2, 3, 1, 2, 0, 2 },
    { 2,
      3, 0, 3, 2, 3, 0, 1,
      3, 0, 1, 2, 3, 1, 2 }, // 0101
    { 2,
      3, 2, 3, 1, 3, 0, 2,
      3, 0, 2, 1, 3, 0, 1 }, // 0110
    { 1, 3, 0, 3, 2, 3, 1, 3 },  
    { 1, 3, 0, 3, 1, 3, 2, 3 },
    { 2,
      3, 2, 3, 0, 2, 1, 3,
      3, 0, 2, 0, 1, 1, 3 }, // 1001
    { 2,
      3, 0, 3, 0, 1, 2, 3,
      3, 0, 1, 1, 2, 2, 3 },
    { 1, 3, 2, 3, 0, 2, 1, 2 }, //1011
    { 2,
      3, 0, 3, 1, 3, 0, 2,
      3, 1, 3, 1, 2, 0, 2 }, //1100
    { 1, 3, 1, 3, 1, 2, 0, 1 },
    { 1, 3, 0, 3, 0, 1, 0, 2 },
    { 0 }
};
const int tet_num_surf = 4;
const int tetra_surface_table[][3] = {
   {0, 1, 2}, {0, 3, 1}, {0, 2, 3}, {2, 1, 3}
};


/** read in the point file */
bool Tetrahedral :: read( const char *fname )
{
  gzFile in;

  try {
    in = openFile( fname, "tetras" );
  } catch (...) {return false;}

  const int bufsize=1024;
  char buff[bufsize];
  if ( gzgets(in, buff, bufsize) == Z_NULL ) return false;
  sscanf( buff, "%d", &_n );
  _node    = (int*)malloc(4*_n*sizeof(int));
  _region = new int[_n];

  for ( int i=0; i<_n; i++ ) {
    gzgets(in, buff, bufsize);
    if ( sscanf( buff, "%d %d %d %d %d", _node+4*i, _node+4*i+1,
                 _node+4*i+2, _node+4*i+3, _region+i  ) < 5    ) {
      // if region not specified, assign a default of -1
      _region[i] = -1;
    }
  }
  gzclose(in);
  return true;
}


/** draw the faces of an element not contaiining a node
 *
 * \param n do not draw faces associated with this node
 */
void Tetrahedral::draw_out_face( int n )
{
#if 0
  for ( int i=0; i<_n*_ptsPerObj; i++ )
    if ( _node[i] == n ) {
      int elenode = (i/_ptsPerObj)*_ptsPerObj;
      glBegin(GL_TRIANGLES);
      for ( int j=0; j<4; j++ )
        if ( _node[elenode+j] != n )
          glVertex3fv( _pt->pt(_node[elenode+j]) );
      glEnd();
    }
#endif
}


/** determine if a clipping plane intersects an element.
 *
 *  \param pd     visible points
 *  \param cp     clipping plane, cp[0]x + cp[1]y +cp[2]z + cp[3] = 0
 *  \param e      the element in the list
 *  \param interp construct to interpolate data
 *
 *  \return a surface element if intersection, NULL otherwise
 *
 *  \post interp may be allocated
 */
SurfaceElement*
Tetrahedral::cut( char *pd, GLfloat* cp,
                  Interpolator<DATA_TYPE>* &interp, int e )
{
  return planecut( pd, cp, interp, tet_num_edge, tet_edges, e );
}


const int*
Tetrahedral::iso_polys(unsigned int index)
{
  return tetra_iso_table[index];
}


/* 
 * return list of lists of nodes defining the bounding surfaces
 *
 * \param nl  nodelists
 * \param v   element number
 * \param tri return only tris
 *
 * \param pointer to a vector of vectors
 */
int 
Tetrahedral::surfaces( int nl[][MAX_NUM_SURF_NODES+1], bool tris, int v )
{
  return make_surf_nodelist( v, nl, tet_num_surf, 
             int(tetra_surface_table[1]-tetra_surface_table[0]), 
                                (const int **)tetra_surface_table, tris );
}


const int*
Tetrahedral::edges( int &n)
{
  n = tet_num_edge;
  return (const int*)tet_edges;
}
