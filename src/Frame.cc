#ifdef OSMESA
#  define GL_GLEXT_PROTOTYPES
#  include <GL/osmesa.h>
#  define USE_FBO           // antialiasing
#  undef USE_FLTK_IMG
#elif  defined(__APPLE__)
#  define GL_SILENCE_DEPRECATION
#  include <OpenGL/gl3.h> 
#else
#  if defined(WIN32)
#    define GLEW_STATIC 1
#  endif
#  include <GL/glew.h>
static bool initglew = false;
#endif

#include "Frame.h"

bool has_ext( std::string, const char * );

/**
 * @brief make FrameBufferObject
 *
 * @param w            width
 * @param h            height
 * @param fb[out]      framebuffer identifier
 * @param cb[out]      colour buffer 
 * @param db[out]      depth buffer
 * @param multisampled multisampling?
 */
void 
make_framebuffer( GLsizei w, GLsizei h, GLuint &fb, GLuint &cb, GLuint &db, bool multisampled=false ) 
{
#ifdef USE_FBO
    glGenFramebuffers(1, &fb);
    glBindFramebuffer(GL_FRAMEBUFFER, fb);
    glGenRenderbuffers(1, &cb);
    glBindRenderbuffer(GL_RENDERBUFFER, cb);
    if( multisampled ) {
      glRenderbufferStorageMultisample(GL_RENDERBUFFER, 4, GL_RGBA8, w, h );
    } else {
      glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA8, w, h );
    }
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, cb);
    glGenRenderbuffers(1, &db);
    glBindRenderbuffer(GL_RENDERBUFFER, db);
    if( multisampled ) {
      glRenderbufferStorageMultisample(GL_RENDERBUFFER, 4, GL_DEPTH_COMPONENT24, w, h );
    } else {
      glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, w, h );
    }
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, db);
    GLenum gle=glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if( gle!=GL_FRAMEBUFFER_COMPLETE ) {
      std::cout << "Frame buffer incomplete -> aborting:" << gle << std::endl;
      throw 1;
    }
#endif
}


/** General constructor */
Frame :: Frame( TBmeshWin *t, int w, int h, bool fa, bool fbo ) :_tbwm(t),_w(w),_h(h),
            _full_alpha(fa), _buffer(new GLubyte[w*h*4])
{
#ifdef OSMESA
    if (!OSMesaMakeCurrent((OSMesaContext)(_tbwm->context()), _buffer, GL_UNSIGNED_BYTE, w, h)) {
      std::cout << "Failed OSMesaMakeCurrent" << std::endl;
      throw 1;
    }
#elif !defined(__APPLE__)
  if( !initglew ) {
    initglew = true;
    GLenum err = glewInit();
    if (GLEW_OK != err) {
      std::cerr << "GLEW problem -- aborting!" << std::endl;
    }
  }
#endif

#ifdef USE_FBO
  _fbo = _fbo && fbo;

  if( _fbo ) {
    make_framebuffer( w, h, _fbms, _color_rbms, _depth_rbms, true );
    make_framebuffer( w, h, _fb,   _color_rb,   _depth_rb );
    glBindFramebuffer(GL_FRAMEBUFFER, 0); // restore the display
  }
#endif //USE_FBO
}


Frame :: ~Frame()  
{
  delete_objs();
}


void 
Frame :: delete_objs()
{
  if (_buffer) delete[] _buffer;
#ifdef USE_FBO
  if( _fbo  ){
    glDeleteRenderbuffers(1, &_color_rb);
    glDeleteRenderbuffers(1, &_depth_rb);
    glDeleteFramebuffers(1, &_fb);
    glDeleteRenderbuffers(1, &_color_rbms);
    glDeleteRenderbuffers(1, &_depth_rbms);
    glDeleteFramebuffers(1, &_fbms);
  }
#endif
}


/** output specified frames
 *
 * \param w     output image width
 * \param h     output image height
 * \param fname base file name
 * \param f0    first frame number
 * \param f1    largest frame = f0+n-1
 * \param stride output frame number stride
 *
 * \note \p fname will have ".png" removed if present and XXXXX.png 
 *       where XXXXX is the frame number
 *
 * \note the frame numbers written are those which satisfy f0<=n<=f1
 *
 * \return the number of frames written
 */
int
Frame :: write( int w, int h, std::string fname, int f0, int f1, int stride )
{
  if( !_tbwm->set_time(f0) ) return 0;

  if( has_ext( fname, ".png") )
    fname = fname.substr(0,fname.length()-4);

  int numout=0;
  for( int f=f0; f<=f1; f+=stride) {
    if( !_tbwm->set_time(f) )
      break;
    std::string file=fname;
    char strnum[256];
    snprintf( strnum, 256, "%05d.png", f );
    file += strnum;
    dump( w, h, file );
    numout++;
  }
  return  numout;
}


/** output specified frame
 *
 * \param w     output image width
 * \param h     output image height
 * \param fname file name
 * \param f     frame number
 *
 * \note \p fname will have ".png" appended if not present
 */
int
Frame :: write( int w, int h, std::string fname, int f )
{
  if( f>=0 && !_tbwm->set_time(f) ) return 0;

  if( !has_ext(fname,".png") ) fname+= ".png";

  dump( w, h, fname );
  return 1;
}


/* dump the frame buffer into a PNG file
 *
 * \param w
 * \param h
 * \param fname
 */
void 
Frame :: dump( int w, int h, std::string fname, bool trim )
{
  FILE *out = fopen( fname.c_str(), "w" );
  PNGwrite* pngimg = new PNGwrite( out );
  pngimg->depth( 8*sizeof(GLubyte) );

  std::map<std::string,std::string> metadata;
  if( _tbwm->dataBuffer ) {
    std::string s =  _tbwm->dataBuffer->file();
    if( s.find(".datH5:") != std::string::npos )
      metadata["Data File"] = s.c_str();
    else
      metadata["Data File"] = realpath( s.c_str(), NULL );
    char cscale[1024];
    snprintf(cscale, 1024, "data range = [%f, %f], %d levels", 
            _tbwm->cs->min(), _tbwm->cs->max(), _tbwm->cs->size() );
    metadata["Colour Scale"] = cscale;
    char time[1024] = "Frame 0";
    if( _tbwm->data ) 
      snprintf( time, 1024, "Frame %d --> %f ms",  _tbwm->time(),
               _tbwm->time()*_tbwm->dataBuffer->dt()+_tbwm->dataBuffer->t0() );
    metadata["Time"] = time;
  } 
  if(_tbwm->auxGrid) metadata["Auxiliary Grid"] = _tbwm->auxGrid->file();
  if(_tbwm->vecdata) metadata["Vector Data"]    = _tbwm->vecdata->file();

  std::string ptsfile(_tbwm->model->file());
  ptsfile += ".bpts";
  char *mpath = realpath( ptsfile.c_str(), NULL );
  if( !mpath ) {
    ptsfile.erase(ptsfile.size()-4,1); //remove "b"
    mpath = realpath( ptsfile.c_str(), NULL );
  }
  metadata["Mesh"] = mpath;

  pngimg->description( metadata );
  free(mpath);

  if( _tbwm->transBgd() )
    pngimg->colour_type( PNG_COLOR_TYPE_RGB_ALPHA );
  else
    pngimg->colour_type( PNG_COLOR_TYPE_RGB );

  fill_buffer( w, h );
  GLubyte *newbuff = _buffer;
  int nw=w, nh=h;
  
  int align;
  glGetIntegerv(GL_PACK_ALIGNMENT, &align);

  if( trim ) {
    auto bgdF = _tbwm->bgd();
    GLubyte bgd[] = { (GLubyte)(bgdF[0]*255), (GLubyte)(bgdF[1]*255), 
                      (GLubyte)(bgdF[2]*255), (GLubyte)(_tbwm->transBgd()?0:255) };
    newbuff = tightBB(nw, nh, bgd, align);
  }
  
  pngimg->size( nw, nh );

#ifdef USE_FLTK_IMG
  pngimg->write( newbuff, 1, true );
#else
  pngimg->write( newbuff, align );
#endif
  delete pngimg;

  if(trim) delete[] newbuff;

  if( _fbo )
    glBindFramebuffer(GL_FRAMEBUFFER, 0); // restore the display
}


void 
Frame::fill_buffer( int w, int h, bool alpha )
{
  if( _buffer==NULL || _w!=w || _h!=h ) {   // resize 
    
    if( _buffer )   // not first time, destroy old buffers/contexts
      delete_objs();
	
    _w = w;
    _h = h;
    _buffer = new GLubyte[w*h*4];

#ifdef USE_FBO
    if( _fbo ) {
      make_framebuffer( w, h, _fbms, _color_rbms, _depth_rbms, true );
      make_framebuffer( w, h, _fb,   _color_rb,   _depth_rb );
    }
#endif
  }

#ifdef USE_FBO
   if( _fbo )
     glBindFramebuffer(GL_FRAMEBUFFER, _fbms);
#endif
  
  _tbwm->invalidate();
#if !defined(USE_FLTK_IMG)
  if( _fbo )
#endif
    _tbwm->offscreen(true);
  
#ifdef USE_FLTK_IMG
  auto old_dev = Fl_Surface_Device::surface();
  Fl_Image_Surface img( w, h );
  img.set_current();
  img.draw( _tbwm );
  memcpy( _buffer, img.image()->data()[0], 3*w*h );
  old_dev->set_current();
#else
  _tbwm->draw();
#endif

#if !defined(USE_FLTK_IMG)
  if( _fbo )
#endif
    _tbwm->offscreen(false);

#if defined(USE_FLTK_IMG)
  return;
#endif

  if( _fbo ){
#ifdef USE_FBO
    glBindFramebuffer(GL_READ_FRAMEBUFFER, _fbms);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _fb);
#ifndef OSMESA
    glDrawBuffer(GL_BACK);
#endif //OSMESA
    glBlitFramebuffer( 0, 0, w, h, 0, 0, w, h, GL_COLOR_BUFFER_BIT, GL_NEAREST );
    glBindFramebuffer(GL_FRAMEBUFFER, _fb);
#endif //USE_FBO
  } else {
    glReadBuffer(GL_BACK);
  }

  if( _tbwm->transBgd() || alpha ){
    glReadPixels(0,0,w,h,GL_RGBA,GL_UNSIGNED_BYTE,(GLvoid *)_buffer);
    if( !_full_alpha )
      for( int i=0; i<w*h; i++ )
        if( _buffer[i*4+3] > 0 ) _buffer[i*4+3] = 255;
  } else
    glReadPixels(0,0,w,h,GL_RGB,GL_UNSIGNED_BYTE,(GLvoid *)_buffer);
}



/**
 * @brief  trim as much background as possible and make background transparent
 *
 * @param nx[out] new width
 * @param ny[out] new height
 * @param bgd     background colour
 * @param align   align on this many bytes
 *
 * \return the data of the trimmed image, delete when finished with it
 */
GLubyte *
Frame::tightBB( int &nx, int &ny, GLubyte *bgd, int align )
{
  int llx, lly, urx, ury;
  int csz        = _tbwm->transBgd() ? 4 : 3;
  int pixel_data = csz*_w;
  int pad        = (align - (pixel_data%align))%align; 

  for( int j=0; j<_h; j++ ) 
    for( int i=0; i<_w; i++ ) 
      if( memcmp(_buffer+(j*_w+i)*csz+j*pad, bgd, sizeof(GLubyte)*csz) ){
        lly = j;
        j   = _h;
        break;
      }
    
  for( int i=0; i<_w; i++ )
    for( int j=0; j<_h; j++ )
      if( memcmp(_buffer+(j*_w+i)*csz+j*pad, bgd, sizeof(GLubyte)*csz) ){
        llx = i;
        i   = _w;
        break;
      }

  for( int j=_h-1; j>=0; j-- )
    for( int i=0; i<_w; i++ )
      if( memcmp(_buffer+(j*_w+i)*csz+j*pad, bgd, sizeof(GLubyte)*csz) ){
        ury = j;
        j   = -1;
        break;
      }

  for( int i=_w-1; i>=0; i-- )
    for( int j=0; j<_h; j++ )
      if( memcmp(_buffer+(j*_w+i)*csz+j*pad, bgd, sizeof(GLubyte)*csz) ){
        urx = i;
        i   = -1;
        break;
      }

  nx = urx - llx + 1;
  ny = ury - lly + 1;

  int npad   = (align - (csz*nx%align))%align; 
  GLubyte *newimg = new GLubyte[ny*(nx*csz+npad)];
  for( int j=lly; j<=ury; j++ ) {
    memcpy( newimg+(j-lly)*(nx*csz+npad), _buffer+(llx+j*_w)*csz+j*pad, nx*sizeof(GLubyte)*csz );
  }
  return newimg;
}


