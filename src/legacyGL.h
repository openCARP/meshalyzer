#ifndef LEGACY_GL_H
#define LEGACY_GL_H

#include "Quaternion.h"

#ifdef OSMESA
#  include <GL/osmesa.h>
#elif defined(__APPLE__)
#  define GL_SILENCE_DEPRECATION
#    include <OpenGL/gl3.h> // defines OpenGL 3.0+ functions
#else
#    if defined(WIN32) || defined(__CYGWIN__)
#      define GLEW_STATIC 1
#    endif
#    include <GL/glew.h>
#endif
#include <map>

#define NUM_CP 6

extern       GLfloat CLIP_EQN[NUM_CP][4];
extern const GLenum  CLIP_PLANE[];
extern const GLenum  CLIP_DISTANCE[];

extern std::map<void*,Quaternion> LightDir;
extern std::map<void*,M4f> ModelViewMat;
extern std::map<void*,M4f> ProjectionMat;

void ortho_proj(void*, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
void light_dir( void *, const Quaternion& );
Vector3D<GLfloat> view_mult( void *, Vector3D<GLfloat> );
inline Quaternion light_dir( void *cntxt ){ return LightDir[cntxt]; }
void translate_mv( void*, GLfloat x, GLfloat y, GLfloat );
void scale_mv( void*, GLfloat x, GLfloat y, GLfloat z );
void mult_mv( void*, M4f m );
void identity_mv(void*);
bool clip_on( int );
void clip_on( bool * );
bool clip_off( int );
void clip_off( bool * );
bool clipping( int );
void clip_eqn( int i, GLfloat* coeffs );

#endif
