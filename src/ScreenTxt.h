#ifndef SCREENTXT_H
#define SCREENTXT_H

#ifdef OSMESA
#  include <GL/osmesa.h>
#elif defined(__APPLE__)
#  define GL_SILENCE_DEPRECATION
#  include <OpenGL/gl3.h>
#else
#  if defined(WIN32) || defined(__CYGWIN__)
#    define GLEW_STATIC 1
#  endif
#  include <GL/glew.h>
#endif

int generate_shader_program(const char* vertex_source, const char* fragment_source);

inline int get_screen_txt_shader()
{
  const char *vtxShader = 
  "#version 410 core\n"
  "in vec4 coord;\n"
  "out vec2 texcoord;\n"
  "\n"
  "void main(void) {\n"
  "  gl_Position = vec4(coord.xy, 0, 1);\n"
  "  texcoord = coord.zw;\n"
  "}\n";

  const char *fragShader =
  "#version 410 core\n"
  "in vec2 texcoord;\n"
  "layout(location=0) out vec4 fragColor;\n"
  "uniform sampler2D tex;\n"
  "uniform vec4 color;\n"
  "\n"
  "void main(void) {\n"
  "  fragColor = vec4(1., 1., 1., texture(tex, texcoord).r) * color;\n"
  "}\n";

  return generate_shader_program( vtxShader, fragShader );
}

#include <freetype2/ft2build.h>
#include FT_FREETYPE_H

enum TxtPos { N, UL, UC, UR, LL, LC, LR, L, C, R };

class ScreenTxt {

  public:
    ScreenTxt(const char *fnt=NULL );
    ~ScreenTxt();
    void  colour( const GLfloat col[4] ){ memcpy( _col, col, 4*sizeof(GLfloat) ); }
    void  render(const char *text, float x, float y, int w, int h, float fs, int vert=0);
    float size( const char *txt, int w, int h, float fs, float& txtW, float& txtH, int vert=0 );
    void  position( const char *, TxtPos, int w, int h, float fs, float &x, float &y, int vert=0 ); 
  private:

    FT_Library _ft;
    FT_Face    _roman;
    FT_Face    _greek;
    FT_Face    _face;               // current face
    GLuint     _vbo;
    GLfloat    _col[4] = {0,0,0,1}; // black
    GLuint     _attribute_coord;
    GLuint     _uniform_tex;
    GLuint     _uniform_color;
    GLint      _program;
    GLuint     _vao;
    GLuint     _tex;
    float      _sx;                 // x pixel to NSC scaling factor
    float      _sy;                 // y pixel to NSC scaling factor
    int        _vert;               // horizontal(0), up (1) or down (-1)
    int        _fs=0;

    int  process(const char *p0, const char *p1, float fntsz, float &x0, float &y0);
    void process_size(const char *p0, const char *p1, float fntsz, int &x, int &ymin, int &ymax );
    int  char_render(const char c, float &x, float &y, float fs);
    int  char_size(const char c, float fs, int &ymin, int &ymax); 
};

#endif
