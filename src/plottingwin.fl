# data file for the Fltk User Interface Designer (fluid)
version 1.0308
header_name {.h}
code_name {.cc}
decl {\#include "TBmeshWin.h"} {public global
}

decl {class TBmeshWin;} {public local
}

decl {class AuxGrid;} {public local
}

decl {\#include "plottingwin.h"} {private local
}

decl {\#include "Graph.h"} {private local
}

decl {\#include "FL/fl_ask.H"} {private local
}

decl {\#include <iostream>} {private local
}

decl {class PlotWin;} {public local
}

decl {\#include <algorithm>} {private local
}

decl {\#include <vector>} {private local
}

decl {\#include <string>} {public global
}

class CurveInfo {open
} {
  Function {CurveInfo(PlotWin*pw)} {open
  } {
    Fl_Window win {
      label {Curve Legend} open
      xywh {1307 586 425 215} type Single hide resizable
    } {
      Fl_Browser info {
        callback {int sel = o-> value();
if( sel == 0 ) return;
std::string label = o->text(sel);
const char *str = fl_input( "enter new label" );
if( str ) {
  std::string::size_type endform = label.find(":")+2;
  std::string newlabel = label.substr(0, endform );
  newlabel += str;
  o->text(sel,newlabel.c_str() );
  o->data(sel,(void*)1);
}}
        xywh {0 0 425 185} type Select resizable
      }
      Fl_Return_Button {} {
        label close
        callback {win->hide();}
        xywh {330 185 95 30}
      }
      Fl_Button {} {
        label {refresh from current file}
        callback {_pw->update_curves(true);
o->redraw();}
        xywh {165 185 165 30} color 238
      }
      Fl_Button {} {
        label {add from current file}
        callback {_pw->update_curves(false);}
        xywh {0 185 165 30} color 134 selection_color 110
      }
    }
    code {_pw=pw;} {}
  }
  Function {add_line( std::string s )} {} {
    code {info->add( s.c_str() );} {}
  }
  Function {replace_line( std::string label, int line )} {open
  } {
    code {if( line <= info->size() )
  info->text( line, label.c_str() );
else
  info->add( label.c_str(), 0 );} {}
  }
  Function {relabelled( int line )} {open return_type bool
  } {
    code {return line<=info->size() && (size_t)(info->data(line))==1;} {}
  }
  Function {clear()} {open
  } {
    code {info->clear();} {}
  }
  decl {PlotWin *_pw;} {private local
  }
}

class PlotWin {open
} {
  decl {std::vector<double> data;} {private local
  }
  decl {int datasize;} {private local
  }
  decl {double datamin;} {private local
  }
  decl {int tm;        // datum to highlight} {private local
  }
  decl {int _id;  // vertex id} {private local
  }
  decl {std::vector<double>xv;} {private local
  }
  decl {double tmx[2], tmy[2];} {private local
  }
  decl {Fl_Button *but;} {public local
  }
  decl {bool rotated;} {private local
  }
  decl {TBmeshWin *mwtb=NULL;} {private local
  }
  decl {AuxGrid *ag=NULL;} {public local
  }
  decl {template<class T>void set_data( int, int, T *, int t=-1, float dt=1,float torg=0, T* xd=NULL);} {public local
  }
  decl {template<class T>double make_set(int n,T *ds,double *dd,T *xs,double *xd,float dt,float t0);} {private local
  }
  decl {void highlight( int );} {public local
  }
  decl {void rotate( bool );} {public local
  }
  decl {void writedata();} {private local
  }
  decl {std::string _datafile = "time series";} {private local
  }
  Function {PlotWin( std::string title, void *t, bool tb=true )} {} {
    Fl_Window window {
      label {Time plot}
      user_data this
      callback {this->window->hide();} open
      xywh {1209 892 520 200} type Double box UP_BOX resizable
      code0 {\#include <cstring>}
      code1 {\#include <string>} visible
    } {
      Fl_Box graph {
        xywh {0 0 520 170} resizable
        code0 {\#include "Graph.h"}
        class Graph
      }
      Fl_Group button_grp {
        xywh {5 170 510 25} box ENGRAVED_BOX color 52
      } {
        Fl_Return_Button {} {
          label Close
          callback {this->window->hide();}
          xywh {15 175 75 15} labelsize 10
        }
        Fl_Button {} {
          label Write
          callback {writedata();}
          xywh {275 175 70 15} labelsize 10
        }
        Fl_Light_Button rotbut {
          label Rotate
          callback {rotate(o->value());}
          xywh {105 175 70 15} labelsize 10
        }
        Fl_Button {} {
          label Pop
          xywh {190 175 70 15} labelsize 10
        }
        Fl_Light_Button autoscalebut {
          label Autoscale
          callback {autoscale(o->value());}
          xywh {440 175 70 15} value 1 labelsize 10
        }
        Fl_Button {} {
          label Hold
          callback {graph->copy_curve(0);
if( graph->crvi_vis() )
  static_curve_info_cb(graph);}
          xywh {360 175 70 15} labelsize 10
        }
      }
    }
    code {graph->num_dynamic(2);
xv.resize(1);
data.resize(1);
tm = 0;
datasize = -1;
rotated = false;
window->copy_label(title.c_str());
window->user_data(this);
if( tb ) 
  mwtb = (TBmeshWin*)t;
else
  ag = (AuxGrid*)t;} {}
  }
  decl {~PlotWin()} {public local
  }
  Function {autoscale(int a)} {} {
    code {graph->autoscale(a);} {}
  }
  Function {datafile(std::string d)} {} {
    code {_datafile = d;
int i0 = d.rfind("/");
std::string wlab;
if ( i0 < std::string::npos )
  wlab = d.substr(i0+1,std::string::npos);
else
  wlab = d;
window->label( wlab.c_str() );} {}
  }
  Function {update_curves(bool replace)} {} {
    code {const int ndyn=2;

// copy the ids
std::vector<int> ids;
for( int i=0; i<graph->n(); i++ ) 
  ids.push_back( graph->id(i) );

if( replace ) {
  graph->clear_curves();
} else { //only add curves once
  std::sort(ids.begin()+ndyn, ids.end());
  ids.erase( unique( ids.begin()+ndyn, ids.end() ), ids.end() );
}

int n;
if( mwtb ) n = mwtb->dataBuffer->max_tm()+1;

for( int i=ndyn; i<ids.size(); i++ ) {
    float  *yf = NULL;
    float  dt, t0;
    if( mwtb ) {
      yf = new float[n];
      mwtb->dataBuffer->time_series( ids[i], yf );
      dt = mwtb->dataBuffer->dt();
      t0 = mwtb->dataBuffer->t0();
    } else {
      n  = ag->time_series( ids[i], yf );
      dt = ag->dt();
      t0 = ag->t0();
    }
    double *x  = new double[n];
    double *y  = new double[n];
    make_set( n, yf, y, (float*)0, x, dt, t0 );
    delete[] yf;
    int setno = replace ? i : graph->n();
    double annot;
    if( !mwtb->tm_annot(ids[i], annot) )
      graph->set_2d_data( x, y, annot, n, setno, ids[i], _datafile );
    else
      graph->set_2d_data( x, y, n, setno, ids[i], _datafile );
}
static_curve_info_cb(graph);} {}
  }
  Function {clear_annots()} {} {
    code {graph->clear_annots();} {}
  }
  Function {aux_plot()} {
    comment {is this for an auxiliary grid?} selected return_type bool
  } {
    code {return ag != NULL;} {}
  }
}
