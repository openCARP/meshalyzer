/** \file Connections are segments defined by 2 points. The node array is
 * twice as large as the number of connections. For connection i, the nodes
 * defining it are located in _node[2*i] and node[2+i=1]
 */
#include "DrawingObjects.h"

#include "VecData.h"
#ifdef USE_HDF5
#include <ch5/ch5.h>
#endif


/**
 * @brief buffer connections based on lines in space. In 3D, make cylinders and end caps
 *
 * @param rd     render buffer
 * @param col    color if no data
 * @param cs     colour map for data
 * @param data   data to colour
 * @param dop    data opacity
 * @param cmode  colour mode
 * @param reg    region
 */
void
Connection::buffer( RenderLines& rd, const GLfloat *col, Colourscale* cs,
                             DATA_TYPE *data, dataOpac* dop, char cmode, GLushort *reg )
{
  int        ln          = 0;
  SHORTVEC(nrml, 0., 0, 1.);
  GLfloat   *cpubuff     = NULL;
  GLubyte   *colbuff     = NULL;
  bool       translucent = false;
  bool       ecol        = cmode&(CM_ELEM+CM_COL_ELEM);

  std::vector<DATA_TYPE>eledata;
  if( cmode&CM_COL_ELEM ) 
    data = dat_to_elem(eledata, data);

  rd.as_strip( true );
  for( int i=0; i<num(); i++ ) {
    GLushort vreg = reg ? reg[i] : 0;
    bool in_line = ln<_lines.size() && i==_lines[ln].first;
    int stop     = in_line ? _lines[ln].second : i;
    int nseg     = stop - i + 1;
    GLfloat *buff = cpubuff = (GLfloat *)realloc( cpubuff, (nseg+1)*VBO_ELEM_SZ*sizeof(GLfloat) );
    GLubyte *cbuf = colbuff = (GLubyte *)realloc( colbuff, (nseg+1)*VBO_RGBA_SZ );
    int   cdet = ecol ? i : _node[i*2]; 
    const GLfloat *c = colour( cdet, col, data, cs, dop );
    buff += buff_vtx( buff, cbuf, (*_pt)[_node[i*2]], nrml, c, vreg );
    for( ; i<=stop; i++ ){
      cdet  = ecol ? i : _node[i*2+1]; 
      c     = colour( cdet, col, data, cs, dop );
      if( c[3]<opaque_min ) translucent = true;
      buff += buff_vtx( buff, cbuf, (*_pt)[_node[i*2+1]], nrml, c, vreg );
    }
    if( in_line ) ln++;
    i--;
    rd.buffer_line( cpubuff, colbuff+(ecol?VBO_RGBA_SZ:0), nseg, ecol );
  } 
  free( cpubuff );
  free( colbuff );
  rd.opaque(translucent ? 0 : -1);
}


/**
 * @brief recolour 3D lines
 *
 * @param rd     VBO for lines
 * @param col    non-datified colour
 * @param cs     colour scale
 * @param data   data
 * @param dop    data opacity
 * @param cmode  colour mode
 */
void
Connection::recolour( RenderLines& rd, const GLfloat *col, Colourscale* cs,
                             DATA_TYPE *data, dataOpac* dop, char cmode )
{
  SHORTVEC(nrml, 0., 0, 1.);
  int        ln          = 0;
  GLubyte   *cpubuff     = NULL;
  bool       translucent = false;
  GLuint     glob_nvtx   = 0;
  bool       facet       = cmode&(CM_ELEM+CM_COL_ELEM);
    
  std::vector<DATA_TYPE>eledata;
  if( cmode&CM_COL_ELEM ) 
    data = dat_to_elem( eledata, data);

  for( int i=0; i<num(); i++ ) {
    bool in_line     = ln<_lines.size() && i==_lines[ln].first;
    int stop         = in_line ? _lines[ln].second : i;
    int nseg         = stop - i + 1;
    GLubyte *buff    = cpubuff = (GLubyte *)realloc( cpubuff, (nseg+1)*VBO_RGBA_SZ );
    int   cdet       = facet? i : _node[i*2]; 
    const GLfloat *c = colour( cdet, col, data, cs, dop );
    GLubyte bcol[4];
    memcpy( buff, float2ubyte( c, bcol ), VBO_RGBA_SZ );
    buff += 4;
    for( ; i<=stop; i++ ){
      cdet  = facet ? i : _node[i*2+1]; 
      c     = colour( cdet, col, data, cs, dop );
      if( c[3]<opaque_min ) translucent = true;
      memcpy( buff, float2ubyte( c, bcol ), VBO_RGBA_SZ );
      buff += 4;
    }
    glob_nvtx += rd.recolour_line( cpubuff+(facet?VBO_RGBA_SZ:0), nseg, facet, glob_nvtx, ln );
    if( in_line ) ln++;
    i--; // was stop+1
  } 
  free( cpubuff );
  rd.opaque(translucent ? 0 : -1);
}


/** add a list of connections
 *  \param n  number of connections
 *  \param nl node list [c0_0, c0_1, c1_0, c1_1, c2_0, etc]
 */
void Connection:: add( int n, int *nl )
{
  _n += n;
  _node=(int*)realloc(_node, _n*_ptsPerObj*sizeof(int));
  memcpy( _node+(_n-n)*_ptsPerObj, nl, n*sizeof(int)*_ptsPerObj );
}


/** read in the connection file */
bool Connection::read( const char *fname )
{
  gzFile in;

  try {
    in = openFile( fname, ".cnnx" );
  } catch (...) { return false; }

  const int bufsize=1024;
  char      buff[bufsize];
  if ( gzgets(in, buff, bufsize) == Z_NULL ) return false;
  if ( sscanf( buff, "%d", &_n ) != 1 ) return false;
  _node = (int*)malloc(2*_n*sizeof(int));
  int  start_ln = -1;
  int  region   = -1;
  bool reg_read = false;
  int  prev_reg, last_i;
  int  nseg = 1;
  for ( int i=0; i<2*_n; i+=2 ) {
    prev_reg = region;
    gzgets(in, buff, bufsize);
    if(sscanf(buff, "%d %d %d", _node+i, _node+i+1, &region)!=3)
      region = -1;
    else {
      if( i && region==prev_reg ) 
        nseg++;
      else {
        // check for 1 segment cable
        if( i && nseg==1 ) _lines.push_back( std::make_pair(last_i, last_i) );
        nseg = 1;
      }
    }
    if( start_ln>=0 && (region!=prev_reg || _node[i]!=_node[i-1]) ){
      _lines.push_back( std::make_pair(start_ln, last_i) );
      start_ln = -1;
    } else if(start_ln<0 && i && _node[i]==_node[i-1] && region==prev_reg)
      start_ln =  last_i;
    reg_read = true;
    last_i = i/2;
  } 
  if( start_ln>=0 ) _lines.push_back( std::make_pair(start_ln, _n-1) );
  gzclose(in);
  //if( !reg_read ) make_lines();

  return true;
}


/** read in the connection file */
bool Connection::read_cables( const char *fname )
{
  gzFile in;

  try {
    in = openFile( fname, ".cables" );
  } catch (...) { return false; }

  std::cerr << "cable files are ignored, specify cable numbers" << std::endl;
  return false;

  const int bufsize=1024;
  char      buff[bufsize];
  if ( gzgets(in, buff, bufsize) == Z_NULL ) return false;
  int num;
  if ( sscanf( buff, "%d", &num ) != 1 ) return false;

  std::vector<int>node;
  gzgets(in, buff, bufsize);
  int start, end;
  int c0 = 0;
  sscanf( buff, "%d", &start );
  for ( int i=0; i<num; i++ ) {
    gzgets(in, buff, bufsize);
    sscanf( buff, "%d", &end );
    for( int j=start; j<end-1; j++ ) {
      node.push_back(j);
      node.push_back(j+1);
    }
    _lines.push_back(std::make_pair(_n+c0,_n+node.size()/2));
    start = end;
    c0 = node.size()/2+1;
  }
  gzclose(in);
  add( node.size()/2, node.data() );
  return true;
}

/**
 * @brief  read in the line file with the format:
 *
 * #lines
 * cnnx_start0 cnnx_end0
 * cnnx_start1 cnnx_end1
 *
 * cnnx_startN cnnx_endN
 *
 * @param fname   basename
 * @param replace replace existing lines?
 *
 * @return true iff file read 
 *
 * @note lines replaced if specified and #lines read in from file
 */
bool Connection::read_lines( const char *fname, bool replace )
{
  gzFile in;

  try {
    in = openFile( fname, ".lines" );
  } catch (...) { return false; }

  const int bufsize=1024;
  char      buff[bufsize];
  if ( gzgets(in, buff, bufsize) == Z_NULL ) return false;
  int num;
  if ( sscanf( buff, "%d", &num ) != 1 ) return false;

  if( replace ) _lines.clear();

  for ( int i=0; i<num; i++ ) {
    gzgets(in, buff, bufsize);
    int start, end;
    sscanf( buff, "%d %d", &start, &end );
    _lines.push_back(std::make_pair(start,end) );
  }
  gzclose(in);
  return true;
}


/** construct lines based on connections */
    void
Connection:: make_lines()
{
  int start_ln = -1;

  for( int i=0; i<2*_n; i+=2 ) {
    if( start_ln>=0 && i && _node[i]!=_node[i-1] ){
      _lines.push_back( std::make_pair(start_ln, i/2-1) );
      start_ln = -1;
    } else if( start_ln<0 && i && _node[i]==_node[i-1] )
      start_ln =  i/2-1;
  }
  if( start_ln>=0 ) _lines.push_back( std::make_pair(start_ln, _n-1));
}


#ifdef USE_HDF5
bool Connection::read(hid_t hdf_file)
{
  ch5_dataset dset_info;
  if( ch5m_conn_get_info(hdf_file, &dset_info) )
    return false;
  
  _n = dset_info.count;
  _node = (int*)malloc(_n * dset_info.width*sizeof(int));
  if ( ch5m_conn_get_all(hdf_file, _node) ){
    std::cerr << "Error reading in connections" << std::endl;
    _n = 0;
    delete _node;
    return false;
  }
  
  return true;
}
#endif
