#include "DrawingObjects.h"
#include "VecData.h"
#include "gzFileBuffer.h"
#include "AsciiReader.hpp"
#include <DataOnDemand.h>
#include<cstdio>
#ifdef USE_HDF5
#include <ch5/ch5.h>
#endif

/** buffer many PPoints
 *
 *  \param rd       render VBO object
 *  \param p0       first index of point to draw
 *  \param p1       last index of point to draw
 *  \param colour   colour to use if no data
 *  \param data     data associated with nodes (NULL for no data display)
 *  \param stride   draw every n'th point
 *
 *  \return number of visible nodes
 */
int 
PPoint::buffer( RenderSpheres &rd, int p0, int p1, const GLfloat *defcol, Colourscale* cs,
                  DATA_TYPE* data, int stride, dataOpac* dataopac )
{
  if ( p0>=_n || p1>=_n ) return 0;

  int nvis=0;
  for( int i=p0; i<=p1; i++ ) if( _visible[i] ) nvis++;
  GLfloat *buff = rd.buff_append( nvis );

  int current=0;
#pragma omp parallel for
  for ( int i=p0; i<=p1; i+=stride ) 
    if ( _visible[i] ) {
      int buffent;
#pragma omp critical
      buffent = current++;
      rd.buffer_sphere(buff, buffent, _pts+3*i, colour(i,defcol,data,cs,dataopac), 0 );
    }
  return nvis;
}


/** read in the point file */
bool PPoint :: read( const char *fname )
{
  std::string fn(fname);

  if( fn.rfind(".bpts") != std::string::npos )
    readBinary( fname );
  else if( fn.rfind(".pts") != std::string::npos )
    readASCII( fname );
  else {
     if( fn.back() != '.' ) fn += ".";
     try {
       fn += "bpts";
       readBinary( fn.c_str() );
     }
     catch(...){
       fn.resize(fn.size() - 4);
       fn += "pts";
       readASCII( fn.c_str() );
     }
  }

  for( int i=0; i<3*_n; i+=3 ) {
    for ( int ti=0; ti<3; ti++ ) {
      if ( !i || _pts[i+ti]>_max[ti] ) _max[ti] = _pts[i+ti];
      if ( !i || _pts[i+ti]<_min[ti] ) _min[ti] = _pts[i+ti];
    }
  }
  
  // centre the model about the origin and find maximum distance from origin
  for ( int ti=0; ti<3; ti++ ) _offset[ti] = (_min[ti]+_max[ti])/2;

  return true;
}


bool PPoint :: readBinary( const char* fname )
{
  FILE* pts_file = fopen(fname, "r" );

  if(pts_file == NULL) throw 3;

  unsigned long int numpts;
  char header[BIN_HDR_SIZE] = {};
  fread(header, sizeof(char), BIN_HDR_SIZE, pts_file);

  sscanf(header, "%d", &_n);
  _pts = (GLfloat *)malloc(_n*3*sizeof(GLfloat));

  for(unsigned long int i=0; i<_n; i++)
    if( fread(_pts+i*3, sizeof(GLfloat), 3, pts_file) < 3 )
      throw -3;

  fclose(pts_file);
  
  return true;
}


bool PPoint :: readASCII( const char* fn )
{
  std::string file(fn);

  auto pos = file.rfind(".pts");
  if( pos==std::string::npos || pos != file.size()-4 ) {
    if( file.back() != '.' ) file += ".";
    file += "pts";
  }

  AsciiReader asciirdr(file,3);

  if ( sscanf( asciirdr.peek(0), "%d", &_n ) != 1 ) throw 2;

  if ( _base1 ) _n++;					// add initial bogus point
  _pts = (GLfloat *)malloc(_n*3*sizeof(GLfloat));

  GLfloat min[3], max[3];

#pragma omp parallel for 
  for ( int i=0; i<3*_n; i+=3 ) {

    if ( i==3 && _base1 ) {				// copy the first point
      for ( int j=0; j<3; j++ )
        _pts[i+j] = _pts[i+j-3];
      continue;
    }

    if(sscanf( asciirdr.peek( i/3+1 ), "%f %f %f", _pts+i, _pts+i+1, _pts+i+2 )<3)
      throw 3;
  }

  return true;
}


#ifdef USE_HDF5
bool PPoint :: read(hid_t hdf_file)
{
  ch5_dataset info;
  if (ch5m_pnts_get_info(hdf_file, &info)) return false;
  
  _pts = (GLfloat*) malloc(info.count * info.width * sizeof(GLfloat));
  _n   = info.count;
  if (ch5m_pnts_get_all(hdf_file, _pts)) {
    free(_pts);
    return false;
  }
  
  // Centre model on origin
  for (int i = 0; i < _n * 3; i += 3) {
    for (int j = 0; j < 3; j++) {
      if (i == 0 || _pts[i+j] > _max[j]) _max[j] = _pts[i+j];
      if (i == 0 || _pts[i+j] < _min[j]) _min[j] = _pts[i+j];
    }
  }
  for (int i = 0; i < 3; i++) _offset[i] = (_min[i]+_max[i])/2;
  
  
  return true;
}
#endif


/** reallocate for more points and set if specified 
 *
 *  \param n number to add
 *  \param p points stored in array as x1 y1 z1 x2 y2 z2 ... xn yn zn
 */
void
PPoint :: add( const GLfloat *p, int n )
{
  _n += n;
  _pts = (GLfloat *)realloc( _pts, _n*3*sizeof(GLfloat) );
  _visible.resize(_n);
  
  if( !p ) return;

  memcpy( _pts+3*(_n-n), p, n*3*sizeof(GLfloat) );
  
  // Centre model on origin
  for (int i = (_n-n)*3; i < _n * 3; i += 3) {
    for (int j = 0; j < 3; j++) {
      if (i == 0 || _pts[i+j] > _max[j]) _max[j] = _pts[i+j];
      if (i == 0 || _pts[i+j] < _min[j]) _min[j] = _pts[i+j];
    }
  }
  for (int i = 0; i < 3; i++) _offset[i] = (_min[i]+_max[i])/2;

  setVis( true );
}


/** make the points dynamic 
 *
 *  \param fn  the name of the dynamic points file
 *  \param ntm the number of times currently loaded
 *
 *  \retval 0 success
 */
int
PPoint :: dynamic( const char *fn, int ntm )
{
  DataOnDemand<float>* newDynPt = new DataOnDemand<float>( fn, _n );

  if( ntm==1 || ntm==0 || ( newDynPt->max_tm()+1==ntm ) ) {
    delete _dynPt;
    _dynPt = newDynPt; 
    return 0;
  } else 
    throw FrameMismatch( ntm, newDynPt->max_tm()+1 );
}


/** set points to the current time 
 *
 *  \param a the time
 *
 *  \note if the requested time is greater than the maximum and the 
 *        maximum is greater than 0, it is an error
 */
void PPoint :: time( int a )
{
  if( _dynPt==NULL || a==_tm || a>_dynPt->max_tm() ) return;

  if( (_pts = _dynPt->slice( a )) ) 
    _tm = a;
}
