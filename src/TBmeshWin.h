#ifndef TBmeshWin_h
#define TBmeshWin_h

enum GridType { ScalarDataGrid, VecDataGrid, AuxDataGrid, DynPtGrid, TmAnnotate, NoDataGrid };

#include <string>
#include <cstring>
#include <vector>
#include <set>
#include <zlib.h>
#include "AuxGrid.h"
#include "trimesh.h"
#include "ClipPlane.h"
#include "DataAllInMem.h"
#include "ThreadedData.h"
#include "isosurf.h"
#include "IsoSurface.h"
#include "IsoLines.h"
#include "TimeLink.h"
#include "DeadDataGUI.h"
#include "Frame.h"
#include "CS_Choice.h"
#include "select_util.h"
#include "ColBarGUI.h"
#include "ScaleBar.h"

#ifdef USE_HDF5
#include "hdf5.h"
#endif

#define RENDER    1
#define PICK_VTX  2
#define PICK_AUX  6
#define MV_AXES   8
#define SEL_AUX(A) (A&4)
#define SELECT(A)  (A&2)


// flags for displaying data = 2^(Object_t)
const unsigned int Vertex_flg   = 1<<Vertex;
const unsigned int Cable_flg    = 1<<Cable;
const unsigned int Cnnx_flg     = 1<<Cnnx;
const unsigned int SurfEdge_flg = 1<<SurfEle;
const unsigned int Surface_flg  = 1<<Surface;
const unsigned int VolEle_flg   = 1<<VolEle;
const unsigned int ObjFlg[] = { Vertex_flg, Cable_flg, Cnnx_flg, SurfEdge_flg, 
                                                      Surface_flg, VolEle_flg };

class Sequence;
class HiLiteInfoWin;
class Controls;
class DataOpacity;
class ClipPlane;
class IsosurfControl;
class PlotWin;
class AuxGrid;

class TBmeshWin:public Fl_Gl_Tb_Window
{
  private:
    HiLiteInfoWin* hinfo;			     // window with highlight details
    bool           fill_assc_obj = true; // fill associated object
    Fl_Window*     flwin;
    std::string    flwintitle;
  public:
    Controls*      contwin;
    TBmeshWin(int x, int y, int w, int h, const char *l = 0);
    virtual ~TBmeshWin();

    Model*         model;
    virtual void draw();
    virtual int handle( int );
    void read_model(Fl_Window*, const char *fn, bool no_elems, bool base1=false);
#ifdef USE_HDF5
    void read_model(Fl_Window*, hid_t hdf_file, bool no_elems, bool base1=false);
#endif
    void highlight(Object_t obj, int a, bool=false);
    inline void set_hilight( bool a ){ hilighton=a; redraw(a?VBO_Hilight:VBO_None); }
    inline void dispmode( Display_t d ){ disp = d; redraw(); }
    inline Display_t dispmode(void){ return disp; }
    void showobj( Object_t o, bool *r, bool a ){model->showobj(o,r,a);redraw(VBO_Visible);}
    inline bool showobj( Object_t o, int s ){ return model->showobj(o,s);}
    GLfloat* get_color( Object_t obj, int s );
    void set_color( Object_t, int, float, float, float, float );
    void visibility( bool *, bool a );
    inline bool visibility( int s ){return model->visibility(s);}
    void opacity( int s, float opac );
    inline GLfloat opacity( int s ){return model->opacity(s);}
    inline unsigned int datify(){ return datadst; }
    inline void datify( Object_t obj ){ if(obj==All)datadst=(1<<maxobject)-1;
                                else datadst |= ObjFlg[obj]; redraw(VBO_Colour); }
    inline void undatify( Object_t obj ){if(obj==All)datadst=0;else datadst&=~ObjFlg[obj];redraw(VBO_Colour);}
    Colourscale *cs;
    int add_surface( const char * );   	// add a surface
    int get_data( const char *, Myslider* mslider=NULL );	// get data file
    inline void bgd( float w ){bc[0]=bc[1]=bc[2]=w;valid(0);redraw();}
    const GLfloat* bgd(){return bc;}
    void optimize_cs( bool update=false );		// calibrate colour scale
    void randomize_color( Object_t );	// randomize surface colours
    float get_maxdim(void){ return model->maxdim(); } //!< model maximum dimension
    float screen_dim(void){ return model->maxdim()/trackball.GetScale(); }  //!< final screen dimension
    float asp_xf(){return _aspect_xf;}
    float asp_yf(){return _aspect_yf;}
    inline double vertex_val( int a ){ if (have_data!=NoData) return data[a]; 
                                                               else return 0;}
    inline void  assc_obj( Object_t o, bool fo=false )  { vert_asc_obj=o; 
                                           fill_assc_obj=fo; redraw(VBO_Hilight); }
    inline int reg_first( int s, Object_t t ) { return  model->reg_first( s,t ); }
    void hiliteinfo();
    void select_hi( int );
    void controlwin( Controls *c ){ contwin = c; }
    void solid_hitet( bool a ){ fill_hitet = a; }
    void stride( Object_t t, int s ){ if (s) {model->stride(t, s);redraw();} }
    int  stride( Object_t t ){ return model->stride(t); }
    void output_png( const char *, Sequence *sq=NULL, bool=false );
    void output_pdf( char *fn, bool PS );
    void animate_delay(float a){ frame_delay = a; }
    void animate_skip( int a, void *, bool = false );
    bool set_time(int a);
    int  time(){return tm;}
    void autocolour( bool a ){autocol = a;}
    friend void animate_cb( void *v );
    DataOpacity *dataopac;			// data opacity
    ClipPlane* cplane;
    int       getVecData(void *, const char *vptsfile=NULL);
    VecData  *vecdata = NULL;
    void      select_vertex();
    PlotWin  *timeplotter;
    void      timeplot();
    bool      recording = false;			   // true if recording events
    void      record_events( char * ); // output the frame buffer after each change
    void      dump_vertices(const char *);
    void      compute_normals();
    void      region_vis( int *, int, bool *);
    friend    class Sequence;
    void      facetshade( bool a ){ facetshading=a;valid(0);redraw(VBO_Visible); }
    void      elem_color( bool a ){ col_by_elem=a;valid(0);redraw(VBO_Visible); }
    void      cull( bool c, int b=0 ){backface_culling=c;lit_bf=b;illuminate(lightson);valid(0);redraw(VBO_Lighting);}
    void      headlamp( bool a ){ headlamp_mode=a; redraw(); }
    void      surfVis( std::vector<int>&, bool );
    void      surfFilled( std::vector<int>&, bool );
    void      surfOutline( std::vector<int>&, bool );
    void      surfOutColor( std::vector<int>&, GLfloat * );
    void      surfFillColor( std::vector<int>&, GLfloat *);
    void      determine_cutplane( int cp );
    void      cutplane_color( GLfloat *cpc ){memcpy(_cutplane_col,cpc,4*sizeof(GLfloat) );redraw(VBO_Clip_Color);}
    void      threeD( Object_t o, int r, bool b ){model->threeD(o,r,b); 
                           if(o==Cnnx)redraw(VBO_Cnnx|VBO_Hilight);else if(o==Vertex)redraw(VBO_Pt_Color|VBO_Hilight);else redraw();}
    bool      threeD( Object_t o, int r=0 ){ return model->threeD(o,r); }
    void      size( Object_t o, int r, float b ){model->size(o,r,b); 
                           if(o==Cnnx)redraw(VBO_Cnnx|VBO_Hilight);else if(o==Vertex)redraw(VBO_Pt|VBO_Hilight);else redraw();}
    float     size( Object_t o, int r ){ return model->size(o,r); }
    IsosurfControl *isosurfwin;
    int       readAuxGrid( void *, const char* agfile, bool fake=false );
    AuxGrid  *auxGrid = NULL;
    TimeLink *tmLink  = NULL;
    void      signal_links( int );
    void      transBgd( bool a ){ bgd_trans=a;valid(0); }
    bool      transBgd( ){ return bgd_trans; }
    void      norot(bool a){_norot=a;}
    void      CheckMessageQueue();
    int       ProcessLinkMessage(const LinkMessage::CmdMsg& msg);
    void      SendViewportSyncMessage();
    void      SendTimeSyncMessage();
    void      SendColourSyncMessage();
    void      SendClipSyncMessage();
    void      SendWinszSyncMessage();
    void      SendTmPlotSyncMessage();
    void      SendHiliteSyncMessage();
    int       read_dynamic_pts( const char *, Myslider * );
    bool      compat_tm( int t, GridType gt=NoDataGrid ){ return (t==1 || max_time(gt)==0 || max_time(gt)==t-1); }
    int       max_time( GridType g=NoDataGrid );
    void      branch_cut(double,double,float);
    void      ctr_on_vtx( int vtx );
    bool      anim_loop = false;
    void      axes(bool b){_axes=b; redraw(VBO_Axes); }
    void      axes( std::ostream& os ) { os << _axsz << " " << _axx << " " << _axy; }
    void      axes( bool v, float s, float x, float y ){ _axes=v; _axsz=s; _axx=x; _axy=y;redraw(VBO_Axes); }
    void      axis_move( bool b ){ if(b)renderMode|=MV_AXES;else renderMode&=(~0^MV_AXES); }
    void      saveAux( char *fn, int s );
    void      mk_tmlink(){if(!tmLink)tmLink = new TimeLink(timeLinks);}
    void      redraw( unsigned int s ){ sully(s); redraw(); }
    void      illuminate(bool on=true);	 		 // light the model
    bool      illuminated(void){return lightson;} // model lit?
    void      draw_all_pts( bool a ){ _draw_all_vert=a; redraw(VBO_Pt); }
    void      cs_adj(CS_Choice *, const char *);
    void      offscreen( bool b ){_offscreen=b;}
    void      surfclip( int s, int c, bool v );
    GLint     surfclip( int s ){ return _rd_op.clip_obey(s); }
    void      clip_cut_surf( bool v ){_clip_cut_surface=v;}
    void *    context();
    void      context( void *c );
    void      cut_surf_light( GLfloat a ){for(int i=0;i<NUM_CP;i++)
                                             if(_cutsurface[i])_cutsurface[i]->light(a);}
    void      win_sz( int w, int h ){ Fl_Gl_Tb_Window::size(w,h); }
    void      get_win_sz( int &w, int &h ){ w=Fl_Gl_Tb_Window::w();h=Fl_Gl_Tb_Window::h(); }
    ColBarGUI *cbgui = NULL;
    const DATA_TYPE* currdata(int &n){
                 if(dataBuffer){n=dataBuffer->slice_sz();return (const DATA_TYPE*)data;}else{n=0;return NULL;} }
    void      select_aux(bool b){ renderMode=(b?PICK_AUX:RENDER); } //!< select aux vtx mode
    void      clear_grid(GridType);
    void      read_tm_annos( const char* );
    int       tm_annot(int i,double &d){if(_tm_annot.size()){d=_tm_annot[i];return 0;}else return 1.;}
    friend class Controls;
    friend class HDF5DataBrowser;
    friend class Frame;
    friend class ObjProps;
    friend class colourChoice;
    friend class IsosurfControl;
    friend class PlotWin;
    using Fl_Gl_Tb_Window::redraw; 
  private:
    int        hilight[maxobject];	// which object to highlight
    bool	   hilighton = false;	// whether to highlight
    Display_t  disp = asSurface;	// type of display
    unsigned int   datadst;			// which object gets data coloured
    GLfloat tet_color[4]     = {1.,1.,1.,1.},
            hitet_color[4]   = {1.,0.,0.,1.},
            hiele_color[4]   = {0.,0.,1.,1.},
            hicable_color[4] = {0.,1.,1.,1.},
            hicnnx_color[4]  = {1.,1.,0.,1.}, 
            hipt_color[4]    = {1.,0.,1.,1.},
            hiptobj_color[4] = {1.,0.8,0.,1.},
            bc[4];
    DataClass<DATA_TYPE>*  dataBuffer = NULL;
    DATA_TYPE* data = NULL;			    // data to display
    Data_Type_t  have_data = NoData;    // true if data file has been read
    GLfloat*   colourize( float );	    // set GL colour for data value
    Object_t   vert_asc_obj = SurfEle;	// object to draw with vertex
    double     solid_angle3Dpt( int v, int a, int b, int c );
    bool	   fill_hitet =  false;     // true to fill hilighted tetrahedron
    float      frame_delay = 0.01;      // delay between frames
    int        frame_skip = 0;     	    // direction and #frames to skip
    int        tm = 0;
    int        numframes = 0;
    bool	   autocol = false;		// set colour range every frame

    bool	  lightson = true;				// light or not
    bool      draw_surface(Surfaces *, GLfloat *&, GLubyte *&, bool, bool );
    void      draw_surfaces(RenderTris &rd, std::vector<vtx_z> &trans, bool );
    void      draw_sorted_elements( RenderTris &, std::vector<vtx_z> &elems, bool, bool, bool );
    void      sully( unsigned long s ) { 
                           if(s&VBO_Time) { _redraw_state |= VBO_Colour|VBO_Isoline;
                                            _redraw_state |= VBO_Aux| VBO_Vector|VBO_Hilight;
                                            if(model->pt.dynamic()){_redraw_state|=VBO_Position;}}
                            else _redraw_state |= (Sullied)s; }
    void      draw_cnnx(RRegion *);
    void      draw_vertices(RRegion *);
    void      draw_axes();
    void      draw_highlights();
    void      draw_clip_planes();
    void      draw_cut_planes( );
    void      draw_iso_surfaces( );
    void      draw_iso_lines();
    void      draw_cbar();
    void      trim_vertices( std::vector<bool>&, bool );
    bool      translucency( Object_t, GLfloat *, bool=false ); 
    void      draw_vol_mesh();            // !< draw volume mesh

    GLenum    renderMode;                 // mode for drawing
    void      register_vertex( int i );   // for picking
    bool      process_hits(Frame &);	  // determine picked node
    DATA_TYPE* timevec = NULL;
    unsigned long framenum=0;			  // keep track if a frame is drawn
    bool      dump_vert_list = false;
    DataReaderEnum getReaderType( const char * );
    bool      facetshading = false;	// do not blend normal over surface elements
    bool      col_by_elem = false;	// do not blend colour over surface elements
    bool      backface_culling = false;	// do not cull CW elements
    int       lit_bf = 0; // light backface?
    bool      headlamp_mode = true;	// headlamp lighting mode - light in eye coords
    CutSurfaces **_cutsurface;      // clipped surfaces
    IsoLine    *isoline = NULL;
    std::set<int>   timeLinks;           // other processes linked to this one
    bool       bgd_trans = false;   //!< transparent background
    bool       _norot = false;      //!< allow rotations
    float      _dt;                 //!< increment between time slices
    void       set_windows( Fl_Window* );
    bool       _spinning=false;     //!< are we still spinning
    // constants
    static unsigned int MAX_MESSAGES_READ;
    bool           _branch_cut = false;
    double         _branch_range[2];
    DeadDataGUI   *deadData;
    bool           _axes=false;
    float          _axx=0., _axy=0, _axsz=1.,_axdx=0.02,_axds=0.02;  // !< axes position and size
    bool           _eleBasedData=false;
    std::vector<vtx_z>  _trans_elems;
    std::vector<vtx_z>  _trans_wf;
    unsigned int   _redraw_state = VBO_All;
    GLfloat        _cutplane_col[4] = {1.,0,0,1.};
    RenderTris     _rd_op;                       //!< filled elements
    RenderTris     _rd_wf;                       //!< element outlines
    RenderLines    _rd_cnnx;                     //!< connections
    RenderSpheres  _rd_pt;                       //!< vertices
    RenderHilights _rd_hi;                       //!< highlights
    SelectVtx      _select;                      //!< select vertices
    bool           _draw_all_vert = false;       //!< draw all vertices for each regions
    bool           _offscreen = false;           //!< drawing into offscreen buffer?
    bool           _clip_cut_surface = true;     //!< clip cutting surfaces
    ScaleBar      *_scalebar = NULL;             //!< scale bar
    float          _aspect_xf = 1.;              //!< x scaling due to window aspect ratio
    float          _aspect_yf = 1.;              //!< y scaling due to window aspect ratio
    std::vector<double> _tm_annot;               //!< time annotations
};

#include "DataOpacity.h"
#endif
