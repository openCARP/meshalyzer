#include "DrawingObjects.h"

const int pyr_num_edge=8;
static int pyr_edges[pyr_num_edge][2] =
  {
    {0,1},{1,2},{2,3},{3,0},{0,4},{1,4},{2,4},{3,4}
  };
static const int pyramid_iso_table[][15] = {
    { 0 },
    { 1, 3, 0, 1, 0, 3, 0, 4 }, 
    { 1, 3, 1, 0, 1, 2, 1, 4 },
    { 1, 4, 0, 4, 4, 1, 1, 2, 0, 3 },
    { 1, 3, 1, 2, 2, 3, 2, 4 },
    { 1, 4, 0, 4, 4, 2, 2, 3, 3, 0 },
    { 1, 4, 1, 4, 1, 0, 2, 3, 2, 4 },
    { 1, 5, 2, 4, 1, 4, 0, 4, 0, 3, 3, 2 },
    { 1, 3, 2, 3, 3, 0, 3, 4 },
    { 1, 4, 1, 0, 0, 4, 4, 3, 3, 2 },
    { 2, 3, 0, 1, 1, 2, 1, 4, 3, 0, 3, 3, 4, 2, 3 },
    { 1, 5, 1, 4, 4, 0, 4, 3, 3, 2, 2, 1 },
    { 1, 4, 1, 2, 2, 4, 4, 3, 3, 0 },
    { 1, 5, 2, 4, 4, 3, 0, 4, 0, 1, 1, 2 },
    { 1, 5, 0, 1, 1, 4, 4, 2, 4, 3, 0, 3 },
    { 1, 4, 0, 4, 4, 1, 4, 2, 4, 3 },   
    { 1, 4, 0, 4, 4, 1, 4, 2, 4, 3 },
    { 1, 5, 0, 1, 1, 4, 4, 2, 4, 3, 0, 3 },
    { 1, 5, 2, 4, 4, 3, 0, 4, 0, 1, 1, 2 },
    { 1, 4, 1, 2, 2, 4, 4, 3, 3, 0 },
    { 1, 5, 1, 4, 4, 0, 4, 3, 3, 2, 2, 1 },
    { 2, 3, 0, 1, 1, 2, 4, 1, 3, 0, 3, 3, 4, 2, 3 },
    { 1, 4, 1, 0, 0, 4, 4, 3, 3, 2 },
    { 1, 3, 2, 3, 3, 0, 3, 4 },
    { 1, 5, 2, 4, 1, 4, 0, 4, 0, 3, 3, 2 },
    { 1, 4, 1, 4, 1, 0, 2, 3, 2, 4 },
    { 1, 4, 0, 4, 4, 2, 2, 3, 3, 0 },
    { 1, 3, 1, 2, 2, 3, 2, 4 },
    { 1, 4, 0, 4, 4, 1, 1, 2, 0, 3 },
    { 1, 3, 1, 0, 1, 2, 1, 4 },
    { 1, 3, 0, 1, 0, 3, 0, 4 },
    { 0 }
};
const int pyr_num_surf = 5;
const int pyr_surface_table[][4] = {
   {0,1,2,3}, {4,2,1,-1}, {4,3,2,-1}, {4,0,3,-1}, {4,1,0,-1}
};


/** read in the point file */
bool Pyramid :: read( const char *fname )
{
  gzFile in;

  try {
    in = openFile( fname, "pyr" );
  } catch (...) {return false;}

  const int bufsize=1024;
  char buff[bufsize];
  if ( gzgets(in, buff, bufsize) == Z_NULL ) return false;
  sscanf( buff, "%d", &_n );
  _node    = new int[5*_n];
  _region = new int[_n];

  for ( int i=0; i<_n; i++ ) {
    gzgets(in, buff, bufsize);
    if ( sscanf( buff, "%d %d %d %d %d %d", _node+5*i, _node+5*i+1,
                 _node+5*i+2, _node+5*i+3, _node+5*i+4, _region+i  ) < 6   ) {
      // if region not specified, assign a default of -1
      _region[i] = -1;
    }
  }
  gzclose(in);
  return true;
}


/** draw the faces of an element not contaiining a node
 *
 * \param cn do not draw faces associated with this node
 */
void Pyramid::draw_out_face( int cn )
{
#if 0
  for ( int i=0; i<_n*_ptsPerObj; i++ )
    if ( _node[i] == cn ) {
      int elenode = (i/_ptsPerObj)*_ptsPerObj;
      int nn = i%_ptsPerObj;

      if ( nn==4 ) {
        glBegin(GL_QUADS);
        glVertex3fv( _pt->pt(_node[elenode+0]) );
        glVertex3fv( _pt->pt(_node[elenode+1]) );
        glVertex3fv( _pt->pt(_node[elenode+2]) );
        glVertex3fv( _pt->pt(_node[elenode+3]) );
        glEnd();
      } else {
        glBegin(GL_TRIANGLES);
        glVertex3fv( _pt->pt(_node[elenode+(nn+3)%4]) );
        glVertex3fv( _pt->pt(_node[elenode+(nn+2)%4]) );
        glVertex3fv( _pt->pt(_node[elenode+4]) );

        glVertex3fv( _pt->pt(_node[elenode+(nn+2)%4]) );
        glVertex3fv( _pt->pt(_node[elenode+(nn+1)%4]) );
        glVertex3fv( _pt->pt(_node[elenode+4]) );
        glEnd();
      }
    }
#endif
}


/** determine if a clipping plane intersects an element.
 *
 *  \param pd     visible points
 *  \param cp     clipping plane, cp[0]x + cp[1]y +cp[2]z + cp[3] = 0
 *  \param e      the element in the list
 *  \param interp construct to interpolate data
 *
 *  \return a surface element if intersection, NULL otherwise
 *
 *  \post interp may be allocated
 */
SurfaceElement*
Pyramid::cut( char *pd, GLfloat* cp,
              Interpolator<DATA_TYPE>* &interp, int e )
{
  return planecut( pd, cp, interp, pyr_num_edge, pyr_edges, e );
}


/* return the list of intersection polygons for isosurfaces
 *
 *  \param index for each node, the bit is true if the isoval is exceeded
 *  
 * \return the row in the table
 */
const int* Pyramid::iso_polys(unsigned int index)
{
  return pyramid_iso_table[index];
}


/* 
 * return list of lists of nodes defining the bounding surfaces
 *
 * \param ft   face table
 * \param v    element number
 * \param tris return all tris
 *
 * \pre the size of ft is big enough
 */
int 
Pyramid::surfaces( int ft[][MAX_NUM_SURF_NODES+1], bool tris, int v )
{
  return make_surf_nodelist( v, ft, pyr_num_surf, 
        int(pyr_surface_table[1]-pyr_surface_table[0]), (const int **)pyr_surface_table, tris );
}


const int*
Pyramid::edges(int &n)
{
  n = pyr_num_edge;
  return (const int*)pyr_edges;
}
