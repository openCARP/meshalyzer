#ifndef DRAWGL_H
#define DRAWGL_H

#include <FL/Fl_Gl_Window.H>
#include <FL/Fl_Single_Window.H>
#include "Fl_Gl_Tb_Window.h"
#include "Myslider.h"

typedef enum disp_t { asSurface, asTetMesh, asIsosurface } Display_t;
typedef enum data_state_t { NoData, Static, Dynamic } Data_Type_t;
enum DataColouring { off, on, opacval };

#define DATA_TYPE float

static const float opaque_min = 0.95;      //!< opague if at least this
static const float translucent_min = 0.02; //!< do not draw if bbelow this

#endif
