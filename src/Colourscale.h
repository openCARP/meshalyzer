/*
 * Class for a colour map which returns an RGBA value for a data value.
 *
 * To add a new colour scale
 *
 *    - give it a unique name and add it to the \p cs_scales vector below
 *    - if it is defined as a function, code it in Colourscale::scale 
 *    - If it is a table, define it as a static 2D array (type or range do not matter)
 *      in colour_maps.h, and add it to Colourscale::scale using interp_map() 
 */
#ifndef COLOURSCALE_INC
#define COLOURSCALE_INC

#ifndef OSMESA
#  if defined(__APPLE__)
#    include <OpenGL/gl3.h> 
#  else
#    if defined(WIN32) || defined(__CYGWIN__)
#      define GLEW_STATIC 1
#    endif
#    include <GL/glew.h>
#  endif
#endif

#include <map>
#include <cmath>
#include <vector>
#include <map>
#include "PNGwrite.h"

// note that the following enumeration is held for legacy reasons and new colour scales
// will be defined solely by std::strings
typedef enum {
  CS_HOT=2, CS_GREY=4, CS_RGREY=7, CS_GGREY=8, CS_BGREY=9, CS_RAINBOW=0, CS_BL_RAINBOW=1,
  CS_COLD_HOT=3, CS_CG=5, CS_MATLAB=6, CS_ACID=10, CS_P2G=11, CS_VIRIDIS=12,
  CS_VIRIDIS_LIGHT=13, CS_MAGMA=14, CS_INFERNO=15, CS_DISTINCT=16, CS_PuOr=17, CS_TWILIGHT=18,
  CS_KINDLMANN=19, CS_MATLAB_REV=20
} CScale_t;


// define all new colour maps in this vector and DO NOT define an enum
// The order here is the order in the list.
static
std::vector<const char *> cs_scales{
  "Bl_Rainbow",
  "Turbo",
  "MATLAB",  // Jet
  "Durrer",
  //"CG",
  "Hot",
  "Kindlmann",
  "Viridis",
  "Viridis_light",
  "Magma",
  "Inferno",
  "Plasma",
  "B2G2R",
  "Grey",
  "RGrey",
  "GGrey",
  "BGrey",
  "CoolWarm",
  "RdBu",
  "Cold_Hot",
  "PuOr",
  "PiYG",
  "BrBG",
  "Pink2Green",
  "Acid",
  "Rainbow",
  "Twilight",
  "Distinct",
  "CET_I1"
};


typedef enum {
  NO_DEAD=0, DEAD_MIN=1, DEAD_MAX=2, DEAD_NaN=4
} DeadRange;

class Colourscale
{
  public:
    Colourscale(int ts, CScale_t tcs);
    Colourscale(int ts=64, std::string tcs="Bl_Rainbow");
    Colourscale( std::vector<GLfloat[3]>& m );
    void     scale( CScale_t ce );              // set scale
    void     scale( std::string ce );           // set scale
    std::string scaleStr( ) {return scaletype;} // get scale
    CScale_t scale(void){ return scaleIdx; }
    void     calibrate( double, double );                  // set range
    float    min(){return mindat;}                   // return minimum data value
    float    max(){return maxdat;}                   // return maximum data value
    int      size(){ return n; }                       // get the size
    void     size( int );                                // set the size
    template<typename T,size_t N> void interp_map(const T (&cscale)[N][3]);
    GLfloat* entry(int a){return cmap[a];}      // return an entry
    GLfloat *colorvec( double, float alpha=1 );
    inline int colorIdx( double );
    void     output_png( const char * );                 // output the map
    void     deadColour( GLfloat *, GLfloat );
    GLfloat *deadColour(){return _deadColour;}
    void     deadRange( double min, double max, bool nan, DeadRange dr );
    bool     deadRange( ){ return _deadRange!=NO_DEAD; }
    bool     deadOpaque(){  return _deadRange!=NO_DEAD && _deadColour[3]>=0.95; }
    bool     deadTrans(){   return _deadRange!=NO_DEAD && _deadColour[3]<0.95; }
    void     noDead( void ){_deadRange=NO_DEAD;};
    void     get_deadRange( double &min, double &max, DeadRange &dr )
                          { min=_deadMin;max=_deadMax;dr=_deadRange;}
    bool isDead( double val ) { return !NO_DEAD && (
             ( _deadRange&DEAD_MIN && val<_deadMin ) ||
             ( _deadRange&DEAD_MAX && val>_deadMax ) ||
             ( _deadRange&DEAD_NaN && std::isnan(val) )      );}
    bool     loaded(){ return _loaded; }
    void     load() { _loaded = true; }
    void     reverse(bool r){_rev=r;scale(scaletype);}
    template<typename T> void RGBmap( T* m );
  private:
    GLfloat** cmap;							// the map
    int       n = 0;				  	    // size of map
    double    a, b;							// map data to colour map
    std::string    scaletype;
    CScale_t  scaleIdx;
    double    mindat=0., maxdat=1.;
    DeadRange _deadRange=NO_DEAD;             // what to ignore
    double    _deadMin=0, _deadMax=1.;        // ignore below and above these values
    bool      _deadNaN=false;                 // ignore NaN's
    GLfloat   _deadColour[4]={0.,0.,0.,1.};   // how to colour dead data
    bool      _loaded=false;
    bool      _rev=false;                     // reverse the scale
};


/**
 * @brief populate colour map from discrete colour scale table
 *
 * @tparam T        type of table entries
 * @tparam N        number of entries in table (deduced)
 * @param &cscale   statically-defined colour scale table
 */
template<typename T,size_t N>
void
Colourscale:: interp_map( const T (&cscale)[N][3] )
{
  float normalize = cscale[0][0];
  for ( int i=0; i<N; i++ ) 
    for ( int j=0; j<3; j++ ) 
      if( cscale[i][j] > normalize ) 
        normalize = cscale[i][j];

  for ( int i=0; i<n; i++ ) {
    float idxf = float(i)/float(n-1)*(N-1);
    int   idx  = (int)idxf;
    float d    = idxf - idx;
    for ( int j=0; j<3; j++ ) {
      if( idx==N-1 ) 
        cmap[i][j] = (float)cscale[idx][j];
      else
        cmap[i][j] = (1.-d)*cscale[idx][j] + d*cscale[idx+1][j];
      cmap[i][j] /= normalize;
    }
  }
}



/* populate a vector with RGB values of colour map
 *
 * \param rgb vector to fill
 * \param s   number of colours 
 *
 * \pre \p rgb must be at 3*size() 
 *
 * \post \p rgb is filled with colour components. If rgb is an integral
 *       type, the range is [0,255], o.w., [0,1.]
 */
template<typename T> 
void 
Colourscale::RGBmap( T* rgb )
{
  float scale = 1.;
  if( std::is_integral<T>::value ) scale = 255;
  for( int j=0; j<n; j++ ) {
    GLfloat *col = entry(j);
    for( int k=0; k<3; k++ ) 
      rgb[3*j+k] = col[k]*scale;
  }
}

#endif

