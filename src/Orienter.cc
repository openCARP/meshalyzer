#include "Orienter.h"
#include <cmath>

Orienter::Orienter(int x, int y, int w, int h, const char *l )
    : Directionizer(x, y, w, h, l)
{
  _eyeSys = false;
  redraw();
}


void
Orienter:: draw()
{ 
  static Quaternion oldv;  //old view


  gl_settings();

  
  // need a new context when window is closed and then a new one redrawn
  if ( context_valid()==0 ) {
    if( _rd )delete _rd;
    _rd = new RenderTris(context());
    if( _rd_lines ) delete _rd_lines;
    _rd_lines = new RenderLines(context());
  }
 
  // rotate clipping plane only if the orienter was moved, and 
  // not if the model was rotated
  //
  // do not update clipping planes while moving orienter, only after stopped
  if( !trackball.transformed ){ 
    GLfloat *x = static_cast<ClipPlane*>(parent()->user_data())->plane(_cp);
    Quaternion cp_norm(0, x[0], x[1], x[2]);
    cp_norm.Normalize();
    Quaternion new_norm= trackball.qSpin*cp_norm*trackball.qSpin.GetConjugate();
    ((ClipPlane *)(parent()->user_data()))->update_dir( new_norm.x,
                         new_norm.y, new_norm.z, _cp, true );    //do not update while spinning trackball
  } else if( oldv==_view )
   ((ClipPlane *)(parent()->user_data()))->normcb( _cp, false ); //update if not triggered because model view changed
  
  trackball.Rotation(_view); // adjust for model rotation
  trackball.DoTransform(context());

  if( !_rd->inited() )
    init_graphics();
    
  _rd->mesh_render();
  _rd_lines->mesh_render();

  trackball.Rotation(_view.GetConjugate()); // remove model rotation
  oldv = _view;
}

/** work on a new clipping plane, update the direction dispalyed
 *
 * \param cp clipping plane
 */
void 
Orienter :: cp( int cp )
{
  _cp = cp;
  GLfloat *x = static_cast<ClipPlane*>(parent()->user_data())->plane(cp);
  V3f plane( x[0], x[1], x[2] );
  V3f zaxis(0.,0.,1.);
  float angle = -acos( plane.Dot(zaxis) );
  V3f rv = plane.Cross(zaxis);
  if( rv.Norm() < 0.01 ) rv = V3f(1.,0,0);
  trackball.SetRotation(angle, rv);
  parent()->redraw();
}
