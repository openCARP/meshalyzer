#include "plottingwin.h"
#include <FL/Fl_Widget.H>
#include <FL/Fl_File_Chooser.H>

/** set the data to plot
 *
 *  \param id   vertex ID
 *  \param n    number of points
 *  \param d    ordinates
 *  \param t    current time
 *  \param xd   abscissa
 *  \param torg initial time
 *  
 *  \note memory is allocated to hold the data
 */
template<class T>
void PlotWin :: set_data( int id, int n, T *d, int t, float dt, float torg,
                                                                  T *xd )
{
  xv.resize(n);
  data.resize(n);
  datasize = n;
 
  datamin = make_set( n, d, data.data(), xd, xv.data(), dt, torg );
  _id = id;
  highlight( t );
  window->label(_datafile.c_str() );
}

template void PlotWin :: set_data<float>( int id, int n, float *d, int t, float dt, float torg, float *xd );
template void PlotWin :: set_data<double>( int id, int n, double *d, int t, float dt, float torg, double *xd );

/**
 * @brief copy data into a set
 *
 * @tparam T
 * @param n    number of points
 * @param ds   ordinate to copy
 * @param dd   ordinate destination
 * @param xd   abscissa destination
 * @param dt   time increment
 * @param torg time origin
 * @param xs   abscissa source
 *
 * @pre dd and xd are allocated
 */
template<class T>
double PlotWin :: make_set( int n, T *ds, double *dd, T* xs, double *xd, float dt, float torg )
{
  double dmin;

  dmin = ds[0];
  for ( int i=0; i<n; i++ ) {
    dd[i] = ds[i];
    if ( ds[i]<dmin ) dmin = ds[i];
    xd[i] =  xs ? xs[i] : torg + i*dt;
  }
  return dmin;
}

template double PlotWin :: make_set<float>( int n, float *ds, double *dd, float* xs, double *xd, float dt, float torg );
template double PlotWin :: make_set<double>( int n, double *ds, double *dd, double* xs, double *xd, float dt, float torg );

PlotWin :: ~PlotWin ()
{
#ifdef USE_GNUPLOT
  graph->~Fl_Gnuplot();
#endif

}

void PlotWin :: highlight( int tindx=-1 )
{
  if ( !(window->visible()) ) return;
  double *x, *y;
  if ( !rotated ){
    x = xv.data();
    y = data.data();
  } else {
    x = data.data();
    y = xv.data();
  }
  double annot;
  if( mwtb && !mwtb->tm_annot( _id, annot ) )
    graph->set_2d_data(x,y,annot,datasize,0,_id,_datafile);
  else
    graph->set_2d_data(x,y,datasize,0,_id,_datafile);
  tmx[0] = tmx[1] = xv[tindx];
  tmy[0] = datamin;
  tmy[1] = data[tindx];
  if ( !rotated )
    graph->set_2d_data(tmx,tmy,2,1);
  else
    graph->set_2d_data(tmy,tmx,2,1);
#ifdef USE_GNUPLOT
  window->redraw();
#else
  graph->redraw();
#endif
}

void PlotWin :: rotate( bool a )
{
  if ( a!=rotated ) {
    rotated = a;
    graph->rotate();
    rotbut->value(a);
    window->size( window->h(), window->w() );
    highlight();
  }
}

#include <fstream>
void PlotWin :: writedata()
{
  char *ofname = fl_file_chooser( "Select output file", "*.dat", "" );
  if ( ofname == NULL ) return;
  std::ofstream of( ofname );
  for ( int i=0; i<graph->n(); i++ ) {
    if ( i==1 ) continue; //set 1 is the time highlight bar
    graph->write( of, i );
    of << std::endl;
  }
}




