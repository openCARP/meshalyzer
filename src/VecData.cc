#include "VecData.h"

#include <cstdio>
#include <cstdlib>
#include <FL/Fl_File_Chooser.H>
#include <FL/Fl_Color_Chooser.H>
#include <zlib.h>
#include <cstring>
#include <iostream>
#include <math.h>
#include "IGBheader.h"

const float vecsize          = 0.1;		//!< relative length of maximum vector by default
const int   vec_num_sides    = 6;       //!< number of sides for 3D vectors

template< class S>
void read_IGB_vec_data( S* vdata, S* sdata, IGBheader& h )
{
  S* vd=NULL;
  
  if( h.num_components() == 4 )
    vd = new S[h.slice_sz()*h.num_components()];

  int nread;

  for( int i=0; i<h.t(); i++ ){
	if( !vd ) 
	  nread = h.read_data( vdata+i*h.num_components()*h.slice_sz() );
	else {
	  nread = h.read_data( vd );
	  for( int j=0; j<h.slice_sz(); j++ ) {
		for( int k=0; k<3; k++ )
		  vdata[3*j+k+i*h.slice_sz()*3] = vd[j*4+k];
		sdata[j+i*h.slice_sz()] = vd[j*4+3];
	  }
	}
	if( nread != h.slice_sz() ) {
	  h.t(i);
	  break;
	}
  }

  if( vd ) delete[] vd;
}


/** just copy all nondata fields, the ones
 *  related to display
 *
 * \param v vector data
 */
VecData &
VecData::operator=( const VecData* v )
{
  if( this == v )
    return *this;

  _length     = v->_length;
  maxmag      = v->maxmag;
  _stride     = v->_stride;
  _length_det = v->_length_det;
  _colour_det = v->_colour_det;
  autocal     = v->autocal;
  _stoch      = v->_stoch;
  _draw_heads = v->_draw_heads;
  _3D         = v->_3D;
  memcpy( _colour, v->_colour, 4*sizeof(GLfloat) );
  cs->size(v->cs->size());

  if( !sdata ) {
    if( _length_det == Scalar ) 
      _length_det = Vector;
    if( _colour_det == Scalar ) 
      _colour_det = Vector;
  }

  return *this;
}


VecData::VecData(const char* vptfile)
{
  cs = new Colourscale;

#ifdef USE_HDF5
  if( strstr( vptfile, ".datH5:vector/" ) )
    read_vec_HDF5( vptfile );
  else
#endif
    read_vec_nonHDF5( vptfile );

  //determine the largest magnitude vector and scalar extrema
  maxmag = mag2( vdata );
  float  tmp;
  for ( int i=1; i<numpt; i++ )
    if ( (tmp=mag2( vdata+i*3 )) > maxmag )
      maxmag = tmp;
  maxmag = sqrt(maxmag);

  if ( sdata != NULL ) {
    int i=0;
    while( !isfinite(sdata[i]) ) i++;
    scalar_min = scalar_max = sdata[i];
    for ( ; i<numpt; i++ ) {
      if( !isfinite(sdata[i]) ) continue;
      if( sdata[i] > scalar_max ) scalar_max = sdata[i];
      if( sdata[i] < scalar_min ) scalar_min = sdata[i];
    }
  }
  
  optimize_cs();
  optimize_len();

  _rd.set_material(0,(const GLfloat[]){0.7, 0.5, 80, 1.} );
  _rd.ambient  = 0.35;
  _rd.diffuse  = 0.5;
  _rd.specular = 0.7;
}


#ifdef USE_HDF5
/** read HDF5 vector data 
 *
 * \param vecspec vector grid specification
 */
void
VecData::read_vec_HDF5( const char*vecspec )
{
  unsigned int    indx;
  std::string gtype;
  ch5s_vector_grid info;
  std::string vn = vecspec;
  int parse_HDF5_grid( const char*, std::string&, unsigned int& );

  hid_t hin = H5Fopen(vn.substr(0,vn.find_last_of(":")).c_str(),
                                        H5F_ACC_RDONLY, H5P_DEFAULT);
  if( hin ==H5I_INVALID_HID )
    throw 1;

  if( parse_HDF5_grid( vn.c_str(), gtype, indx ) || gtype!="vector" ||
          ch5s_vector_grid_info(hin, indx, &info ) )
    throw 1;

  numpt = info.num_vectors;
  pts   = new GLfloat[numpt*3];
  numtm = info.max_time_steps;
  ch5s_vector_read_points( hin, indx, pts );

  vdata = (float *)realloc( vdata, 3*info.num_vectors*numtm*
                                                        sizeof(float) );
  if( info.num_components==3 ) {  // no scalar data
    ch5s_vector_read( hin, indx, 0, numtm-1, vdata );

  } else {
    sdata = (float *)realloc( sdata, numpt*numtm*sizeof(float) );
    float *tdata = new float[numpt*info.num_components];
    for( int i=0; i<numtm; i++ ) {
      ch5s_vector_read( hin, indx, i, i, tdata );
      for( int j=0; j<numpt; j++ ){
        memcpy( vdata+(i*numpt+j)*3, tdata+4*j, sizeof(float)*3 );
        sdata[i*numpt+j] = tdata[4*j+3];
      }
    }
    delete[] tdata;
  }
  ch5_close( hin );
}
#endif


/** read  IGB or ASCII vector data
 *
 * \param vptfile vector points file
 */
void
VecData::read_vec_nonHDF5( const char *vptfile )
{
  const int bufsize=1024;
  char fn[1024], buff[bufsize];

  strcpy( fn, vptfile );

  // read in the vector points file (.vpts[.gz])
  gzFile in;
  if ( (in=gzopen(fn, "r" )) == NULL )
    if ( (in=gzopen(strcat(fn,".gz"),"r")) == NULL ) {
      fn[strlen(fn)-3] = '\0';
      if ( (in=gzopen(strcat(fn,".vpts"),"r")) == NULL )
        if ( (in=gzopen(strcat(fn,".gz"),"r")) == NULL ) {
          fn[strlen(fn)-7] = '\0';
          if ( (in=gzopen(strcat(fn,"vpts"),"r")) == NULL )
            if ( (in=gzopen(strcat(fn,".gz"),"r")) == NULL ) {
              std::cerr << "Error: cannot open file: " << fn << std::endl;
              throw -1;
            }
        }
    }
  _absfile = realpath( fn, NULL );

  if ( gzgets(in, buff, bufsize) == Z_NULL ) return;
  if ( sscanf( buff, "%d", &numpt ) != 1 ) return;
  pts = new GLfloat[numpt*3];
  for ( int i=0; i<3*numpt; i+=3 ) {
    if ( gzgets(in, buff, bufsize) == Z_NULL ) return;
    if ( sscanf( buff, "%f %f %f", pts+i, pts+i+1, pts+i+2 ) != 3 ) return;
  }
  gzclose( in );

  // read in the vector data (.vec[.gz])
  // first figure out the name of the data file
  char* sp=strstr( fn, "vpts.gz" );
  if ( sp == NULL ) sp = strstr( fn, "vpts" );
  *sp='\0';

  if ( (in=gzopen( strcat(fn, "vec"), "r")) == NULL )
    if ( (in=gzopen( strcat(fn, ".gz"), "r")) == NULL ) {
      std::cerr << "No data file found to match points file: " << fn << std::endl;
      throw -1;
    }

  IGBheader h(in);
  if( !h.read() ) {               // IGB file
	vdata = (float *)realloc( vdata, 3*h.t()*h.slice_sz()*sizeof(float) );
    if( h.type() == IGB_VEC4_f || h.type() == IGB_VEC4_d ) {  // scalar data
      sdata = (float *)realloc( sdata, h.t()*h.slice_sz()*sizeof(float) );
	}
	read_IGB_vec_data( vdata, sdata, h );
	numtm = h.t();

  } else {                           // text file
	gzrewind(in);
	// determine if scalar data
	if ( gzgets( in, buff, bufsize ) == Z_NULL ) return;
	float tscal[4];
	int npl = sscanf(buff,"%f %f %f %f", tscal, tscal+1, tscal+2, tscal+3 );
	const char *scanstr;
	if ( npl==4 )
	  scanstr="%f %f %f %f";
	else if ( npl==3 )
	  scanstr="%f %f %f";
	else
	  throw -1;
	gzrewind( in );

	int nread;
	do {
	  vdata = (float *)realloc( vdata, 3*(++numtm)*numpt*sizeof(float) );
	  if ( npl==4 ) sdata = (float *)realloc( sdata, numtm*numpt*sizeof(float) );
	  float* vdp = vdata + 3*(numtm-1)*numpt;
	  float* sdp = sdata + (numtm-1)*numpt;
	  for ( nread=0; nread<numpt; nread++ ) {
		if ( gzgets( in, buff, bufsize ) == Z_NULL ) break;
		if ( sscanf( buff, scanstr, vdp, vdp+1, vdp+2, sdp ) != npl ) break;
		vdp += 3;
		sdp++;
	  }
	} while ( nread == numpt );
	numtm--;
	if ( !numtm ) {
	  delete[] pts;
	  std::cerr << "Not enough data in file" << std::endl;
	  throw -1;
	}
  }
}


VecData::~VecData()
{
  if ( pts ) delete[] pts;
  if ( vdata != NULL ) {free( vdata ); vdata = NULL; }
  if ( sdata != NULL ) {free( sdata ); sdata = NULL; }
}


/*
 * choose the colour to draw the vectors
 */
void
VecData::colourize()
{
  double r=_colour[0],g=_colour[1],b=_colour[2];
  if ( fl_color_chooser( "Vector colour", r, g, b ) ) {
    _colour[0] = r;
    _colour[1] = g;
    _colour[2] = b;
  }
}


/**
 * @brief  draw the vectors
 *
 * @param tm           time to draw
 * @param maxdim       maximum model dimension
 * @param redraw_state reason for redraw
 * @context            OpenGL context
 */
void
VecData::draw(int tm, float maxdim, unsigned int redraw_state, void *context)
{
  if ( !_disp ) return;
  if ( tm >= numtm ) tm=numtm-1;							// for now

  bool refill_buff = _rd.setup_render_data(context) || tm!=_last_tm ||
                                                redraw_state&VBO_Vector;
  if( !refill_buff ) {
    _rd.mesh_render();
    return;
  }

  _last_tm = tm;

  if( autocal ) {
    optimize_len();
    optimize_cs();
  }

  int numdrawn   =  0;
  if( maxmag==0 && _length_det!=FixedVCdata ) return;

  int    offset = tm*numpt;
  float* vdp    = vdata + 3*offset;
  float* sdp    = sdata + offset;

  // determine vector length
  float base_size = maxdim*vecsize*_length/2.;
  float maxvec    = base_size;
  if ( _length_det == Vector )
    base_size /= maxmag;
  else if ( _length_det == Scalar && sdata != NULL )
    base_size /= maxmag; 

  _rd.sides( _3D?vec_num_sides:0, (_3D&&_draw_heads)?vec_num_sides*3*3:0 );
  int nvtx       = _3D ? arrow3D_nvtx( vec_num_sides, _draw_heads, !_draw_heads ) : 2;
  int narrows    = (numpt+_stride-1)/_stride;
  GLfloat *buff0 = _rd.buffer( narrows*nvtx );
  _rd.lineWidth  = 10.;

  // calculate standardized 3D arrow (unit size in +z)
  RenderBuffer std_arrow(1);
  if( _3D ) {
    std_arrow.buffer( nvtx );
    GLfloat c[4] = {0,1,0,1};
    GLfloat *abuf = std_arrow.buffer();
    std_arrow.buff_3Darrow( abuf, _draw_heads?1:2, _draw_heads?1:-1, 1./_ratio, 2./_ratio, 
                             vec_num_sides, c, !_draw_heads );
  }

#pragma omp parallel for shared(numdrawn)
  for ( int i=0; i<numpt; i++ ) {

    if( _stride>1 ) {
      if( _stoch ) {
        if( random() > RAND_MAX/_stride )
          continue;
      } else {
        if( (i+_start)%_stride )
          continue;
      }
    }

    // determine size to draw vectors
    float size   = base_size;
    if( vdp[i*3]==0 && vdp[i*3+1]==0 && vdp[i*3+2]==0 ) continue;
    float magvec = magnitude(vdp+i*3);
    if (  _length_det == Vector ) 
      size *= std::min(magvec, maxmag);
    else if ( _length_det == Scalar && sdata != NULL ) 
      size *= std::min(fabs(sdp[i]),maxmag);
    if ( !isfinite(size) || size<_min_rel_sz*base_size ) {
      continue; // don't draw tiny vectors
    }

    bool skip;
    int currvtx;
#pragma omp critical 
    {
      skip = numdrawn>=narrows;
      currvtx = numdrawn;
      if( !skip ) numdrawn++;
    }
    if( skip ) continue; 

    // determine vector colour
    GLfloat *colf = _colour;
    if ( _colour_det==Vector)
      colf = cs->colorvec( magvec );
    else if ( _colour_det==Scalar && sdata!=NULL )
      colf = cs->colorvec( sdp[i] );
    GLubyte  col[4];
    for( int c=0; c<4; c++ ) 
      col[c] = (GLubyte)(colf[c]*255.);

    // translate, rotate and draw
    if( _3D ) {
      GLfloat *vtx = buff0+currvtx*nvtx*VBO_ELEM_SZ;
      // We must move translate and rotate the vector to get the proper orientation
      GLfloat rotvect[3], zaxis[]={0,0,1};
      float angle  = -acos( dot( vdp+i*3, zaxis )/magvec );
      // determine rotation axis noting degenerate case when vector lies on z-axis
      scale( cross( vdp+i*3, zaxis, rotvect ), 1./magvec );
      if( mag2(rotvect)<0.0001 ) assign( rotvect, 1., 0., 0. );// degenerate case 
      Quaternion rot;
      rot.SetRotationAboutAxis( angle, V3f(rotvect) );
      M4f rotmat;
      rot.SetOglRotationMatrix(rotmat);
      for( int j=0; j<nvtx; j++ ){
        GLfloat *arrow_dat = std_arrow.buffer()+j*VBO_ELEM_SZ;
        scale( assign( vtx, arrow_dat ), size );
        vtx[2] += size;            // base at origin
        V3f rpt = rotmat*vtx;
        vec_add( pts+i*3, rpt.e, vtx );
        float n[3];
        FLOAT_VCP( n, (char*)(arrow_dat)+VBO_NORM_OFFSET );
        rpt = rotmat*n;
        SHORT_VCP( (char *)vtx+VBO_NORM_OFFSET, rpt.e );
        _rd.buff_col( vtx, col );
        vtx += VBO_ELEM_SZ;
      }
      if( !_draw_heads ) {
#pragma omp critical
      {
        _rd.cyl_indices( currvtx*nvtx, 1 );
      }
      ;}
    } else {
      GLfloat *buff = buff0+2*i*VBO_ELEM_SZ;
      GLfloat vec[3];
      scale( assign(vec, vdp+i*3 ), size/magvec );
      SHORTVEC(n,1,0,0);
      GLfloat end[3];
      vec_add( assign( end, pts+i*3 ), vec, end );
      buff += _rd.buff_vtx( buff, pts+i*3, n, col, 0 );
      buff += _rd.buff_vtx( buff, end, n, col, 0 );
    }
  }
  _rd.buffer(numdrawn*nvtx);
  _rd.adjust_n();
  _rd.opaque(-1);
  _rd.update_nodalbuff();
  _rd.mesh_render();
}


    void
VecData::optimize_cs()
{
  if ( _colour_det==Vector) {
    float *vdp = vdata + 3*_last_tm*numpt;
    float magn, min;
    float max = min = mag2( vdp );
    for ( int i=0; i<numpt; i++ ) {
      if ( (magn=mag2(vdp)) > max )
        max = magn;
      if ( magn < min )
        min = magn;
      vdp += 3;
    }
    cs->calibrate( sqrt(min), sqrt(max) );
  } else if ( _colour_det==Scalar ) {
    float *sdp = sdata + _last_tm*numpt;
    int i=0;
    scalar_min = scalar_max = nanf("");
    while( !isfinite(sdp[i])&& i<numpt ) i++;
    if( i==numpt ){
      return;
    }
    scalar_min=sdp[i];
    scalar_max=sdp[i];
    for ( i++; i<numpt; i++ ) {
      if( !isfinite(sdp[i]) ) continue;
      if ( sdp[i]<scalar_min ) scalar_min = sdp[i];
      if ( sdp[i]>scalar_max ) scalar_max = sdp[i];
    }
    cs->calibrate( scalar_min, scalar_max );
  }
}


void
VecData::optimize_len( int ctm )
{
  if ( _length_det==Vector) {
    float *vdp = vdata + 3*(ctm<0?_last_tm:ctm)*numpt;
    float magn;
    maxmag = mag2( vdp );
    for ( int i=1; i<numpt; i++ ) {
      if ( (magn=mag2(vdp)) > maxmag )
        maxmag = magn;
      vdp += 3;
    }
    maxmag = sqrt(maxmag);
  } else if ( _length_det==Scalar && sdata) {
    float *sdp = sdata + (ctm<0?_last_tm:ctm)*numpt;
    maxmag = nanf("");
    int i=0;
    while( !isfinite(sdp[i]) && i<numpt ) i++;
    if( i==numpt ){
      maxmag=0;
      return;
    }
    maxmag=fabs(sdp[i]);
    for ( i++; i<numpt; i++ ) {
      if( !isfinite(sdp[i]) ) continue;
      if ( fabs(sdp[i])>maxmag ) maxmag = fabs(sdp[i]);
    }
  }
}
