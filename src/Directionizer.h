#ifndef DIRECTIONIZER_H
#define DIRECTIONIZER_H

#include "drawgl.h"
#include <FL/Fl_Double_Window.H>
#include "VecData.h"
#include "render_utils.h"

class TBmeshWin;

class  Directionizer : public Fl_Gl_Tb_Window 
{
    protected:
        Quaternion    _view;             //!< additional rotation of world
        GLuint        _dl;               //!< display list
        void           init_graphics();
        void           gl_settings();
        TBmeshWin     *win = NULL;
        bool          _eyeSys  = true;   //!< defined in eye coordinates, object o.w.
        RenderTris    *_rd       = NULL;
        RenderLines   *_rd_lines = NULL;
        void           arrow(GLfloat, GLfloat, GLfloat, GLfloat, int, const GLfloat*);
        void           hoops(const GLfloat *);
    public:
        Directionizer(int x, int y, int w, int h, const char *l = 0);
        virtual ~Directionizer(){}
        virtual void draw();
        virtual int handle( int );
        void    window( TBmeshWin *w ){ win = w; }
        bool    setView(Quaternion v){
          if(v!=_view && !_eyeSys ){_view=v;redraw();return true;}else return false;
        }
        Quaternion View() { return _view; }
        V3f     dir(V3f v){ return trackball.qRot.Rotate(v); }
        void    Rot( Quaternion rot ) { trackball.qRot = rot; }
        Quaternion Rot( void ) { return trackball.qRot; }
        void    eye( bool b ) { _eyeSys = b; }    
        bool    eye( void ) { return _eyeSys; }    
        void    spinOnly(bool b){ trackball.spinOnly=b; }
        void    reset(){ trackball.Reset(); }
        void    recolor( GLubyte *&, int n, GLubyte * );   //!< recolor n triangles
};



#endif
