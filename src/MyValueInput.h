/** \class MyValueInput 
 *  \brief value input with mouse wheel incrementing
 *
 *  Spinning the mouse wheel increases/decreases the value. pressing on control or
 *  shift at the same time increases the de/increment
 */
#ifndef MY_VALUE_INPUT_H
#define MY_VALUE_INPUT_H

#include <FL/Fl_Value_Input.H>
#include <cstdio>

class MyValueInput : public Fl_Value_Input
{
  public:
    MyValueInput( int, int, int, int, const char *L=0 );
    int handle( int );
    int format(char *buffer){const char* fmt="%.6g";if(step()==1.)fmt="%.0f"; return snprintf( buffer,128,fmt,value());}
};

#endif
