#ifndef LINKMESSAGES_H
#define LINKMESSAGES_H

#include "Quaternion.h"
#include "Vector3D.h"
#include "Colourscale.h"

namespace LinkMessage {

  enum Msgtype { LINK, UNLINK, VIEWPORT_SYNC, TIME_SYNC, COLOUR_SYNC, DIFFUSE_LINK, 
                 CLIP_SYNC, WINSZ_SYNC, HILITE_SYNC, TMPLOT_SYNC };

  struct TrackBallState{
    float scale;          
    V3f trans;            //!< translation normalized by model size
    Quaternion qSpin;     //!< incremental spin from standardview
    Quaternion qRot;      //!< rotation relative to standard view
  };

  struct ColourScaleState {
    float     min, max;
    int       scale;
    int       levels;
    short     deadDataOn;
    short     ignoreNaN;
    short     minDeadOn;
    short     maxDeadOn;
    float     minDead;
    float     maxDead;
    float     deadcol[3];
    float     deadopac;
  };

  struct ClipState {
    V3f        cnorm[6]; //!< rotations from ref direction to clipping plane normal 
    GLfloat    inter[6]; //!< clipping plane intercept
    int        state[6]; //!< display state
  };

  struct WindowState {
    int w;
    int h;
  };

  struct HilightState {
    int vtx;
    int surfEle;
    int volEle;
    int cnnx;
    int cable;
    int auxVtx;
  };

  struct TmPlotState {
    double x0, x1;
  };

  struct CmdMsg {
    pid_t   senderPid;
    pid_t   receiverPid;
    Msgtype command;
    int     sliderTime;
    int     newlink;

    TrackBallState   trackball;
    ColourScaleState colour;
    ClipState        clip;
    WindowState      winsz;
    HilightState     hilight;
    TmPlotState      tmplot;
  };

  struct MsgBuf {
    long   mtype;
    CmdMsg cmdmsg;
  } ;

}


#endif
