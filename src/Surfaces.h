#ifndef SURFACES_H
#define SURFACES_H

#include <stdlib.h>
#include "Region.h"
#include "short_float.h"
#include "legacyGL.h"
#include "Colourscale.h"
#include <string>
#include <fstream>
#include <unordered_set>
#include <memory>

#define  CSET(V,C,A)  V[0]=C;V[1]=C;V[2]=C;V[3]=A;

struct vtx_z {
  int   i;     //!< point index
  float z;     //!< z depth
  short s;     //!< surface
};


struct free_delete
{
    void operator()(void* x) { free(x); }
};


class Surfaces
{
  public:
    Surfaces( PPoint *pl=NULL ): _p(pl){}
    Surfaces( Surfaces &s );
    GLfloat* fillcolor( ){ return _fillcolor; }
    void     fillcolor(float r, float g, float b, float a=1);
    void     fillcolor(const GLfloat *c){fillcolor(c[0],c[1],c[2],c[3]);}
    GLfloat* outlinecolor( ){ return _outlinecolor; }
    void     outlinecolor(float r, float g, float b, float a=1);
    bool     visible(){return is_visible;}
    void     visible(bool a){ is_visible=a; }
    bool     filled( void ){  return _filled; }
    void     filled( bool a ){ _filled=a; }
    bool     outline( void ){  return _outline; }
    void     outline( bool a ){ _outline=a; }
    void     get_vert_norms( short_float *&vn ){ vn = _vertnorm; }
    void     determine_vert_norms( PPoint & );
    SurfaceElement*&         ele( int a ){ return _ele[a]; }
    std::vector<SurfaceElement*>& ele(){return _ele;}
    void     addele(int a,SurfaceElement*e,int s=-1){_ele[a]=e;_sele[a]=s;}
    void     appendEle(SurfaceElement *e,int s=-1){_ele.push_back(e);_sele.push_back(s);}
    int      num() const {return _ele.size();}
    void     num(int a){_ele.resize(a);_sele.resize(a, -1);}
    int      numtri(){ if(_numtri<0)count_tris();return _numtri; }  
    void     count_tris();
    void     buffer( GLfloat *&, char=0 );
    void     buffer_opaque( float, GLfloat *, Colourscale *, DATA_TYPE *,
                     dataOpac*, GLubyte *& colbuf, GLfloat *&, char=0 );
    void     buffer_elem(int, GLfloat *&, char=0);
    void     buffer_reg( RRegion **, GLfloat *& );
    void     colour( GLfloat *, Colourscale *, DATA_TYPE *,
                     dataOpac*, GLubyte *&, char=0 );
    void     colour_elem(int, GLfloat*,Colourscale*,DATA_TYPE*,
                            dataOpac*, GLubyte *&, char=0);
    void     colour_reg( RRegion **, GLubyte *& );
    void     zsort( void *, int, bool );
    void     label( std::string s ){ _label=s; }
    std::string   label( void ) { return _label; }
    void     flip_norms();
    void     correct_branch_elements( GLdouble *, DATA_TYPE *, 
                          Colourscale *, int, dataOpac * );
    void     to_file( std::ofstream &of );
    void     diffuse( GLfloat c, GLfloat a=1. ) { CSET(_diffuse,c,a) }
    GLfloat  diffuse( void ){ return _diffuse[0]; }
    void     specular( GLfloat c, GLfloat a=1. ) { CSET(_specular,c,a) }
    GLfloat  specular( void ){ return _specular[0]; }
    void     shiny( GLfloat s ) { _shiny=s; }
    GLfloat  shiny( void ) { return _shiny; }
    void     backlight( GLfloat b ) { _backlight=b; }
    GLfloat  backlight( void ) { return _backlight; }
    void     set_material( void );
    auto     zl_begin() { return _zlist.begin(); }
    auto     zl_end() { return _zlist.end(); }
    int      zl_sz(void){ return _zlist.size();}
    void     index( short i ){ _index = i; }
    short    index(){ return _index; }
    int      num_trans_tri() {int n=0; for(auto &l:_zlist)n+=_ele[l.i]->ntris(); return n;}
    DATA_TYPE* to_elem(GLfloat *ptdata);
    const    std::unordered_set<unsigned int>& vertices(){return _vert;}
    void     src_ele(int e, int s){ _sele[e] = s; }
    long     src_ele(int a){ return _sele[a]; }
    bool     vol_assoc(){ return _vol_assoc; }
    void     vol_assoc(bool b){ _vol_assoc=b; }
    void     clip( int a, bool v ){ _clip[a] = v; }
    bool     clip( int a ){ return _clip[a]; }
    void     assign_reg( RRegion **, int );
  protected:
    PPoint   *_p;
    GLfloat  _fillcolor[4]    = { 1., 0.5, 0.1, 1 };
    GLfloat  _outlinecolor[4] = { 0.125, 0.8, 0.7, 1 };
    bool      is_visible      = true;
    bool     _filled          = true;        //!< draw filled
    bool     _outline         = false;       //!< draw the outline
    short_float*  _vertnorm   = NULL;        //!< vertex normals
    std::unordered_set<unsigned int>_vert;   //!< vertices for which normals are computed
    std::vector<SurfaceElement*> _ele;       //!< list of elements to draw
    std::vector<long> _sele;                 //!< volume eles from which surface eles derived
    std::vector<DATA_TYPE> _eledat;          //!< nodal data projected onto elements
    std::string   _label      = "";
    GLfloat  _diffuse[4]      = {0.6,0.6,0.6,1.};
    GLfloat  _specular[4]     = {0.75,0.75,0.75,1.};
    GLfloat  _shiny           = {80.};
    GLfloat  _backlight       = 0.5;
    int      _oldstride       = 0;
    GLfloat  _oldmvp[4]{};              //!< row of model view projection matrix
    std::vector<vtx_z> _zlist;          //!< list of elements sorted by Z
    int      _numtri          = -1;     //!< number of triangles in surface
    short    _index;                    //!< index into surface list
    bool     _vol_assoc       = false;  //!< surface elements associated with a volume element?
    std::vector<bool> _clip   = std::vector<bool>(NUM_CP,true); //!< surface clipped by a certain plane?
    std::vector<int> _reg;              //!< region for each element
};

#endif
