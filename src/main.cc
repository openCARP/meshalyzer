#include "trimesh.h"
#include "isosurf.h"
#include "gitversion.h"
#include <string>
#include <cstring>
#include <sstream>
#include <libgen.h>
#include <getopt.h>
#include <FL/Fl_Text_Display.H>
#include <signal.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include "Frame.h"
#include <unistd.h>
#ifdef USE_HDF5
#include <hdf5.h>
#include <ch5/ch5.h>
#include "HDF5DataBrowser.h"
#endif

pid_t master = 0;
Controls *ctrl_ptr;
TBmeshWin *trball_ptr;
Fl_Window *win_ptr;

#ifdef OSMESA
#include <sys/mman.h>
#include <spawn.h>
sem_t *new_cnt;

extern char **environ;

sem_t * make_sem( std::string base, int val=0 ) {
      base += std::to_string(master);
      return sem_open( base.c_str(), O_CREAT, S_IRWXU, val );
}
#else

#ifndef __APPLE__
  #ifdef USE_GLUT 
    #include <FL/glut.H>
  #else
    #include <GLFW/glfw3.h>
  #endif
#endif

#endif //OSMESA

sem_t *meshProcSem;               // global semaphore for temporal linking
sem_t *linkingProcSem;            // global semaphore for process linking

#define SET_HI( A, B ) \
    if( trball_ptr->model->number(B) ) \
        control.A##hi->maximum( trball_ptr->model->number(B)-1 ); \
    else \
        control.A##hi->deactivate();

#define SIGSET(V,F,S) struct sigaction V;\
  V.sa_sigaction = F;\
  V.sa_flags     = SA_SIGINFO;\
  sigfillset( &V.sa_mask );\
  sigaction( S, &V, NULL );

/** do clean up if ctrl+C pressed
 */
void do_cleanup( int sig, siginfo_t *si, void *v )
{
#ifdef OSMESA
  std::cerr << "Cleaning up semaphores and shm" << std::endl;
  std::stringstream nw_name("/nw_");
  nw_name << master;
  std::stringstream cnt_name("/cnt_");
  cnt_name << master;
  sem_unlink( nw_name.str().c_str() );
  shm_unlink( cnt_name.str().c_str() );
#endif
  //delete win_ptr->trackballwin;
  exit(0);
}


/**
 * @brief determine if filename has an extension
 *
 * @param a   filename
 * @param ext extension
 *
 * @return true iff \p ext at end of \p a
 */
bool
has_ext( std::string a, const char *ext )
{
  return a.rfind(ext)!=std::string::npos && a.rfind(ext)==a.size()-strlen(ext);
}


/** new argv for a spawned mesalyzer process
 *
 * @param argc  number of args
 * @param argv  args given
 * @param proc  process number 
 * @param nproc total number of processes
 * @param f0    starting frame number
 * @param nfr   number of frames
 *
 * \return new argv
 **/
char **
sp_argv( int argc, char **argv, int proc, int nproc, int f0, int nfr )
{
    char **nargv = new char*[argc+6];
    int pos = 0;
    nargv[pos++] = strdup( "mesalyzer" );
    for( int i=1; i<argc; i++ )
      nargv[pos++] = strdup(argv[i]);
    nargv[pos++] = strdup("--nproc=1");
    char opt[1024];
    snprintf( opt, 1024, "--frame=%d", f0+proc );
    nargv[pos++] = strdup( opt );
    snprintf( opt, 1024, "--pngstep=%d", nproc );
    nargv[pos++] = strdup( opt );
    snprintf( opt, 1024, "--link=%ld", (long)master );
    nargv[pos++] = strdup( opt );
    if( nfr != -1 ){
      snprintf( opt, 1024, "--numframe=%d", nfr-proc );
      nargv[pos++] = strdup( opt );
    }
    nargv[pos++] = NULL;
    return nargv;
}


/** compute the surfaces and output them 
 *  the program should not exit if the filename
 *  begins with "+". If the filename=="(+)?/dev/null",
 *  do not write the file
 *
 * \retval true exit after call
 * \retval false do not exit
 */
bool
compute_write_surfaces( Model *model, std::string sf, bool flip )
{
  int ns  = model->numSurf();
  model->add_region_surfaces();
  //model->add_surface_from_elem( );

  for( int i=ns; i<model->numSurf(); i++ )
    if( flip ) 
      model->surface(i)->flip_norms();

  bool firstplus=false;
  if(sf[0] == '+') {
    firstplus = true;
    sf.erase(0,1);
  }

  if( sf != "/dev/null" ) {
    if(sf.back() != '.')
      sf += ".";
    sf += "surf";
    std::ofstream of(sf);
    for( int i=ns; i<model->numSurf(); i++ )
      model->surface(i)->to_file(of);
    std::cout << "Finished writing " << sf << std::endl;
  }

  if( firstplus ) {
    for( int s=ns; s<model->numSurf(); s++ ) 
      ctrl_ptr->surflist->add( model->surface(s)->label().c_str(),1);
  }

  return firstplus;
}


/** animate in response to a signal received: SIGUSR1 for forward, 
 *  SIGUSR2 for backward
 *
 * \param sig the signal
 */
void animate_signal( int sig, siginfo_t *si, void *v ) 
{
  int fs = ctrl_ptr->frameskip->value();
  fs *= sig==SIGUSR1 ? 1 : -1;

  int newtm = ctrl_ptr->tmslider->value() + fs;

  if( newtm<0 )
    newtm = ctrl_ptr->tmslider->maximum();
  if( newtm> ctrl_ptr->tmslider->maximum() )
    newtm = 0;

  trball_ptr->set_time( newtm );
  ctrl_ptr->tmslider->value(newtm);

  sem_post( meshProcSem );
}


 /** if the signal is SIGALRM, a new message queue is connected
  *  to this process, create bi-directional linking
  *
  * \param sig the signal
  */
void process_linkage_signal( int sig, siginfo_t *si, void *v ) 
{

  // 1. call createBiDirectionalMessageQueue
  if (sig != SIGALRM){
    return;
  }

  // temporarily block signals until this action is completed
  sigset_t intmask;
  sigemptyset(&intmask);
  sigaddset(&intmask, SIGALRM);
  sigprocmask(SIG_BLOCK, &intmask, NULL); 

  sem_post( linkingProcSem );        // signal msg received
  //win_ptr->trackballwin->mk_tmlink();
  trball_ptr->mk_tmlink();

  //win_ptr->trackballwin->CheckMessageQueue();  
  trball_ptr->CheckMessageQueue(); 

  sigprocmask(SIG_UNBLOCK, &intmask, NULL);
}


/** read in the version and license information
 *
 * \param infotxt info window text
 */
void
read_version_info( Fl_Browser *infotxt )
{
  std::string line;
  infotxt->add("\n");
  infotxt->add("@cAuthor: Edward Vigmond");
  line = "@cversion: ";
  line += gitversion;
  infotxt->add( "\n" );
  infotxt->add( line.c_str() );
  infotxt->add( "\n" );
  infotxt->add("@chttps://git.opencarp.org/openCARP/meshalyzer");
  infotxt->add("\n");
  infotxt->add( "@creleased under GNU Genral Public License v3.0\n");
}

/**
 * @brief make copires of surfaces
 *
 * @param cpSurf comma separated list of surface labels and/or indices
 * @param model  model with surfaces
 */
void
copy_surfs( std::string cpSurf, Model* model )
{
  char   delim = ',';
  while(1) {
    auto pos             = cpSurf.find(delim);
    int  surf            = -1;
    std::string surfSpec = cpSurf.substr(0,pos);
    for( int s=0; s<model->numSurf(); s++ ) {
      if( surfSpec == model->surface(s)->label() ){
        surf = s;
        break;
      }
    }
    if( surf == -1 ) {           // maybe the surface index given
      try {
        surf = std::stoi(cpSurf.substr(0,pos));
      }
      catch(...) {
        std::cerr <<"Unable to interpret surface: " 
                                     <<cpSurf.substr(0,pos)<<std::endl;
      }
    }
    if( surf>=0 && surf<model->numSurf() )
      model->copy_surface(surf);
    else if( surf != -1 || surfSpec=="-1" )
      std::cerr <<"Ignoring out-of-range surface index: "<<surf<<std::endl;
    if( pos == std::string::npos ) break;
    cpSurf = cpSurf.substr(pos+1);
  } 
}


/** make OpenGL context */
void
make_context()
{
#ifdef OSMESA
  const int attribs[] = { OSMESA_FORMAT,      OSMESA_RGBA, 
                          OSMESA_DEPTH_BITS,           32,
                          OSMESA_STENCIL_BITS,          0,
                          OSMESA_ACCUM_BITS,            0,
                          OSMESA_PROFILE, OSMESA_CORE_PROFILE,
                          OSMESA_CONTEXT_MAJOR_VERSION, 4,
                          OSMESA_CONTEXT_MINOR_VERSION, 1,
                          0, 0 };
    auto ctx = OSMesaCreateContextAttribs( attribs, NULL );
    if( !ctx ) {
      std::cout << "Failed OSMesaCreateContextAttribs" << std::endl;
      throw 1;
    }
    trball_ptr->context( ctx );

#elif !defined(__APPLE__)
#  ifdef USE_GLUT
    int   argc = 2;
    char *argv[] = { strdup("meshalyzer"), strdup("-iconic") };
    glutInit(&argc, argv);
    glutInitWindowSize(0,0);
    glutCreateWindow("Take it eaze");
#  else
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
    auto window = glfwCreateWindow(512, 512,"", NULL, NULL);
    if( !window ) {
      std::cerr << "minimum GL version not supported!" << std::endl;
      exit(1);
    }
    glfwMakeContextCurrent(window);
#  endif  // USE_GLUT
#endif
}

/* output a flyby sequence offscreen 
 *
 * \param flyby basename including directory
 * \param tbmw  trackball widget
 * \param pngsz image size
 */
void
os_flyby( std::string filebase, TBmeshWin *tbmw, int w, int h )
{
  make_context();

  tbmw->transBgd(false);
  ctrl_ptr->tb_winsz(w,h);

  // maybe need to create directory
  size_t slash = filebase.rfind('/');
  if( slash != std::string::npos ) {
    std::string dirname = filebase.substr(0,slash);
    if( mkdir( dirname.c_str(), 0744 )==-1 && errno!=EEXIST ) {
      std::cerr << "Exiting: Cannot create directory " << dirname << std::endl;
      exit(1);
    }
  }
  ctrl_ptr->flyby->fly(filebase);  
  exit(0);
}


/** output a sequence of PNG images offscreen
 *
 * \param filename file or directory (numf>1) name
 * \param f0       first frame number
 * \param numf     number of frames
 * \param tbwm     rendering window
 * \param w        image width
 * \param h        image height
 * \param nproc    number of processes concurrently writing
*/
void
os_png_seq( std::string filename, int f0, int numf, TBmeshWin *tbwm, int w, int h, int nproc )
{
  make_context();

  int f1 = f0+numf-1;

  // strip ".png" from file name if present
  if( has_ext(filename, ".png") )
    filename.erase(filename.length()-4);
 
  // output sequence into a directory which we may need to create
  if( numf>1 ) {
    tbwm->transBgd(false);
    if( mkdir( filename.c_str(), 0744 )==-1 && errno!=EEXIST ) {
      std::cerr << "Exiting: Cannot create directory "<<filename<<std::endl;
      exit(1);
    }
    filename += "/frame"; // base name for images
  }
  
  int psize_x = w;
  int psize_y = h;

#ifndef OSMESA
  psize_x *= tbwm->pixels_per_unit();// for RETINA displays
  psize_y *= tbwm->pixels_per_unit();// for RETINA displays
#endif

  Frame frame( tbwm, w, h );

  if( numf==1 ) {
    frame.write( psize_x, psize_y, filename, f0 );
  } else {
#ifdef OSMESA
    int nw = frame.write( psize_x, psize_y, filename, f0, f1, nproc );

    if( nproc==1){
      std::cerr << "Wrote " << nw << " frames\n" << std::endl;
      exit(0);
    } 
    
    // create shared memory needed for interprocess communication
    std::string cnt_name("/cnt_");
    cnt_name += std::to_string(master);
    int shm = shm_open(cnt_name.c_str(), O_CREAT|O_RDWR, 0666);
    ftruncate(shm, 2*sizeof(int));
    int *nwr = (int*)mmap(NULL, 2*sizeof(int), PROT_READ|PROT_WRITE, MAP_SHARED, shm, 0 ); 

    // count the number of frames written by all processes
    do {
      sem_wait( new_cnt );
      if( master != getpid() ) { //child process finished
        nwr[0] += nw;
        nwr[1]++;
        sem_post( new_cnt );
      } else if( nwr[1]==nproc-1 ) {  // master checks if all children finished
        nwr[0] += nw;
        std::cerr << "Wrote " << nwr[0] << " frames\n" << std::endl;
        std::string new_name = "/nw";
        new_name += std::to_string(master);
        sem_unlink(new_name.c_str());
        shm_unlink(cnt_name.c_str());
        break;
      } else {                       // master when children not finished
        sem_post( new_cnt );
        sleep(1);
      }
    } while( master==getpid() );

#else
    int nw = frame.write( psize_x, psize_y, filename, f0, f1 );
    if( nw != numf ) 
      std::cerr << "Only " << nw << " of " << numf << " frames written" << std::endl;
#endif
  }
  exit(0);
}


/* find out where model is located
 *
 * \param fn specified file name
 *
 * \return path to the model
 */
std::string
find_model_dir( std::string fn )
{
  if( fn.empty() ) return "";

  if( std::ifstream(fn.c_str()) ) return fn;
  if( std::ifstream((fn+"pts").c_str()) ) return fn+"pts";
  if( std::ifstream((fn+"bpts").c_str()) ) return fn+"bpts";
  if( std::ifstream((fn+".pts").c_str()) ) return fn+".pts";
  if( std::ifstream((fn+".bpts").c_str()) ) return fn+".bpts";
  if( fn.at(0) == '/' ) return "";

  char *path_var = NULL;
  char *mod_dir_env = getenv("MESHALYZER_MODEL_DIR");
  if( mod_dir_env ) 
    path_var = strdup(mod_dir_env);
  char *ptr = strtok( path_var, ":" );
  while( ptr ) {
    std::string filename = ptr;
    filename += "/" + fn;
    if( std::ifstream(filename.c_str()) ) return filename;
    if( std::ifstream((filename+"pts").c_str()) ) return filename+"pts";
    if( std::ifstream((filename+"bpts").c_str()) ) return filename+"bpts";
    if( std::ifstream((filename+".pts").c_str()) ) return filename+".pts";
    if( std::ifstream((filename+".bpts").c_str()) ) return filename+".bpts";
    ptr = strtok( NULL, ":"  );
  }
  return "";
}


/* read in a CG (CoolGraphics) input file
 *
 * \param fin     file name
 * \param w       display widget
 * \param control control widget
 */
//void process_cg_format(char *fin, Meshwin *w, Controls* control, bool no_elems)
void process_cg_format(char *fin, Fl_Window *win, TBmeshWin *tbmw, bool no_elems)
{
  static int  bufsize=1024;
  char        buff[bufsize];

  std::string ptfile = fin;
  ptfile.erase( ptfile.size()-5, 5 );
  tbmw->read_model(win, ptfile.c_str(), no_elems, true );

  // figure out the directory with the ".cg_in" file
  // all others are assumed to be in the same directory
  std::string dir(dirname(fin));
  dir += "/";

  // read in the surface files
  int         numtrif;
  gzFile in = gzopen( fin, "r" );
  gzgets(in, buff, bufsize);
  sscanf( buff, "%d", &numtrif );
  gzgets(in, buff, bufsize); 			// read in the obligatory zero
  for ( int i=0; i<numtrif; i++ ) {
    gzgets(in, buff, bufsize );
    std::stringstream line( buff );
    std::string trifile;
    line >> trifile;
    trifile = dir+trifile+".tri";
    tbmw->add_surface( trifile.c_str() );
  }
  gzclose( in );

  // compute surface normals
  //w->trackballwin->model->_triele->compute_normals(0,
  //w->trackballwin->model->_triele->num()-1 );

  // read in the data
  std::string datafile=ptfile;
  datafile += "t0";
  tbmw->get_data(datafile.c_str(), ctrl_ptr->tmslider );
}

/* Reads in an HDF5 input file.
 *
 * \param fin      file name
 * \param w        display widget
 * \param control  control widget
 */
void process_h5_format(char *fin, Fl_Window *win, TBmeshWin *trackball, bool no_elems)
{
#ifdef USE_HDF5
  hid_t file;
  
  if (ch5_open(fin, &file)) {
    std::cerr << "Invalid HDF5 file or not found" << std::endl;
    exit(1);
  }
  trball_ptr->read_model(win, file, no_elems, false);
  ch5_close(file);
#else
  assert(0);
#endif
}


/** output usage 
 */
void
print_usage(void) 
{
  std::cout << "meshalyzer [options] model_base[.[pts]] [file.igb|file.dat|file.datH5:nodal/#] [file.EXT] " << std::endl;
  std::cout << "           EXT in {xfrm,mshz,vpts,pts_t,dynpt}" << std::endl;
  std::cout << "with options: " << std::endl;
  std::cout << "--1                   | -1  use one window" << std::endl;
  std::cout << "--2                   | -2  separate control and model windows (default)" << std::endl;
  std::cout << "--iconifycontrols     | -i  iconify controls on startup" << std::endl;
  std::cout << "--no_elem             | -n  do not read element info" << std::endl;
  std::cout << "--help                | -h  print this message" << std::endl;
  std::cout << "--groupID=GID         | -g  meshalyzer group" << std::endl;
  std::cout << "--PNGfile=file        | -P  output PNGs and exit" << std::endl;
  std::cout << "--frame=file          | -f  first frame for PNG dump (-1=do not set)" << std::endl;
  std::cout << "--flipNorms           | -l  flip normal orientation" << std::endl;
  std::cout << "--numframe=num        | -N  number of frames to output (default=1)" << std::endl;
  std::cout << "--size=W[xH]          | -s  output size of PNG in pixels (default=512x512)" << std::endl;
#ifdef OSMESA
  std::cout << "--nproc=num           | -p  #parallel procs for PNG sequences" << std::endl;
  std::cout << "--link=PID            | -L  PID of master mesalyzer process" << std::endl;
  //std::cout << "--pngstep=num         | -d  stride for PNGs" << std::endl;
#else
  std::cout << "--link=PID            | -L  PID of meshalyzer process to link" << std::endl;
#endif
  std::cout << "--flyby=file          | -F  perform a flyby" << std::endl;
  std::cout << "--cpSurf=s[,s[,s...]] | -c  copy the surfaces listed" << std::endl;
  std::cout << "--compSurf=(+)?[file]       compute surfaces, +=do not exit after" << std::endl;
  exit(0);
}


static struct option longopts[] = {
  { "iconifycontrols", no_argument, NULL, 'i' },
  { "no_elem"        , no_argument, NULL, 'n' },
  { "help"           , no_argument, NULL, 'h' },
  { "1"              , no_argument, NULL, '1' },
  { "2"              , no_argument, NULL, '2' },
  { "groupID"        , 1          , NULL, 'g' },
  { "PNGfile"        , 1          , NULL, 'P' },
  { "frame"          , 1          , NULL, 'f' },
  { "flipNorms"      , no_argument, NULL, 'l' },
  { "link"           , 1          , NULL, 'L' },
  { "numframe"       , 1          , NULL, 'N' },
  { "size"           , 1          , NULL, 's' },
#ifdef OSMESA
  { "nproc"          , 1          , NULL, 'p' },
  { "pngstep"        , 1          , NULL, 'd' },
#endif
  { "flyby"          , 1          , NULL, 'F' },
  { "compSurf"       , 2          , NULL, 'S' },
  { "cpSurf"         , 1          , NULL, 'c' },
  { NULL             , 0          , NULL,  0  }
};


int
main( int argc, char *argv[] )
{
  Fl::scheme("gleam");

#ifdef __APPLE__
  Fl::use_high_res_GL(IS_RETINA);
#endif

#ifdef USE_HDF5
  H5Eset_auto1(NULL, NULL);// silence HDF errors
#endif

  bool   oneWin         = true;
  bool   iconcontrols   = false;
  bool   no_elems       = false;
  bool   threadedReader = true;
  char  *PNGfile        = NULL;
  int    png_w          = 512;
  int    png_h          = 512;
  const char *grpID     = "0";
  int    frame0         = -1,
         numframe       = 1,
         pngstep        = 1,
         nproc          = 1;
  char  *surfFile       = NULL;
  bool   flipNorms      = false;
  bool   size_spec      = false;
  pid_t  linkee         = 0;
  std::string cpSurfs; 
  std::string flyby;

  int ch;
  while( (ch=getopt_long(argc, argv, "12c:d:F:f:g:hilL:nN:P:p:s:S:t", longopts, NULL)) != -1 )
	switch(ch) {
		case '1':
			if(!PNGfile) oneWin = true;
			break;
		case '2':
			oneWin = false;
			break;
        case 'c':
            cpSurfs = optarg;
            break;
#ifdef OSMESA
        case 'd':
            pngstep = std::stoi(optarg);
            break;
#endif
		case 'F':
			flyby  = optarg;
            oneWin = false;
			break;
		case 'f':
			frame0 = std::stoi(optarg);
			break;
        case 'g':
            grpID = strdup(optarg);
            break;
		case 'h':
			print_usage();
			break;
		case 'i':
			iconcontrols = true;
			break;
        case 'l':
            flipNorms = true;
            break;
        case 'L':
            linkee = std::stoi(optarg);
            break;
        case 'n':
			no_elems = true;
			break;
		case 'N':
			numframe = std::stoi(optarg);
			break;
        case 'P':
            PNGfile = strdup(optarg);
            oneWin  = false;
            break;
		case 'p':
			nproc = std::stoi(optarg);
            break;
        case 's':
            {
              size_spec = true;
              std::string geom(optarg);
              size_t ex;
              png_w = std::stoi(geom,&ex);
              if( ex != geom.size() ){ 
                png_h = std::stoi( geom.substr(ex+1),NULL );
              } else
                png_h = png_w;
            }
            break;
        case 'S' : 
            surfFile = strdup(optarg?optarg:"");
            break;
        case 't':
            threadedReader = true;
            break;
        case '?':
            std::cerr << "Unrecognized option --- bailing" << std::endl;
            exit(1);
		default:
			break;
	}
  
  // spawn new processes since it is too difficult to copy tbwm with all 
  // of its dynamic allocations
  master = getpid();
#ifdef OSMESA
  if( nproc>1 ) {
    new_cnt = make_sem("/nw", 1);  // parent makes semaphore`
  } else {
    if( linkee ) master = linkee;  // spawned child needs PID of parent
    new_cnt = make_sem("/nw");
  }
  for( int i=1; i<nproc;i++ ) {
    pid_t sp_pid;
    char **new_argv = sp_argv( argc, argv, i, nproc, frame0, numframe );
    posix_spawnp( &sp_pid, argv[0], NULL, NULL, new_argv, environ );
    std::cout << "spawned child " << i << ": " << sp_pid << std::endl;
  }
#endif

  Fl::gl_visual(FL_RGB|FL_DOUBLE|FL_DEPTH|FL_ALPHA|FL_OPENGL3);
  Controls control;
  ctrl_ptr = &control;

  if( oneWin ) {
    win_ptr    = control.window;
    trball_ptr = control.tbwin2;
  } else {
    MeshWin *mwin = new MeshWin;
    trball_ptr    = mwin->trackballwin;
    win_ptr       = mwin->trackballwin;
    win_ptr->position(1,1);
    control.two_wins(win_ptr->x(),win_ptr->y());
  }
  trball_ptr->controlwin( &control );
  control.outputwin(trball_ptr);

  int model_index=optind;
  while( model_index<argc && argv[model_index][0]=='-' )
	model_index++;

  std::string model_path = find_model_dir( model_index<argc?argv[model_index]:"" );

  if ( has_ext(model_path, ".cg_in") )
    process_cg_format( argv[1], win_ptr, trball_ptr, no_elems );
  else if ( has_ext(model_path, ".modH5") )
    process_h5_format(argv[1], win_ptr, trball_ptr, no_elems);
  else
    trball_ptr->read_model( win_ptr, model_path.c_str(), no_elems );

  SET_HI( tet,  VolEle  );
  SET_HI( ele,  SurfEle );
  SET_HI( cab,  Cable   );
  SET_HI( vert, Vertex  );
  SET_HI( cnnx, Cnnx    );

  ProgInfo info;
  read_version_info( info.infotxt );
  control.proginfo = info.proginfo;

  bool vectordata=false;

  std::string dir = argc>=2? dirname(argv[1]) : ".";
  dir += "/";

  // look for default state
  std::string defstate = getenv("HOME");
  defstate += "/.default.mshz";
  struct stat buf;
  if( !stat( defstate.c_str(), &buf) ) 
    control.restore_state( defstate.c_str() );

#ifdef __APPLE__
    if( !oneWin ) win_ptr->show();
#else
  if( !PNGfile && !flyby.size() ) {
#ifdef OSMESA
    std::cerr << "PNGfile must be specified with mesalyzer!" << std::endl;
    exit(1);
#else
    if( !oneWin ) win_ptr->show();
#endif
  }
#endif

  // deal with command line files specified
  int mshz_pos=-1, xfrm_pos=-1;
  for ( int i=model_index+1; i<argc; i++ ) {
    if ( argv[i][0] == '-' ) 
      continue;
    if ( has_ext(argv[i], ".tri") || has_ext(argv[i], ".surf") ) {
      if ( trball_ptr->add_surface(argv[i])< 0 ) {
        std::string altdir = dir;
        altdir += argv[i];
        trball_ptr->add_surface(altdir.c_str());
      }
#ifdef USE_HDF5
    } else if ( has_ext( argv[i], ".datH5" ) ) {
      HDF5DataBrowser *brow =  new HDF5DataBrowser( argv[i], trball_ptr );
      control.add_recent(argv[i],HDF5);
#endif
    } else if ( strstr( argv[i], ".datH5:nodal/" ) != NULL ) {
      trball_ptr->get_data(argv[i], control.tmslider );
      control.add_recent(argv[i],HDF5);
    } else if ( has_ext( argv[i], ".vpts" )    ||
              strstr( argv[i], ":vector/" ) != NULL ){
      vectordata = !trball_ptr->getVecData(control.tmslider, argv[i]);
      control.add_recent(argv[i],VECT);
    } else if ( has_ext( argv[i], ".pts_t" )            ||
                has_ext( argv[i], ".pts"   )            ||
                strstr( argv[i], ":auxGrid/" ) != NULL ) {
      bool fake = has_ext(argv[i], ".pts");
      if( !trball_ptr->readAuxGrid( control.tmslider, argv[i], fake ) ) {
        control.auxgridgrp->activate();
        control.add_recent(argv[i],AUX);
      }
    } else if ( has_ext( argv[i], ".dynpt" ) ){
      trball_ptr->read_dynamic_pts( argv[i], control.tmslider );
      control.add_recent(argv[i],DYN_PTS);
    } else if ( has_ext( argv[i], ".atm" )  ||
                strstr( argv[i], ":tm_annot/" ) != NULL ){
      trball_ptr->read_tm_annos( argv[i] );
      //control.add_recent(argv[i],HDF5);
    } else if ( has_ext( argv[i], ".dat" ) ) {
      trball_ptr->get_data(argv[i], control.tmslider );
      control.add_recent(argv[i],ASCII);
    } else if ( has_ext( argv[i], ".xfrm" ) )
      xfrm_pos = i;
    else if ( has_ext( argv[i], ".mshz" ) )
      mshz_pos = i;
    else {
      trball_ptr->get_data(argv[i], control.tmslider );
      control.add_recent(argv[i],IGB);
    }
  }

  if( cpSurfs.size() )
    copy_surfs( cpSurfs, trball_ptr->model );

  if( flipNorms ) trball_ptr->model->flip_norms();
  control.set_region_list( trball_ptr->model );
  trball_ptr->cplane->calc_intercepts();

  for ( int i=0; i<trball_ptr->model->numSurf(); i++ ) {
    control.surflist->add(trball_ptr->model->surface(i)->label().c_str(), 1);
  }
  if ( vectordata ) control.vectorgrp->activate();
  control.mincolval->value(trball_ptr->cs->min());
  control.maxcolval->value(trball_ptr->cs->max());

  if( mshz_pos >= 0 ) control.restore_state( argv[mshz_pos] );
  if( xfrm_pos >= 0 ) trball_ptr->trackball.read( argv[xfrm_pos] );

  if( !PNGfile && !flyby.size() )
    control.window->show();

  if ( trball_ptr->auxGrid ) control.auxgridgrp->activate();
  if ( iconcontrols && !oneWin ) control.window->iconize();

  // set up named semaphore for meshProcSem
  std::string semstr = "/mshz_";
  semstr += std::to_string(getuid());
  semstr += "_";
  semstr += grpID;
  meshProcSem = sem_open( semstr.c_str(), O_CREAT, S_IRWXU, 0 );
  if( meshProcSem==SEM_FAILED )
    perror("Temporal linking not possible");

  // set up signal handling
  SIGSET( sigact,     animate_signal,         SIGUSR1 )
  SIGSET( sigLinkAct, process_linkage_signal, SIGALRM )
  SIGSET( sigCleanup, do_cleanup,             SIGINT )
  sigaction( SIGUSR2, &sigact, NULL );

  // set up named semphore for linkingProcSem
  std::string linkageStr = "/linkage_";
  linkageStr += std::to_string(getuid());
  linkageStr += "_";
  linkageStr += grpID;
  linkingProcSem = sem_open( linkageStr.c_str(), O_CREAT, S_IRWXU, 0 );
  if (linkingProcSem == SEM_FAILED)
    perror("Message Queue inter-process communication not possible");

  if( surfFile ) {
    if( *surfFile=='\0' ) 
        surfFile = strdup(trball_ptr->model->file());
    if(!compute_write_surfaces( trball_ptr->model, surfFile, flipNorms ) )
      exit(0);
  }

  // just output images, no interaction
  if( (PNGfile || flyby.size()) ) {
    if( !size_spec ) trball_ptr->get_win_sz( png_w, png_h );
    control.tb_winsz(png_w,png_h);
    trball_ptr->win_sz(png_w,png_h);
  } 
  if( PNGfile ) {
    if( frame0<0 )   frame0   = trball_ptr->time();
    if( numframe<0 ) numframe = trball_ptr->max_time()-frame0+1;
    os_png_seq( PNGfile, frame0, numframe, trball_ptr, png_w, 
                                   png_h, master==getpid()?nproc:pngstep);
  } else if( flyby.size() ) {
    os_flyby( flyby, trball_ptr, png_w, png_h );
  }

  if( oneWin ) trball_ptr->show();

  if( linkee ) {
    trball_ptr->mk_tmlink();
    trball_ptr->tmLink->link(linkee);
  }

  Fl::run();
}

