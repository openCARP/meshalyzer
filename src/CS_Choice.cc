#include "CS_Choice.h"
#include "TBmeshWin.h"
#include<FL/Fl_XPM_Image.H>
#include<string>
#include<cstring>
#include<vector>
#include<map>
#include<iostream>
 
/**
 * @brief callback for menuitems
 *
 * @param w CS_Choice widget 
 * @param d colour scale name
 */
void cs_select_cb( Fl_Widget *w, void *d )
{
  CS_Choice  *cs_ch = (CS_Choice *)w;
  TBmeshWin  *mwtb  = (TBmeshWin*)(cs_ch->data());
  mwtb->cs_adj(cs_ch, (const char *)d);
}


CS_Choice :: CS_Choice( int X, int Y, int W, int H, const char *L) : Fl_Choice( X, Y, W, H, L)
{
    down_box(FL_BORDER_BOX);
    labelsize(10);
    align(Fl_Align(FL_ALIGN_BOTTOM));

    // dynamically generate menu
    if( !_csmenu )
      make_menu( W, H );

    auto it = std::find(cs_scales.begin(), cs_scales.end(), "Bl_Rainbow" );
    value( it - cs_scales.begin() );
}

    
/**
 * @brief create the images for the menu
 *
 * @param W    width of Fl_Choice widget
 * @param H    height of image
 * @param rev  draw reversed
 */
void
CS_Choice:: make_menu(int W, int H, bool rev)
{     
  Fl_Menu_Item defcon = {0, 0, 0};
  _csmenu = new Fl_Menu_Item[cs_scales.size()+1];
  Colourscale cs(256);
  // make a 256x1 image of each colour map
  for( int i=0; i<cs_scales.size(); i++ ) {
    _csmenu[i] = defcon;
    cs.scale( cs_scales[i] );
    uchar *rgb = new uchar[cs.size()*3];
    cs.RGBmap( rgb );
    if( rev ){
      GLfloat tmp[3];
      for( int i=0; i<cs.size()/2; i++ ){
        memcpy( tmp, rgb+i*3, sizeof(rgb[0])*3 );
        memcpy( rgb+i*3, rgb+3*(cs.size()-1-i), sizeof(rgb[0])*3 );
        memcpy( rgb+3*(cs.size()-1-i), tmp, sizeof(rgb[0])*3 );
      }
    }
    Fl_RGB_Image img(rgb, cs.size(), 1);
    // scale image to proper size for button
    Fl_Image *img_cs = img.copy( W-35, H );
    _csmenu[i].label( cs_scales[i] );
    _csmenu[i].image(img_cs);
    _csmenu[i].callback( cs_select_cb, (void*)(cs_scales[i]) );
    delete[] rgb;
  }
  _csmenu[cs_scales.size()] = {0,0,0,0,0,0,0,0,0};
  menu(_csmenu);
}


/**
 * @brief get current colour scale 
 *
 * @return name of the colour map
 */
std::string
CS_Choice :: value()
{
  int i = Fl_Choice::value();
  return( cs_scales[i] );
}


/**
 * @brief set colour scale. If \p cs is an integer, try deprecated
 *        select by index
 *
 * @param cs colour map name
 */
void
CS_Choice:: value( std::string cs )
{
  auto it = std::find(cs_scales.begin(), cs_scales.end(), cs );
  if ( it == cs_scales.end() ) {
    int index;
    try {
      index = std::stoi(cs);
      it = std::find(cs_scales.begin(), cs_scales.end(), cs_indx_map[index] );
    }
    catch(...){
      std::cerr << "Colour scale " << cs << "not found, using Bl_Rainbow" << std::endl;
      auto it = std::find(cs_scales.begin(), cs_scales.end(), "Bl_Rainbow" );
    }
  }
  Fl_Choice::value( it - cs_scales.begin() );
}


/** update the colour bars
 *
 * \param reverse reverse the colour bars?
 */
void 
CS_Choice::update(bool reverse)
{
  auto cval = value();
  delete[] _csmenu;
  make_menu( w(), h(), reverse );
  value(cval);
  redraw();
}
