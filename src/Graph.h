#ifndef GRAPH_H
#define GRAPH_H
#include <cstdio>
#include <vector>
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/fl_draw.H>
#include <ostream>
#include "plottingwin.h"

const int num_labels=7;
const int max_label_len=25;

const double BOGUS_ANNOTE=-666.66;

const double nlm1= double(num_labels-1);

class Graph : public Fl_Widget
{
    void draw();
    int handle(int);
  public:
    //! The contructor
    /*!
     * \param X  x-position
     * \param Y  y-position
     * \param W  width
     * \param H  height
     */
    Graph(int X,int Y,int W,int H) : Fl_Widget(X,Y,W,H),
            v_autoscale(true), zero_yaxis(false){}
    int  set_2d_data(const double *, const double *, int n, int c, int id=-1, std::string fn="" );
    int  set_2d_data(const double *, const double *, double, int n, int c, int id=-1, std::string fn="" );
    void reset_view( void );
    void range( double &, double &, double &, double& );
    void set_range( double, double, double, double, bool=false, bool=false );
    void set_X( double xa, double xb ) {set_range(xa, xb, y0, y1);redraw();}
    void autoscale( bool a ){v_autoscale=a;}
    void copy_curve(int);
    void clear_curves();
    void scale();
    void write(std::ostream&, int);
    int  n(){ return _id.size(); }
    void to_world( int x, int y, double &wx, double &wy );
    void rotate();
    int  id( int a ){ if(a<_id.size()&&a>=0)return _id[a];else return -1; }
    std::string  datf( int a ){ if(a<_id.size()&&a>=0)return _datf[a];else return std::string(""); }
    void num_dynamic( int a ){ num_dyn=a; }
    void toggle_zero_yaxis(){ zero_yaxis=!zero_yaxis;}
    bool crvi_vis(){return crvi && crvi->win->visible();}
    void delete_curve(int);
    void clear_annots() { t_annot.assign(t_annot.size(),BOGUS_ANNOTE);
                          v_annot.assign(v_annot.size(),BOGUS_ANNOTE); }
    friend void static_curve_info_cb( Fl_Widget *w );
    void extrema( double &ymin, double &ymax );
    void round_axis_limits(double &amin, double &amax);
  private:
    std::vector<const double*> xv, yv;
    std::vector<double> t_annot;
    std::vector<double> v_annot;
    std::vector<std::string> _datf;                         // data file
    double  x0, x1, y0, y1;                                 // data range being plotted
    double  xmin, xmax, ymin, ymax;                         // data extrema
    std::vector<int>    _id;                                // identifier of set
    std::vector<int>    np;                                 // number of points per set
    char    xlabel[num_labels][max_label_len], ylabel[num_labels][max_label_len];
    int     num_dyn;                                        // !< \# non-static curves
    void    change_view( int, int, int, int );
    void    make_labels(void);
    bool    v_autoscale;                                    // autoscale with set change
    bool    zero_yaxis;
    double  nicenum( double, int, int );
  public:
    CurveInfo *crvi=NULL;
};

#endif
