#ifdef OSMESA
#  define  GL_GLEXT_PROTOTYPES
#  include <GL/osmesa.h>
#elif defined(__APPLE__)
#  define GL_SILENCE_DEPRECATION
#  include <OpenGL/gl3.h> 
#else
#  if defined(WIN32) || defined(__CYGWIN__)
#    define GLEW_STATIC 1
#  endif
# include <GL/glew.h>
#endif

#include "render_func.h"
#include <string>
#include <iostream>
#include <sstream>
#include <set>
#include "VecData.h"
#include "select_util.h"

/**
 * @brief copy vertex data to GPU
 *
 * @param unbind unbind buffer after transfer?
 */
void 
SelectVtx::init_nodalbuff(bool unbind)
{
  glBindBuffer(GL_ARRAY_BUFFER, gl_nodalbuff);
  glBufferData(GL_ARRAY_BUFFER, nodalbuff.size()*sizeof(float), nodalbuff.data(), GL_STATIC_DRAW);

  if(unbind)
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}


/**
 * @brief do all the ugly setting up for OpenGL including
 * defining the vertex buffer and binding to OGL context
 *
 * \return true iff the initialization was operformed
 */
bool 
SelectVtx::setup_render_data(void *c)
{
  if(!init) {
#if !defined( __APPLE__) && !defined(OSMESA)
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if (GLEW_OK != err) {
      std::cerr << "GLEW problem -- aborting!" << std::endl;
    }
#endif
    if( c!= NULL ) _context = c;
    shader = get_select_shader();

    set_identity_4x4_mat(tfm);

    glGenVertexArrays(1, &gl_vertexarray);
    glBindVertexArray(gl_vertexarray);
    glGenBuffers(1, &gl_nodalbuff);

    init_nodalbuff(false);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * BUFF_SZ, (void*)(0));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(float) * BUFF_SZ, (void*)(COL_OFFSET*sizeof(float)));

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    init = true;
    return true;
  } else
    return false;
}


SelectVtx::~SelectVtx()
{
  if( init ) {
    glDeleteBuffers( 1,  &gl_nodalbuff );
    glDeleteVertexArrays( 1, &gl_vertexarray );
    glDeleteProgram(shader);
  }
}


void
SelectVtx::set_clipping(int shader_program) 
{
  // now feed in the uniform input data (mostly the transformation matrices)
  // into the shader program
  set_shader_view_matrices(shader_program,_context);
  set_shader_mat4(shader_program, "model", &tfm[0][0]);

  GLfloat noclip[4] = { 0.f, 0.f, 0.f, 1000.f };
  for( int i=0; i<NUM_CP; i++ ) {
    GLfloat *clipEqn = clipping(i) ? CLIP_EQN[i]: noclip;
    std::stringstream clipEqnStr;
    clipEqnStr << "clipEqn[" << i << "]";
    set_shader_vec4(shader_program, clipEqnStr.str().c_str(), clipEqn);
  }
}


/** finish rendering */
void
SelectVtx :: finish_render()
{
  // unbind render data and shader
  glBindVertexArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0 );
  glUseProgram(0);

  // Forces the execution of OpenGL commands
  glFlush();
  _dirty = false;
}


/** render the surface elements
 *
 * \param wireframe   render wireframe else solid elements
 *
 * \pre \p ntris is the number of triangles to render
 * \pre \p n_opaque is the number of opaque vertices (3/tri)
 * \pre \p nodalbuff is filled first with opaque vertices, followed by translucent
 * \pre \p material[i] has the info for surface[i]
 */
void 
SelectVtx:: mesh_render()
{
  glBindVertexArray(gl_vertexarray);
  glBindBuffer(GL_ARRAY_BUFFER, gl_nodalbuff);

  glClearColor(0,0,0,0);
  glClear(GL_COLOR_BUFFER_BIT);
  glPointSize(ptSize);
  glUseProgram(shader);

  set_clipping(shader);

  glDisable(GL_BLEND);
  glDepthMask(GL_TRUE);
  glDrawArrays(GL_POINTS, 0, _n);

  finish_render();
}


