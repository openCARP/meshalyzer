#ifndef MODEL_H
#define MODEL_H
#include "DrawingObjects.h"
#include <string>
#include <cstring>
#include <vector>
#include <zlib.h>
#include "Colourscale.h"
#include "IGBheader.h"
#include "HiLiteWinInfo.h"
#include "Surfaces.h"
#include "Region.h"
#include "AsciiReader.hpp"
#ifdef USE_HDF5
#include <ch5/ch5.h>
#endif
#ifdef USE_VTK
class vtkUnstructuredGrid;
#endif


class DataOpacity;

class Model
{
  public:
    Model();
    ~Model();
    bool         read(const char *fn, bool base1, bool no_elems);
#ifdef USE_HDF5
    bool         read(hid_t hdf_file, bool base1, bool no_elems);
    bool         read_instance( hid_t, unsigned int, unsigned int, float *& );
#endif
#ifdef USE_VTK
    bool         read_vtu(const char* filename, bool no_elem, bool legacy=false );
    bool         read_objects(vtkUnstructuredGrid* grid, bool no_elem );
#endif
    bool         read_instance( gzFile, gzFile );
    int          add_surface_from_tri( std::string );
    int          add_surface_from_surf( std::string, bool=false );
    int          add_surface_from_elem( std::string );
    int          copy_surface( int s );
    int          numSurf(void){return _surface.size();}
    int          numSurfEle(void){int n=0;for(auto &s:_surface)n+=s->num();return n;}
    int          firstSurfEle(int s){return globalElemnum( s, 0 ); }
    Surfaces*    surface(int s){ return _surface[s]; }
	void         surfKill( int s );
    int          add_region_surfaces( void );
    int          add_cnnx_from_elem( std::string );
    int          reg_first( int s, Object_t t ){return _region[s]->first(t); }
    RRegion*     region(int s){ if(s>=0)return _region[s];else return (RRegion*)(_region); }
    const GLfloat*  pts(int t=0)const {return pt.pt(0);} // in future, pts move
    int    number( Object_t );
    const int*   volEle(int a=0)   const { return _vol[a]->obj(0); }
    int          numVol(){ return _vol.size(); }
    SurfaceElement*  element(int s,int a=0) const{ return _surface[s]->ele(a); }
    const int*   cnnx(int a=0)    const { return _cnnx->obj(a); }
    int          num_cnnx() const {if(_cnnx)return _cnnx->num();else return 0;}
    float        maxdim()         const { return _maxdim; }
    void         draw_tet( int, bool, DATA_TYPE* );
    void         hilight( HiLiteInfoWin*, int * );
    void         showobj( Object_t obj, bool *, bool f );
    bool         showobj( Object_t o, int s ) {return _region[s<0?0:s]->show(o);}
    bool         visibility( int s ){return _region[s<0?0:s]->visible();}
    void         visibility( int, bool a );
    GLfloat      opacity( int s ){return (_region[s<0?0:s]->get_color(Surface))[3];}
    GLfloat*     get_color( Object_t obj, int s=0 );
    void         set_color( Object_t obj, int s, float r, float g, float b, float a );
    void         set_mat( int s, float d, float sp, float sh, float b=0.5 );
    void         get_mat( int s, float &d, float &sp, float &sh, float &b );
    void         opacity( int s, float opac );
    void         randomize_color( Object_t obj );
    void         hilight_info( HiLiteInfoWin*, int*, DATA_TYPE *d=NULL, bool eb=false );
    void         stride( Object_t o, int s ){_outstride[o]=s;}
    int          stride( Object_t o ) const {return _outstride[o]; }
    const        GLfloat* pt_offset() const {return pt.offset();}
    bool         base1() const {return _base1; }
    const        short_float* vertex_normals(Surfaces*);
    const        GLfloat* elem_vertex_normals(Surfaces*, int e);
    void         flip_norms(){ for( auto s : _surface ) s->flip_norms(); }
    int          maxtm(){ return _numtm-1 ; }
    void         threeD( Object_t o, int r, bool b ){ _region[r<0?0:r]->threeD(o,b); }
    bool         threeD( Object_t o, int r ){ return _region[r<0?0:r]->threeD(o); }
    bool         threeD( Object_t o ){bool d=false; for(int r=0;r<_numReg;r++)d|=threeD(o,r);return d;}
    void         size( Object_t o, int r, float s ){ _region[r<0?0:r]->size(o, s); }
    float        size( Object_t o, int r ){ return _region[r<0?0:r]->size(o); }
    float        max_size( Object_t o );
    const char  *file()const{return _file.c_str();}
    bool         twoD(){return _2D; }  //!< is model a sheet in z-plane?
    Quaternion   syncRefRot( void ) {return _refRot; }
    void         syncRefRot( const Quaternion &q  ) { _refRot = q; }
    void         associate_surf_with_vol();

    PPoint             pt;
    Connection*      _cnnx=NULL;
    std::vector<VolElement*> _vol;
    int              _numReg=0;
    int               localElemnum(int, int&);
    int               globalElemnum(int, int);
    MultiPoint*      _elems;
  private:
    void             read_region_file( std::string );
    void             read_normals( gzFile, const char * );
    int             _outstride[maxobject];// stride to use when outputting
    void             find_max_dim_and_bounds();
    void             determine_regions();
    bool             read_elem_file( std::string );
    void             increase_ele( int );
    bool             check_element( SurfaceElement *e );
#ifdef USE_HDF5
    bool             read_elements(hid_t);
    bool             add_elements(hid_t hdf_file);
    void             add_regions(hid_t hdf_file);
    void             add_surfaces(hid_t hdf_file);
    void             add_surfaces(int *elements, int count, int max_width, char *name);
#endif
#ifdef USE_VTK
    bool             read_elements( vtkUnstructuredGrid* grid );
    bool             add_surfaces( vtkUnstructuredGrid* grid );
#endif
    
    RRegion**        _region=NULL;       //!< element regions
    std::vector<Surfaces*> _surface;
    float           _maxdim;		     //!< maximum physical dimension
    bool            _base1=false;  	     //!< whether node numbering starts at 1
    short_float*    _vertnrml=NULL;		 //!< vertex normals
    std::vector<bool>     allvis;
    int             _numtm;
    int              new_region_label();
    std::string     _file;               //!< base file name
    bool            _2D=false;
    Quaternion      _refRot;
    AsciiReader     _elemfile;
    bool            _surf2vol=false;     //!< volumes associated with surfaces determined
    std::string     bin_or_ascii_elem( std::string fn );
    bool            read_elem_binary( std::string elemf );
};

#endif
