/** \file Wrap up disparate data files into one HDF5 file
 */
#include <iostream>
#include <sstream>
#include <cstdio>
#include <ctype.h>
#include <string.h>
#include <sys/stat.h>
#include "IGBheader.h"
#include "ch5/ch5.h"
#include "cmdline.h"
#include <string>
using namespace std;

/** macro to check for possibly gzipped filename extension
 *
 *  \param A the flename
 *  \param B the extension
 */
#define GZIPPED_OR_NOT(A,B) \
    ( strstr(A+strlen(A)-strlen(B),   B      ) || \
    strstr(A+strlen(A)-strlen(B)-3, B".gz" )     )

#define OPT_START(A,B) (!strncmp(A,B,strlen(B)))

#define LABEL      "l",  'l'
#define UNITS      "u",  'u'
#define TM_UNITS   "tm", 's'
#define QUANTITIES "q",  'q'
#define COMMENT    "c",  'c'
#define DT         "dt", 'd'
#define T0         "t0", 'o'
#define BINTYPE    "b",  'b'

#define NodL      "--no"
#define NodS      "-n"
#define VecL      "--v"
#define VecS      "-v"
#define TSL       "--ti"
#define TSS       "-T"
#define AuxL      "--au"
#define AuxS      "-a"
#define AsAuxL    "--as"
#define AsAuxS    "-g"
#define DynL      "--dy"
#define DynS      "-d"
#define TxtL      "--te"
#define TxtS      "-s"
#define MshL      "--ms"
#define MshS      "-m"
#define TmAnnL    "--tm"
#define TmAnnS    "-i"   // should be unmatchable

enum Group{ Txt, Mshz_State, Tm_Annot } ;


bool
has_ext( string fn, const char *ext )
{
  return fn.size()>=strlen(ext)+1 && fn.substr(fn.size()-strlen(ext))==ext;
}


/** read in and convert binary data
 *
 * \tparam D destination buffer
 * \tparam T source binary type
 *
 * \param out final buffer
 * \param n   number of elements
 * \param data_in input file
 *
 * \pre \p out is allocated
 * \post \p out is populated
 */
template<class T, class D>
void buffer_data( D *out, int n, gzFile data_in )
{
  T* src = new T[n];
  gzread( data_in, src, sizeof(T)*n );
  for( int i=0; i<n; i++ )
    out[i] = src[i];
  delete[] src;
}


/* try opening a gzipped file
 *
 * \param fn filename
 */
gzFile
open_gzip( const char *fn )
{
  gzFile f = gzopen( fn, "r" );
  if( f==NULL ) {
    printf( "Error opening file: %s\n\n", fn );
    exit(1);
  }
  return f;
}


/** get number of bytes in a file
 */
u_long
count_bytes( const char *fn )
{
  struct stat buf;
  stat( fn, &buf );
  return buf.st_size;
}


/** return the first line not containing data, ignore commented lines
 *
 * \param in  input file
 * \param buf buffer
 * \param sz  size of buf
 *
 * \return NULL if error or EOF, buf o.w.
 * \post \p buf will contain data
 */
char *scan_line( gzFile in, char *buf, int sz )
{
  int i;
  do {
    if( gzgets( in, buf, sz )== NULL )
      return NULL;
    i=0;
    while( isspace(buf[i]) )
      i++;
  } while( buf[i]=='#' );
  return buf;
}


/** open an associated file which has the same basename but different suffix
 *  and possibly gzipped
 *  
 *  \note each file may or may not be gzipped
 *
 * \param fn          original filename
 * \param oldext      old extension
 * \param newext      new extension
 * \param[out] new_fn if requested, new filename
 *
 * \return file pointer to assciated file, NULL on failure
 */
gzFile
associated_file( const char *fn, const char *oldext, const char *newext,
                                             char ** new_fn=NULL )
{
  string vfn = fn;
  // remove ".gz" if present
  if( vfn.find( ".gz", vfn.size()-3 ) != string::npos )
    vfn = vfn.substr(0,vfn.size()-3);
  // remove old extension and add the new
  vfn = vfn.substr(0, vfn.size()-strlen(oldext)) + newext;

  gzFile vin = gzopen( vfn.c_str(),"r" );
  if( vin==NULL )
    vin  = gzopen( (vfn+=".gz").c_str(), "r" );
  
  if( new_fn )
    *new_fn = strdup( vfn.c_str() );
  return vin;
}


/** return the position of an option in word list
 *
 * \param argc  number of words
 * \param argv  words
 * \param lopt  long option
 * \param sopt  short option
 * \param which which occurrence (0-based)
 *
 * \return index if found, -1 o.w.
 */ 
int
find_index( int argc, char **argv, const char *lopt, const char *sopt, int which )
{ 
  int found = 0;
  for( int i=1; i<argc; i++ ) {
    if( !strncmp( argv[i], lopt, strlen(lopt)  ) ||
        !strncmp( argv[i], sopt, strlen(sopt)  )  )
      if( found++ == which )
        return i;
  }
  return -1;
}


/** determine if a new data file specified in option list
 *
 * \param opt option being examined
 *
 * \return true if a new file keyword, false o.w.
 */
bool
file_start( const char *opt )
{
  if( OPT_START(opt,NodL)   ||   // --nodal
      OPT_START(opt,NodS)   ||
      OPT_START(opt,VecL)   ||   // --vector
      OPT_START(opt,VecS)   ||    
      OPT_START(opt,TSL)    ||   // --time-series
      OPT_START(opt,TSS)    || 
      OPT_START(opt,AuxL)   ||   // --auxgrid
      OPT_START(opt,AuxS)   ||
      OPT_START(opt,AsAuxL) ||   // --as-auxgrid
      OPT_START(opt,AsAuxS) ||
      OPT_START(opt,DynL)   ||   // --dynpts
      OPT_START(opt,DynS)   ||
      OPT_START(opt,TmAnnL) ||   // --tm-annot
      OPT_START(opt,TmAnnS) ||   // 
      OPT_START(opt,MshL)   ||   // --mshz
      OPT_START(opt,MshS)   ||
      OPT_START(opt,TxtL)   ||   // --text
      OPT_START(opt,TxtS)     )
    return true;
  else
    return false;
}


/** return the option value if present for specified file
 *
 * \param long    long version of option
 * \param short   short option version
 * \param argc    number of words
 * \param argv    words
 * \param fn_index[in/out] on input, index file under consideration,
 *                         on output, word in which opton value found
 * \param empty   on failure, return an empty string instead of NULL
 *
 * \return option if present,\p NULL or "" o.w.
 * \Note   on failure, \p fn_index is unchanged
 * \note   searching begins at the word \p fn_index+1
 */
const char*
get_option_val( const char *lng, char shrt, int argc,
                             char **argv, int &fn_index, bool empty=false )
{
  if( fn_index==-1 )
    return NULL;

  char lg[1024];
  snprintf( lg, 1024, "--%s", lng );
  char sh[1024];
  snprintf( sh, 1024, "-%c", shrt );

  for( int i=fn_index+1; i<argc; i++ ) {
    if( file_start( argv[i] ) )
      return empty?"":NULL;
    if( OPT_START( argv[i], lg ) ) { // --l
      if( index( argv[i], '=' ) ){   // --l=
        fn_index = i;
        return index( argv[i], '=' )+1;
      } else {
        return argv[i+1];            // --l by itself
      }
    }
    if( !strcmp( argv[i], sh ) ) {// -s by itself
      fn_index = i+1;
      return argv[i+1];
    }
    if( OPT_START( argv[i], sh ) ) {// -s(opt)
      fn_index = i;
      return argv[i]+2;
    }
  }
  return empty?"":NULL;
}


/** count noncommented lines in a file 
 *
 * \param fn filename
 *
 * \return number of lines
 *
 * \note empty lines and commented lines having '#" as the first byte 
 *       and are not counted
 */
int
count_lines( const char *fn )
{
  gzFile in = open_gzip( fn );
  int count = 0;

  char buf[2048];
  while( gzgets( in, buf, 2048 ) != NULL && buf[0]!='#' && buf[0]!='\n' )
    count++;
  gzclose( in );

  return count;
}


/** return the number of nodes, going right from filename, and if none, going left
 *
 * \param argc number of words
 * \param argv words
 * \param fn_i index of filename
 *
 * \return number of nodes if found, -1 o.w.
 */
int
find_numnodes( int argc, char **argv, int fn_i )
{
  for( int i=fn_i+1; i<argc; i++ ) {
    if( file_start( argv[i] ) )
      break;
    if( OPT_START( argv[i], "--nu") ) {// --numnodes 
      char *ptr = index( argv[i], '=' );
      if(ptr) 
        return atoi(ptr+1);
      else
        return atoi(argv[i+1]);
    }
    if( !strcmp( argv[i], "-N" ) )
      return atoi( argv[i+1] );
    if( OPT_START( argv[i], "-N" ) )
      return atoi(argv[i]+2);
  }
  for( int i=fn_i-1; i>0; i-- ) {
    if( OPT_START( argv[i], "--nu") ) {
      char *ptr = index( argv[i], '=' );
      if(ptr) 
        return atoi(ptr+1);
      else
        return atoi(argv[i+1]);
    }
    if( !strcmp( argv[i], "-N" ) )
      return atoi( argv[i+1] );
    if( OPT_START( argv[i], "-N" ) )
      return atoi(argv[i]+2);
  }
  return -1;
}


/**
 * @brief delete a data file
 *
 * @param h5out  HDF5 file
 * @param delopt dataset to be removed (type:name)
 *
 * \return 0 on success
 */
void
delete_data( hid_t h5out, string delopt ){
  string type=delopt.substr(0,delopt.find_last_of(":"));
  string name=delopt.substr(delopt.find_last_of(":")+1);
  herr_t status=0;
  if( type=="n"|| type=="d" )
    status =  ch5s_nodal_del( h5out, name.c_str() );
  else if( type=="v" ) 
    status =  ch5s_vector_del( h5out, name.c_str() );
  else if( type=="a" || type=="g" ) 
    status =  ch5s_aux_time_del( h5out, name.c_str() );
  else if( type=="m" ) 
    status =  ch5_state_del( h5out, name.c_str() );
  else if( type=="a" ) 
    status =  ch5_text_del( h5out, name.c_str() );
  else if( type=="i" ) 
    status =  ch5_annot_del( h5out, name.c_str() );
  else if( type=="T" ) 
    status =  ch5s_series_del( h5out, name.c_str() );
  else
    status = 1;

  if( status ) {
    cerr << "Data set not found: " << delopt << endl;
    exit(1);
  }
}


/** put an IGB file into an HDF5 file
 *
 * \param h5out
 * \param fn
 * \param label
 * \param type
 */
void
add_nodal_igb( hid_t h5out, const char *fn, const char* label, 
                                                   ch5s_nodal_type type ) 
{
  IGBheader h(open_gzip(fn));
  if( h.read() )
    exit(1);

  int grid_i =  
     ch5s_nodal_create_grid( h5out, h.x()*h.y()*h.z(), h.t(), h.org_t(),
             h.inc_t(), type, label, h.unites_t(), h.unites(), NULL );
  if( grid_i < 0 ) {
    printf( "Cannot create grid: %s\n\n", fn );
    exit(1);
  }

  float *data = new float[h.x()*h.y()*h.z()*h.data_size()/sizeof(float)];
  for( int i=0; i< h.t(); i++ ) {
    h.read_data( data, 1, NULL );
    ch5s_nodal_write( h5out, grid_i, i, i, data );
  }
  delete[] data;
  h.close();
}


/** write data from an ASCII or binary data file into the HDF5 file. ASCII files with multiple
 *  fields may select the field by appending ::N to the filename where N is the 0-based column number
 * 
 * \param h5out  HDF5 file
 * \param fn     filename
 * \param argc
 * \param argv
 * \param which  which nodal file occurrence
 *
 * \note if \p btype option is found, assume a binary file with no 
 *       header, o.w., ASCII
 */
void
add_nodal( hid_t h5out, string fn, int argc, char **argv, int which ) 
{
  int fi, fn_i = find_index( argc, argv, NodL, NodS, which );

  int nnodes=find_numnodes( argc, argv, fn_i );
  if( nnodes<1 ) {
    printf( "\nError: numnodes not specified for non-IGB nodal file: %s", fn.c_str());
    exit(1);
  }  

  const char *units   = get_option_val( UNITS,    argc, argv, fi=fn_i ); 
  const char *label   = get_option_val( LABEL,    argc, argv, fi=fn_i ); 
  const char *t_units = get_option_val( TM_UNITS, argc, argv, fi=fn_i ); 
  const char *comment = get_option_val( COMMENT, argc, argv, fi=fn_i ); 
  const char *bintype = get_option_val( BINTYPE, argc, argv, fi=fn_i ); 
  float       t0      = atof(get_option_val( T0, argc, argv, fi=fn_i, true )); 

  int numt;
  int elesz = 0;
  string scanner;                     //!< scan str for ASCII nodal
                                      //
  if( bintype ){
    if( !strcmp(bintype,"float") ) 
      elesz=sizeof(float);
    else if( !strcmp(bintype,"double") ) 
        elesz=sizeof(double);
    else if( !strcmp(bintype,"int") ) 
        elesz=sizeof(int);
    else if( !strcmp(bintype,"uint") ) 
        elesz=sizeof(unsigned int);
    numt = count_bytes( fn.c_str() )/elesz/nnodes;
  } else {
    size_t fld, nf;
    if( (fld=fn.rfind("::")) != string::npos ) {
      try {
        nf=stoi(fn.substr(fld+2));
      }
      catch(...){
        cerr << "could not parse field number: " << fn.substr(fld) << endl;
        throw 1;;
      }
      for(int i=0; i<nf; i++)
        scanner += "%*f ";
      scanner += "%f";
      fn.erase(fld);
    } else {
      scanner = "%f";
    }
    numt = count_lines( fn.c_str() )/nnodes;
  }

  if( !numt ) {
    printf( "\nFile does not have enough data: %s\n\n", fn.c_str() );
    exit(1);
  }

  int grid_i = ch5s_nodal_create_grid( h5out, nnodes, numt, t0, 1, CH5_SCALAR,
                                      label, t_units, units, comment );

  if( grid_i < 0 ) {
    printf( "Cannot create grid: %s\n\n", fn.c_str() );
    exit(1);
  }

  gzFile data_in     = open_gzip( fn.c_str() );
  float *data_buffer = new float[nnodes];

  for( int i=0; i<numt; i++ ) {
    if( bintype ) {
      if( !strcmp(bintype,"float") ) 
        buffer_data<float>( data_buffer, nnodes, data_in );
      else if( !strcmp(bintype,"double") ) 
        buffer_data<double>( data_buffer, nnodes, data_in );
      else if( !strcmp(bintype,"int") ) 
        buffer_data<int>( data_buffer, nnodes, data_in );
      else if( !strcmp(bintype,"uint") ) 
        buffer_data<unsigned int>( data_buffer, nnodes, data_in );
    } else {
      char buf[2048];
      for( int j=0; j<nnodes; j++ ) {
        gzgets( data_in, buf, 2048 );
        sscanf( buf, scanner.c_str(), data_buffer+j );
      }
    }
    ch5s_nodal_write( h5out, grid_i, i, i, data_buffer );
  }
  gzclose( data_in );
  delete[] data_buffer;
}


/** write vector data to HDF5 file
 *
 * \param h5out  HDF5 data file
 * \param fn     data file (.vpts)
 * \param argc
 * \param argv
 * \param which  which vector file occurrence
 */
void
add_vector_data( hid_t h5out, const char *fn, int argc, char **argv, int which ) 
{
  int fi, fn_ind = find_index( argc, argv, VecL, VecS, which );
  char buf[2048];
  
  const char *comment = get_option_val( COMMENT, argc, argv, fi=fn_ind ); 
  // units may appear twice, first for vector then scalar data
  const char* v_units = get_option_val( UNITS, argc, argv, fi=fn_ind ); 
  const char *s_units = NULL;
  if( v_units )
    s_units = get_option_val( UNITS, argc, argv, fi ); 

  // labels may appear twice, first for vector then scalar data
  const char* vlabel = get_option_val( LABEL, argc, argv, fi=fn_ind );
  const char* slabel = NULL;
  if( vlabel )
    slabel = get_option_val( LABEL, argc, argv, fi );

  // open point file and get the number of nodes
  gzFile pt_in = open_gzip( fn );
  int numnodes;
  gzgets( pt_in, buf, 2048 );
  sscanf( buf, "%d", &numnodes );

  // open vector data file
  char *vec_fn=NULL;
  gzFile vin;
  if(!(vin=associated_file( fn, "vpts", "vec", &vec_fn ))) {
    printf( "Error: no matching vector file for pts file: %s\n\n", vec_fn );
    exit(1);
  }

  bool is_igb=false;
  int num_t, dt, num_data;
  const char* t_units;
  float t0;

  // determine whether vec file is IGB or ASCII
  IGBheader h(vin);
  if( h.read(true) ) {
    gzrewind( vin );
    num_t    = count_lines( vec_fn )/numnodes;
    dt       = atof(get_option_val( DT, argc, argv, fi=fn_ind, true )); 
    t_units  = get_option_val( TM_UNITS, argc, argv, fi=fn_ind ); 
    gzgets( vin, buf, 2048 );
    float a[4];
    num_data = sscanf( buf, "%f %f %f %f", a, a+1, a+2, a+3 );
    gzrewind( vin );
  } else {
    is_igb = true;
    num_t   = h.t();
    dt      = h.inc_t();
    t0      = h.org_t();
    t_units = h.unites_t();
    num_data= h.type()==IGB_VEC3_f ? 3 : 4;
  }
  if( vec_fn ) free( vec_fn );

  if( num_data==4 && !slabel )
    slabel = "scalar data";

  // output nodal positions
  float  *pt = new float[3*numnodes];
  for( int i=0; i<numnodes; i++ ) {
    scan_line( pt_in, buf, 2048 );
    sscanf( buf, "%f %f %f", pt+i*3, pt+3*i+1, pt+i*3+2 );
  }

  int grid_i =  ch5s_vector_create_grid( h5out, numnodes, num_t, t0, dt, pt, 
                  vlabel, num_data==3? NULL : slabel, t_units, v_units, comment );  
  delete[] pt;

  //output vector data
  float *data = new float[numnodes*num_data];
  for( int i=0; i< num_t; i++ ) {
    if( is_igb ) {
      h.read_data( data, 1, NULL );
    } else {
      float a[4];
      for( int j=0; j<numnodes; j++ ) {
        scan_line( vin, buf, 2048 );
        sscanf( buf, "%f %f %f %f", a, a+1, a+2, a+3 );
        for( int k=0; k<num_data; k++ )
          data[j*num_data+k] = a[k];
      }
    }
    ch5s_vector_write( h5out, grid_i, i, i, data );
  }
  delete[] data;

  gzclose( vin );
}


/**
 * @brief  write an auxiliary grid
 *
 * @param ptin       points file
 * @param num_pt_t   number of times for points
 * @param elem_in    elements file
 * @param num_ele_t  number of times for elements
 * @param data_in    pointer to data file or an IGBheader
 * @param num_dat_t  number of times for data
 * @param h5out      output file
 * @param ai         grid index
 * @param fn         aux_grid:datafile 
 * @param fake       really a regular grid?
 * @param copy       index of aux_grid to copy, make links to pts & elems if >=0
 *
 * \note  if \p fn ends in ".igb", data will be treated as an IGB file, o.w. ASCII
 *
 * \post input files are closed
 */
void
write_aux_grid( gzFile ptin, int num_pt_t, gzFile elem_in, int num_ele_t, 
                void*  data_in, int num_dat_t, hid_t h5out, int ai, const char *fn,
                                              bool fake = false, int copy=-1 )
{
  char buf[2048];

  int num_t = num_pt_t;
  if( elem_in ) num_t = max( num_ele_t, num_pt_t );
  if( data_in ) num_t = max( num_dat_t, num_pt_t );

  float *pt  = NULL;
  bool   igb = false;
  int    np  = 0;

  for( int i=0; i<num_t; i++ ) {
    
    int             ne    = 0;
    float          *dat       = NULL;
    unsigned int   *ele_lst   = NULL;
    int             max_width = 0;

    if( (!i || num_pt_t>1) && copy<0 ) {
      scan_line( ptin, buf, 2048 );
      sscanf( buf, "%d", &np );
      pt = new float[3*np];
      for( int j=0; j<np; j++ ) {
        scan_line( ptin, buf, 2048 );
        sscanf( buf, "%f %f %f", pt+j*3, pt+j*3+1, pt+j*3+2 );
      }
    } else if( copy>=0 && !i ) {
      ch5s_aux_time_step info;
      ch5s_aux_time_step_info( h5out, copy, 0, &info );
      np = info.num_points;
    }

    if( elem_in && (!i || num_ele_t>1) && copy<0 ) {
      int max_sz=0, cur_sz=0;
      scan_line( elem_in, buf, 2048 );
      sscanf( buf, "%d", &ne );
      for( int j=0; j<ne; j++ ) {
        scan_line( elem_in, buf, 2048 );
        istringstream oss( buf );
        string type;
        oss >> type;
        ch5m_element_type itype = 
          static_cast<ch5m_element_type>(ch5m_elem_get_type_by_prefix( type.c_str()));
        int sz=ch5m_elem_get_width_for_type(itype);
        if( sz>max_width )
          max_width = sz;
        if( cur_sz+sz+1>max_sz ) {
          max_sz += 1000000;
          ele_lst = (unsigned int *)realloc( ele_lst, max_sz*sizeof(int) );
        }
        ele_lst[cur_sz++] = itype;
        for(int k=0; k<sz; k++)
          oss >> ele_lst[cur_sz++];
      }
    } else if( elem_in )
      ne = -1;

    if( data_in ) {
      igb = has_ext( fn, ".igb" );
      if( !fake && !igb ) {
        int nd;
        scan_line( (gzFile)data_in, buf, 2048 );
        sscanf( buf, "%d", &nd );
        if( nd != np ) {
          printf( "Aux grid #points != #data: %s\n\n", fn );
          exit(1);
        }
      } 
      dat = new float[np];
      if( igb )
        ((IGBheader*)data_in)->read_data(dat,1);
      else {
        for( int j=0; j<np; j++ ) {
          scan_line( (gzFile)data_in, buf, 2048 );
          sscanf( buf, "%f", dat+j );
        }
      }
    }

    int retval;
    if( !i && copy>=0 )
      retval = ch5s_aux_write_copy( h5out, ai, np, dat, copy );
    else 
      retval = ch5s_aux_write_next( h5out, ai, np, (!i||num_pt_t>1)?pt:NULL, 
                                                ne, max_width, ele_lst, dat );

    free( ele_lst );
    if( (num_pt_t>1 || i==num_t-1) && pt ) delete[] pt;
    delete[] dat;

    if( retval ) {
      printf( "\nFailure writing AuxGrid (%s) at time %d\n", fn, i );
      exit(1);
    }
  }

  if( ptin  && copy<0)
    gzclose( ptin );
  if( elem_in && copy<0 )
    gzclose( elem_in );
  if( data_in  && igb) 
    ((IGBheader*)data_in)->close();
  else
    gzclose( (gzFile)data_in );
}


void
add_auxgrid( hid_t h5out, const char *fn, int argc, char **argv, int which ) 
{
  int   fi,   fn_i     = find_index( argc, argv, AuxL, AuxS, which );
  const char *units    = get_option_val( UNITS,    argc, argv, fi=fn_i ); 
  const char *tm_units = get_option_val( TM_UNITS, argc, argv, fi=fn_i ); 
  const char *label    = get_option_val( LABEL,    argc, argv, fi=fn_i ); 
  const char *comment  = get_option_val( COMMENT,  argc, argv, fi=fn_i ); 
  float       dt       = atof(get_option_val( DT, argc, argv, fi=fn_i, true )); 
  float       t0       = atof(get_option_val( T0, argc, argv, fi=fn_i, true )); 
  
  gzFile ptin = open_gzip( fn );
  char buf[2048];
  scan_line( ptin, buf, 2048 );
  int num_pt_t;
  sscanf( buf, "%d", &num_pt_t );

  int ai = ch5s_aux_create_grid( h5out, t0, dt, label, tm_units, units, comment );

  gzFile elem_in = associated_file( fn, "pts_t", "elem_t" );
  gzFile data_in = associated_file( fn, "dat_t", "dat_t"  );

  int num_ele_t=0, num_dat_t=0;
  if( elem_in ){
    scan_line( elem_in, buf, 2048 ); 
    sscanf( buf, "%d", &num_ele_t );
    if( num_ele_t!=1 && num_ele_t!=num_pt_t && num_pt_t!=1 ){
      fprintf(stderr,"number of times do not match for elems and pts: %s\n",fn);
      exit(1);
    }
  }
  int num_t = max(num_ele_t, num_pt_t );

  if( data_in ){
    scan_line( data_in, buf, 2048 ); 
    sscanf( buf, "%d", &num_dat_t );
    if( num_dat_t!=num_t && num_t!=1 ){
      fprintf(stderr,"number of data times do not match data/pts: %s\n",fn);
      exit(1);
    }
  }
  write_aux_grid( ptin, 1, elem_in, 1, data_in, num_dat_t, h5out, ai, fn ); 
}


/**
 * @brief write out normal grid as auxiliary grid
 *
 * @param h5out the HDF5 file
 * @param fn    model filename
 * @param data  optional data file name
 * @param argc  
 * @param argv
 * \param which  which fake auxgrid file occurrence
 *
 * \return the number of grids added
 */
int
fake_auxgrid( hid_t h5out, const char *fn, const char *data, int argc, char **argv, int which ) 
{
  int   fi,   fn_i     = find_index( argc, argv, AsAuxL, AsAuxS, which );
  const char *units    = get_option_val( UNITS,    argc, argv, fi=fn_i ); 
  const char *tm_units = get_option_val( TM_UNITS, argc, argv, fi=fn_i ); 
  const char *comment  = get_option_val( COMMENT,  argc, argv, fi=fn_i ); 
  std::string label    = get_option_val( LABEL,    argc, argv, fi=fn_i, true ); 
  float       dt       = atof(get_option_val( DT, argc, argv, fi=fn_i,  true )); 
  float       t0       = atof(get_option_val( T0, argc, argv, fi=fn_i,  true )); 
  std::string datastr(data?data:"");

  gzFile ptin = open_gzip( fn );
  int numnodes;
  char buf[2048];
  gzgets( ptin, buf, 2048 );
  sscanf( buf, "%d", &numnodes );
  gzrewind(ptin);
  gzFile elem_in = associated_file( fn, "pts", "elem" );
  int orig_gid;
  int ndata = data?  std::count( datastr.begin(), datastr.end(), ':' )+1 : 0;
  int nlabs = label.size() ? std::count( label.begin(),   label.end(),   ':' )+1 : 0;

  for( int j=0; j<ndata; j++ ){
    
    // extract label from colon separated list
    std::string datlab;
    if( label.length() && j<nlabs ){
      size_t stop = label.find(":");
      datlab = label.substr(0,stop);
      label=label.substr(stop+1);
    } else
      datlab = std::to_string(ch5s_aux_grid_count(h5out))+"_data";

    int        num_dat_t = 0;
    gzFile     data_in   = NULL;
    IGBheader* hdr       = NULL;
    std::string dataname(fn);

    if( data ){
      // extract label from :-separated list
      size_t stop = datastr.find(":");
      std::string datafile = datastr.substr(0,stop);
      datastr=datastr.substr(stop+1);

      // get number of time steps in data
      if( has_ext(datafile,".igb") ){
        data_in = gzopen( datafile.c_str(), "r" );
        hdr = new IGBheader(data_in,true);
        num_dat_t = hdr->t();
        t0        = hdr->org_t();
        dt        = hdr->inc_t();
        tm_units  = hdr->unites_t();
        units     = hdr->unites();
      } else {
        int numdat = count_lines(datafile.c_str());
        num_dat_t  = numdat/numnodes;
      }

      if( num_dat_t && !data_in ) data_in = gzopen( datafile.c_str(), "r" );
      dataname += ":";
      dataname += datafile;
    }
    int ai = ch5s_aux_create_grid( h5out, t0, dt, datlab.c_str(), tm_units, units, comment );
    if( !j ) orig_gid = ai;

    write_aux_grid( ptin, 1, elem_in, 1, hdr?hdr:(void*)data_in, num_dat_t, h5out, 
                                        ai, dataname.c_str(), true, j?orig_gid:-1); 
  }
  return ndata;
}


/** add free format text to the data file
 *
 * \param h5out HDF5 file
 * \param fn    data file
 * \param argc  number of words
 * \param argv  word list
 * \param which which text file occurrence
 * \param grp   type of text file
 */
void
add_text( hid_t h5out, const char *fn, int argc, char **argv, int which, Group grp ) 
{
  int fi, fn_i;
  if( grp == Txt )
   fn_i= find_index( argc, argv, TxtL, TxtS, which );
  else if( grp == Mshz_State )
   fn_i= find_index( argc, argv, MshL, MshS, which );
  else if( grp == Tm_Annot )
   fn_i= find_index( argc, argv, TmAnnL, TmAnnS, which );

  const char *label = get_option_val( LABEL, argc, argv, fi=fn_i ); 
  if( !label ) label = strdup(fn);
  const char *comment = get_option_val( COMMENT, argc, argv, fi=fn_i ); 

  gzFile in = open_gzip( fn );
  int ti;
  if( grp == Txt )
    ti = ch5_text_create( h5out, label, comment );
  else if( grp == Mshz_State )
    ti = ch5_state_create( h5out, label, comment );
  else if( grp == Tm_Annot )
    ti = ch5_annot_create( h5out, label, comment );

  const int chunk=1000001;
  int       nread;
  char *buf = new char[chunk];
  do { 
    nread=gzread( in, buf, chunk-1);
    buf[nread] = '\0';
    if( grp == Txt )
      ch5_text_append( h5out, ti, buf );
    else if( grp == Mshz_State )
      ch5_state_append( h5out, ti, buf );
    else if( grp == Tm_Annot )
      ch5_annot_append( h5out, ti, buf );
  } while( nread==chunk );

  gzclose( in );
}


/** add a timeseries to the data file
 *
 * \param h5out HDF5 file
 * \param fn    data file
 * \param argc  number of words
 * \param argv  word list
 * \param which which time series file occurrence
 */
void
add_timeseries( hid_t h5out, const char *fn, int argc, char **argv, int which ) 
{
  int fi, fn_i = find_index( argc, argv, TSL, TSS, which );
  const char *units   = get_option_val( UNITS, argc, argv, fi=fn_i ); 
  const char *label   = get_option_val( LABEL, argc, argv, fi=fn_i ); 
  const char *quant   = get_option_val( QUANTITIES, argc, argv, fi=fn_i ); 
  const char *comment = get_option_val( COMMENT, argc, argv, fi=fn_i ); 

  // determine number of columns in data
  gzFile in = open_gzip( fn );
  bool white     = true; // true if last char read whitespace
  int  numfields = 0;
  int  byte;
  while( (byte=gzgetc(in)) != '\n' && byte!='#' ) {
    bool read_white = isspace( byte );
    if( white && !read_white ) 
      numfields++;
    white = read_white;
  } 
  gzrewind( in );

  int num_t = count_lines(fn);

  int si = ch5s_series_create( h5out, num_t, numfields-1, label, quant, 
                                                             units, comment );
  
  for( int i=0; i<num_t; i++ ) {
    double data[numfields];
    char   buf[2048];
    gzgets( in, buf, 2048 );
    char *lr=buf;
    for( int j=0; j<numfields; j++ ) {
     int nr;
     sscanf( lr, "%lf %n", data+j, &nr );
     lr += nr;
    }
    ch5s_series_write( h5out, si, i, i, data );
  }
  gzclose( in );
}


/** store original command line 
 *
 * \param argc 
 * \param argv
 *
 * \return a copy of argv
 */
char ** 
store_orig( int argc, char **argv )
{
  char **o = new char*[argc];
  for( int i=0; i<argc; i++ )
    o[i] = strdup( argv[i] );
  return o;
}


int 
main( int argc, char *argv[] )
{
  gengetopt_args_info args_info;

  char **orig = store_orig( argc, argv );

  // let's call our cmdline parser 
  if (cmdline_parser (argc, argv, &args_info) != 0)
    exit(1);

  if( argc == 1 || args_info.inputs_num != 1 ) {
    cmdline_parser_print_help();
    exit(0);
  }

  hid_t hdf_out;
  if( args_info.append_flag || args_info.delete_given )
    ch5_open( args_info.inputs[0], &hdf_out );
  else
    ch5_create( args_info.inputs[0], &hdf_out );

  for( unsigned int i=0; i<args_info.delete_given; i++ ) {
    delete_data( hdf_out, args_info.delete_arg[i] );
  }

  for( unsigned int i=0; i<args_info.nodal_given; i++ ) {
    int fi = find_index( argc, orig, NodL, NodS, i );
    const char*lbl = get_option_val( LABEL, argc, orig, fi );
    if( GZIPPED_OR_NOT( args_info.nodal_arg[i], "igb") )
      add_nodal_igb( hdf_out, args_info.nodal_arg[i], lbl, CH5_SCALAR );
    else
      add_nodal( hdf_out, args_info.nodal_arg[i], argc, argv, i );
  }

  for( unsigned int i=0; i<args_info.vector_given; i++ ) {
     add_vector_data( hdf_out, args_info.vector_arg[i], argc, orig, i );
  }
                   
  for( unsigned int i=0; i<args_info.dynpts_given; i++ ) {
    int fi = find_index( argc, orig, DynL, DynS, i);
    const char*lbl = get_option_val( LABEL, argc, orig, fi );
    add_nodal_igb( hdf_out, args_info.dynpts_arg[i], lbl, CH5_DYN_PTS );
  }
  
  for( unsigned int i=0; i<args_info.time_series_given; i++ ) {
    add_timeseries( hdf_out, args_info.time_series_arg[i], argc, argv, i );
  }

  for( unsigned int i=0; i<args_info.auxgrid_given; i++ ) {
    add_auxgrid( hdf_out, args_info.auxgrid_arg[i], argc, argv, i );
  }

  int n_asAuxGrid =0;
  for( unsigned int i=0; i<args_info.as_auxgrid_given; i++) {
    char *data = NULL;
    if( i<args_info.as_auxgrid_given-1 ){
      if( !has_ext(args_info.as_auxgrid_arg[i+1], ".pts") )
        data = args_info.as_auxgrid_arg[i+1];
    }
    fake_auxgrid( hdf_out, args_info.as_auxgrid_arg[i], data, argc, argv, n_asAuxGrid++ );
    if( data ) i++;
  }

  for( unsigned int i=0; i<args_info.mshz_given; i++ ) {
    add_text( hdf_out, args_info.mshz_arg[i], argc, argv, i, Mshz_State );
  }

  for( unsigned int i=0; i<args_info.tm_annot_given; i++ ) {
    add_text( hdf_out, args_info.tm_annot_arg[i], argc, argv, i, Tm_Annot );
  }

  for( unsigned int i=0; i<args_info.text_given; i++ ) {
    add_text( hdf_out, args_info.text_arg[i], argc, argv, i, Txt );
  }

  ch5_close( hdf_out );

  return 0;
}

