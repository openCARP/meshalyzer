FROM nvidia/opengl:base-ubuntu20.04

ARG name="meshalyzer-appimage-builder"
ARG maintainer="axel.loewe@kit.edu"
ARG description="Building meshalyzer AppImage"

LABEL name=${name} \
      maintainer=${maintainer} \
      description=${description}

ARG GID=1000
ARG UID=1000
ARG UNAME=meshalyzer

RUN groupadd --gid $GID --non-unique $UNAME
RUN useradd --create-home --non-unique --shell /bin/bash --uid $UID --gid $GID $UNAME

# Required to install tzdata
RUN ln -fs /usr/share/zoneinfo/Europe/Berlin /etc/localtime

# install required packages
RUN apt -y update && DEBIAN_FRONTEND=noninteractive apt -y install \
    build-essential \
    cmake \
    curl \
    file \
    freeglut3-dev \
    libfltk1.3-dev \
    gcc \
    g++ \
    git \
    libfreetype-dev \
    libglew-dev \
    libgomp1 \
    libjpeg-dev \
    libpng-dev \
    make \
    libgl1-mesa-dev \    
 && apt clean \
 && rm -rf /tmp/* /var/cache/apt/archives
RUN gcc --version | head -1

# install vtk 9 (ubuntu 20.04 only provides up to vtk7 via apt)
WORKDIR /tmp
RUN curl -L https://www.vtk.org/files/release/9.3/VTK-9.3.1.tar.gz | tar xz \
 && cd $(ls | grep VTK) \
 && mkdir _build \
 && cd _build \
 && cmake .. \
 && make install \
 && echo /usr/local/lib > /etc/ld.so.conf.d/usr-local.conf \
 && ldconfig \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /meshalyzer
COPY . .

RUN ./appimage/build.sh
RUN cd $(ls | grep build) && make install

USER $UNAME
ENTRYPOINT ["meshalyzer"]
