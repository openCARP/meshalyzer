# meshalyzer

Meshalyzer is a graphical program for display time dependent data on 3D finite elment meshes. Of course, it can show static data on fewer dimensions as well.
It is developed to be compatible with the cardiac simulation environment [openCARP](http://www.opencarp.org).

<p align="center">
  <img src="examples.png" width="1000" title="output examples" alt="examples">
</p>

It uses its own file formats which are simple and easily converted to/from more popular formats like VTK.

## Features

* can work with very large data files (TB)
* offscreen rendering
* isosurfaces / lines
* cutting planes
* output images (with metadata) or sequences of images to make movies
* can save state
* link multiple instances to maintain view, colour scale, etc. across them
* can display time-dependent scalar and vector data
* display auxiliary grids
* fly through model views

## Installation

Please follow steps in the [INSTALL](INSTALL.md) file.

## VTK formats

Support for reading the VTK formats, the legacy format (.vtk) as well the unstructured grid format (.vtu), has been included. You need to edit the **make.conf** manually to specify the directory containing the dynamically linked libraries (`VTK_LIBDIR`) and the directory with the VTK header files (`VTK_INCDIR`). They
are commented out by default.

For Mac users, the easiest is to install `vtk` via [homebrew](https://brew.sh/): `brew install vtk`

    VTK_LIBDIR = /usr/local/lib???
    VTK_INCDIR = /usr/local/include/vtk-8.?
    
## Opening the example

The `example` folder contains an ellipsoid mesh representing a simplified left ventricle. 
The mesh is provided in VTK binary and openCARP ASCII format. Additionally, you can find 
simulated transmembrane voltage data in igb format in `example/vm.igb`.
You can visualize these data using meshalyzer, to do that please follow the steps below:

* Open meshalyzer
* Navigate to the `example` folder and open the `ellipsoid.pts` file
* To load the simulated data on the mesh click on the `File` menu
* Click on `Read IGB data` and go to `example`
* Open `vm.igb`
* To do everything from the command line with the VTK mesh:
    `meshalyzer ellipsoid.vtk vm.igb`
* Read the manual to understand all the capabilities of the program

## Rendering Versions

Under Linux and Windows, rendering has been improved to take advantage of modern OpenGL features. 
This will affect chiefly lighting and rendering speed. If you are running on an Appl machine there is one additional consideration.
If your screen is a Retine display, make sure that the following is set in *make.conf*:

    HAVE_RETINA := 1

If it is not a retina screen, set this variable to 0. You will know if this setting is correct because if not, either only 1/4 of the window will be used, or you will only see one quarter of the model.

## Run-time issues

1. Most of the time, a bad mesh or bad data file is responsible for crashes.
    Ensure that
    1. node indexing starts at 0 and the number of nodes is correct
    2. there are no gaps in numbering
    3. the data file did not get truncated. Use
    `igbhead --jive`
    to correct if necessary.
    4. file formats are respected.

2. It is possible that internally, a bad memory operation is performed. Try setting
`export MALLOC_CHECK_=1`
and see if it disappears.

3. Make sure the proper dynamic libraries are getting linked. 

   There can be conflicts with *fltk* or *vtk* if
   multiple versions are installed. In these cases a simple solution is to write a 
   wrapper script which properly sets the environment variables like below:

    ```bash
    #!/bin/bash
    export LD_LIBRARY_PATH=/path_to_dylib1_dir:/path_to_dylib2_dir
    /path_to_meshalyzer/meshalyzer $*
    ```

4. Offscreen rendering crashes or produces a black file.

   Try changing the screen dump method by changing the value of **OFFSCREEN** in *make.conf*.
   If that does not work, try using *GLFW* instead of *GLUT* by setting 

    `GLUT_ON := 0` 

   in *make.conf* and retrying the different OFFSCREEN methods (make sure *GLFW* is installed).

#### Acknowledgement

The broadcast symbol was provided by [Online Web Fonts](http://www.onlinewebfonts.com).
