/**
* \file state.h
* \brief Meshalyzer state file related functions
*/

#ifndef _CH5_ANNOT_H
#define _CH5_ANNOT_H

#if !defined(_CH5_H) && defined(__cplusplus)
extern "C" {
#endif

#include "../text.h"

typedef ch5_text_file ch5_annot_file;

#define CH5_ANNOT_GROUP_NAME       "tm_annots"
#define CH5_ANNOT_FILE_NAME_PREFIX "TM_ANNOT"

#define ch5_annot_create(H,L,C) _ch5_text_create(H,L,C,CH5_ANNOT_GROUP_NAME) 
#define ch5_annot_info(H,i,I)   _ch5_text_info(H,i,I,CH5_ANNOT_GROUP_NAME)
#define ch5_annot_free_info(I)  ch5_text_free_info(I)
#define ch5_annot_flush( H, i)  _ch5_text_flush( H, i,CH5_ANNOT_GROUP_NAME)
#define ch5_annot_append(H,i,T) _ch5_text_append(H,i,T,CH5_ANNOT_GROUP_NAME)
#define ch5_annot_read(H, i, O) _ch5_text_read(H,i,O,CH5_ANNOT_GROUP_NAME)
#define ch5_annot_count(H)      _ch5_count(H,CH5_ANNOT_GROUP_NAME)
#define ch5_annot_del(H, S)     _ch5_delete( H, CH5_ANNOT_GROUP_NAME, S )//!< delete set \p S from file \p H


#if !defined(_CH5_H) && defined(__cplusplus)
}
#endif

#endif
