/**
* \file state.h
* \brief Meshalyzer state file related functions
*/

#ifndef _CH5_STATE_H
#define _CH5_STATE_H

#if !defined(_CH5_H) && defined(__cplusplus)
extern "C" {
#endif

#include "../text.h"

typedef ch5_text_file ch5_state_file;

#define CH5_STATE_GROUP_NAME       "mshz_states"
#define CH5_STATE_FILE_NAME_PREFIX "MSHZ_STATE"

#define ch5_state_create(H,L,C) _ch5_text_create(H,L,C,CH5_STATE_GROUP_NAME) 
#define ch5_state_info(H,i,I)   _ch5_text_info(H,i,I,CH5_STATE_GROUP_NAME)
#define ch5_state_free_info(I)  ch5_text_free_info(I)
#define ch5_state_flush( H, i)  _ch5_text_flush( H, i,CH5_STATE_GROUP_NAME)
#define ch5_state_append(H,i,T) _ch5_text_append(H,i,T,CH5_STATE_GROUP_NAME)
#define ch5_state_read(H, i, O) _ch5_text_read(H,i,O,CH5_STATE_GROUP_NAME)
#define ch5_state_count(H)      _ch5_count(H,CH5_STATE_GROUP_NAME)
#define ch5_state_del(H, S)     _ch5_delete( H, CH5_STATE_GROUP_NAME, S )//!< delete set \p S from file \p H


#if !defined(_CH5_H) && defined(__cplusplus)
}
#endif

#endif
