/**
* \file text.h
* \brief Text file related functions
*/

#ifndef _CH5_TEXT_H
#define _CH5_TEXT_H

#if !defined(_CH5_H) && defined(__cplusplus)
extern "C" {
#endif

#include "types.h"
#include <hdf5.h>

#define CH5_TEXT_GROUP_NAME       "text_files"
#define CH5_TEXT_FILE_NAME_PREFIX "TextFile"
#define CH5_TEXT_CHUNK_SIZE       100000

#define ch5_text_create(H,L,C) _ch5_text_create(H,L,C,CH5_TEXT_GROUP_NAME) 
#define ch5_text_info(H,i,I)   _ch5_text_info(H,i,I,CH5_TEXT_GROUP_NAME)
#define ch5_text_flush( H, i)  _ch5_text_flush( H, i,CH5_TEXT_GROUP_NAME)
#define ch5_text_append(H,i,T) _ch5_text_append(H,i,T,CH5_TEXT_GROUP_NAME)
#define ch5_text_read(H, i, O) _ch5_text_read(H,i,O,CH5_TEXT_GROUP_NAME)
#define ch5_text_count(H)      _ch5_count(H,CH5_TEXT_GROUP_NAME)
#define ch5_text_del(H, S)     _ch5_delete( H, CH5_TEXT_GROUP_NAME, S )
#define ch5_text_free(S)       _ch5_text_free( S )

int _ch5_text_create(hid_t hdf_file, const char *label, const char *comment, const char* group);

int _ch5_text_info(hid_t hdf_file, unsigned int index, ch5_text_file *info, 
                                                   const char*group);

int ch5_text_free_info(ch5_text_file *info);

int _ch5_text_flush( hid_t hdf_file, int index, const char* group);

int _ch5_text_append(
  hid_t hdf_file,
  unsigned int file_index,
  const char *in,
  const char* group
);

int _ch5_text_read(
  hid_t hdf_file,
  unsigned int file_index,
  char ***out,
  const char* group
);

void _ch5_text_free( char ***txt );

#if !defined(_CH5_H) && defined(__cplusplus)
}
#endif

#endif
