#!/bin/bash
# build meshalyzer and generate its appimage

BUILD_DIR=_build

PARENT_PATH=$( cd $(dirname $BASH_SOURCE) ; pwd -P )
MESHALYZER_DIR=$PARENT_PATH/..
BUILD_APP_DIR=$MESHALYZER_DIR/$BUILD_DIR

# build meshalyzer and install to AppDir
mkdir -p $BUILD_APP_DIR
cd $BUILD_APP_DIR
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr
make install DESTDIR=AppDir

# run linuxdeploy to generate AppImage
export VERSION=latest
chmod +x $MESHALYZER_DIR/appimage/linuxdeploy-x86_64.AppImage
$MESHALYZER_DIR/appimage/linuxdeploy-x86_64.AppImage --appimage-extract
./squashfs-root/AppRun --appdir AppDir \
                       --desktop-file $MESHALYZER_DIR/appimage/meshalyzer.desktop \
                       --icon-file $MESHALYZER_DIR/appimage/meshalyzer.png \
                       --output appimage
